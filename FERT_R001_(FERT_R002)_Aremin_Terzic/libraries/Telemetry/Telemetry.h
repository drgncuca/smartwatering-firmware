#include <String.h>

#include <TimeLib.h>
#include <SHT1x.h>

#ifndef Telemetry_h                                                   
  #define Telemetry_h 
  
//================================  B O A R D   HW    V E R S I O N   =================================//
//#define BOARD_FERT_R001
#define BOARD_FERT_R002
//#define BOARD_SENS_R001
//================================   P O D E S I T I    P R E    I S P O R U K E  ====================//

#define secondsWaitingSMS 20  // Podesiti na 15 - 20,  4s * 15 = 60s cekanje na SMS
#define GSM_SMS_Replay       // Ako korisik hoce povratni SMS -> Otkomentarisati
//#define SKIP_Operater_Detecting   // zakomentarisati ovo !!!! ( samo za debagovanje)

//---------------------------------------------------------------------------------------------
#define BUFF_RTC_TIME 1 // Za debagovanje stavi 256
#define MSG_BUF  830

#define BUFF_SMS 40
#define BUFF_MOB_NUM 20

//============================= V A L V E S   &&    R E L A Y S  ==========================//
#define AC_Valves

#define NUM_SLOTS 3 // each SLOT consume 150B of memory

#define MainPump

//============================= D E B U G   D I R E C T I V E ============================//

//#define DEBUG_SENTEK
//#define DEBUG_Server_Comun
#define DEBUG                //210B
#define DEBUG_Extend
//#define DEBUG_Telemetry
//#define DEBUG_Fertilisation
//#define DEBUG_FLOW
//#define DEBUG_EC
//#define DEBUG_EC_PID
//#define DEBUG_Temperature
//#define DEBUG_ON          //od GSM modula
//#define DEBUG_Operater
//#define TIME_F_Measure
//#define DEBUG_PT1000
//#define Operater_Setings_Skip
//#define DEBUG_Power_Saving
//#define DEBUG_PWM_Generator

//=================================   M O D U L E S  ========================================//
//#define MIXER             //120B

//#define SENTEC_Include   //400B
//#define SENTEK_Salinity   
//#define SENTEK_Include_Extend_Sens  //170B

#define GSM_Carrier_Change 

//#define DC_Power_SENS    // Battery
//#define AC_Power_FERT	   // 230Vac
//#define Power_Saving

#define FLOW
//#define INTERRUPTFLOW   //200B
//#define FLOW_Toggling

//#define Sensor_EC      //488B
//#define Sensor_EC_PID  //82B   

//#define Sensor_pH

//#define Fertilisation       //138B
//#define fertPIDregulation 
//#define fert_Iout_Fix20mA  
//#define FERT_0
//#define FERT_2
//#define FERT_3
//#define FERT_4

//#define Sensor_10HS  //Senzor Electrical Conductivity (u obliku Viljuske - seljacki :) )

//#define SHT1sensor   //I2C senzor temp i vlage
																		 
//=================================== P O W E R ==========================================//
#define AC_power_IN 7	

void setupWatchDogTimer(void);
void enterSleep(void);
void PowerStandBy(int StandByINseconds);
void PowerWakeUP(void);	
int getPower(void) ;
				  
//==============================   Temperature PT1000  =================================//
#define RREF      4300.0
#define RNOMINAL  1000.0

// Pins for SPI comm with the AD9833 IC
#define DATA  50  ///< SPI Data pin number
#define CLK   52  ///< SPI Clock pin number
#define FSYNC 53  ///< SPI Load pin number (FSYNC in AD9833 usage)

void setupTemperaturePT1000(void);
void testTemperaturePT1000(void);
float getTemepraturePT1000(void);

//=================================  RS485  =========================================//
void RS485setup(void);
void RS485readEnableCH(int chanell);
void RS485writeStringLN(String string);
void RS485readCharArray(int chanell, char *CharArray);
String RS485readString(int chanell);

//================================   SENTEK  =======================================//
// za FERT R001 ReceiveChanellRS485 = 4, za SENS ReceiveChanellRS485 = 1

//#define SENTEK_FERT_R002
//#define SENTEK_FERT_R001
#define SENTEK_SENS_R001

#ifdef  SENTEK_FERT_R001
	#define ReceiveChanellRS485 4
#endif
#if defined  (SENTEK_SENS_R001) || defined  (SENTEK_FERT_R002)
	#define ReceiveChanellRS485 1
#endif

#ifdef SENTEC_Include
    void setSANTEKsensors(void);
	float getSENTEKtemperature(byte debpth);
	float getSENTEKmoisture(byte debpth);
	float getSENTEKsalinity(byte debpth);
	void setSANTEKsensors(void);	
	float hexStringToFloat(char temp[], int sensor);	
	void reqSENTEKpresetHoldReg_Temperature(void);
	void reqSENTEKpresetHoldReg_Moisture(void);
	void reqSENTEKpresetHoldReg_Salinity(void);	
#endif	

//--------------------------------------------------------------------------------------
#define CLS          "\033[2J" 

#define relayFertPump_1 35
#define relayFertPump_2 34
#define relayFertPump_3 36
#define relayFertPump_4 37
//#define relayFertPump_3 59

#define relay_stop   LOW
#define relay_start  HIGH

#define ph_sensor_pin A2  // pH senzor analog IN
//-------------------------Carier Operater Changing----------------------------------------------//
#define EERPOM_mem_loc_SW  64
#define EEPROM_mem_loc_Operater 62
#define EERPOM_mem_loc_CarrierData 0

#define Telenor 1
#define Vip 2
#define Telekom 3

 struct carrier
{
  char apnname[12];
  char apnuser[12];
  char apnpass[12];
  char operater[12];
} ;

extern carrier carrierdata; 

 struct struct_operater_vip
{
  char apnname[12]="internet";
  char apnuser[12]="internet";
  char apnpass[12]="internet";  
} ;
extern struct_operater_vip operaterVIP;

 struct struct_operater_MTS
{
  char apnname[12]="gprswap"; // GPRSINTERNET, GPRSWAP , 3Gnet
  char apnuser[12]="mts";
  char apnpass[12]="064";  
} ;
extern struct_operater_MTS operaterMTS;

 struct struct_operater_Telenor
{
  char apnname[12]="internet";
  char apnuser[12]="telenor";
  char apnpass[12]="gprs";  
} ;
extern struct_operater_Telenor operaterTelenor;

void doesOperaterChanging(void);
bool startsWith(const char *pre, const char *str);
void initOperators(void);
void deleteALLsms(void);
float getRSSI(void);

//=============================  Temperature and Humidity SHTx sensor ==========================//
#define dataPin  20
#define clockPin 21

extern float temperature;
extern float humidity;
extern SHT1x SHT1x_sensor;
//================================   Electrical  Conductivity  =================================//
#define NMBRsampling  200  // broj semplova SINusnog signala EC drajvera
#define NMBRaveraging   3  //usrednjavanje min i max vrednosti amplitude

#define ECoffsetCorectiveValue 0.35  // Korektivna vrednost EC-a koja se dodata na zadatu vrednost od strane korisnika. Vrednost EC-a na sondi i nakraju napojne cevni nije isti. Verovatno trebaneko vreme da se EC ustabili u sistemu

struct EC_struct {
  float measured; 
  float ecK;// Y = k*X +M za korekciju pri merenju EC-a
  float ecM; 
#ifdef Sensor_EC_PID 
  float desired;
  float ecKp;
  float ecKi;
  float ecKd;
//float CH5_Curr_Strenght;  
//float CH4_Curr_Strenght;  
  float CH3_Curr_Strenght;   //Za buduce prosirenje za drugu pumpu za EC PID
  float CH2_Curr_Strenght;   //Glavni pumpa za EC PIDr
//float CH1_Curr_Strenght;  //Ovaj je rezervisan za Fertilizaciju
#endif
};
extern EC_struct ECmeasuring;
// extern byte FERTchanel2 ;
// extern byte FERTchanel3 ;

void  setConductivityDriver(int frequency, int signalShape);
void  StopCurrentDriver( byte CurrDrvChanPin);
float getELconductance(void);
void  EC_PID_Control_Checking(byte CurrDrvChanPin, float  ECdesired);
//================================   FLOW  =================================================//
#define FlowSensorCH1 0
#define FlowSensorCH2 1
#define FlowSensorCH3 2
#define FlowSensorCH4 3
#define FlowSensorCH5 4

// Konstante racunate na bazi protokomera od 33imp/S. Ovde jos figurise i ulazni stepen pretvaranja impulsa u napon + ADC konverzija (5V * digit / 1024)

#define flowCH5_ConstantImpuls33PerSecond 99.0 
#define flowCH4_ConstantImpuls33PerSecond 99.0 
#define flowCH3_ConstantImpuls33PerSecond 99.0 
#define flowCH2_ConstantImpuls33PerSecond 99.0 
#define flowCH1_ConstantImpuls33PerSecond 99.0 

struct irrigation_struct_FlowMetering
{
	float irrigationFlowVolume;	
	unsigned long ElapsedTime;
	float flowMeterCalibKons;
#ifdef DEBUG_FLOW 	
	float flow ;
#endif
//	double flowmeter_capacity;
//	bool flowMeterConfigChanged;
};

extern  irrigation_struct_FlowMetering  irrigationFlowMetering[];

//===============================   Current Driver  ============================================//
/* Io = D * 5V / 180R.   
 *  25mA -> 4.5V -> D = 90%
 *  20mA ->      -> D = 86%
 *  4mA  -> 0.7V -> D = 17% 
 */

  #define minDutyCycle 16 // 16% -> 4mA
//#define maxDutyCycle 90 // 92% -> 25mA
  #define maxDutyCycle 86 // 90% -> 20mA

// razmotriti da se PWM smanji sa 50 na 30Hz-a
  #define PWMperiodPulse  20 // 20mS period tj frequencija PWMa: pulse + pause = 20mS -> 50Hz, inicijalno D = 25%.

#ifdef fert_Iout_Fix20mA
	#define PWMpulseTogle 200
#endif

#ifndef fert_Iout_Fix20mA
	#define PWMpulseTogle 30 // 20mS perioda PWM-a: pulse + pause = 20mS -> 50Hz, inicijalno D = 25%.
#endif


  #define CurrDrvCH1 5  //CH1 pH - fertilizaciju
  #define CurrDrvCH2 3  //CH2 EC PID - glavna upravljacka petlja za pumpu
  #define CurrDrvCH3 6  //CH3 EC PID - pomocna pumpa
  #define CurrDrvCH4 9  //CH4 - nije u upotrebi
  #define CurrDrvCH5 8  //CH5 - nije u upotrebi
  
void CurrentDriverSet(byte dutyCycle, byte CurrDrvChanel);

//===============================    Fertilisation    ============================================//
extern float fertCurrentDriverCoeff;
//  float fertFlowMeterCallCoeff;
  
struct fert_struct
{
  int fertilization_pin;
  int fertilization_status; // 0 = off, 1 = on
  String fertilization_id;
  String fertilization_irrigation_id;
  int fertilization_duration;
  unsigned long fertilization_started_time; 

          
};
extern fert_struct fertilizations[];

struct pH_struct {
  int rawPh;
  float pH;
  float desiredPh;
  float ferphK;   // Y = k*X +M za korekciju pri merenju pH
  float ferphM;	
  float ferKp;
  float ferKi;
  float ferKd;
};
extern pH_struct pHmeasuring;

extern int valves_fert[] ;

void valve_start_fertilization(int fertnum);
void valve_stop_fertilization(int fertnum);
void fertilizationDriverSetup(void);
float getPh(void);
void fertilization_start(String fertilizationid, String irrigationid, int duration, int pinnumber, int fertnum, float fert_pHtarget, float fertCurrCoeff);
void fertilizationCheck(byte CurrDrvChanel, float pHtarget);
void initialiseFertilisationParameters(void);
float gerDutyCycle_fertPh_PID(float pHtarget);
//---------------------------------------------------------------------------------
void resetFlowMetVolume(byte flowmeter_sensor);
float getCurrentFlow(int );
//void Flow(void);
void initialiseIrrFlowMet(void);
float getCurrentVOL(int flowmeter_sensor);
void FlowDebug(int flowmeter_sensor);
//time_t currentTime(void);
				
#endif