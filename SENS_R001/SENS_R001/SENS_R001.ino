#include <EEPROM.h>
#include "EEPROMAnything.h"
#include <SoftwareSerial.h>
#include <Wire.h>
#include <TimeLib.h>
#include <ArduinoJson.h>
#include "SIM900.h"
#include "ds3231.h"
#include "inetGSM.h"
#include "sms.h"
#include <String.h>
#include "Telemetry.h"					
#include <SPI.h>
#include <Adafruit_MAX31865.h>

#include <MemoryFree.h>

#ifdef Sensor_EC
  Adafruit_MAX31865 TemperaturePT1000 = Adafruit_MAX31865(49);
#endif

#ifdef INTERRUPTFLOW
  #include <FlowMeter.h>
#endif  

InetGSM inet;
//TODO Check max size of buffer
StaticJsonBuffer<650> jsonBuffer;
int numdata;
boolean gsm_started = false;

#ifdef Power_Saving
  int PowerStandByTimeInMin   = 0;
#endif


bool isperm3 = false;
bool isautomode = false;

char server[] = "app.smartwatering.rs";

//har parcel_id[] = "rjTXqd"; // Testing Single Device FERT+Zone, Kruska NS !!
  
//char parcel_id[] = "MGTnV5"; //FERT DID for TESTING  (DID) // Testing multi device
//char parcel_id[] = "NmL73F"; //Zone DID for TESTING  (DID) // Testing multi device

//char parcel_id[] = "A4dgm";  // Lesnik Zvornik  (PID)
//char parcel_id[] = "yuIFb";  //seskejabuke
//char parcel_id[] = "HxK8jk"; //Aleksandar Rancic, Borovnice, FERT 
//char parcel_id[] = "TRZyr";  //Darko Ristic, Borovnica Ub, PID
//char parcel_id[] = "HIsWGI"; //Ivica Todorovic Borovnica. DID
//char parcel_id[] = "IS7Zj"; //Delta Agrar, Jabuka, Tornjas. PID , DC-ventili
//char parcel_id[] = "qWJdQZ"; //Borovnica Trlić FERT 10 Zones
//char parcel_id[] = "LgomjY"; //Borovnica Trlić Meteo 1 : 
//char parcel_id[] = "tCHkuC"; //Borovnica Trlić Meteo 2 :  
//char parcel_id[] = "JXUnNw"; //Zemlja i Sunce - Parcela Ivan
//char parcel_id[] = "gHduMG";//Borovnica Aleksandar Uzice, DC Relays permanent ON/OFF [DEL-13]
//char parcel_id[] = "sfWSqk";//Kupina Andrija Jelinac 4 zone AC [DEL-5]
//char parcel_id[] = "LgomjY";//Trlic FERT [DEL-1] pH, EC, SENTEK, STH20.
//char parcel_id[] = "tCHkuC";//Trlic FERT [DEL-1] SENTEK, STH20.
//char parcel_id[] = "aQjkKo";// Tornjos II
//char parcel_id[] = "ffCIYQ";//Darko Smiljkovski - Lesnik
//char parcel_id[] = "eD1BdU";//Sava Coop testna stanica
//char parcel_id[] = "8taxAn";//Damir Bulj
//char parcel_id[] = "8YOyxi";//Vladan Redzic Ecoland JAbuka  (Vlaški do)
//char parcel_id[] = "7aCids";//Radoica Radomirovic - Borovnica (Leposavic, Kosovo)
//char parcel_id[] = "B7FmJb";//Marko Smederevo : Zone 1-4
//char parcel_id[] = "M9xswP";//Marko Smederevo : Zone 5-8 
//char parcel_id[] = "9YyhWN";//Milos Aleksic Lesnik 
//char parcel_id[] = "7m4wBq";//Aleksandar Rancic, Borovnica, Zones 
//char parcel_id[] = "uPt2he";//Moshe Lifhitz, Borovnica, SENS
//char parcel_id[] = "tIzUOI";//Ivica Donji Racnik
//char parcel_id[] = "OqLzTt";//Ivica Donji Racnik
//char parcel_id[] = "qTY5yN";//DEL-28
//char parcel_id[] = "s4IUok";//Strahinja Malin START Airplant Cortanovci
//char parcel_id[] = "TkEoPS";//Poljoprivredni Fax
//char parcel_id[] = "X8pZeK";//Superior doo
//char parcel_id[] = "LgomjY";//Trlic SENS 1
//char parcel_id[] = "tCHkuC";//Trlic SENS 2
//char parcel_id[] = "V7qLc8";//Nikola Smederevo SENS-2
//char parcel_id[] = "mNokrM";//MK Group Goran Jašin-Kukuruz
//char parcel_id[] = "QfFJIZ";//Maja Pavlović Srpski Itebej-Kukuruz

//char parcel_id[] = "WkHVTS";//MDAgrotime-A (deviceID=68) Mila Dragicevic - Borovnica Mrcajevci 
//char parcel_id[] = "xCRXez";//MDAgrotime-B (deviceID=69)
//char parcel_id[] = "MDwItI";//MDAgrotime-C (deviceID=70)
//char parcel_id[] = "uvMKFv";//MDAgrotime-D (deviceID=71)
//char parcel_id[] = "s4NYuv";//Milovan Draskic - Sikole Lesnik Negotin
//char parcel_id[] = "YSFJjy";//Armin Terzic BiH demo
//char parcel_id[] = "XkPlnZ";//Plastenik BiH + FERT
//char parcel_id[] = "twFTmT";//Smart Watering Sajam
//char parcel_id[] = "N28dUb";//Delta Agrar
//char parcel_id[] = "YikgZN";//Željko Đorović
//char parcel_id[] = "CKDAjE";//Aleksandar Milic Borovnice plastenik Mladenovac
//char parcel_id[] = "uPt2he";//Moshe Lifshiz
//char parcel_id[] = "XdK9h4";//Marko Radovanovic Kragujevac
//char parcel_id[] = "ZXBvrc";//Zoran Gvozdenovic Borovnica KG Kragujevac
char parcel_id[] = "kWHyUL";//Goran Banja Luka 
//char parcel_id[] = "ir9FQo";//Aleksandar Milic Borovnice plastenik Mladenovac

byte number_of_zones = 4;

struct irrigation_struct
{
  int duration = 0;
  long amount = 0L;
  int status[4] = {0, 0, 0, 0}; // 0 = off, 1 = on
  unsigned long started_time[4] = {0L, 0L, 0L, 0L};
  unsigned long max_duration = 0;
  String irrigation_id = "";
  float current[4] = {0, 0, 0, 0};
} zonal_irrigation_struct; 

irrigation_struct  zonal_irrigations[4];

irrigation_struct_FlowMetering  irrigationFlowMetering[5]; // Globalna promenljiva koja sadrzi informacije o protekloj kolicini VODE na 5 kanalala flowMeter-a i proteklo vreme.

#ifdef DC_Valves  //DC1_P  2   3   4
  byte valves_on[]  = {33, 31, 40, 39};
                   //DC1_N  2   3   4
  byte valves_off[] = {32, 30, 41, 75};
  byte valvedelay = 100; // neka vrednost po difoultu u slucaju da zakaze komunikacija sa serverom 
#endif
 
#ifdef FLOW_Toggling
  byte flow_pin_Toggling = 15;// za FERT -> UART3_RX, na FERT1 PCB-u je oznacen sa: UART3_TX. 
//  byte flow_pin_Toggling = 72;// za SENS -> FLOW.
#endif

#ifdef MainPump // za SENS R001 main pump DC_valves[3]
  byte mainpump_pin = 29; // Main Pump Relay AS IS AC_Valves for Zone 8.
#else
  byte mainpump_pin = 87;   //Some Dummies Pin
#endif

float el_conductance = 0;

//----------Temperature and Humidity-----
#ifdef SHT1sensor
  float temperature;
  float humidity;
  SHT1x SHT1x_sensor(   20  ,   21);
#endif

int sim_restart_pin = 7;
int arduino_restart_pin = 12;

String params = "";
//TODO: Check max size of the message and reduce it's size accordingly
char msg[MSG_BUF]; 

carrier carrierdata;
struct_operater_vip operaterVIP;
struct_operater_MTS operaterMTS;
struct_operater_Telenor operaterTelenor;
struct_operater_BHmob operaterBHmob;

// last time successfully contacted server
unsigned long last_seen = 0L;
#ifdef INTERRUPTFLOW
	// fmeter sensor
	double flowmeter_capacity = 30;
	double flowmeter_kfactor = 4.25;
	double flowmeter_mfactor = 1;

	FlowSensorProperties flowmeter_sensor = {flowmeter_capacity, flowmeter_kfactor, {flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor}};
	FlowMeter Meter = FlowMeter(2, flowmeter_sensor);
	long flowmeter_period = 1000; // one second (in milliseconds)
	long flowmeter_lasttime = 0;

void MeterISR()
{  
  Meter.count();// let our flow meter count the pulses
}
#endif

#if defined FLOW || defined INTERRUPTFLOW					   
struct flowmeter_struct
{
  float currentflow;
  float currentvolume;
};
#endif

bool carrierChange = false;

#ifdef TIME_F_Measure
unsigned long currentTimeMilles;
#endif
//--------------------------------------------------------------------------------------
void setup()
{  
  digitalWrite(arduino_restart_pin, HIGH);
  delay(200);
  pinMode(arduino_restart_pin, OUTPUT);
  Serial.begin(115200); // Serial connection.
  
  RS485setup(); 
#ifdef SENTEC_Include  
    setSANTEKsensors(); 
    sensorSampledMask();
    
  #ifdef DEBUG_SENTEK_Write_to_Excel
    pinMode(4, INPUT_PULLUP); // PWM test pin
    delay(10);
    if(digitalRead(4) == 0) goto SENTEK_GoTo;
  #endif 						
#endif
  Serial.println(F("(RE)start uC"));

  initialiseIrrFlowMet(); //REsetovanje Zapremine i globalnog proteklog vremena na 0 vrednosti.
  
  Wire.begin();
  
#ifdef Sensor_EC  
  setConductivityDriver(70, 1); // setovanje SINusnog generatora: 70Hz, 1-> sinus.  
  setupTemperaturePT1000();// Setovanje temperaturnog senzora za PT1000.
#endif

#ifdef SERVISE_Setings
  DS3231_init(DS3231_INTCN);
  DS3231_clear_a1f();
#endif
  
  unsigned long now = millis(); ///  ?  ?   ?  ?   ?  ?   ?  ?   ?  ?   ?  ?   ?  ?   ?  ?   ?  ?   ?  ?   ?  ?   ?  ?   ?  ? 
  struct ts t;

#ifdef SERVISE_Setings  //podesavanje vremena
  t.sec = 0;
  t.min = 30;
  t.hour =18;
  t.mday = 4;
  t.mon = 4;
  t.year = 2020;
  DS3231_set(t);
#endif

  DS3231_get(&t);

#ifdef INTERRUPTFLOW
  // enable a call to the 'interrupt service handler' (ISR) on every rising edge at the interrupt pin
  attachInterrupt(INT7, MeterISR, RISING);
  // sometimes initializing the gear generates some pulses that we should ignore
  Meter.reset();
#endif
						   
  pinMode(sim_restart_pin, OUTPUT);
  digitalWrite(sim_restart_pin, HIGH);
  digitalWrite(sim_restart_pin, LOW);
  delay(500);
  digitalWrite(sim_restart_pin, HIGH);
  delay(50);

#ifdef DEBUG
  Serial.print(F("GSM="));
#endif 
  
  if (gsm.begin(9600))
  {
    gsm_started = true;    
#ifdef DEBUG
    Serial.println(F("READY"));
#endif 

//Pri Debagovanju, preskoci jedan deo koda, da ubrza startovanje uC-a
#ifdef SKIP_Operater_Detecting
    goto skip_operater_detecting;
#endif

  }
  else
  {
    gsm_started = false;
#ifdef DEBUG
    Serial.println(F("IDLE"));
#endif
  
  }

doesOperaterChanging();// Promena operatera

EEPROM_readAnything(EERPOM_mem_loc_CarrierData, carrierdata);

#ifdef DEBUG_Operater 
    char tmp[2]="00"; 
    Serial.println("\n---------------------------");    
    Serial.println("Sadrzaj EEPROM-a : ");   
    EEPROM_readAnything(64, tmp);
    Serial.println(String(tmp).c_str());     
    Serial.println("Data in the Carrier Data Structure");
    Serial.println(carrierdata.apnname);
    Serial.println(carrierdata.apnuser);
    Serial.println(carrierdata.apnpass);        
#endif; 

if (gsm_started)
  {
    Serial.print(F("GPRS="));    
//Pri Debagovanju, preskoci jedan deo koda, da ubrza startovanje uC-a    
#ifdef SKIP_Operater_Detecting  
    skip_operater_detecting:
       
    Serial.print(F("GPRS="));  
    if (inet.attachGPRS("internet", "internet", "internet"))
    {
#else
    if (inet.attachGPRS(String(carrierdata.apnname).c_str(), String(carrierdata.apnuser).c_str(), String(carrierdata.apnpass).c_str()))
    {
#endif    
    
#ifdef DEBUG
      Serial.println(F("ATTACHED")); 
#endif
#ifdef DEBUG
     gsm.SimpleWriteln("AT+CIFSR");//Read IP address.
     delay(5000);     
     gsm.WhileSimpleRead();//Read until serial buffer is empty
#endif
    }
    else
    {
#ifdef DEBUG
      Serial.println(F("ERROR"));
#endif
    }
    delay(1000); 
  }
else
  {
#ifdef DEBUG  
   Serial.println(F("GSM&GPRS=NOTstarted"));
#endif  
  }  

#ifdef DC_Valves
   for (byte i=0; i < 4; i++ )
  {
    pinMode(valves_on[i], OUTPUT);
    digitalWrite(valves_on[i],LOW);
    
    pinMode(valves_off[i], OUTPUT);
    digitalWrite(valves_off[i],LOW);
    
  }
  DDRG  = DDRG  | 0x08; // Relay DC4_N <OUT>  
  PORTG = PORTG & 0xF7; // Relay DC4_N <LOW>

#endif

#ifdef FLOW_Toggling
   pinMode(flow_pin_Toggling, INPUT);
//   DDRE  = DDRE & 0xEF;  // Za SENS - FLOW input 
#endif

  pinMode(mainpump_pin, OUTPUT);
  digitalWrite(mainpump_pin, relay_stop);

  pinMode(sim_restart_pin, OUTPUT);
  digitalWrite(sim_restart_pin, HIGH); 
  
#ifdef DEBUG_SENTEK_Write_to_Excel
   SENTEK_GoTo:
   // za SENTEK Kalibraciju MOD, gde se vrti samo petlja za uzorkovanje Salinity-a. Jumperom se prespoji UART3_RX na GND.
#endif					  
#ifdef SENTEC_Include         
  reqSENTEKpresetHoldReg_Temperature();delay(400); 
  reqSENTEKpresetHoldReg_Moisture();delay(400); 
#endif
#ifdef SENTEK_Salinity   
  reqSENTEKpresetHoldReg_Salinity();delay(400); 
#endif
};
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void loop()
{
#ifdef DEBUG_BASIC
  Serial.println(F("Usao u loop"));
#endif

#if defined FLOW || defined INTERRUPTFLOW
  struct flowmeter_struct flow = checkFlowMeter(); //azurira vrednost protoka i zapremine
  checkValves(flow.currentvolume);
#else
  checkValves(0);
#endif
 
#ifdef INTERRUPTFLOW  
   Meter.reset();				 
#endif				   
#ifdef FLOW_Toggling 
   bool currentFlowStatus = checkFlow() ? 1 : 0;  // ako ima bilo kakvog protoka vraca '1'
#endif

  if(isautomode) {
    checkLastSeen();
  }

#ifdef SHT1sensor
   Wire.end();  // u konfliktu dva I2C-a RTC i Temp-Humid senzor
   temperature = SHT1x_sensor.readTemperatureC();  
   humidity = SHT1x_sensor.readHumidity();
   Wire.begin();    
#endif

#ifdef Sensor_EC
  el_conductance = getELconductance(); 
#endif  

  params = "";  
  params.concat("did=" + String(parcel_id));
//params.concat("pid=" + String(parcel_id));
 
//----------------------- SENTEK sensors 1, 2, 3 -------------------------
#ifdef SENTEC_Include   

        reqSENTEKpresetHoldReg_Temperature();
        //delay(200);  
        params.concat("&t1=" + String(getSENTEKtemperature(1))); //temperature
        //delay(200);  
        params.concat("&t2=" + String(getSENTEKtemperature(2)));
        //delay(200);  
        params.concat("&t3=" + String(getSENTEKtemperature(3)));  
        //delay(200);        
        reqSENTEKpresetHoldReg_Moisture();
        //delay(200);
        params.concat("&m1=" + String(getSENTEKmoisture(1))); //moisture
        //delay(200);        
        params.concat("&m2=" + String(getSENTEKmoisture(2)));
        //delay(200);
        params.concat("&m3=" + String(getSENTEKmoisture(3)));
       
#ifdef SENTEK_Salinity 
   #ifdef DEBUG_SENTEK_Write_to_Excel  
      while (digitalRead(4) == 0){        
            //delay(200);
            reqSENTEKpresetHoldReg_Salinity();
            delay(900);
            params.concat("&s1=" + String(getSENTEKsalinity(1)));//salinity
            //delay(200);
            params.concat("&s2=" + String(getSENTEKsalinity(2)));
            //delay(200);
            params.concat("&s3=" + String(getSENTEKsalinity(3))); 
            //delay(200);         
      }		
    #endif							  

        //delay(200);
        reqSENTEKpresetHoldReg_Salinity();
        delay(900);
        params.concat("&s1=" + String(getSENTEKsalinity(1)));//salinity
        //delay(200);
        params.concat("&s2=" + String(getSENTEKsalinity(2)));
        //delay(200);
        params.concat("&s3=" + String(getSENTEKsalinity(3)));
        
   #endif   
#endif
//----------------------- SENTEK sensors 4, 5, 6 -------------------------
#ifdef SENTEK_Include_Extend_Sens

        reqSENTEKpresetHoldReg_Temperature();
        //delay(200);  
        params.concat("&t4=" + String(getSENTEKtemperature(4))); //temperature
        //delay(200);  
        params.concat("&t5=" + String(getSENTEKtemperature(5)));
        //delay(200);  
        params.concat("&t6=" + String(getSENTEKtemperature(6)));  
        //delay(200);
        reqSENTEKpresetHoldReg_Moisture();
        //delay(800);
        params.concat("&m4=" + String(getSENTEKmoisture(4))); //moisture
        //delay(200);
        params.concat("&m5=" + String(getSENTEKmoisture(5)));
        //delay(200);
        params.concat("&m6=" + String(getSENTEKmoisture(6)));
    #ifdef SENTEK_Salinity        
        delay(600);
        reqSENTEKpresetHoldReg_Salinity();
        //delay(50);
        params.concat("&s4=" + String(getSENTEKsalinity(4)));//salinity
        //delay(50);
        params.concat("&s5=" + String(getSENTEKsalinity(5)));
        //delay(50);
        params.concat("&s6=" + String(getSENTEKsalinity(6)));
    #endif
#endif

#ifdef Sensor_10HS
  params.concat("&h1=" + String(analogRead(A6))); // Analogni ulaz od EC-a. Custom resenje za Plastenik BiH
#endif  

#ifdef SHT1sensor
  params.concat("&ah=" + String(humidity));
  params.concat("&at=" + String(temperature));
#endif  
//  params.concat("&temp=" + String(0));

#ifdef Sensor_BGT_WSD2
  params.concat("&ah=" + String(getBGThumidity()));
  params.concat("&at=" + String(getBGTtemperature()));
#endif

#ifdef DC_Power_SENS  
  params.concat("&pow=" + String((float)analogRead(A8) * 0.0148 ));// Battery Voltage for SENS board
  params.concat("&sol=" + String((float)analogRead(A9) * 0.0280 ));// Solar Voltage for SENS board
#endif
  
#ifdef Sensor_pH
int tempPH = getPh();
  params.concat("&ferph=" + String(tempPH ));//salje se RAW pH.
#endif
 
#if defined FLOW || defined INTERRUPTFLOW 
  params.concat("&cfl=" + String(flow.currentflow));
  params.concat("&tfv=" + String(flow.currentvolume));  
#endif  

#ifdef FLOW_Toggling  
  params.concat("&cfl=" + String(currentFlowStatus));
#endif    

#ifdef Sensor_EC
  params.concat("&ecv=" + String(el_conductance));
#endif

#ifdef Sensor_WindSpeed
int temp_windSpeed = analogRead(A6); // ADC od EC-a
  params.concat("&ws=" + String(temp_windSpeed));

#endif

  String activeirrigations;
  for (byte i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id != "")
    {
      activeirrigations.concat("," + String(zonal_irrigations[i].irrigation_id));
    }
  }
  activeirrigations.remove(0, 1);
  params.concat("&iids=" + String(activeirrigations));

#ifdef DEBUG_BASIC  
  Serial.println(params.c_str());
#endif

  memset(msg, 0, sizeof(msg)); 
//apiCall(server, 80, "/api/post/sync.php", params.c_str(), msg, MSG_BUF);
  apiCall(server, 80, "/api/post/sync_device.php", params.c_str(), msg, MSG_BUF);  

//delay(5000);/// debagovanje power saving mode, problem sa millis, interupt funkcionalnoscu.....
char *jsonResponse = strstr(msg, "{");   
#ifdef DEBUG_BASIC  
    Serial.println(jsonResponse);
#endif
//--------------------------------------------------------------------------------
  JsonObject &root = jsonBuffer.parseObject(jsonResponse);
//--------------------------------------------------------------------------------

  JsonVariant numberofzones_json = root["zones"];
  if (numberofzones_json.success())
  {
    if (number_of_zones != root["zones"])
    {
      number_of_zones = root["zones"];
    }
  }

if ( (number_of_zones<1) || (number_of_zones > 4)  ) number_of_zones = 4;

#ifdef DC_Valves
  JsonVariant valvedelay_json = root["valvedelay"];
  if (valvedelay_json.success())
  {
    if (valvedelay != root["valvedelay"])
    {
      valvedelay = root["valvedelay"];
    }
  }
  if ( (valvedelay < 10) || (valvedelay > 1000) ) valvedelay =100;
  
#endif
//---------------------------------------------------------------------
  JsonObject &flowmeter_config = root["flowconf"];
  if (flowmeter_config.success())
  {
#ifdef FLOW
    irrigationFlowMetering[FlowSensorCH5].flowMeterCalibKons = flowmeter_config["kf"];
#endif

#ifdef INTERRUPTFLOW
bool flow_config_changed = false;
    if (flowmeter_config["cap"] != flowmeter_capacity)
    {
      flowmeter_capacity = flowmeter_config["cap"];
      flow_config_changed = true;
    }
    if (flowmeter_config["kf"] != flowmeter_kfactor)
    {
      flowmeter_kfactor = flowmeter_config["kf"];
      flow_config_changed = true;
    }
    if (flowmeter_config["mf"] != flowmeter_mfactor)
    {
      flowmeter_mfactor = flowmeter_config["mf"];
      flow_config_changed = true;
    }

    if (flow_config_changed)
    {
      FlowSensorProperties flowmeter_sensor = {flowmeter_capacity, flowmeter_kfactor, {flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor}};
      Meter = FlowMeter(2, flowmeter_sensor);
    }
#endif								 
  }


  JsonObject &start_irrigation = root["irr"];
  if (start_irrigation.success())
  {
    // TODO: check if this irrigationid is started
    if (strcmp(start_irrigation["irrigationid"].as<String>().c_str(), root["irr"]["iid"].as<String>().c_str()) != 0)
    {
      isautomode = start_irrigation["automode"];
      if (start_irrigation["zonal"])
      {
        if (start_irrigation["duration"])
        {
          //per time
          isperm3 = false;
          zonal_irrigation_start(start_irrigation["iid"].as<String>(), start_irrigation["duration"], start_irrigation["zones"]);
          //Serial.println("Per Time");
          //zonal_irrigation_start_DBG(start_irrigation["iid"].as<String>(), start_irrigation["duration"], start_irrigation["zones"]);
        }
        else
        {
          //per m3
          isperm3 = true;
          zonal_irrigation_start(start_irrigation["iid"].as<String>(), start_irrigation["amount"], start_irrigation["zones"], start_irrigation["maxduration"]);          
        }
      }
    }
  }



//---------EC calbracioni koeficijent EC = k*X + m -----------------------------------------
#ifdef Sensor_EC
 JsonVariant EC_CalibrCoeff_json = root["eck"];
    if (EC_CalibrCoeff_json.success())
    {
      if ( EC_calibr_Coeff != root["eck"])
      {
           EC_calibr_Coeff = root["eck"];
      }
    }

 JsonVariant EC_CalibrCoeff_M_json = root["ecm"];
    if (EC_CalibrCoeff_M_json.success())
    {
      if ( EC_calibr_M_Coeff != root["ecm"])
      {
           EC_calibr_M_Coeff = root["ecm"];
      }
    }
#endif    

//--------------------------------------------------------------------------------																				  
#ifdef Power_Saving

 JsonVariant PowerStandByTime_json = root["slp"];
    if (PowerStandByTime_json.success())
    {
      if (PowerStandByTimeInMin != root["slp"])
      {
          PowerStandByTimeInMin = root["slp"];
      }
   }
   
  PowerStandBy(PowerStandByTimeInMin); // kad proradi podeasvanje sa servera -> otkomentarisati
  //PowerStandBy(1); // Stand-BY in minute, dok ne proradi sa servera slanje inf o sllep-u
  PowerWakeUP(PowerStandByTimeInMin);

#endif		

JsonVariant stop_irrigation_id_json = root["stopirr"];
  if (stop_irrigation_id_json.success())
  {    
    char irrid[5];
    strcpy(irrid, root["stopirr"].as<const char *>());
#ifdef DEBUG_Extend
      Serial.print(F("[stopirr] "));Serial.println(irrid);
#endif   

    stop_irrigation(irrid);    
  } 
  
jsonBuffer.clear();

}//--------- END  ----  of  -----  L O O P ( )  -----------------


//per duration
void zonal_irrigation_start(String irrigationid, int duration, JsonArray &zones)
{
  for (int i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id == "") // Trazi prvi prazan ID u SLOT-u, i ispodesava ga.... 
    {
      zonal_irrigations[i].irrigation_id = irrigationid;
      for (int zone : zones)
      {
        delay(170);
        zonal_irrigations[i].duration = duration;								 
#ifdef DC_Valves
    startValve(zone-1);
    //startValve(3); //MAIN pump, Custom resnej za MK
#endif        
        //digitalWrite(mainpump_pin, relay_start);        
        zonal_irrigations[i].started_time[zone - 1] = currentTime();
        zonal_irrigations[i].status[zone - 1] = 1;
#ifdef DEBUG
        String logger = "Palim zonu " + String(zone);
        Serial.println(logger);
#endif
      }
#ifdef DEBUG
      String logger = "Started zonal irrigation ID " + String(zonal_irrigations[i].irrigation_id);
      Serial.println(logger);
#endif
      params = "";
      memset(msg, 0, sizeof(msg));
      params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));
      apiCall(server, 80, "/api/post/irrigation_started.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif

      return;
    }
  } //for (int i = 0; i < NUM_SLOTS; i++)	  
  
}

//per m3
void zonal_irrigation_start(String irrigationid, long amount, JsonArray &zones, int max_duration)
{

  for (int i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id == "")
    {
      zonal_irrigations[i].irrigation_id = irrigationid;
      for (int zone : zones)
      {
        delay(100);
        zonal_irrigations[i].amount = amount;
        zonal_irrigations[i].max_duration = max_duration * 60 * 60; // max_duration je vreme u SATIMA koje server salje kao zastitu

#ifdef DC_Valves
    startValve(zone-1);
    //startValve(3); //MAIN pump, Custom resnej za MK
#endif 
        zonal_irrigations[i].current[zone - 1] = 0;
        zonal_irrigations[i].status[zone - 1] = 1;
        zonal_irrigations[i].started_time[zone - 1] = currentTime();
#ifdef DEBUG
        String logger = "Starting zone " + String(zone);
        Serial.println(logger);
#endif
      }
#ifdef DEBUG
      String logger = "Started zonal irrigation ID " + String(zonal_irrigations[i].irrigation_id);
      Serial.println(logger);
#endif
      params = "";
      memset(msg, 0, sizeof(msg));
      params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));
      apiCall(server, 80, "/api/post/irrigation_started.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);

#endif
      return;
    }
  }
  
 
}

void stop_irrigation(String irrigationid)
{
  // stop zonal irrigation
  for (int i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id == irrigationid)
    {
      // turn of all active zones
      for (int checking_zone = 1; checking_zone <= number_of_zones; checking_zone++)
      {
        if (zonal_irrigations[i].status[checking_zone - 1] == 1)
        {
delay(200);
    stopValve(checking_zone-1);
#ifdef DEBUG_Extend
     Serial.print(F("Gasim zonu "));Serial.println(checking_zone);
#endif          
          zonal_irrigations[i].status[checking_zone - 1] = 0;
        }
        zonal_irrigations[i].started_time[checking_zone - 1] = 0L;
      }
      zonal_irrigations[i].irrigation_id = "";
      zonal_irrigations[i].duration = 0;

      // check to turn off pump if there is no active irrrigation
      bool is_some_irrigation_on = false;
      for (int checking_slot = 0; checking_slot < NUM_SLOTS; checking_slot++)
      {
        if (zonal_irrigations[checking_slot].irrigation_id != "")
        {
          for (int checking_zone = 1; checking_zone <= number_of_zones; checking_zone++)
          {
            if (zonal_irrigations[checking_slot].status[checking_zone - 1] == 1)
            {
              is_some_irrigation_on = true;
              break;
            }
          }
        }
      }
      
#ifdef DEBUG
      String logger = "Stopped irrigation ID " + String(irrigationid);
      Serial.println(logger);
#endif
      params = "";
      params.concat("iid=" + String(irrigationid));
      memset(msg, 0, sizeof(msg));
      apiCall(server, 80, "/api/post/irrigation_stopped.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif
    }
  }
}

void checkValves(float totalVolume)
{

//--------------------------------------------------------------------
  // zonal check, po vremenu....
  if (!isperm3)
  { 
    for (int i = 0; i < NUM_SLOTS; i++)
    {
      if (zonal_irrigations[i].irrigation_id != "")
      {
        for (int processing_zone = 1; processing_zone <= number_of_zones; processing_zone++)
        {
          if ((zonal_irrigations[i].status[processing_zone - 1] == 1) && !(currentTime() - zonal_irrigations[i].started_time[processing_zone - 1] < zonal_irrigations[i].duration * 60L))
          {
delay(170);            
           
#ifdef DC_Valves
    stopValve(processing_zone-1);
#endif  
#ifdef DEBUG
    Serial.print(F("Gasim Zonu[min] "));Serial.println(processing_zone);
#endif          
            zonal_irrigations[i].status[processing_zone - 1] = 0;
            bool is_some_zone_on = false;
            for (int checking_zone = 1; checking_zone <= number_of_zones; checking_zone++)
            {
              if (zonal_irrigations[i].status[checking_zone - 1] == 1)
              {
                is_some_zone_on = true;
                break;
              }
            }
            if (is_some_zone_on == false)
            {
              bool is_some_irrigation_on = false;
              for (int checking_slot = 0; checking_slot < NUM_SLOTS; checking_slot++)
              {
                if (zonal_irrigations[checking_slot].irrigation_id != "")
                {
                  for (int checking_zone = 1; checking_zone <= number_of_zones; checking_zone++)
                  {
                    if (zonal_irrigations[checking_slot].status[checking_zone - 1] == 1)
                    {
                      is_some_irrigation_on = true;
                      break;
                    }
                  }
                }
              }

              if (!is_some_irrigation_on)
              {
                //digitalWrite(mainpump_pin, relay_stop);
                //stopValve(3);//MAIN pump, Custom resnej za MK
                //  main_pump_status = 0;
              }
#ifdef DEBUG
              String logger = "Completed zonal irrigation ID " + String(zonal_irrigations[i].irrigation_id);
              Serial.println(logger);
#endif
              params = "";
              params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));
              memset(msg, 0, sizeof(msg));
              apiCall(server, 80, "/api/post/irrigation_finished.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
              char *jsonResponse = strstr(msg, "{");
              Serial.println(jsonResponse);
#endif

              // clear slot
              zonal_irrigations[i].irrigation_id = "";
              zonal_irrigations[i].duration = 0;
              for (int checking_zone = 1; checking_zone <= number_of_zones; checking_zone++)
              {
                zonal_irrigations[i].status[checking_zone - 1] = 0;
                zonal_irrigations[i].started_time[checking_zone - 1] = 0L;
              }
              initialiseIrrFlowMet();// resetuje parametre o protekloj kolicini vode i vreme start-stop.

            }
          }
        }
      }
    }
  }
  //---po m3
  else
  {
    for (int i = 0; i < NUM_SLOTS; i++)
    {
      if (zonal_irrigations[i].irrigation_id != "")
      {
        for (int processing_zone = 1; processing_zone <= number_of_zones; processing_zone++)
        {
          if (zonal_irrigations[i].status[processing_zone - 1] == 1)
          {                                                                        
            zonal_irrigations[i].current[processing_zone - 1] += totalVolume ; 
#ifdef DEBUG             
//Serial.print(F(" Volume "));Serial.println(zonal_irrigations[i].current[processing_zone - 1]);
#endif           
            if (zonal_irrigations[i].current[processing_zone - 1] >= zonal_irrigations[i].amount)
            {
delay(170);              
          
#ifdef DC_Valves
    stopValve(processing_zone-1);
#endif     
#ifdef DEBUG        
     Serial.print(F("Gasim Zonu[m3] "));Serial.println(processing_zone);
#endif          
              zonal_irrigations[i].status[processing_zone - 1] = 0;
              bool is_some_zone_on = false;
              for (int checking_zone = 1; checking_zone <= number_of_zones; checking_zone++)
              {
                if (zonal_irrigations[i].status[checking_zone - 1] == 1)
                {
                  is_some_zone_on = true;
                  break;
                }
              }
              if (is_some_zone_on == false)
              {
                #ifdef DEBUG
                  String logger = "Completed zonal irrigation ID " + String(zonal_irrigations[i].irrigation_id);
                  Serial.println(logger);
                #endif

                params = "";
                params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));
                memset(msg, 0, sizeof(msg));
                apiCall(server, 80, "/api/post/irrigation_finished.php", params.c_str(), msg, MSG_BUF);

                //digitalWrite(mainpump_pin, relay_stop);
                //stopValve(3);//MAIN pump, Custom resnej za MK
               // main_pump_status = 0;

                // clear slot
                zonal_irrigations[i].irrigation_id = "";
                zonal_irrigations[i].amount = 0L;
                zonal_irrigations[i].max_duration = 0;
                for (int checking_zone = 1; checking_zone <= number_of_zones; checking_zone++)
                {
                  zonal_irrigations[i].status[checking_zone - 1] = 0;
                  zonal_irrigations[i].current[checking_zone - 1] = 0;
                }
                initialiseIrrFlowMet();
              }
            }
//Serial.print(" Pased time ");Serial.println( currentTime() - zonal_irrigations[i].started_time[processing_zone - 1]);
//Ako je proteklo zadatao MAX vreme za navodnjavanje, sistem se automatski gasi.
//max_duration je vreme u satima !!!!!!!
            if ((zonal_irrigations[i].status[processing_zone - 1] == 1) && zonal_irrigations[i].max_duration > 0 && (currentTime() - zonal_irrigations[i].started_time[processing_zone - 1] > zonal_irrigations[i].max_duration ))
            {

#ifdef DC_Valves
    stopValve(processing_zone-1);
#endif 	
#ifdef DEBUG        
     Serial.print(F("Gasim Zonu[max_dur expir] "));Serial.println(processing_zone);
#endif		
              //digitalWrite(mainpump_pin, relay_stop);
              //stopValve(3); //MAIN pump, Custom resnej za MK
              zonal_irrigations[i].status[processing_zone - 1] = 0;
              bool is_some_zone_on = false;
              for (int checking_zone = 1; checking_zone <= number_of_zones; checking_zone++)
              {
                if (zonal_irrigations[i].status[checking_zone - 1] == 1)
                {
                  is_some_zone_on = true;
                  break;
                }
              }
              if (is_some_zone_on == false)
              {
        #ifdef DEBUG
            String logger = "Stopped zonal irrigation ID " + String(zonal_irrigations[i].irrigation_id) + " - max duration";
            Serial.println(logger);
        #endif
                params = "";
                params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));
                params.concat("&maxduration=1");
                memset(msg, 0, sizeof(msg));
                apiCall(server, 80, "/api/post/irrigation_stopped.php", params.c_str(), msg, MSG_BUF);
        #ifdef DEBUG
            char *jsonResponse = strstr(msg, "{");
            Serial.println(jsonResponse);
        #endif

                // clear slot
                zonal_irrigations[i].irrigation_id = "";
                zonal_irrigations[i].amount = 0L;
                zonal_irrigations[i].max_duration = 0;
                for (int checking_zone = 1; checking_zone <= number_of_zones; checking_zone++)
                {
                  zonal_irrigations[i].status[checking_zone - 1] = 0;
                  zonal_irrigations[i].current[checking_zone - 1] = 0;
                }
                initialiseIrrFlowMet();
              }
            }
          }
        }
      }
    }
  }
}

time_t currentTime()
{
  struct ts t;
  tmElements_t te;
  DS3231_get(&t);
  //char buff[BUFF_RTC_TIME];
  //snprintf(buff, BUFF_RTC_TIME, "Trenutno vreme %d.%02d.%02d %02d:%02d:%02d", t.year, t.mon, t.mday, t.hour, t.min, t.sec);
  //Serial.println(buff);

  te.Second = t.sec;
  te.Minute = t.min;
  te.Hour = t.hour;
  te.Day = t.mday;
  te.Month = t.mon;
  te.Year = t.year - 1970;

  time_t timeInSeconds = makeTime(te);
  return timeInSeconds;
}
//==================================================================================================================================
int apiCall(const char *server, int port, const char *path, const char *parameters, char *result, int resultlength)
{

  int completed = 0;
  delay(200);
/*  
  Serial.print("CALL: "); Serial.print(server);Serial.print(" ");
   Serial.print(port);Serial.print(" ");
   Serial.print(path);Serial.print(" ");
   Serial.print(parameters);Serial.print(" ");
   Serial.print(result);Serial.print(" ");
   Serial.print(resultlength);Serial.println(" ");
 */ 
  completed = inet.httpPOST(server, port, path, parameters, result, resultlength);
  
  if (completed == 0)
  {
#ifdef DEBUG
    Serial.println(F("Dettach GPRS connection..."));
#endif
    inet.dettachGPRS();
    delay(2000);
#ifdef DEBUG
    Serial.println(F("Restarting SIM module..."));
#endif
    digitalWrite(sim_restart_pin, LOW);
    delay(500);
    digitalWrite(sim_restart_pin, HIGH);
    delay(500);
#ifdef DEBUG
    Serial.println(F("GSM Shield testing."));
#endif
    // Start configuration of shield with baudrate.
    // For http uses is recommended to use 4800 or slower.
    if (gsm.begin(9600))
    {
#ifdef DEBUG
      Serial.println(F("\nGSM = READY"));
#endif
      gsm_started = true;
    }
    else
    {
#ifdef DEBUG
      Serial.println(F("\nGSM = IDLE"));
#endif
    }
    if (gsm_started)
    {
#ifdef DEBUG
      Serial.println(F("Attach GPRS connection..."));
#endif
      gsm.SimpleWriteln("AT+CGATT=1");
      delay(3000);

      if (inet.attachGPRS(String(carrierdata.apnname).c_str(), String(carrierdata.apnuser).c_str(), String(carrierdata.apnpass).c_str()))
      {
#ifdef DEBUG
        Serial.println(F("status=ATTACHED"));         
        gsm.SimpleWriteln("AT+CIFSR");
        delay(5000);
      //Read until serial buffer is empty.s
        gsm.WhileSimpleRead();
#endif
      }
      else
      {
#ifdef DEBUG
        Serial.println(F("status=ERROR"));
#endif
      }
    
    }
    delay(1000);
  }

  
  if (completed == 0)
  {
    // check to turn off pump if there is no active irrrigation
    bool is_some_irrigation_on = false;
    for (int checking_slot = 0; checking_slot < NUM_SLOTS; checking_slot++)
    {
      if (zonal_irrigations[checking_slot].irrigation_id != "")
      {
        for (int checking_zone = 1; checking_zone <= number_of_zones; checking_zone++)
        {
          if (zonal_irrigations[checking_slot].status[checking_zone - 1] == 1)
          {
            is_some_irrigation_on = true;
            break;
          }
        }
      }
    }
    if (!is_some_irrigation_on)
    {
#ifdef DEBUG
      Serial.println(F("Reseting Arduino..."));
#endif
      digitalWrite(arduino_restart_pin, LOW); // Reset Arduino
//      delay(5000);
    }
    else
    {
#ifdef DEBUG
      Serial.println(F("Not reseting Arduino beacuse of active irrigation."));
#endif
    }
  }
  return completed;
}

#ifdef FLOW
struct flowmeter_struct checkFlowMeter()
{
  // output some measurement result
  #ifdef  DEBUG_Telemetry                                                                       //merenje protoka vode na kanalu CH = 5
    Serial.println("Currently " + String(getCurrentFlow(3)*60) + " L/min, " + String(getCurrentVOL(FlowSensorCH5)) + "L total.");
  #endif
  struct flowmeter_struct flow;
  flow.currentflow = getCurrentFlow(FlowSensorCH5)*60; //getCurrentFlow vraca protok u sekundama, pa *60 dobija se u min/L protok
  flow.currentvolume = getCurrentVOL( FlowSensorCH5);  
#ifdef DEBUG_Telemetry
  FlowDebug( FlowSensorCH5);
#endif
  return flow;
}
#endif

#ifdef INTERRUPTFLOW
struct flowmeter_struct checkFlowMeter()
{
struct flowmeter_struct flow_tmp;
    // process the (possibly) counted ticks
  Meter.tick(flowmeter_period); 
  flow_tmp.currentflow = Meter.getCurrentFlowrate();
  flow_tmp.currentvolume = Meter.getCurrentVolume();
  Meter.reset();
  return flow_tmp;
}
#endif
			
void checkLastSeen() {
  if (last_seen > 0 && !(currentTime() - last_seen < 20 * 60L)) {
    stopAll();
  }
}

void stopAll() {

  // stop zonal irrigations
  for (int checking_slot = 0; checking_slot < NUM_SLOTS; checking_slot++) {
    for (int checking_zone=1; checking_zone <= number_of_zones; checking_zone++) {
      
           
#ifdef DC_Valves
    stopValve(checking_zone-1);
#endif
#ifdef DEBUG_Extend      
     Serial.print(F("Gasim ZONU: "));Serial.println(checking_zone);
#endif      
      zonal_irrigations[checking_slot].status[checking_zone-1] = 0;
      zonal_irrigations[checking_slot].started_time[checking_zone-1] = 0L;
      zonal_irrigations[checking_slot].current[checking_zone-1] = 0;
    }
    zonal_irrigations[checking_slot].irrigation_id = "";
    zonal_irrigations[checking_slot].amount = 0L;
    zonal_irrigations[checking_slot].duration = 0;
    zonal_irrigations[checking_slot].max_duration = 0;
  }

  initialiseIrrFlowMet();
}
//---------------DC Valves permanent ON/OFF-----------------------------------------
#ifdef DC_Valves_Permanent
void startValve(int zone) {
    digitalWrite(valves_off[zone], HIGH);// Releji za N (negativan) prikljucak ventila
    if (zone == 3) { 
      PORTG = PORTG | 0x08; 
     }
  #ifdef DEBUG_Extend
        Serial.print("DC Relay Start, zone: ");Serial.println(zone+1);
  #endif 
}

void stopValve(int zone) {
    digitalWrite(valves_off[zone], LOW);   //Releji za N (negativan) prikljucak ventila
    if (zone == 3) { 
      PORTG = PORTG & 0xF7; 
    } 
  #ifdef DEBUG_Extend
        Serial.print("DC Relay STOP, zone: ");Serial.println(zone+1);
  #endif
}
#endif
//------------------------------------------------------------------------
#if defined(DC_Valves) && !defined(DC_Valves_Permanent)
void startValve(int zone) {
	
    
    if (zone == 3) { 
		PORTG = PORTG & 0xFB; //Reley za P na '0'
		PORTG = PORTG | 0x08; //Relay za N na '1' 	
		delay(valvedelay);		
		PORTG = PORTG & 0xF7; //Relay za N na '0'
	}
	else{		
		digitalWrite(valves_on[zone], LOW);  // Releji za P (pozitivan) prikljucak vential 
		digitalWrite(valves_off[zone], HIGH);// Releji za N (negativan) prikljucak ventila
		delay(valvedelay);
		digitalWrite(valves_off[zone], LOW);			
		
	}
  #ifdef DEBUG_Extend
        Serial.print("DC Relay Start: ");Serial.println(zone+1);
  #endif 
}

void stopValve(int zone) {	
    
    if (zone == 3) {
		PORTG = PORTG & 0xF7;  //Relay za N na '0'
		PORTG = PORTG | 0x04;  //Reley za P na '1'
		delay(valvedelay);
		PORTG = PORTG & 0xFB;  //Reley za P na '0'
	}
	else{
		digitalWrite(valves_off[zone], LOW);   //Releji za N (negativan) prikljucak ventila	
		digitalWrite(valves_on[zone], HIGH);  // Releji za P (pozitivan) prikljucak vential   
		delay(valvedelay);
		digitalWrite(valves_on[zone], LOW); 		
	}
	
    
  #ifdef DEBUG_Extend
        Serial.print("DC Relay STOP: ");Serial.println(zone+1);
  #endif
}
#endif

#ifdef FLOW_Toggling
bool checkFlow() {
     byte flowreadcount = 0;  
     byte firstflowstate = digitalRead(flow_pin_Toggling);
//     byte firstflowstate = ( PINE & 0x80);
     while (flowreadcount < 30) {
        byte flowstate = digitalRead(flow_pin_Toggling); // Za FERT
//        byte flowstate = ( PINE & 0x80);               // Za SENS
        if (flowstate != firstflowstate) {
          return true;
        }
        delay(50);
        flowreadcount++;
     }  
     return false;
  }
#endif

#ifdef TIME_F_Measure
void funcTimeMeasure(String funcBlock){
    Serial.print("Exe Time of "); Serial.print(funcBlock);Serial.print(" = "); Serial.print((float)(millis()-currentTimeMilles),0); Serial.println(" mS");
    currentTimeMilles = millis();
}
#endif

/*
int signalQuality(){
    //Read until serial buffer is empty
    Serial.println("Checking signal quality");
    gsm.SimpleWriteln("AT+CSQ");
    delay(1000);
    Serial.println("2-9 Marginal, 10-14 OK, 15-19 GOOD, 20-30 Excelent");
    gsm.WhileSimpleRead();
}
*/
