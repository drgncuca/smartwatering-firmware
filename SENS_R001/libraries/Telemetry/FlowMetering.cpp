#include <Arduino.h>
#include "Telemetry.h"


void initialiseIrrFlowMet(void){
	int i = 0;
	while(i < 5)// 5  kanala za merenje protoka
	{
		irrigationFlowMetering[i].irrigationFlowVolume = 0.00;
		irrigationFlowMetering[i].ElapsedTime=0.00;
		irrigationFlowMetering[i].flowMeterCalibKons =1;
		i++;
	}	
		
}
//-----------------------------------------------------------------------
float getCurrentVOL(int flowmeter_sensor){

unsigned long currentTime = millis();


unsigned long samplElapsedTime = currentTime  - irrigationFlowMetering[flowmeter_sensor].ElapsedTime; // proteklo vreme od zadnjeg semplovanja
if (samplElapsedTime < 0) {samplElapsedTime = 0;} //kad miles() funkcija dodje do kraja i pocne ispoceta da broji, prethodna jednacina bi imala ogromanu negativnu vrednost		
irrigationFlowMetering[flowmeter_sensor].ElapsedTime = currentTime;

//integralenje protoka po vremenu: Protok * Vreme = ZAPREMINA.	
float  deltaVolume = (getCurrentFlow(flowmeter_sensor)  *  (float)(samplElapsedTime)  ) /1000.0; //    protok * vreme = zapremina. millis() vraca u mS vreme.
	
#ifdef DEBUG_FLOW 	
	irrigationFlowMetering[flowmeter_sensor].flow = getCurrentFlow(flowmeter_sensor);
#endif																				  
//Kad ce resetovati ova promenljiva ? ? ?  
irrigationFlowMetering[flowmeter_sensor].irrigationFlowVolume = irrigationFlowMetering[flowmeter_sensor].irrigationFlowVolume + deltaVolume;	  
return deltaVolume;//irrigationFlowMetering[flowmeter_sensor].irrigationFlowVolume;		
}
//-------------------------------------------------------	
								// od 0 do  4
float getCurrentFlow(int flowmeter_sensor){  //vraca vrednost protoka Litara / Sekundi 

int flowIDchanel ;
float tempFlow=0;
 
switch (flowmeter_sensor){
	 case 0:flowIDchanel = A3; break;
	 case 1:flowIDchanel = A1; break;
	 case 2:flowIDchanel = A0; break;
	 case 3:flowIDchanel = A4; break;
	 case 4:flowIDchanel = A5; break;	 
	 default : break;
}
  
  int i = 0;
  int sum = 0;
  float flowSensValue = 0;
  
  //filtriranje signala sa ADC-a
  while (i < 4)
  {
    sum = sum + (float)analogRead(flowIDchanel);
    delay(1);
    i++;
  }
  flowSensValue = (float)sum * 0.25;//averaging
  // vraca vrednost Litara/Sekundi, za sistem od 33imp/L 
  tempFlow = irrigationFlowMetering[flowmeter_sensor].flowMeterCalibKons * flowSensValue / flowConstantImpuls33PerSecond;  
  if (tempFlow<0.006) {tempFlow = 0;} //  pojavljuje se gresa/sum pri nekoj matematici, Istraziti gde je....
  
  return tempFlow;
 
}
 //-------------------------------------------------------------------------------------
#ifdef DEBUG_FLOW    //ChanelS = [0..4]
void FlowDebug(int flowmeter_sensor){ // funkcija za debagovanje 			 
	  Serial.println(F("------------------------------------"));
				   
				   
						  
																						   
											 
							  
																						 
	  Serial.print(F("FLOW CH-"));                       Serial.print(flowmeter_sensor+1); 
//		Serial.print(F(", dV: "));           Serial.print(getCurrentVOL( flowmeter_sensor));
		Serial.print(F(", FLOW: ")); Serial.print(irrigationFlowMetering[flowmeter_sensor].flow);Serial.print(F("L/s"));	 	
		Serial.print(F(", V: "));    Serial.print(irrigationFlowMetering[flowmeter_sensor].irrigationFlowVolume);
		Serial.print(F("L,Time: "));Serial.print(((irrigationFlowMetering[flowmeter_sensor].ElapsedTime)/1000.0) );Serial.println(F("S."));
	  
}
#endif


