#include <Arduino.h>
#include "Telemetry.h"

#ifdef Sensor_BGT_WSD2
int getBGTtemperature(void){

return  ((float)analogRead(A12) / 1024.0) * 70.0 - 20.0; /// Temperature =  ADC * A /1024 + B, A= 70, B= -20.  [CAR_to_VOLT_Out_CH1]
	
	
}

int getBGThumidity(void){

return  ((float)analogRead(A13) / 1024.0 ) * 100.0 ; /// humidity =  ADC * A /1024 + B, A= 100, B= 0.  [CAR_to_VOLT_Out_CH2]
	
	
}

#endif