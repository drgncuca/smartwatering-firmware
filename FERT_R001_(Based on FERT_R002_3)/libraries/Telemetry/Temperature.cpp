#include <Arduino.h>
#include "Telemetry.h"
#include <SPI.h>
#include <Adafruit_MAX31865.h>

#ifdef Sensor_EC

extern Adafruit_MAX31865 TemperaturePT1000;

void setupTemperaturePT1000(void){
	
TemperaturePT1000.begin(MAX31865_3WIRE);  // set to 2WIRE or 4WIRE as necessary		
	
}

float getTemepraturePT1000(void){
	
return 	TemperaturePT1000.temperature(RNOMINAL, RREF);
	
}
#endif


#ifdef DEBUG_PT1000
void testTemperaturePT1000(void){

while(Serial.read() != '0'){

   //
   Serial.println("\033[0;0H");
  Serial.println("Temperature Test :\n");
  
   uint16_t rtd = TemperaturePT1000.readRTD();
 // Serial.print("RTD value: "); Serial.println(rtd); 
  float ratio = rtd;
  ratio /= 32768;
  //Serial.print("Ratio = "); Serial.println(ratio,2);
  Serial.print("Resistance = "); Serial.print(RREF*ratio,1);Serial.println(" ohm");
  Serial.print("Temperature = "); Serial.print(TemperaturePT1000.temperature(RNOMINAL, RREF));Serial.println(" Celsus");

  // Check and print any faults
  uint8_t fault = TemperaturePT1000.readFault();
  if (fault) {
    Serial.print("Fault 0x"); Serial.println(fault, HEX);
    if (fault & MAX31865_FAULT_HIGHTHRESH) {
      Serial.println("RTD High Threshold"); 
    }
    if (fault & MAX31865_FAULT_LOWTHRESH) {
      Serial.println("RTD Low Threshold"); 
    }
    if (fault & MAX31865_FAULT_REFINLOW) {
      Serial.println("REFIN- > 0.85 x Bias"); 
    }
    if (fault & MAX31865_FAULT_REFINHIGH) {
      Serial.println("REFIN- < 0.85 x Bias - FORCE- open"); 
    }
    if (fault & MAX31865_FAULT_RTDINLOW) {
      Serial.println("RTDIN- < 0.85 x Bias - FORCE- open"); 
    }
    if (fault & MAX31865_FAULT_OVUV) {
      Serial.println("Under/Over voltage"); 
    }
    TemperaturePT1000.clearFault();
  }
  Serial.println();
  delay(500);  
  }  
}
#endif
