#include <EEPROM.h>
#include "EEPROMAnything.h"
#include <SoftwareSerial.h>
#include <Wire.h>
#include <TimeLib.h>
#include <ArduinoJson.h>
#include "SIM900.h"
#include "ds3231.h"
#include "inetGSM.h"
#include "sms.h"
#include <String.h>
#include "Telemetry.h"					
#include <SPI.h>
#include <Adafruit_MAX31865.h>

#define FirwareVersion "FV112"

#ifdef Sensor_EC
  Adafruit_MAX31865 TemperaturePT1000 = Adafruit_MAX31865(49);
  EC_struct ECmeasuring;
#endif

#ifdef INTERRUPTFLOW
  #include <FlowMeter.h>
#endif  

InetGSM inet;
//TODO Check max size of buffer
StaticJsonBuffer<750> jsonBuffer; // bilo je 650

boolean gsm_started = false;

//bool is_some_zone_on = false;

bool isperm3 = false;

bool isautomode = false;

byte fertMode = 0;

bool startedNewIrigation = false;

byte networLostCounter = 0;

char server[] = "app.smartwatering.rs";

//char parcel_id[] = "rjTXqd"; //  Single Device FERT+Zone, Kruska NS !!
  
//char parcel_id[] = "MGTnV5"; //FERT DID for TESTING  (DID) // Testing multi device (Tresnja Irig)  fertMode == 2
//char parcel_id[] = "NmL73F"; //Zone DID for TESTING  (DID) // Testing multi device (Tresnja Irig)

 //char parcel_id[] = "y6ljUx";  // Lesnik Novi Sad - Lazar (  wizardlaki91@gmail.com )
//char parcel_id[] = "A4dgm";  // Lesnik Zvornik  (pid)

//char parcel_id[] = "yuIFb";  //seskejabuke
//char parcel_id[] = "HxK8jk"; //Aleksandar Rancic, Borovnice, FERT 
//char parcel_id[] = "TRZyr";  //Darko Ristic, Borovnica Ub, PID
//char parcel_id[] = "HIsWGI"; //Ivica Todorovic Borovnica Donji Račnik
//char parcel_id[] = "IS7Zj"; //Delta Agrar, Jabuka, Tornjas. PID , DC-ventili
//char parcel_id[] = "qWJdQZ"; //Borovnica Trlić FERT 10 Zones
//char parcel_id[] = "LgomjY"; //Borovnica Trlić Meteo 1 : 
//char parcel_id[] = "tCHkuC"; //Borovnica Trlić Meteo 2 :  
//char parcel_id[] = "JXUnNw"; //Zemlja i Sunce - Parcela Ivan
//char parcel_id[] = "gHduMG";//Borovnica Aleksandar Uzice, DC Relays permanent ON/OFF [DEL-13]
//char parcel_id[] = "sfWSqk";//Kupina Andrija Jelinac 4 zone AC [DEL-5]
//char parcel_id[] = "LgomjY";//Trlic FERT [DEL-1] pH, EC, SENTEK, STH20.
//char parcel_id[] = "tCHkuC";//Trlic FERT [DEL-1] SENTEK, STH20.
//char parcel_id[] = "aQjkKo";// Tornjos II
//char parcel_id[] = "ffCIYQ";//Darko Smiljkovski - Lesnik
//char parcel_id[] = "eD1BdU";//Sava Coop testna stanica
//char parcel_id[] = "8taxAn";//Damir Bulj
//char parcel_id[] = "8YOyxi";//Vladan Redzic Ecoland JAbuka  (Vlaški do)
//char parcel_id[] = "7aCids";//Radoica Radomirovic - Borovnica (Leposavic, Kosovo)
//char parcel_id[] = "B7FmJb";//Marko Smederevo : Zone 1-4
//char parcel_id[] = "M9xswP";//Marko Smederevo : Zone 5-8 
//char parcel_id[] = "9YyhWN";//Milos Aleksic Lesnik 
//char parcel_id[] = "7m4wBq";//Aleksandar Rancic, Borovnica, Zones 
//char parcel_id[] = "uPt2he";//Moshe Lifhitz, Borovnica, SENS
//char parcel_id[] = "tIzUOI";//Ivica Donji Racnik
//char parcel_id[] = "OqLzTt";//Ivica Donji Racnik
//char parcel_id[] = "qTY5yN";//DEL-28
//char parcel_id[] = "s4IUok";//Strahinja Malin START Airplant Cortanovci 
//char parcel_id[] = "qWJdQZ";//Trlic [DEL-36]
//char parcel_id[] = "8Ch2xq";//Nikola MSederevo  [DEL-29]
//char parcel_id[] = "7aCids";//Radoica Radomirovic [DEL-47]
//char parcel_id[] = "erI09D";//AQM Mileta [DEL-45]  
//char parcel_id[] = "Rp8Gt";// Zeljko Balac Borovnica [DEL-33]
//char parcel_id[] = "HxK8jk";//Aleksandar Rancic
//char parcel_id[] = "8XP4YK";//AQM Gorevnica Nenad Tusevljak (Szojevci)
//char parcel_id[] = "9qErR";//Darko Calic
//char parcel_id[] = "AwQ3dJ";//Armin Terzic
char parcel_id[] = "4FTeqk";//Sasa Tirnanic Krnjevo
//char parcel_id[] = "Knv5hB";// Ceramilac-Royal Stroj Group- Lesnik Kosjeric
//char parcel_id[] = "59h1lk";//Milovan Draskic
//char parcel_id[] = "CYLb62";//MDAgrotime-A Mila Dragicevic - Borovnica Mrcajevci
//char parcel_id[] = "EwMfBR";//Plastenik BiH + SENS
//char parcel_id[] = "zjXIFW";//Smart Watering - Voćnjak Sajam 
//char parcel_id[] = "8uHTk0";//Perica Zekonja
//char parcel_id[] = "2HsdL3";//Metodije Labović	 Borovnica Kupusina
//char parcel_id[] = "Y5X9LR";//Željko Pavlović Borovnica Krupanj
//char parcel_id[] = "nXr6cl";//AQM Vladimirci Borovnica
//char parcel_id[] = "wlTppU";//Sasa Milic Borovnice plastenik Mladenovac
//char parcel_id[] = "2rdlwY";//AQM Grgeteg Svetlana vukajlovic
//char parcel_id[] = "S5UfNQ";//Milan Ninić ZR Borovnica Aradac Aradac Srpski
//char parcel_id[] = "4KVmjd";//Milan Rakočević  Prokuplje 
//char parcel_id[] = "7h8qHe";//Petar Marinković
//char parcel_id[] = "JXUnNw";//Ivan Brestovac

//char parcel_id[] = "Nlc32F";//Goran Banja Luka Main - Water level - Lesnik
//char parcel_id[] = "8cn6WR";//Goran Banja Luka  - Dole 1
//char parcel_id[] = "GlGzVr";//Goran Banja Luka  - Dole 2
//char parcel_id[] = "Kd45GU";//Goran Banja Luka  - Dole 3
//char parcel_id[] = "s4IUok";//Strahinja Malin Cortanovci Airplant
//char parcel_id[] = "XRjxOU";//Delta Agrar Sveti Nikola Sečanj
//char parcel_id[] = "yjNu3H";//Marija Mijailović [NEISPRAVNA  Ploca]
//char parcel_id[] = "vkX6pK";//Marko Radovanović KG
//char parcel_id[] = "I43J5k";//AQ Manojlović Duško Manojlović
//char parcel_id[] = "srojqK";//Goran Miljkovic Migor Speacers
//char parcel_id[] = "3G0Lwv";//Ivan Sirogojno
//char parcel_id[] = "I6GdgA";//Mladen Kosmaj Borovnica	
//char parcel_id[] = "kkjWwv";//Borovnica Pancevo
//char parcel_id[] = "HQmOW7";//
//char parcel_id[] = "Glyhon";//
//char parcel_id[] = "r1BskW";//
//char parcel_id[] = "EubqiR";//Carlos Santos  12 zona
//char parcel_id[] = "ZES79u";//Carlos Santos   16 zona
//char parcel_id[] = "Lqrofw";//Delta Agrar Čelarevo
//char parcel_id[] = "H6qqyi";//Aleksandra, Dusko Zminjak
//char parcel_id[] = "aprGnO";//Matijevic doo Novo Orahovo	
//char parcel_id[] = "r1BskW";//"Eco Fruta Luan Kosovo"
//char parcel_id[] = "Glyhon";//Vuk Radovic
//char parcel_id[] = "11eULk";//Armin Terzić Fruit Prom doo
//char parcel_id[] = "r1BskW";//"Eco Fruta Lua Kosovo"
//char parcel_id[] = "hiGwGO"; //SiQ Sertifikacija
//char parcel_id[] = "ZVfQjp"; //Bojan Kalicanin ARDUINO
//char parcel_id[] = "ety2Wz"; //DiVision Agro

//char parcel_id[] = "6rLjL5"; //Eco Fruits Batlava Borovnica	- Kosovo		
//char parcel_id[] = "4HKTFQ"; //Eco Fruits Batlava Borovnica	- Kosovo	
//char parcel_id[] = "Od7yvs"; //Eco Fruits Batlava Borovnica	- Kosovo	
//char parcel_id[] = "jxVUao"; //Eco Fruits Batlava Borovnica	- Kosovo

//char parcel_id[] = "XW0nyR"; //MonteGrow
//char parcel_id[] = "QyDIur";//Fruit Prom Fakultet PFF Sarajevo
//char parcel_id[] = "aStZXU";//ECO Fruits 7
//char parcel_id[] = "VCWldo";//Eco Fruits 8

//char parcel_id[] = "XTxByQ";//Antun Epler
//char parcel_id[] = "TwFjOV";//Antun Epler
//char parcel_id[] = "IHZZSe";//Antun Epler


struct irrigation_struct
{
  unsigned int duration = 0; // vreme trajanja navodnjavanja u sekundama
  unsigned long amount = 0L; // Kolicina vode u litrima
  byte status[14] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; // 0 = off, 1 = on
  //byte status[19] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 , 0, 0, 0, 0, 0}; // 0 = off, 1 = on
  unsigned long started_time =  0L;// u sekundama
//  unsigned int max_duration = 0; // max vreme navodnjavanja po zapremini, zastitni parametar.
  String irrigation_id = "";
  float current = 0;// kolicinu vode u litrima.
  //float current = 0L; 
} zonal_irrigation_struct;

//pH global variabla
pH_struct pHmeasuring;

unsigned long irigationTimeShiftGap = 0;
unsigned long irigationTimeShiftGap_old = 0;
unsigned int maxIrigtionTimeShiftGap = 29; // sekundi max koliko sme biti praznog hoda u smenskok navodnjavanju


fert_struct fertilizations[4];

irrigation_struct  zonal_irrigations[NUM_SLOTS];

irrigation_struct_FlowMetering  irrigationFlowMetering[5]; // Globalna promenljiva koja sadrzi informacije o protekloj kolicini VODE na 5 kanalala flowMeter-a i proteklo vreme.


#define MAX_ZONES 14

byte number_of_zones = MAX_ZONES;

     //ARDUINO   // 1   2   3   4   5   6   7   8
  //byte valves[] = {22, 23, 24, 25, 26, 27, 28, 29};

    //FERT R001   // 1   2   3   4   5   6   7   8   9   10 |--NOT USED---|
  //byte valves[] = {24, 23, 22, 25, 26, 27, 28, 29, 34, 35, 30, 41, 40, 39};

// FERT R002     1   2   3   4   5   6   7   8   9   10  11  12  13  14     15  16  17  18  19, 20
//byte valves[] = {24, 23, 22, 25, 26, 27, 28, 29, 34, 35, 37, 36, 33, 32};//, 31, 30, 41, 40, 39, 48};    
//byte valves[] = {24, 23, 22, 25, 26, 27, 28, 29, 34, 35, 37, 36, 33, 32, 40};//, 31, 30, 41, 40, 39, 48};   

 //  FERT R003    1   2   3   4   5   6   7   8   9   10  11  12  13  14     15  16  17  18  19, 20      
byte valves[] = {24, 23, 22, 25, 26, 27, 28, 29, 37, 36, 35, 34, 33, 32};//, 31, 30, 41, 40, 39, 48};   

 //FERT R003 (custom) 1   2   3   4   5   6   7   8   9   10  11  12  13  14  15  16  17  18  19     
//byte    valves[] = {24, 23, 22, 25, 26, 27, 28, 29, 37, 36, 35, 34, 33, 32, 30, 41, 40, 39, 48};
  
            //Valves  16  17  18  19
byte valves_fert[4] = {30, 41, 40, 39};
			//za ARDUINO
//byte valves_fert[4] = {34, 35, 36, 39};

byte mainpump_pin = 31; // Main Pump Relay AC15. 
//byte mainpump_pin = 41; // Main Pump Relay AC17. - na Ploci B016 relej 15 neradi

#ifdef FLOW_Toggling
  byte flow_pin_Toggling = 15;
#endif


//----------Temperature and Humidity-----
#ifdef SHT1sensor
  float temperature;
  float humidity;
  SHT1x SHT1x_sensor(   20  ,   21);
#endif

float ph_value = 0;

#ifdef MIXER
// mixer FERT R002
//byte mixer_pin = 32; // Relay - 14.
// mixer FERT R003
byte mixer_pin = 48; // Relay - 20.

byte mixer_status = 0; // 0 = off, 1 = on
String mixer_id = "";
String mixer_irrigation_id = "";
int mixer_duration = 0;
unsigned long mixer_started_time = 0L;
#endif

byte sim_restart_pin = 7;
byte arduino_restart_pin = 12;

String params = "";
String paramsTMP = "";
//TODO: Check max size of the message and reduce it's size accordingly
char msg[MSG_BUF]; 

carrier carrierdata;
struct_operater_vip operaterVIP;
struct_operater_MTS operaterMTS;
struct_operater_Telenor operaterTelenor;
struct_operater_BHmob operaterBHmob;
struct_operater_mtelCG operaterMTEL_CG;	
struct_operater_MTEL operaterMTEL;
struct_operater_BHeronet operaterBHeronet;
struct_operater_Haloo operaterHaloo;

// last time successfully contacted server
unsigned long last_seen = 0L;
#ifdef INTERRUPTFLOW
	// fmeter sensor
	double flowmeter_capacity = 60;
	double flowmeter_kfactor = 0.55;
	double flowmeter_mfactor = 1;

	FlowSensorProperties flowmeter_sensor = {flowmeter_capacity, flowmeter_kfactor, {flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor}};
	FlowMeter Meter = FlowMeter(2, flowmeter_sensor);
	long flowmeter_period = 1000; // one second (in milliseconds)
	long flowmeter_lasttime = 0;

void MeterISR()
{  
  Meter.count();// let our flow meter count the pulses
}
#endif

#if defined FLOW || defined INTERRUPTFLOW					   
struct flowmeter_struct
{
  float currentflow;
  float currentvolume;
};
#endif

bool carrierChange = false;

#ifdef TIME_F_Measure
unsigned long currentTimeMilles;
#endif


//============================================= S E T U P =========================================================
void setup()
{  

  Serial.begin(115200); // Serial connection.
  Serial.print(F("\n\n(RE)start uC - FW Version : ")); Serial.println(FirwareVersion);

  RS485setup(); 

#ifdef Fertilisation
  initialiseFertilisationParameters();
#endif
  valve_stop_fertilization(1);
  valve_stop_fertilization(2);
  valve_stop_fertilization(3);
  valve_stop_fertilization(4);

  initialiseIrrFlowMet(); //REsetovanje Zapremine i globalnog proteklog vremena na 0 vrednosti.
  
  Wire.begin();
  
#ifdef Sensor_EC  
  setConductivityDriver(); // setovanje SINusnog generatora: 70Hz, 1-> sinus.  
  setupTemperaturePT1000();// Setovanje temperaturnog senzora za PT1000.
#endif

  struct ts t;

  DS3231_get(&t);


				   
 digitalWrite(arduino_restart_pin, HIGH);
  pinMode(arduino_restart_pin, OUTPUT);
  
  pinMode(sim_restart_pin, OUTPUT);
  digitalWrite(sim_restart_pin, HIGH);  
  
  delay(100);  
  digitalWrite(8, HIGH); //on 
  delay(1050);  
  digitalWrite(8, LOW); //off
  delay(100);
  digitalWrite(sim_restart_pin, LOW);
  delay(150);
  digitalWrite(sim_restart_pin, HIGH);
  delay(500);

#ifdef DEBUG
  Serial.print(F("GSM="));
#endif 
  
  if (gsm.begin(9600))
  {
    gsm_started = true;    
#ifdef DEBUG
    Serial.println(F("READY"));
#endif 

//Pri Debagovanju, preskoci jedan deo koda, da ubrza startovanje uC-a
#ifdef SKIP_Operater_Detecting
    goto skip_operater_detecting;
#endif

  }
  else
  {
    gsm_started = false;
#ifdef DEBUG
    Serial.println(F("IDLE"));
#endif
  
  }

doesOperaterChanging();// Promena operatera

EEPROM_readAnything(EERPOM_mem_loc_CarrierData, carrierdata);

#ifdef DEBUG_Operater 
    char tmp[2]="00"; 
    Serial.println("\n---------------------------");    
    Serial.println("Sadrzaj EEPROM-a : ");   
    EEPROM_readAnything(64, tmp);
    Serial.println(String(tmp).c_str());     
    Serial.println("Data in the Carrier Data Structure");+
    Serial.println(carrierdata.apnname);
    Serial.println(carrierdata.apnuser);
    Serial.println(carrierdata.apnpass);        
#endif

if (gsm_started)
  {
    Serial.print(F("GPRS="));    
//Pri Debagovanju, preskoci jedan deo koda, da ubrza startovanje uC-a    
#ifdef SKIP_Operater_Detecting  
    skip_operater_detecting:
    Serial.print(F("GPRS="));  

  if (inet.attachGPRS("internet", "internet", "internet"))
  //if (inet.attachGPRS("GPRSWAP", "MTS", "064"))
    {
#else
    if (inet.attachGPRS(String(carrierdata.apnname).c_str(), String(carrierdata.apnuser).c_str(), String(carrierdata.apnpass).c_str()))
    {
#endif    
    
#ifdef DEBUG_Extend
     Serial.println(F("ATTACHED"));     
     gsm.SimpleWriteln("AT+CIFSR");//Read IP address.
     delay(5000);     
     gsm.WhileSimpleRead();//Read until serial buffer is empty
#endif
    }
    else
    {
#ifdef DEBUG
      Serial.println(F("ERROR"));
#endif
    }
    delay(1000); 
  }
else
  {
#ifdef DEBUG    
   Serial.println(F("GSM&GPRS=NOTstarted"));
#endif  
  }  
  
  // Initialize  PINs of Valeves.
#ifdef AC_Valves  
  for (byte x=0; x < number_of_zones;  x++)
  {
    pinMode(valves[x], OUTPUT);
    digitalWrite(valves[x], relay_stop);
  }
#endif  


  pinMode(mainpump_pin, OUTPUT);
  digitalWrite(mainpump_pin, relay_stop);

#ifdef MIXER
  pinMode(mixer_pin, OUTPUT);
  digitalWrite(mixer_pin, relay_stop);
#endif


#ifdef Water_Level 
  pinMode(14, INPUT_PULLUP); //D-TX - pin 4 - LOW  sensor  
  pinMode(15, INPUT_PULLUP); //D-RX - pin 3 - HIGH sensor
#endif
#ifdef Water_Level_2_3 
  pinMode(57, INPUT_PULLUP);  // Midle level 30%  FLOW_Measure_CH_1
  pinMode(55, INPUT_PULLUP);  // Midle level 60%  FLOW_Measure_CH_2
#endif


#ifdef INTERRUPTFLOW
  // enable a call to the 'interrupt service handler' (ISR) on every rising edge at the interrupt pin

  //pinMode(2, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(2), MeterISR, RISING);

  // sometimes initializing the gear generates some pulses that we should ignore
  Meter.reset();
#endif

#ifdef FLOW_Toggling
   pinMode(flow_pin_Toggling, INPUT);
//   DDRE  = DDRE & 0xEF;  // Za SENS - FLOW input 
#endif

#ifdef FLOW_100L_imp
  // enable a call to the 'interrupt service handler' (ISR) on every rising edge at the interrupt pin
  attachInterrupt(INT7, flowImplseTick100L_Procesing, RISING);
#endif

#ifdef DEBUG_shifts
pinMode( 19, INPUT_PULLUP);
#endif
};
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void loop()
{
#ifdef DEBUG
  Serial.println(F("Usao u loop"));
#endif

  params = "";  
  params.concat("did=" + String(parcel_id));
  params.concat("&" + String(FirwareVersion));

#if defined FLOW || defined INTERRUPTFLOW
  struct flowmeter_struct flow = checkFlowMeter(); //azurira vrednost protoka i zapremine
  checkValves(flow.currentvolume);
#else
  checkValves(0);
#endif

 
#ifdef INTERRUPTFLOW  
   Meter.reset();				 
#endif	
			   
#ifdef FLOW_Toggling 
   bool currentFlowStatus = checkFlow() ? 1 : 0;  // ako ima bilo kakvog protoka vraca '1'
#endif
 
  if(isautomode) {
    checkLastSeen();
  }
  
#ifdef FLOW_100L_imp 

  params.concat("&cfl=" + String(getFlow100L()));
  unsigned int tmpFlowVolume = getCurrentVOLtick100L();
  params.concat("&tfv=" + String(tmpFlowVolume)); 
  checkValves(tmpFlowVolume);

#endif 

#ifdef Sensor_pH  
  ph_value = getPh();
#endif

#ifdef SHT1sensor
   Wire.end();  // u konfliktu dva I2C-a RTC i Temp-Humid senzor
   temperature = SHT1x_sensor.readTemperatureC();  
   humidity = SHT1x_sensor.readHumidity();
   Wire.begin();    
#endif

#ifdef Sensor_EC
//  el_conductance = getELconductance(); 
  ECmeasuring.measured = getELconductance();
#endif  

#ifdef NetSens
 params.concat("&h1=" + String(analogRead(A9))); 
#endif 

#ifdef Sensor_10HS
  params.concat("&h1=" + String(analogRead(A8))); // podeljeno sa 2, jer je analogni ulaz pojacan sa 2x. A8 - Chanl 4 (na PCB je labela AN_IN3)
#endif  
//params.concat("&h2=" + String(0));

#ifdef SHT1sensor
  params.concat("&ah=" + String(humidity));
  params.concat("&at=" + String(temperature));
#endif  
//  params.concat("&temp=" + String(0));

#ifdef AC_Power_FERT
int power = getPower();
  params.concat("&pow=" + String(power));                           // za FERT plocu
#endif

#ifdef MIXER
  params.concat("&mid=" + String(mixer_id));
#endif
  
#ifdef Fertilisation  
  #ifdef FERT_0
     params.concat("&fid="  + String(fertilizations[0].fertilization_id));
  #endif
  #ifdef FERT_2
     params.concat("&fid2=" + String(fertilizations[1].fertilization_id));
  #endif
  #ifdef FERT_3
     params.concat("&fid3=" + String(fertilizations[2].fertilization_id));
  #endif
  #ifdef FERT_4 
     params.concat("&fid4=" + String(fertilizations[3].fertilization_id));
  #endif
#endif

#ifdef Sensor_pH
  params.concat("&ferph=" + String(pHmeasuring.rawPh));//salje se RAW pH.
#endif

#ifndef MainPumpIssueSkip 
	#if defined FLOW || defined INTERRUPTFLOW 
	  params.concat("&cfl=" + String(flow.currentflow));
	  params.concat("&tfv=" + String(flow.currentvolume));  
	#endif 
#endif  

#ifdef MainPumpIssueSkip 
  params.concat("&cfl=" + String(33.44));
  params.concat("&tfv=" + String(44.33));
#endif 

#ifdef FLOW_Toggling  
  params.concat("&cfl=" + String(currentFlowStatus));
#endif    

#ifdef Sensor_EC
  params.concat("&ecv=" + String(ECmeasuring.measured));
#endif


#if defined(Water_Level) && !defined(Water_Level_2_3)
if (!digitalRead(14) && !digitalRead(15)) params.concat("&wl=" + String(4)); // Full tank
if (!digitalRead(14) && digitalRead(15)) params.concat("&wl=" + String(1));  // midle tank
if (digitalRead(14) && digitalRead(15)) params.concat("&wl=" + String(0));   //empty tank
if (digitalRead(14) && !digitalRead(15)) params.concat("&wl=" + String(-1)); // error level tank
#endif

#if defined(Water_Level) && defined(Water_Level_2_3)
if (!digitalRead(14) && !digitalRead(57) && !digitalRead(55) && !digitalRead(15)) params.concat("&wl=" + String(4)); // Full tank
if (!digitalRead(14) && !digitalRead(57) && !digitalRead(55) &&  digitalRead(15)) params.concat("&wl=" + String(3)); // Midle High
if (!digitalRead(14) && !digitalRead(57) &&  digitalRead(55) &&  digitalRead(15)) params.concat("&wl=" + String(2)); // Midle Low
if (!digitalRead(14) &&  digitalRead(57) &&  digitalRead(55) &&  digitalRead(15)) params.concat("&wl=" + String(1)); // Low Level
if ( digitalRead(14) &&  digitalRead(57) &&  digitalRead(55) &&  digitalRead(15)) params.concat("&wl=" + String(0)); // empty tank
if ( digitalRead(14) && !digitalRead(15)) params.concat("&wl=" + String(-1)); // error level tank
#endif

params.concat("&RSSI=" + String(getSignalStrength()));

  String activeirrigations;
  for (byte i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id != "")
    {
      activeirrigations.concat("," + String(zonal_irrigations[i].irrigation_id));
    }
  }
  activeirrigations.remove(0, 1);
  params.concat("&iids=" + String(activeirrigations));

  Serial.println(params);

  memset(msg, 0, sizeof(msg)); 
//  apiCall(server, 80, "/api/post/sync.php", params.c_str(), msg, MSG_BUF);
  apiCall(server, 80, "/api/post/sync_device.php", params.c_str(), msg, MSG_BUF);


  char *jsonResponse = strstr(msg, "{"); 
#ifdef DEBUG			  
  Serial.println(jsonResponse);
  //Serial.println(msg);
#endif
//--------------------------------------------------------------------------------
  JsonObject &root = jsonBuffer.parseObject(jsonResponse);
//--------------------------------------------------------------------------------
 
  JsonVariant numberofzones_json = root["zones"];
  if (numberofzones_json.success())
  {
    if (number_of_zones != root["zones"])
    {
      number_of_zones = root["zones"];
      if (number_of_zones > MAX_ZONES) number_of_zones = MAX_ZONES;
    }
  }

//---------------------------------------------------------------------
  JsonObject &flowmeter_config = root["flowconf"];
  if (flowmeter_config.success())
  {
#ifdef FLOW
    irrigationFlowMetering[FlowSensorCH5].flowMeterCalibKons = flowmeter_config["kf"];
#endif

#ifdef INTERRUPTFLOW
/*
bool flow_config_changed = false;
    if (flowmeter_config["cap"] != flowmeter_capacity)
    {
      flowmeter_capacity = flowmeter_config["cap"];
      flow_config_changed = true;
    }
    if (flowmeter_config["kf"] != flowmeter_kfactor)
    {
      flowmeter_kfactor = flowmeter_config["kf"];
      flow_config_changed = true;
    }
    if (flowmeter_config["mf"] != flowmeter_mfactor)
    {
      flowmeter_mfactor = flowmeter_config["mf"];
      flow_config_changed = true;
    }

    if (flow_config_changed)
    {
      FlowSensorProperties flowmeter_sensor = {flowmeter_capacity, flowmeter_kfactor, {flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor}};
      Meter = FlowMeter(2, flowmeter_sensor);
    }
*/
#endif								 
  }

#ifdef MIXER  
  JsonObject &start_mixer = root["mix"];
  if (start_mixer.success())
  {
    if (strcmp(start_mixer["mid"].as<String>().c_str(), mixer_id.c_str()) != 0)
    {
      mixer_start(start_mixer["mid"].as<String>(), start_mixer["iid"].as<String>(), start_mixer["duration"]);
    }
  }
#endif

  JsonObject &start_irrigation = root["irr"];
  if (start_irrigation.success())
  {
    // TODO: check if this irrigationid is started
    if (strcmp(start_irrigation["irrigationid"].as<String>().c_str(), root["irr"]["iid"].as<String>().c_str()) != 0)
    {
      isautomode = start_irrigation["automode"];
	  startedNewIrigation = true ;
      if (start_irrigation["zonal"])
      {
        if (start_irrigation["duration"])
        {
          //per time
      			if (fertMode == 1) {                                           //duration - kolicina vode u m3 is sinka.                60min max - zastita
      			    isperm3 = true;
      			    zonal_irrigation_start(start_irrigation["iid"].as<String>(), (start_irrigation["duration"]), start_irrigation["zones"], 60);// 60 * 60 = 1h
      			}
      			else {			
      			    isperm3 = false;
      			    zonal_irrigation_start(start_irrigation["iid"].as<String>(), start_irrigation["duration"], start_irrigation["zones"]);  
      			}
        }
        else
        {
          //per m3
          isperm3 = true;       //maxduration - u minutama vrednost u sinku
          zonal_irrigation_start(start_irrigation["iid"].as<String>(), start_irrigation["amount"], start_irrigation["zones"], start_irrigation["maxduration"]);
        }
      }
    }
  }

//-----------------------koeficijent za kalibraciju pH senzora-- Y = k*X + M -----------------------//
   JsonVariant ferphK_json = root["ferphk"];
    if (ferphK_json.success())
    {
      if (pHmeasuring.ferphK != root["ferphk"])
      {
          pHmeasuring.ferphK = root["ferphk"];
      }
    }

   JsonVariant ferphM_json = root["ferphm"];
    if (ferphK_json.success())
    {
      if (pHmeasuring.ferphM != root["ferphm"])
      {
          pHmeasuring.ferphM  = root["ferphm"];
      }
    }
//-----------------Koeficijenti P I D regulatora za pH ----------------------------------
 JsonVariant ferPID_Kp_json = root["ferkp"];
    if (ferPID_Kp_json.success())
    {
      if (pHmeasuring.ferKp != root["ferkp"])
      {
          pHmeasuring.ferKp = root["ferkp"];
      }
    }
 JsonVariant ferPID_Ki_json = root["ferki"];
    if (ferPID_Ki_json.success())
    {
      if (pHmeasuring.ferKi != root["ferki"])
      {
          pHmeasuring.ferKi = root["ferki"];
      }
    }
 JsonVariant ferPID_Kd_json = root["ferkd"];
    if (ferPID_Kd_json.success())
    {
      if (pHmeasuring.ferKd != root["ferkd"])
      {
          pHmeasuring.ferKd = root["ferkd"];
      }
    }
//-------------- Fertilisation  M O D E  -------------------------------------
    JsonVariant isfertperm3_json = root["fertmode"];
    if (isfertperm3_json.success())
    {
      if (fertMode != root["fertmode"])
      {
          fertMode = root["fertmode"];
      }
    }
	if (fertMode > 2) {fertMode = 0;} // zastita
// Serial.print(F("fertMode "));Serial.println(fertMode); 
//---------EC calbracioni koeficijent EC = k*X + m --------------------------
#ifdef Sensor_EC
 JsonVariant EC_CalibrCoeff_json = root["eck"];
    if (EC_CalibrCoeff_json.success())
    {
      if ( ECmeasuring.ecK != root["eck"])
      {
           ECmeasuring.ecK = root["eck"];
      }
    }
    
 JsonVariant EC_CalibrCoeff_M_json = root["ecm"];
    if (EC_CalibrCoeff_M_json.success())
    {
      if ( ECmeasuring.ecM != root["ecm"])
      {
           ECmeasuring.ecM = root["ecm"];
      }
    }
    
#ifdef Sensor_EC_PID  

//-------------  EC desire FERT 2,3,4 ---------------------------------
float tmpDC = 0;
JsonObject &EC2_PID_desireEC = root["fer2"];
    if (EC2_PID_desireEC.success())
    { 
		tmpDC = EC2_PID_desireEC["dec"];
	  	
      if ( tmpDC > 0 )
      {
           ECmeasuring.desired = tmpDC;
       
           goto skipNextDesireEC;
     }
    }

JsonObject &EC3_PID_desireEC = root["fer3"];
    if (EC3_PID_desireEC.success())
    {
		tmpDC = EC3_PID_desireEC["dec"];
      if ( tmpDC > 0)
      {
      
		   ECmeasuring.desired = tmpDC;

           goto skipNextDesireEC;
      }
    }

JsonObject &EC4_PID_desireEC = root["fer4"];
    if (EC4_PID_desireEC.success())
    {
		tmpDC = EC4_PID_desireEC["dec"];
      if ( tmpDC > 0)
      {
           
		   ECmeasuring.desired = tmpDC;
       
           goto skipNextDesireEC;
      }
    }

skipNextDesireEC:
#ifdef DEBUG_EC_PID  
	Serial.print(F("DesireEC: "));Serial.println(ECmeasuring.desired);
#endif
//-----------------Koeficijenti P I D regulatora za EC ----------------------
JsonVariant EC_PID_Kp_json = root["ecp"];
    if (EC_PID_Kp_json.success())
    {
      if (ECmeasuring.ecKp != root["ecp"])
      {
          ECmeasuring.ecKp = root["ecp"];
      }
    }
JsonVariant EC_PID_Ki_json = root["eci"];
    if (EC_PID_Ki_json.success())
    {
      if (ECmeasuring.ecKi != root["eci"])
      {
          ECmeasuring.ecKi = root["eci"];
      }
    }
JsonVariant EC_PID_Kd_json = root["ecd"];
    if (EC_PID_Kd_json.success())
    {
      if (ECmeasuring.ecKd != root["ecd"])
      {
          ECmeasuring.ecKd = root["ecd"];
      }
    }
//---------------- Koeficijenti strujnog drajvera za EC PID upravljanje FERTMOD 0
if (fertMode == 0){
  JsonVariant EC_PID_CH2 = root["ech2"];
    if (EC_PID_CH2.success())
    {
      if (ECmeasuring.CH2_Curr_Strenght != root["ech2"])
      {
          ECmeasuring.CH2_Curr_Strenght = root["ech2"];
      }
    }

  JsonVariant EC_PID_CH3 = root["ech3"];
    if (EC_PID_CH3.success())
    {
      if (ECmeasuring.CH3_Curr_Strenght != root["ech3"])
      {
          ECmeasuring.CH3_Curr_Strenght = root["ech3"];
      }
    }  
	
  JsonVariant EC_PID_CH4 = root["ech4"];
    if (EC_PID_CH4.success())
    {
      if (ECmeasuring.CH4_Curr_Strenght != root["ech4"])
      {
          ECmeasuring.CH4_Curr_Strenght = root["ech4"];          
      }
    } 
    
}

#endif  //Sensor_EC_PID 
    
#endif  //Sensor_EC
												   
												   
	// max vreme izmedju dva smenska navodnjavanja gde je OK da je FERT aktivan......											   
//---------------------------------------------------------------------------
  JsonVariant FERT_maxIrigtionTimeShiftGap = root["mtsg"];
    if (FERT_maxIrigtionTimeShiftGap.success())
    {
      if (maxIrigtionTimeShiftGap != root["mtsg"])
      {
          maxIrigtionTimeShiftGap = root["mtsg"]; 
		  if(maxIrigtionTimeShiftGap > 120) maxIrigtionTimeShiftGap = 120;
		  if(maxIrigtionTimeShiftGap < 15) maxIrigtionTimeShiftGap = 15;
      }
    } 

#ifdef Fertilisation

JsonObject &start_fertilization = root["fer"];
//fertMode =0,2
if (fertMode != 1 ){

#ifdef FERT_0  
  if (start_fertilization.success())
{	
    fertilization_start(start_fertilization["fid"].as<String>() , start_fertilization["iid"].as<String>() , start_fertilization["duration"], 1, start_fertilization["dph"],start_fertilization["dpw"]);
}
#endif

#ifdef FERT_2
  JsonObject &start_fertilization2 = root["fer2"];
  if (start_fertilization2.success())
  {		  
    fertilization_start(start_fertilization2["fid2"].as<String>(), start_fertilization2["iid"].as<String>(), start_fertilization2["duration"], 2, start_fertilization2["dph"],start_fertilization2["dpw"]);
  }
#endif

#ifdef FERT_3
  JsonObject &start_fertilization3 = root["fer3"];
  if (start_fertilization3.success())
  {		
    fertilization_start(start_fertilization3["fid3"].as<String>(), start_fertilization3["iid"].as<String>(), start_fertilization3["duration"], 3, start_fertilization3["dph"],start_fertilization3["dpw"]);
  }
#endif

#ifdef FERT_4 
  JsonObject &start_fertilization4 = root["fer4"];
  if (start_fertilization4.success())
  {	
    fertilization_start(start_fertilization4["fid4"].as<String>(), start_fertilization4["iid"].as<String>(), start_fertilization4["duration"] ,4, start_fertilization4["dph"] ,start_fertilization4["dpw"]);
  }
#endif

#ifdef FERT_5
  JsonObject &start_fertilization5 = root["fer5"];
  if (start_fertilization5.success())
  {	
    fertilization_start(start_fertilization5["fid4"].as<String>(), start_fertilization5["iid"].as<String>(), start_fertilization5["duration"] ,5, start_fertilization5["dph"] ,start_fertilization5["dpw"]);
  }
#endif
}
// FertMode = 1
else{
#ifdef FERT_0  
  if (start_fertilization.success())
{	
    fertilization_start(start_fertilization["fid"].as<String>() , start_fertilization["iid"].as<String>() ,  start_fertilization["amount"].as<float>() , 1, start_fertilization["dph"],start_fertilization["dpw"]);
}
#endif

#ifdef FERT_2
  JsonObject &start_fertilization2 = root["fer2"];
  if (start_fertilization2.success())
  {		  
    fertilization_start(start_fertilization2["fid2"].as<String>(), start_fertilization2["iid"].as<String>(), start_fertilization2["amount"].as<float>(), 2, start_fertilization2["dph"],start_fertilization2["dpw"]);
  }
#endif


#ifdef FERT_3
  JsonObject &start_fertilization3 = root["fer3"];
  if (start_fertilization3.success())
  {		
    fertilization_start(start_fertilization3["fid3"].as<String>(), start_fertilization3["iid"].as<String>(), start_fertilization3["amount"].as<float>(), 3, start_fertilization3["dph"],start_fertilization3["dpw"]);
  }
#endif

#ifdef FERT_4 
  JsonObject &start_fertilization4 = root["fer4"];
  if (start_fertilization4.success())
  {	
    fertilization_start(start_fertilization4["fid4"].as<String>(), start_fertilization4["iid"].as<String>(), start_fertilization4["amount"].as<float>()  ,4, start_fertilization4["dph"] ,start_fertilization4["dpw"]);
  }
#endif

#ifdef FERT_5
  JsonObject &start_fertilization5 = root["fer5"];
  if (start_fertilization5.success())
  {	
    fertilization_start(start_fertilization5["fid5"].as<String>(), start_fertilization5["iid"].as<String>(), start_fertilization5["amount"].as<float>() ,5, start_fertilization5["dph"] ,start_fertilization5["dpw"]);
  }
#endif		
	
}
  
#endif  
//-----------------------------------------------------------------------------------------------------//

JsonVariant stop_irrigation_id_json = root["stopirr"];
  if (stop_irrigation_id_json.success())
  {    
    char irrid[5];
    strcpy(irrid, root["stopirr"].as<const char *>());
#ifdef DEBUG_Extend
      Serial.print(F("Forced Stop Irigation ID "));Serial.println(irrid);
#endif
#ifdef MIXER    
    if (mixer_id != "")  stop_mixer(irrid);
    #ifdef DEBUG_Extend
      Serial.println(F("Forced Stop Mixer "));
    #endif
#endif
      
#ifdef Fertilisation  
    if (fertilizations[0].fertilization_id != "") force_stop_fertilization( 1);
      
    if (fertilizations[1].fertilization_id != "") force_stop_fertilization( 2);
      
    if (fertilizations[2].fertilization_id != "") force_stop_fertilization( 3);
      
    if (fertilizations[3].fertilization_id != "") force_stop_fertilization( 4);
#endif      
    delay(4000); // saceka neko vreme da pumpa ispere cevovod od djubriva 
    stop_irrigation(irrid);    
  }  
jsonBuffer.clear();

  params = "";  
  params.concat("did=" + String(parcel_id));
  params += "&";
#ifdef MIXER  
	if (mixer_id != ""){
    params += "mid=";
		params += mixer_id;
		params += "&";
	}
#endif
		if (fertilizations[0].fertilization_id != ""){
      params += "fid=";
			params += fertilizations[0].fertilization_id;
			params += "&";
    }
		if (fertilizations[1].fertilization_id != ""){
      params += "fid2=";
			params += fertilizations[1].fertilization_id;
			params += "&";
    }
    		if (fertilizations[2].fertilization_id != ""){
      params += "fid3=";
			params += fertilizations[2].fertilization_id;
			params += "&";
    }
    		if (fertilizations[3].fertilization_id != ""){
      params += "fid4=";
			params += fertilizations[3].fertilization_id;
			params += "&";
    }
    		if (fertilizations[4].fertilization_id != ""){
      params += "fid5=";
			params += fertilizations[4].fertilization_id;
			params += "&";
    }



byte i = 0;
	for (i=0; i < NUM_SLOTS; i++)
	{
		if (zonal_irrigations[i].irrigation_id == "")
		{
      break;
		}
	}
if( i>0 ){
  i--;
  params += "iid=";
  params += zonal_irrigations[i].irrigation_id;
}
else if( (i==0) && (zonal_irrigations[0].irrigation_id != "") ){
  params+="iid=";
  params += zonal_irrigations[0].irrigation_id;
}

if(startedNewIrigation == true ){
#ifdef DEBUG  
  Serial.println(params.c_str());
#endif 
	  memset(msg, 0, sizeof(msg));       
    apiCall(server, 80, "/api/post/started.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
    char *jsonResponseee = strstr(msg, "{");
    Serial.println(jsonResponseee);
#endif	
startedNewIrigation = false;				
}

}
//====================================================================================================
//================================ END  OF   L O O P ( )  ===========================================
//====================================================================================================
//per duration
void zonal_irrigation_start(String irrigationid, int duration, JsonArray &zones)
{
#ifdef DEBUG_shifts
 if (digitalRead(19) == 0){
digitalWrite(48,HIGH);	 
return;
 }
 else{
digitalWrite(48,LOW);	 
 }
#endif	
	
  for (byte i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id == "")
    {
	  zonal_irrigations[i].started_time = currentTime(); 																											  
      zonal_irrigations[i].irrigation_id = irrigationid;
#ifdef DEBUG
        Serial.println(F("Palim Zone [per time] :"));
#endif	  
      for (byte zone : zones)
      {
        zonal_irrigations[i].duration = duration * 60L;
        digitalWrite(valves[zone - 1], relay_start);
   		delay(170); 
        zonal_irrigations[i].status[zone - 1] = 1;
#ifdef DEBUG
        Serial.print(F("zona : ")); Serial.println(zone);
#endif
      }

	  digitalWrite(mainpump_pin, relay_start);
#ifdef DEBUG
      String logger = "Started zonal Irrigation [per time] ID " + String(zonal_irrigations[i].irrigation_id);
      Serial.println(logger);
#endif
/*      params = "";
      memset(msg, 0, sizeof(msg));
      params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));
      apiCall(server, 80, "/api/post/irrigation_started.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif
*/ 
      return;
    }
  } //for (int i = 0; i < NUM_SLOTS; i++)
}

//per m3 
void zonal_irrigation_start(String irrigationid, long amount, JsonArray &zones, int max_duration){

  for (byte i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id == "")
    {
	  zonal_irrigations[i].started_time = currentTime(); 
      zonal_irrigations[i].current = 0;                  	  
      zonal_irrigations[i].irrigation_id = irrigationid;
      zonal_irrigations[i].amount = amount; // vrednost u litrima.
      zonal_irrigations[i].duration = max_duration * 60L; // vreme u sekundama  
#ifdef DEBUG_Extend
        Serial.println(F("Palim zone [per m3] :")); 
#endif	  
      for (byte zone : zones) //ukljucuje zadate zone
      {
        delay(150);
        digitalWrite(valves[zone - 1], relay_start);    
        zonal_irrigations[i].status[zone - 1] = 1;
#ifdef DEBUG_Extend
        Serial.print(F("zona : ")); Serial.println(zone);
#endif
      }

	  digitalWrite(mainpump_pin, relay_start);
#ifdef DEBUG_Extend
      Serial.print(F("Started zonal Irigation [per m3] ID:")); Serial.println(zonal_irrigations[i].irrigation_id);
#endif
/*      params = "";
      memset(msg, 0, sizeof(msg));
      params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));
      apiCall(server, 80, "/api/post/irrigation_started.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG_Extend
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif
*/ 
      return;
    }
  }
}


#ifdef MIXER
void mixer_start(String mixerid, String irrigationid, int duration)
{
  mixer_id = mixerid;
  mixer_irrigation_id = irrigationid;
  mixer_duration = duration * 60;
  mixer_status = 1;
  digitalWrite(mixer_pin, relay_start);
  mixer_started_time = currentTime();
#ifdef DEBUG
  Serial.print(F("Palim Mixer ID=")); Serial.println(mixer_id);
#endif
/*
  params = "";
  params.concat("mid=" + String(mixer_id));
  memset(msg, 0, sizeof(msg));
  apiCall(server, 80, "/api/post/mixer_started.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
  char *jsonResponse = strstr(msg, "{");
  Serial.println(jsonResponse);
#endif
*/
delay(3000);
}
#endif

#ifdef Fertilisation												
void fertilization_start(String fertilizationid, String irrigationid, int duration, byte fertnum , float fert_pHtarget, byte pumpPercentFert )
{
    fertilizations[fertnum-1].fertilization_id = fertilizationid;
    fertilizations[fertnum-1].fertilization_irrigation_id = irrigationid;
    
    fertilizations[fertnum-1].fertilization_status = 1;
    fertilizations[fertnum-1].fertilization_started_time = currentTime();
   
//  start_fertilization(fertnum); //ukljucuje relej-pumpu za fertilizaciju. Da li ovo ovde treba ?
    
if ( fertMode == 0 ){

    fertilizations[fertnum-1].fertilization_duration = duration * 60L;    
    valve_start_fertilization(fertnum); //ukljucuje relej-pumpu za fertilizaciju.
    
    if(fertnum == 1){
        pHmeasuring.desiredPh = fert_pHtarget;
#ifdef fertPIDregulation
        fertilizationCheck( fert_pHtarget);// pocni sa merenjem pH i doziranjem djubriva
#endif
    }  
	else{
#ifdef Sensor_EC_PID		
		EC_PID_Control_Checking(ECmeasuring.desired );
#endif
	}
}
// Proporcionalni FERT	
#ifdef FertProportional
if ( fertMode == 1 ){
							
                              // kolicina prihrane u L ! ! ! !! ! ! ! !  !
	fertilizations[fertnum-1].fertilization_duration = duration;
for (byte y = 0; y < number_of_zones; y++){

	if( irrigationid == zonal_irrigations[y].irrigation_id ) {
		if(duration !=0) {         
			fertilizations[fertnum-1].ratioWaterFert = (float)(zonal_irrigations[y].amount) / (float)duration ; // Odnos vode i prihrane.
#ifdef DEBUG_Extend 			
			Serial.print("IRR_Amount "); Serial.print(zonal_irrigations[y].amount); Serial.print(" L, FERT "); Serial.print(fertnum);Serial.print(", Amount ");Serial.print(duration); Serial.print(" L, ratio Water/Fert :"); Serial.println(fertilizations[fertnum-1].ratioWaterFert);
#endif
		}
	break;	
	}		
}
    valve_start_fertilization(fertnum); //ukljucuje  relej-pumpu za fertilizaciju.   
}
#endif
// Fixed FERT, Withought PID regulation just set fix current strenght on driver.

if ( fertMode == 2 ){

    fertilizations[fertnum-1].fertilization_duration =  duration * 60L;  // trajanje u sekundama

	float tmp =  (float)pumpPercentFert * 0.66  +  16.0 ; // Konvertovanje iz procenata 0 - 100%  U  faktor ispune od 17% - 85% koje odgovara strujnom drajveru 4..20mA
	if (pumpPercentFert == 0) tmp = 0 ; // ako korisnik zada 0% znaci da hoce ugasenu pumpu.
	
	if (tmp > maxDutyCycle) {tmp = maxDutyCycle;}
	if (tmp < minDutyCycle) {tmp = 0;}
	
	fertilizations[fertnum-1].pumpPrcntCurrFert  = tmp; 
	
    valve_start_fertilization(fertnum); //ukljucuje  relej-pumpu za fertilizaciju.
	
	byte temCurrentDriver;
	switch(fertnum){
		case 1: temCurrentDriver = CurrDrvCH1; break;
		case 2: temCurrentDriver = CurrDrvCH2; break;
		case 3: temCurrentDriver = CurrDrvCH3; break;
		case 4: temCurrentDriver = CurrDrvCH4; break;
	//  case 5: temCurrentDriver = CurrDrvCH5; break;
	};
	setCurrentDriverPumpFert(fertilizations[fertnum-1].pumpPrcntCurrFert , temCurrentDriver ); // Podesava pumpu, slanjem vrednosti struje, podesava kojim intezitetom ce da radi pumpa, tj doziranje prihrane.
}	

#ifdef DEBUG_Extend  
      String logger = "Start FERT " + String(fertnum) + " fid="  + fertilizationid;
      Serial.println(logger);
#endif
/*
      params = "";
      params.concat("fid=" + String(fertilizationid));
      memset(msg, 0, sizeof(msg));
      apiCall(server, 80, "/api/post/fertilization_started.php", params.c_str(), msg, MSG_BUF);

#ifdef DEBUG_Extend
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif
*/
}
#endif

void stop_irrigation(String irrigationid)
{
  // stop zonal irrigation
  for (byte i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id == irrigationid)
    {
      // turn of all active zones
      for (byte checking_zone = 0; checking_zone < number_of_zones; checking_zone++)
      {
        if (zonal_irrigations[i].status[checking_zone ] == 1)
        {
delay(170);
#ifdef AC_Valves          
     digitalWrite(valves[checking_zone ], relay_stop);
#endif

#ifdef DEBUG_Extend
     Serial.print(F("Gasim zonu "));Serial.println(checking_zone+1);
#endif          
          zonal_irrigations[i].status[checking_zone ] = 0;
        }
        //zonal_irrigations[i].started_time[checking_zone ] = 0L;
      }
      zonal_irrigations[i].irrigation_id = "";
      zonal_irrigations[i].duration = 0;
      zonal_irrigations[i].started_time = 0L;

      // check to turn off pump if there is no active irrrigation
      bool is_some_irrigation_on = false;
      for (byte checking_slot = 0; checking_slot < NUM_SLOTS; checking_slot++)
      {
        if (zonal_irrigations[checking_slot].irrigation_id != "")
        {
          for (byte checking_zone = 0; checking_zone < number_of_zones; checking_zone++)
          {
            if (zonal_irrigations[checking_slot].status[checking_zone ] == 1)
            {
              is_some_irrigation_on = true;
              break;
            }
          }
        }
      }
      if (!is_some_irrigation_on)
      {
        digitalWrite(mainpump_pin, relay_stop);
      }
#ifdef DEBUG_Extend
      String logger = "Stopped irrigation ID " + String(irrigationid);
      Serial.println(logger);
#endif
      params = "";
      params.concat("iid=" + String(irrigationid));
      memset(msg, 0, sizeof(msg));
      apiCall(server, 80, "/api/post/irrigation_stopped.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG_Extend
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif
    }
  }
}

#ifdef MIXER
void valve_mixer_stop() {
   digitalWrite(mixer_pin, relay_stop);
    mixer_status = 0;
    mixer_duration = 0;
    mixer_started_time = 0L;
    String stopped_mixer_id = String(mixer_id);
    mixer_id = "";
    mixer_irrigation_id = "";

#ifdef DEBUG_Extend
    String logger = "Stopped mixer ID " + String(stopped_mixer_id);
    Serial.println(logger);
#endif
}
void stop_mixer(String irrigationid)
{
  if (mixer_irrigation_id == irrigationid)
  {
    valve_mixer_stop();
    params = "";
    params.concat("mid=" + String(mixer_id));
    memset(msg, 0, sizeof(msg));
    apiCall(server, 80, "/api/post/mixer_stopped.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG_Extend
    char *jsonResponse = strstr(msg, "{");
    Serial.println(jsonResponse);
#endif
  }
}
#endif

#ifdef Fertilisation
void stop_fertilization(String irrigationid, byte fertnum)
{
   if (fertilizations[fertnum-1].fertilization_irrigation_id == irrigationid)
   { 

      String tmp = fertilizations[fertnum-1].fertilization_id ; // funkcija ispod resetuje ovaja parametar
      valve_stop_fertilization(fertnum); // Resetuje parametre ferta-a i gasi relej/pumpu.
  
#ifdef DEBUG_Extend
      Serial.print(F("Stop FERT [ ")); Serial.print(fertnum); Serial.print(F(" ] "));  Serial.println(tmp);
#endif
      params = "";
      params.concat("fid=" + tmp);
      memset(msg, 0, sizeof(msg));
      apiCall(server, 80, "/api/post/fertilization_stopped.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif    
    }
}
#endif

#ifdef Fertilisation
void force_stop_fertilization(byte fertnum)
{
      String tmp = fertilizations[fertnum-1].fertilization_id ; // funkcija ispod resetuje ovaja parametar
      valve_stop_fertilization(fertnum); // Resetuje parametre ferta-a i gasi relej/pumpu.
  
#ifdef DEBUG_Extend
      Serial.print(F("Forced Stop FERT { ")); Serial.print(fertnum); Serial.print(F(" } "));  Serial.println(tmp);
#endif
      params = "";
      params.concat("fid=" + tmp);
      memset(msg, 0, sizeof(msg));
      apiCall(server, 80, "/api/post/fertilization_stopped.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif    

}
#endif

//============== P R O C E S I N G ========================================

void checkValves(float deltaVolumePerOneLoop)
{
#ifdef MIXER
	MixerProcesing();
#endif

#ifdef Fertilisation
	FertilisationProcesing();
#endif   


	if (!isperm3)// I R I G A T I O N   per   T I M E =========================================
	{
	  
    IrigationProcesingBYtime();

	}
	else// I R I G A T I O N   per   VOLUME Water and FERT =========================== 
	{
	  IrigationProcesingBYvolumetric(deltaVolumePerOneLoop);
	}
}



///////////////////////////////////////////////////////////////////////////////
void FertilisationProcesing(void){
#ifdef Fertilisation
//======================================= F E R T M O D E  0 ======================================
if ( fertMode == 0){  
//byte fertMode0ECisON = false;	
#ifdef FERT_0
  // fertilization 1 check
  if ( (fertilizations[0].fertilization_status == 1) && ( (currentTime() - fertilizations[0].fertilization_started_time) > fertilizations[0].fertilization_duration))
  {    
#ifdef DEBUG_Extend
    Serial.println(F("GaSim FERT 1")); 
#endif    
valve_stop_fertilization(1); //fertnum = 1;
pinMode(5, OUTPUT);  
digitalWrite(5, LOW);
  }
  
#endif

#ifdef FERT_2 						  
	  if ( (fertilizations[1].fertilization_status == 1) && ( (currentTime() - fertilizations[1].fertilization_started_time) > fertilizations[1].fertilization_duration))
	  { 
	#ifdef DEBUG_Extend
		Serial.println(F("GaSim FERT 2")); 
	#endif 
	   valve_stop_fertilization(2); //fertnum = 2; setuje sve vrednosti na 0, gasi relej koji napaja pumpu 

	  }
#endif

#ifdef FERT_3						  
	  if ( (fertilizations[2].fertilization_status == 1) && ( (currentTime() - fertilizations[2].fertilization_started_time) > fertilizations[2].fertilization_duration))
	  {
	#ifdef DEBUG_Extend
		Serial.println(F("GaSim FERT 3")); 
	#endif
		valve_stop_fertilization(3); 

	  }

#endif

#ifdef FERT_4						  
		  if ( (fertilizations[3].fertilization_status == 1) && ( (currentTime() - fertilizations[3].fertilization_started_time) > fertilizations[3].fertilization_duration))
		  {
		#ifdef DEBUG_Extend
			Serial.println(F("GaSim FERT 4")); 
		#endif
			valve_stop_fertilization(4); 

		  }

#endif


//Serial.print(F("FERTmode=0 procesing")); 


#ifdef Fertilisation
	if(fertilizations[0].fertilization_status == 1){
#ifdef fertPIDregulation
		fertilizationCheck( pHmeasuring.desiredPh);	
#endif

	}
#endif 

#ifdef Sensor_EC_PID
  if (  (fertilizations[1].fertilization_status == 1) || (fertilizations[2].fertilization_status == 1) || (fertilizations[3].fertilization_status == 1)  ) {
	EC_PID_Control_Checking(ECmeasuring.desired );

  }
#endif


}

//======================================= F E R T M O D E  1 ======================================	  

// proprocionalna fertilizacija. Podesava sistem tako da protice odgovarajuce kolicina prihrane po jednom metru kubnom protekle kolicine vode.
#ifdef FertProportional
if ( fertMode == 1){ 
	if (  ! isSomeIrigationON() ){
		if (fertilizations[0].fertilization_status == 1)force_stop_fertilization(1);	
		if (fertilizations[1].fertilization_status == 1)force_stop_fertilization(2);	
		if (fertilizations[2].fertilization_status == 1)force_stop_fertilization(3);
		if (fertilizations[3].fertilization_status == 1)force_stop_fertilization(4);	
	}
	else {
		byte loop = 8; // eksperimantisati ! ! ! Vrti X krugova (merenja, upravljanja), a greska zanemarljiva zavrsi s upravljanjem instant.
		Serial.print(F("Prop. FERT procesing")); 
		while (loop--){
			Serial.print(F(".")); 
			#ifdef FERT_0
			if (fertilizations[0].fertilization_status == 1) { 
				setRatioWaterFert_F1();
			}  
			#endif

			#ifdef FERT_2  
			 if (fertilizations[1].fertilization_status == 1){
					setRatioWaterFert_F2();
				}
			#endif

			#ifdef FERT_3
			 if (fertilizations[2].fertilization_status == 1){
				setRatioWaterFert_F3();
			  }
			#endif

			#ifdef FERT_4
			 if (fertilizations[3].fertilization_status == 1){
				setRatioWaterFert_F4();
			  }
			#endif
			
		}
		Serial.println(F("."));
	}
}
#endif
// Fert MODE 2 where system is controlled ONLY with Current Strength of Driver

if ( fertMode == 2){

#ifdef FERT_0
  if ( (fertilizations[0].fertilization_status == 1) && ( (currentTime() - fertilizations[0].fertilization_started_time) > fertilizations[0].fertilization_duration))
  {    
#ifdef DEBUG_Extend
    //Serial.print("cur time "); Serial.print(currentTime());Serial.print(", fert_1 time "); Serial.print(fertilizations[0].fertilization_started_time);Serial.print(", fert_1 duration "); Serial.println(fertilizations[0].fertilization_duration);
    Serial.println(F("GasiM FerT 1")); 	
#endif    
	stopCurrentDriverPumpFert(CurrDrvCH1);
	valve_stop_fertilization(1);
  }  
  else if (fertilizations[0].fertilization_status == 1) {
		setCurrentDriverPumpFert(fertilizations[0].pumpPrcntCurrFert, CurrDrvCH1);
  }
#endif

#ifdef FERT_2
  // fertilization 2 check
  if ( (fertilizations[1].fertilization_status == 1) && ( (currentTime() - fertilizations[1].fertilization_started_time) > fertilizations[1].fertilization_duration))
  {    
#ifdef DEBUG_Extend
    Serial.println(F("GasiM FerT 2")); 
#endif    
	stopCurrentDriverPumpFert(CurrDrvCH2);
	valve_stop_fertilization(2);
  }
	else if (fertilizations[1].fertilization_status == 1) {
		setCurrentDriverPumpFert(fertilizations[1].pumpPrcntCurrFert, CurrDrvCH2);  

	}
#endif
	
#ifdef FERT_3
  if ( (fertilizations[2].fertilization_status == 1) && ( (currentTime() - fertilizations[2].fertilization_started_time) > fertilizations[2].fertilization_duration))
  {    
#ifdef DEBUG_Extend
    Serial.println(F("GasiM FerT 3")); 
#endif    
	stopCurrentDriverPumpFert(CurrDrvCH3);
	valve_stop_fertilization(3);
  } 
	else if (fertilizations[2].fertilization_status == 1) {
		setCurrentDriverPumpFert(fertilizations[2].pumpPrcntCurrFert, CurrDrvCH3);  

	}  
#endif

#ifdef FERT_4
  // fertilization 4 check
  if ( (fertilizations[3].fertilization_status == 1) && ( (currentTime() - fertilizations[3].fertilization_started_time) > fertilizations[3].fertilization_duration))
  {    
#ifdef DEBUG_Extend
    Serial.println(F("GasiM FerT 4")); 
#endif    
	stopCurrentDriverPumpFert(CurrDrvCH4);
	valve_stop_fertilization(4);
  }
	else if (fertilizations[3].fertilization_status == 1) {
		setCurrentDriverPumpFert(fertilizations[3].pumpPrcntCurrFert, CurrDrvCH4); 

	}  
#endif

}  // if ( fertMode == 2){
	
#endif   // A L L  Fertilisations and ALL mode	
	
}
///////////////////////////////////////////////////////////////////////////////
void IrigationProcesingBYtime(void){
			   
    for (byte Irigation = 0; Irigation < NUM_SLOTS; Irigation++)
    {
		if (zonal_irrigations[Irigation].irrigation_id != "")
		{
unsigned long tmpTime = (currentTime() - zonal_irrigations[Irigation].started_time);
			if (  tmpTime > zonal_irrigations[Irigation].duration  )
			{  
#ifdef DEBUG_Extend
//	Serial.print(F("SLOT [")); Serial.print(Irigation); Serial.print(F("] "));
//	Serial.print(F(" Isteklo Vreme= ")); Serial.print((float)tmpTime/60.0); Serial.print(F(" min"));
	Serial.println(F("Gasim Zone:"));
#endif

				for (byte processing_zone = 0; processing_zone < number_of_zones; processing_zone++)
				{
					if ((zonal_irrigations[Irigation].status[processing_zone] == 1))
					{
						delay(150);       
						digitalWrite(valves[processing_zone], relay_stop);
						zonal_irrigations[Irigation].status[processing_zone] = 0;
					#ifdef DEBUG_Extend
						Serial.print(F("zona :"));Serial.println(processing_zone+1);
					#endif       
					}
				}  
				
				zonal_irrigations[Irigation].duration = 0;   
				zonal_irrigations[Irigation].started_time = 0L;
				
				String temp_irr_id = zonal_irrigations[Irigation].irrigation_id;
		
				zonal_irrigations[Irigation].irrigation_id = "";
				
				if ( ( isSomeIrigationON() == 0) && (isSomeFertilisationON() == 0) )
			    {	

					digitalWrite(mainpump_pin, relay_stop);
				}	
				
			#ifdef DEBUG_Extend
				Serial.print(F("Completed zonal Irigation[by time] ID: ")); Serial.println(temp_irr_id);
			#endif
				params = "";
				params.concat("iid=" + String(temp_irr_id));
				memset(msg, 0, sizeof(msg));
				apiCall(server, 80, "/api/post/irrigation_finished.php", params.c_str(), msg, MSG_BUF);
			#ifdef DEBUG_Extend
				char *jsonResponse = strstr(msg, "{");
				Serial.println(jsonResponse);
			#endif      
				
				  
			}  
		}//if (zonal_irrigations[Irigation].irrigation_id != "")
	}//for (byte Irigation = 0; Irigation < NUM_SLOTS; Irigation++)	

shiftIrigationFertOverlProcesing();	

}
//////////////////////////////////////////////////////////////////////////////
void IrigationProcesingBYvolumetric(float deltaVolumePerOneLoop){
				 
    for (byte i = 0; i < NUM_SLOTS; i++)
    {
     
	 if (zonal_irrigations[i].irrigation_id != "")
      {
		zonal_irrigations[i].current += deltaVolumePerOneLoop; 
#ifdef Irigation_Per_m3 
	Serial.print(F("SLOT [")); Serial.print(i); Serial.print(F("],"));
	Serial.print(F(" Isteklo Vode = ")); Serial.print(zonal_irrigations[i].current); Serial.print(F(" L"));
	//Serial.print(F(" Proteklo Vreme = ")); Serial.print(currentTime() - zonal_irrigations[i].started_time); Serial.println(F(" S"));
#endif			
		if (zonal_irrigations[i].current > zonal_irrigations[i].amount)																														 
            {


#ifdef DEBUG_Extend
	Serial.println(F("Gasim zone: "));
#endif	

				//gasi zone koje su aktivne za SLOT (zadato navodnjavanje)
				for (byte processing_zone = 0; processing_zone < number_of_zones; processing_zone++)
				{
					delay(150);   
					if(zonal_irrigations[i].status[processing_zone ] == 1)
				    {	
					digitalWrite(valves[processing_zone ], relay_stop);	
					zonal_irrigations[i].status[processing_zone ] = 0;					
#ifdef DEBUG_Extend         
	Serial.print(F("zona: "));Serial.println(processing_zone+1); 
#endif 
					}  
				}
			
				// clear slot		   
				String temp_irr_id = zonal_irrigations[i].irrigation_id ;
				zonal_irrigations[i].irrigation_id = "";
                zonal_irrigations[i].amount = 0L;
                zonal_irrigations[i].duration = 0;
				zonal_irrigations[i].current = 0 ;
				initialiseIrrFlowMet();
				
		//proveri da li treba gasiti glavnu pumpu;
				if ( ( isSomeIrigationON() == 0) && (isSomeFertilisationON() == 0) )
			    {		
								
					digitalWrite(mainpump_pin, relay_stop);
			    }
#ifdef DEBUG_Extend
	Serial.print(F("Completed zonal Irigation [by m3] ID: ")); Serial.println(temp_irr_id);
#endif				
                params = "";
                params.concat("iid=" + String(temp_irr_id));
                memset(msg, 0, sizeof(msg));
                apiCall(server, 80, "/api/post/irrigation_finished.php", params.c_str(), msg, MSG_BUF);          
#ifdef DEBUG_Extend
    char *jsonResponse = strstr(msg, "{");
    Serial.println(jsonResponse);
#endif	
			}// Za SLOT [i] istekla zadata kolicina vode, ugasene pripadajuce zone i server obaveste.	
	
	    }
		
// ako je za dati SLOT (zadato navodnjavanje) isteklo max vreme -> gasi navodnjavanje		
		if ( (zonal_irrigations[i].duration != 0) && (  (currentTime() - zonal_irrigations[i].started_time)  >  zonal_irrigations[i].duration))
		{
#ifdef Irigation_Per_m3 
	Serial.print(F("SLOT [")); Serial.print(i); Serial.print(F("],"));
	Serial.print(F("Isteklo Zastitno Vreme = ")); Serial.print(zonal_irrigations[i].duration ); Serial.print(F(" S")); 	Serial.println(F(", Gasim zone: "));   
	
#endif

					valve_stop_fertilization(1);
					valve_stop_fertilization(2);
					valve_stop_fertilization(3);
					valve_stop_fertilization(4);
					delay(3000); // zadrska od 3s da bi se mogao isprati sistem od prihrane.

				for (byte processing_zone = 0; processing_zone < number_of_zones; processing_zone++)
				{
					delay(150);   
					if(zonal_irrigations[i].status[processing_zone ] == 1)
				    {	
					digitalWrite(valves[processing_zone ], relay_stop);	
					zonal_irrigations[i].status[processing_zone ] = 0;					
#ifdef DEBUG_Extend      
	Serial.print(F("zona: "));Serial.println(processing_zone+1); 
#endif 
					}  
				}
#ifdef DEBUG
    String logger = "Completed zonal irrigation[maxdur] ID " + String(zonal_irrigations[i].irrigation_id);
    Serial.println(logger);
#endif			// clear slot		   
				String temp_irr_id = zonal_irrigations[i].irrigation_id ;
				zonal_irrigations[i].irrigation_id = "";
                zonal_irrigations[i].amount = 0L;
                zonal_irrigations[i].duration = 0;
				zonal_irrigations[i].current = 0 ;
				initialiseIrrFlowMet();
				
		//proveri da li treba gasiti glavnu pumpu ?!
				if ( ! isSomeIrigationON() )
			    {											   
					digitalWrite(mainpump_pin, relay_stop);
			    }
        
				params = "";
                params.concat("iid=" + String(temp_irr_id));
                params.concat("&maxduration=1");
                memset(msg, 0, sizeof(msg));
                apiCall(server, 80, "/api/post/irrigation_stopped.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG_Extend
            char *jsonResponse = strstr(msg, "{");
            Serial.println(jsonResponse);
#endif		
			
		}// Isteklo zastitno vreme.			
	} // SLOTS for(....)	
		
shiftIrigationFertOverlProcesing();	
}
/////////////////////////////////////////////////////////
#ifdef MIXER
void MixerProcesing(void){
  if (mixer_status == 1 && ( (currentTime() - mixer_started_time) > mixer_duration ))
  {
    digitalWrite(mixer_pin, relay_stop);
    mixer_status = 0;
    mixer_duration = 0;
    mixer_started_time = 0L;
    String completed_mixer_id = String(mixer_id);
    mixer_id = "";
    mixer_irrigation_id = "";

#ifdef DEBUG_Extend
    String logger = "Completed mixer ID " + String(completed_mixer_id);
    Serial.println(logger);
#endif
    params = "";
    params.concat("mid=" + String(completed_mixer_id));
    memset(msg, 0, sizeof(msg));
    apiCall(server, 80, "/api/post/mixer_finished.php", params.c_str(), msg, MSG_BUF);
    
#ifdef DEBUG_Extend
    char *jsonResponse = strstr(msg, "{");
    Serial.println(jsonResponse);
#endif
  }
}
#endif
////////////////////////////////////////////////////////////////////////////////////////////////////////
time_t currentTime()
{
  struct ts t;
  tmElements_t te;
  DS3231_get(&t);

  te.Second = t.sec;
  te.Minute = t.min;
  te.Hour = t.hour;
  te.Day = t.mday;
  te.Month = t.mon;
  te.Year = t.year - 1970;

  time_t timeInSeconds = makeTime(te);
  return timeInSeconds;
}
//==================================================================================================================================
byte apiCall(const char *server, int port, const char *path, const char *parameters, char *result, int resultlength)
{
  byte completed = 0;
  delay(200);
  completed = inet.httpPOST(server, port, path, parameters, result, resultlength);

  if (completed == 0)
  {
#ifdef DEBUG
    Serial.println(F("Dettach GPRS connection..."));
#endif
    inet.dettachGPRS();
    delay(200);
#ifdef DEBUG
    Serial.println(F("Restarting SIM module..."));
#endif
    digitalWrite(sim_restart_pin, LOW);
    delay(500);
    digitalWrite(sim_restart_pin, HIGH);
    delay(500);
#ifdef DEBUG
    Serial.print(F("GSM = "));
#endif
    // Start configuration of shield with baudrate.
    // For http uses is recommended to use 4800 or slower.
    if (gsm.begin(9600))
    {
#ifdef DEBUG
      Serial.println(F("READY"));
#endif
      gsm_started = true;
    }
    else
    {
#ifdef DEBUG
      Serial.println(F("IDLE"));
#endif
    }
    if (gsm_started)
    {
#ifdef DEBUG
      Serial.print(F("GPRS = "));
#endif
      gsm.SimpleWriteln("AT+CGATT=1");
      delay(3000);

      if (inet.attachGPRS(String(carrierdata.apnname).c_str(), String(carrierdata.apnuser).c_str(), String(carrierdata.apnpass).c_str()))
      {
  
#ifdef DEBUG
        Serial.println(F("ATTACHED"));         
        gsm.SimpleWriteln("AT+CIFSR");
        delay(5000);
      //Read until serial buffer is empty.s
        gsm.WhileSimpleRead();
#endif
      }
      else
      {
#ifdef DEBUG
        Serial.println(F("ERROR"));
#endif
      }
    
    }
    delay(1000);
  }

  
  if (completed == 0)
  {
	if (isSomeIrigationON() == 0)
    {
#ifdef DEBUG
      Serial.println(F("Reseting Arduino..."));
#endif
      digitalWrite(arduino_restart_pin, LOW); // Reset Arduino
//      delay(5000);
    }
    else
    {
#ifdef DEBUG
      Serial.println(F("Not reseting Arduino beacuse of active irrigation."));
#endif
    }
  }
  
  return completed;
}

#ifdef FLOW 
// VRACA trenutni protok [L/min], i proteklu kolicinu vode u[L] u jednom loop-u za glavnog cevovoda (FlowSensorCH5). I takodje radi obracun protoka za ostale protokomere																																										 
struct flowmeter_struct checkFlowMeter()
{
  struct flowmeter_struct flow;
  flow.currentflow   = getCurrentFlow(FlowSensorCH5)*60; //getCurrentFlow vraca protok L/s, sa *60 dobija se u L/min protok
  flow.currentvolume = getCurrentVOL( FlowSensorCH5); // kolicina vode u litrima za jedan loop 
	
#ifdef FertProportional	
	getCurrentVOL( FlowSensorCH1);
	getCurrentVOL( FlowSensorCH2);
	getCurrentVOL( FlowSensorCH3);
  getCurrentVOL( FlowSensorCH4);
  getCurrentVOL( FlowSensorCH5);
#endif
#ifdef DEBUG_FLOW
    FlowDebug( FlowSensorCH1);
    FlowDebug( FlowSensorCH2);
	FlowDebug( FlowSensorCH3);
    FlowDebug( FlowSensorCH4);
    FlowDebug( FlowSensorCH5);						  
#endif  
return flow;

}
#endif


#ifdef INTERRUPTFLOW																																								 
struct flowmeter_struct checkFlowMeter()
{

  Meter.tick(flowmeter_period);
  struct flowmeter_struct flow;
  flow.currentflow = Meter.getCurrentFlowrate() / 10;
  flow.currentvolume = Meter.getCurrentVolume();	
return flow;
}
#endif
			
void checkLastSeen() {
  if (last_seen > 0 && !(currentTime() - last_seen < 20 * 60L)) {
#ifdef DEBUG_Extend      
     Serial.println(F("Stop ALL"));
#endif 						 
    stopAll();
  }
}

void stopAll() {

#ifdef Fertilisation
  // stop fertilization
  valve_stop_fertilization(1);
  valve_stop_fertilization(2);
  valve_stop_fertilization(3);
  valve_stop_fertilization(4);
  delay(2000);// saceka da se zaustavi isticanje prihrane u sistem.......
#endif

#ifdef MIXER    
  valve_mixer_stop();//stop mixer
#endif
  
  // stop zonal irrigations
  for (byte checking_slot = 0; checking_slot < NUM_SLOTS; checking_slot++) {
    for (byte checking_zone=0; checking_zone < number_of_zones; checking_zone++) {
      
#ifdef AC_Valves            
    digitalWrite(valves[checking_zone ], relay_stop);
#endif            

#ifdef DEBUG_Extend      
     Serial.print(F("Gasim ZONU: "));Serial.println(checking_zone+1);
#endif      
      zonal_irrigations[checking_slot].status[checking_zone] = 0;
    // zonal_irrigations[checking_slot].started_time[checking_zone] = 0L;
      zonal_irrigations[checking_slot].current= 0;
    }
    zonal_irrigations[checking_slot].started_time = 0L;
    zonal_irrigations[checking_slot].irrigation_id = "";
    zonal_irrigations[checking_slot].amount = 0L;
    zonal_irrigations[checking_slot].duration = 0;
    zonal_irrigations[checking_slot].duration = 0;

  }
  digitalWrite(mainpump_pin, relay_stop);

  initialiseIrrFlowMet();
}



#ifdef FLOW_Toggling
bool checkFlow() {
     byte flowreadcount = 0;  
     byte firstflowstate = digitalRead(flow_pin_Toggling);
//     byte firstflowstate = ( PINE & 0x80);
     while (flowreadcount < 30) {
        byte flowstate = digitalRead(flow_pin_Toggling); // Za FERT
//        byte flowstate = ( PINE & 0x80);               // Za SENS
        if (flowstate != firstflowstate) {
          return true;
        }
        delay(50);
        flowreadcount++;
     }  
     return false;
  }
#endif

#ifdef TIME_F_Measure
void funcTimeMeasure(String funcBlock){
    Serial.print("Exe Time of "); Serial.print(funcBlock);Serial.print(" = "); Serial.print((float)(millis()-currentTimeMilles),0); Serial.println(" mS");
    currentTimeMilles = millis();
}
#endif

byte isSomeIrigationON(void){
		
	    byte slot_i  = 0;
	    byte count_active_slots  = 0;	
        for (slot_i = 0; slot_i < NUM_SLOTS; slot_i++){
		    if (zonal_irrigations[slot_i].irrigation_id != "")	count_active_slots++;		
        }
	    if (count_active_slots == 0)return 0;
		else return 1;		

}

byte isSomeFertilisationON(void){
   byte fert_i  = 0;	
        for (fert_i = 0; fert_i < 4; fert_i++){
		    if (fertilizations[fert_i].fertilization_status == 1)	{
				return 1;	
			}				
        }
	    return 0;		
}

void shiftIrigationFertOverlProcesing(void){				
	if ( ! isSomeIrigationON() )
		{
		if(isSomeFertilisationON()){
			irigationTimeShiftGap = currentTime();

			if (irigationTimeShiftGap_old == 0){
				irigationTimeShiftGap_old = irigationTimeShiftGap;
			}	
			if(  (irigationTimeShiftGap - irigationTimeShiftGap_old) > maxIrigtionTimeShiftGap){
				valve_stop_fertilization(1);
				valve_stop_fertilization(2);
				valve_stop_fertilization(3);
				valve_stop_fertilization(4);
				irigationTimeShiftGap_old = 0;
				irigationTimeShiftGap     = 0;
				digitalWrite(mainpump_pin, relay_stop);
			}
		}
		else{
			digitalWrite(mainpump_pin, relay_stop);
		}
	}
	else{
		irigationTimeShiftGap     = 0;
		irigationTimeShiftGap_old = 0;
	}
}