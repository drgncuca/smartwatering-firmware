#include <String.h>

#include <TimeLib.h>
#include <SHT1x.h>

#ifndef Telemetry_h                                                   
  #define Telemetry_h 
  
//================================  B O A R D   HW    V E R S I O N   =================================//
#define BOARD_FERT_R002

//================================   P O D E S I T I    P R E    I S P O R U K E  ====================//

#define secondsWaitingSMS 1  // Podesiti na 15 - 20,  4s * 15 = 60s cekanje na SMS
#define GSM_SMS_Replay       // Ako korisik hoce povratni SMS -> Otkomentarisati

//#define SKIP_Operater_Detecting// zakomentarisati ovo !!!! ( samo za debagovanje)
//#define MainPumpIssueSkip      // zakomentarisati ovo !!!! ( samo za debagovanje)


//---------------------------------------------------------------------------------------------
#define BUFF_RTC_TIME 1 // Za debagovanje stavi 256
#define MSG_BUF  860 // bilo 830

#define BUFF_SMS 50
#define BUFF_MOB_NUM 20

//============================= V A L V E S   &&    R E L A Y S  ==========================//
#define AC_Valves

#define NUM_SLOTS 4 // each SLOT consume 150B of memory

#define MainPump

//============================= D E B U G   D I R E C T I V E ============================//

//#define DEBUG_SENTEK
//#define DEBUG_SENTEK_Write_to_Excel
//#define DEBUG_Server_Comun
#define DEBUG                //210B
#define DEBUG_Extend
//#define DEBUG_Telemetry
//#define DEBUG_Fertilisation
//#define DEBUG_Fertilisation_PID
//#define DEBUG_FertProportional
//#define DEBUG_FLOW
//#define DEBUG_FLOW_INTERUPT
//#define Irigation_Per_m3
//#define DEBUG_EC
//#define DEBUG_EC_PID
//#define DEBUG_Temperature
//#define DEBUG_ON          //od GSM modula
//#define DEBUG_Operater
//#define TIME_F_Measure
//#define DEBUG_PT1000
//#define Operater_Setings_Skip
//#define DEBUG_Power_Saving
//#define DEBUG_PWM_Generator
//#define DEBUG_FertMode_2
//#define DEBUG_shifts



//=================================   M O D U L E S  ========================================//
#define MIXER             //120B

//#define Water_Level      // Low i High value
//#define Water_Level_2_3  // two midle value 30%  i  60%

//#define SENTEC_Include   //400B
//#define SENTEK_Salinity   
//#define SENTEK_Include_Extend_Sens  //170B

#define GSM_Carrier_Change 

//#define DC_Power_SENS    // Battery
//#define AC_Power_FERT	   // 230Vac

#define FLOW
//#define INTERRUPTFLOW   //200B
//#define FLOW_Toggling
//#define FLOW_100L_imp

#define Sensor_EC      //488B
#define Sensor_EC_PID  //82B   

#define Sensor_pH
        
#define Fertilisation       //138B
#define fertPIDregulation   //FERT_Mode_0
//#define FertProportional    //FERT_Mode_1				
//#define fert_Iout_Fix20mA 
#define FERT_0
#define FERT_2
#define FERT_3
#define FERT_4
//#define FERT_5

//#define Sensor_10HS  //Dekagon Senzor: Electrical Conductivity (u obliku Viljuske  )

//#define SHT1sensor   //I2C senzor temp i vlage

//#define NetSens   //Soil Moisture
																		 
//=================================== P O W E R ==========================================//
#define AC_power_IN 7	

void setupWatchDogTimer(void);
void enterSleep(void);
void PowerStandBy(int StandByINseconds);
void PowerWakeUP(void);	
int getPower(void) ;
				  
//==============================   Temperature PT1000  =================================//
#define RREF      4300.0
#define RNOMINAL  1000.0

// Pins for SPI comm with the AD9833 IC
#define DATA  50  ///< SPI Data pin number
#define CLK   52  ///< SPI Clock pin number
#define FSYNC 53  ///< SPI Load pin number (FSYNC in AD9833 usage)

void setupTemperaturePT1000(void);
void testTemperaturePT1000(void);
float getTemepraturePT1000(void);

//=================================  RS485  =========================================//
void RS485setup(void);
void RS485readEnableCH(void);
void RS485writeStringLN(String string);
void RS485readCharArray(char *CharArray);
String RS485readString(void);

//================================   SENTEK  =======================================//

#ifdef SENTEC_Include
    void setSANTEKsensors(void);
	float getSENTEKtemperature(byte debpth);
	float getSENTEKmoisture(byte debpth);
	float getSENTEKsalinity(byte debpth);
	void setSANTEKsensors(void);	
	float hexStringToFloat(char temp[], byte sensor);	
	void reqSENTEKpresetHoldReg_Temperature(void);
	void reqSENTEKpresetHoldReg_Moisture(void);
	void reqSENTEKpresetHoldReg_Salinity(void);	
	float filtering(float s,byte );
	byte LRC (char * char_temp);
	void getEcho(void);
	void getIlegalCommand(void);
	void measureStatus(void);
	void ivenCounter(void);
	void slaveID(void);
	void sensorDepth(void);
	void sensorSampledMask(void);
	void scanSelectionMask(void);
	void detectedSelectionMask(void);
	void numberOfSensors(void);
	byte compareLRC(char * char_temp);
	void resetString(void);
	void writeToExcel( unsigned int row, unsigned int column, unsigned int value);
#endif	

//--------------------------------------------------------------------------------------
#define CLS          "\033[2J" 

#define relayFertPump_1 35
#define relayFertPump_2 34
#define relayFertPump_3 36
#define relayFertPump_4 37
//#define relayFertPump_3 59

#define relay_stop   LOW
#define relay_start  HIGH

#define ph_sensor_pin A2  // pH senzor analog IN
//-------------------------Carier Operater Changing----------------------------------------------//
#define EERPOM_mem_loc_SW  64
#define EEPROM_mem_loc_Operater 62
#define EERPOM_mem_loc_CarrierData 0
#define EERPOM_mem_loc_sms_Confugured 70

#define Telenor 1
#define Vip 2
#define Telekom 3
#define BHmob 4
#define MTEL 5
#define Haloo 6
#define BHeronet 7		
#define mtelCG 8	  

struct carrier
{
  char apnname[20];
  char apnuser[20];
  char apnpass[20];
  char operater[20];
} ;

extern carrier carrierdata; 

 struct struct_operater_mtelCG
{
  char apnname[20]="mtelinternet";
  char apnuser[20]="internet";
  char apnpass[20]="068";  
} ;
extern struct_operater_mtelCG operaterMTEL_CG;

 struct struct_operater_vip
{
  char apnname[20]="internet";
  char apnuser[20]="internet";
  char apnpass[20]="internet";  
} ;
extern struct_operater_vip operaterVIP;

 struct struct_operater_MTS
{
  char apnname[20]="gprswap"; 
  char apnuser[20]="mts";
  char apnpass[20]="064";  
} ;
extern struct_operater_MTS operaterMTS;

 struct struct_operater_Telenor
{
  char apnname[20]="internet";
  char apnuser[20]="telenor";
  char apnpass[20]="gprs";  
} ;
extern struct_operater_Telenor operaterTelenor;

 struct struct_operater_BHmob
{
//char apnname[20]="active.bhmobile.ba"; //pripaid
  char apnname[20]="smart.bhmobile.ba";  //postpaid
  char apnuser[20]="U";
  char apnpass[20]="P";  
} ;
extern struct_operater_BHmob operaterBHmob;

struct struct_operater_MTEL
{
  char apnname[20]="mtelfrend";
  char apnuser[20]="";
  char apnpass[20]="";  
} ;
extern struct_operater_MTEL operaterMTEL;

 struct struct_operater_BHeronet
{
  char apnname[20]="gprs.eronet.ba";
  char apnuser[20]="";
  char apnpass[20]="";  
} ;
extern struct_operater_BHeronet operaterBHeronet;

 struct struct_operater_Haloo
{
  char apnname[20]="web.haloo.ba";
  char apnuser[20]="";
  char apnpass[20]="";  
} ;
extern struct_operater_Haloo operaterHaloo;

void doesOperaterChanging(void);
bool startsWith(const char *pre, const char *str);
void initOperators(void);
void deleteALLsms(void);
float getRSSI(void);
int convertRSSIToDBm(int rssi);
int getSignalStrength(void);

//=============================  Temperature and Humidity SHTx sensor ==========================//
#define dataPin  20
#define clockPin 21

extern float temperature;
extern float humidity;
extern SHT1x SHT1x_sensor;
//================================   Electrical  Conductivity  =================================//
#define NMBRsampling  200  // broj semplova SINusnog signala EC drajvera
#define NMBRaveraging   3  //usrednjavanje min i max vrednosti amplitude



struct EC_struct {
  float measured; 
  float ecK;// Y = k*X +M za korekciju pri merenju EC-a
  float ecM; 
#ifdef Sensor_EC_PID 
  float desired;
  float ecKp;
  float ecKi;
  float ecKd;
//  float CH5_Curr_Strenght;  
  float CH4_Curr_Strenght;   //za EC
  float CH3_Curr_Strenght;   //za EC
  float CH2_Curr_Strenght;   //za EC
  float CH1_Curr_Strenght;  //za Fert po pH
#endif
};
extern EC_struct ECmeasuring;
// extern byte FERTchanel2 ;
// extern byte FERTchanel3 ;

void  setConductivityDriver(void);
void  StopCurrentDriver( byte CurrDrvChanPin);
float getELconductance(void);
void  EC_PID_Control_Checking( float  ECdesired);
//================================   FLOW  =================================================//
#define FlowSensorCH1 0
#define FlowSensorCH2 1
#define FlowSensorCH3 2
#define FlowSensorCH4 3
#define FlowSensorCH5 4

// Konstante racunate na bazi protokomera od 33imp/S. Ovde jos figurise i ulazni stepen pretvaranja impulsa u napon + ADC konverzija (5V * digit / 1024)

#define flowCH5_ConstantImpuls33PerSecond 117.0 
#define flowCH4_ConstantImpuls33PerSecond 117.0 
#define flowCH3_ConstantImpuls33PerSecond 117.0 
#define flowCH2_ConstantImpuls33PerSecond 117.0 
#define flowCH1_ConstantImpuls33PerSecond 117.0 

struct irrigation_struct_FlowMetering
{
	float irrigationFlowVolume;	
	unsigned long ElapsedTime;
	float flowMeterCalibKons;
#ifdef DEBUG_FLOW 	
	float flow ;
#endif
//	double flowmeter_capacity;
//	bool flowMeterConfigChanged;
};

extern  irrigation_struct_FlowMetering  irrigationFlowMetering[];
void FlowDebug(byte flowmeter_sensor);
void resetFlowMetVolume(byte flowmeter_sensor);											   
float getCurrentVOL(byte flowmeter_sensor);	
float getCurrentFlow(byte flowmeter_sensor);									   
void initialiseIrrFlowMet(void);	
		
#ifdef FLOW_100L_imp
void flowImplseTick100L_Procesing(void);
unsigned int getFlowImplseTick100L(void);
float getFlow100L(void);
void resetFlowImpulseTick100L(void);
float getCurrentVOLtick100L(void);
#endif
								

//===============================   Current Driver  ============================================//
/* Io = D * 5V / 180R.   
 *  25mA -> 4.5V -> D = 90%
 *  20mA ->      -> D = 86%
 *  4mA  -> 0.7V -> D = 17% 
 */

  #define minDutyCycle 17 //  -> 4mA
  #define maxDutyCycle 82 //  -> 20mA

// razmotriti da se PWM smanji sa 50 na 30Hz-a
  #define PWMperiodPulse  20 // 20mS period tj frequencija PWMa: pulse + pause = 20mS -> 50Hz, inicijalno D = 25%.

#ifdef fert_Iout_Fix20mA
	#define PWMpulseTogle 200
#endif

#ifndef fert_Iout_Fix20mA
	#define PWMpulseTogle 33 // 20mS perioda PWM-a: pulse + pause = 20mS -> 50Hz, inicijalno D = 25%.
#endif


  #define CurrDrvCH1 5  //CH1 pH - fertilizaciju
  #define CurrDrvCH2 3  //CH2 EC Tank 1
  #define CurrDrvCH3 6  //CH3 EC Tank 2
  #define CurrDrvCH4 9  //CH4 EC Tank 3
  #define CurrDrvCH5 8  //CH5 - nije u upotrebi
  
void CurrentDriverSet(byte dutyCycle, byte CurrDrvChanel);

//===============================    Fertilisation    ============================================//

//  float fertFlowMeterCallCoeff;
  
struct fert_struct
{
  byte fertilization_pin;
  byte fertilization_status; // 0 = off, 1 = on
  String fertilization_id;
  String fertilization_irrigation_id;
  unsigned int      fertilization_duration;
  unsigned long int fertilization_started_time; 
  float ratioWaterFert; // Odnos zapremina ciste vode i prihrane.  
  byte pumpPrcntCurrFert; // vrednost u procentima koliko ce da bude podesen strujni drajver FERT pumpe	
          
};
extern fert_struct fertilizations[];

struct pH_struct {
  int rawPh;
  float pH;
  float desiredPh;
  float ferphK;   // Y = k*X +M za korekciju pri merenju pH
  float ferphM;	
  float ferKp;
  float ferKi;
  float ferKd;
};
extern pH_struct pHmeasuring;

extern byte valves_fert[] ;

void valve_start_fertilization(byte fertnum);
void valve_stop_fertilization(byte fertnum);
void fertilizationDriverSetup(void);
float getPh(void);
//void fertilization_start(String fertilizationid, String irrigationid, int duration, int pinnumber, int fertnum, float fert_pHtarget, float fertCurrCoeff);

void fertilizationCheck(float pHtarget);
void initialiseFertilisationParameters(void);
float gerDutyCycle_fertPh_PID(float pHtarget);
void        setRatioWaterFert_F1(void);
void        setRatioWaterFert_F2(void);
void        setRatioWaterFert_F3(void);
void        setRatioWaterFert_F4(void);
void setCurrentDriverPumpFert(byte pumpPercentFert,  byte CurrDrvChanPin);
void stopCurrentDriverPumpFert(                     byte CurrDrvChanPin);
//---------------------------------------------------------------------------------

				
#endif