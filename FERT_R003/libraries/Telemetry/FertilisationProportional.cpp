#include <Arduino.h>
#include "Telemetry.h"

int ratioWaterFertDutyCycle_OLD_F1 = 45; // eksperimentisati
int ratioWaterFertDutyCycle_F1     = 45;	

int ratioWaterFertDutyCycle_OLD_F2 = 45; // eksperimentisati
int ratioWaterFertDutyCycle_F2     = 45;	

int ratioWaterFertDutyCycle_OLD_F3 = 45; // eksperimentisati
int ratioWaterFertDutyCycle_F3     = 45;	

int ratioWaterFertDutyCycle_OLD_F4 = 45; // eksperimentisati
int ratioWaterFertDutyCycle_F4     = 45;	

#define ratioFLOW_error 0.05

// FLOW sensor parametri & istorija (dinamika ) FLOW greske.
 float errorFLOW0_F1 = 0;
 float errorFLOW1_F1 = 0;
 float errorFLOW2_F1 = 0;
 
 float errorFLOW0_F2 = 0;
 float errorFLOW1_F2 = 0;
 float errorFLOW2_F2 = 0;
 
 float errorFLOW0_F3 = 0;
 float errorFLOW1_F3 = 0;
 float errorFLOW2_F3 = 0;
 
 float errorFLOW0_F4 = 0;
 float errorFLOW1_F4 = 0;
 float errorFLOW2_F4 = 0;

// za FERT Mode 1
#ifdef FertProportional
void setRatioWaterFert_F1(void){
byte Kp = 5;
byte Ki = 4;
byte Kd = 2;

float FlowSensorCH5temp;
float FlowSensorTemp;																					             
float ratioWaterFertTemp =0;

// kalulacija odnosa protoka sa glavnog protokomera i FERT[x] protokomera
 FlowSensorCH5temp = getCurrentFlow(FlowSensorCH5);
 FlowSensorTemp    = getCurrentFlow(FlowSensorCH1);
 
 //ratioWaterFertTemp = 1 + FlowSensorCH5temp / ( FlowSensorTemp + 0.02);// protok na glavnom meracu je zbir protoka vode i prihrane ! (A+B)/B = 1+ A/B . LSB = 0.02 - zastita od delenja s 0
 ratioWaterFertTemp =  FlowSensorCH5temp / ( FlowSensorTemp + 0.01);// 0.01 - zastita od delenja s 0
         errorFLOW0_F1 =  ratioWaterFertTemp - fertilizations[FlowSensorCH1].ratioWaterFert ; // racunanje kolika je greska zadatog odnosa protoka vode i fert-a i dostignutog odnosa protoka... 


if(  ( errorFLOW0_F1 < -ratioFLOW_error )  || (errorFLOW0_F1 > ratioFLOW_error)    ){ // ako greska NIJE zanemarljiva -> procesuiraj

	ratioWaterFertDutyCycle_OLD_F1 = ratioWaterFertDutyCycle_F1;																						
	ratioWaterFertDutyCycle_F1 = (ratioWaterFertDutyCycle_F1 + Kp * ( errorFLOW0_F1 - errorFLOW1_F1) + Ki * errorFLOW0_F1 + Kd * ( errorFLOW0_F1 - 2*errorFLOW1_F1 + errorFLOW2_F1));	//	
	errorFLOW2_F1 = errorFLOW1_F1;
	errorFLOW1_F1 = errorFLOW0_F1;

	if ( (ratioWaterFertDutyCycle_F1  - ratioWaterFertDutyCycle_OLD_F1) > 9 ){ //soft start, da pumpa ne dobije slucajno preveliku vrednos upravljackog signala.
		ratioWaterFertDutyCycle_F1 = ratioWaterFertDutyCycle_OLD_F1 + 9;
	}

	if ( ( ratioWaterFertDutyCycle_OLD_F1 - ratioWaterFertDutyCycle_F1 ) > 9 ){
		ratioWaterFertDutyCycle_F1 = ratioWaterFertDutyCycle_OLD_F1 - 9;
	}

	if (ratioWaterFertDutyCycle_F1 > maxDutyCycle) { ratioWaterFertDutyCycle_F1 = maxDutyCycle;} // Ograniciti struju Io = 20mA  max
	if (ratioWaterFertDutyCycle_F1 < minDutyCycle) { ratioWaterFertDutyCycle_F1 = minDutyCycle;} // 4mA, podesiti pumpu za doziranje tako da ne bude premala vrednost, jer bi predugo trajalo djubrenje.
	}
	else{
#ifdef DEBUG_FertProportional
	Serial.println(F("LOOP Locked "));
#endif	
	}
 CurrentDriverSet(ratioWaterFertDutyCycle_F1 , CurrDrvCH1); 
	
	
	
#ifdef DEBUG_FertProportional
  Serial.println(F("------------------------------------"));  
  Serial.print(F("FERT_1"));  Serial.print(F(" Flow "));Serial.print(FlowSensorTemp); Serial.print(F(" L/s"));Serial.print(F(", Water Flow"));Serial.print(FlowSensorCH5temp);Serial.println(F(" L/s")); 
  Serial.print(F("Water/FERT Ratio = ")); Serial.print(fertilizations[FlowSensorCH1].ratioWaterFert); Serial.print(F(", Measured= ")); Serial.print(ratioWaterFertTemp);  Serial.print(F(", Error= ")); Serial.println(errorFLOW0_F1);   
   Serial.print(F("Current CH1")); Serial.print(F("= ")); Serial.print(0.24 * ratioWaterFertDutyCycle_F1 + 0.13 );Serial.print(F("mA  ("));   Serial.print(ratioWaterFertDutyCycle_F1);Serial.println(F("%)"));
  
#endif 	
}

void setRatioWaterFert_F2(void){
byte Kp = 5;
byte Ki = 4;
byte Kd = 2;

float FlowSensorCH5temp;
float FlowSensorTemp;																					             
float ratioWaterFertTemp =0;


// kalulacija odnosa protoka sa glavnog protokomera i FERT[x] protokomera
 FlowSensorCH5temp = getCurrentFlow(FlowSensorCH5);
 FlowSensorTemp    = getCurrentFlow(FlowSensorCH2);
 
 //ratioWaterFertTemp = 1 + FlowSensorCH5temp / ( FlowSensorTemp + 0.02);// protok na glavnom meracu je zbir protoka vode i prihrane ! (A+B)/B = 1+ A/B . LSB = 0.02 - zastita od delenja s 0
 ratioWaterFertTemp =  FlowSensorCH5temp / ( FlowSensorTemp + 0.01);// 0.01 - zastita od delenja s 0
         errorFLOW0_F2 =  ratioWaterFertTemp - fertilizations[FlowSensorCH2].ratioWaterFert ; // racunanje kolika je greska zadatog odnosa protoka vode i fert-a i dostignutog odnosa protoka... 


if(  ( errorFLOW0_F2 < -ratioFLOW_error )  || (errorFLOW0_F2 > ratioFLOW_error)    ){ // ako greska NIJE zanemarljiva -> procesuiraj

	ratioWaterFertDutyCycle_OLD_F2 = ratioWaterFertDutyCycle_F2;																						
	ratioWaterFertDutyCycle_F2 = (ratioWaterFertDutyCycle_F2 + Kp * ( errorFLOW0_F2 - errorFLOW1_F2) + Ki * errorFLOW0_F2 + Kd * ( errorFLOW0_F2 - 2*errorFLOW1_F2 + errorFLOW2_F2));	//	
	errorFLOW2_F2 = errorFLOW1_F2;
	errorFLOW1_F2 = errorFLOW0_F2;

	if ( (ratioWaterFertDutyCycle_F2  - ratioWaterFertDutyCycle_OLD_F2) > 9 ){ //soft start, da pumpa ne dobije slucajno preveliku vrednos upravljackog signala.
		ratioWaterFertDutyCycle_F2 = ratioWaterFertDutyCycle_OLD_F2 + 9;
	}

	if ( ( ratioWaterFertDutyCycle_OLD_F2 - ratioWaterFertDutyCycle_F2 ) > 9 ){
		ratioWaterFertDutyCycle_F2 = ratioWaterFertDutyCycle_OLD_F2 - 9;
	}

	if (ratioWaterFertDutyCycle_F2 > maxDutyCycle) { ratioWaterFertDutyCycle_F2 = maxDutyCycle;} // Ograniciti struju Io = 20mA  max
	if (ratioWaterFertDutyCycle_F2 < minDutyCycle) { ratioWaterFertDutyCycle_F2 = minDutyCycle;} // 4mA, podesiti pumpu za doziranje tako da ne bude premala vrednost, jer bi predugo trajalo djubrenje.
	}
	else{
#ifdef DEBUG_FertProportional
	Serial.println(F("LOOP Locked "));
#endif	
	}
 CurrentDriverSet(ratioWaterFertDutyCycle_F2 , CurrDrvCH2); 
	

#ifdef DEBUG_FertProportional
  Serial.println(F("------------------------------------"));  
  Serial.print(F("FERT_2"));  Serial.print(F(" Flow "));Serial.print(FlowSensorTemp); Serial.print(F(" L/s"));Serial.print(F(", Water Flow"));Serial.print(FlowSensorCH5temp);Serial.println(F(" L/s")); 
  Serial.print(F("Water/FERT Ratio = ")); Serial.print(fertilizations[FlowSensorCH2].ratioWaterFert); Serial.print(F(", Measured= ")); Serial.print(ratioWaterFertTemp);  Serial.print(F(", Error= ")); Serial.println(errorFLOW0_F2);   
   Serial.print(F("Current CH2")); Serial.print(F("= ")); Serial.print(0.24 * ratioWaterFertDutyCycle_F2 + 0.13 );Serial.print(F("mA  ("));   Serial.print(ratioWaterFertDutyCycle_F2);Serial.println(F("%)"));
  
#endif 	
}

void setRatioWaterFert_F3(void){
byte Kp = 5;
byte Ki = 4;
byte Kd = 2;

float FlowSensorCH5temp;
float FlowSensorTemp;																					             
float ratioWaterFertTemp =0;


// kalulacija odnosa protoka sa glavnog protokomera i FERT[x] protokomera
 FlowSensorCH5temp = getCurrentFlow(FlowSensorCH5);
 FlowSensorTemp    = getCurrentFlow(FlowSensorCH3);
 
 //ratioWaterFertTemp = 1 + FlowSensorCH5temp / ( FlowSensorTemp + 0.02);// protok na glavnom meracu je zbir protoka vode i prihrane ! (A+B)/B = 1+ A/B . LSB = 0.02 - zastita od delenja s 0
 ratioWaterFertTemp =  FlowSensorCH5temp / ( FlowSensorTemp + 0.01);// 0.01 - zastita od delenja s 0
         errorFLOW0_F3 =  ratioWaterFertTemp - fertilizations[FlowSensorCH3].ratioWaterFert ; // racunanje kolika je greska zadatog odnosa protoka vode i fert-a i dostignutog odnosa protoka... 


if(  ( errorFLOW0_F3 < -ratioFLOW_error )  || (errorFLOW0_F3 > ratioFLOW_error)    ){ // ako greska NIJE zanemarljiva -> procesuiraj

	ratioWaterFertDutyCycle_OLD_F3 = ratioWaterFertDutyCycle_F3;																						
	ratioWaterFertDutyCycle_F3 = (ratioWaterFertDutyCycle_F3 + Kp * ( errorFLOW0_F3 - errorFLOW1_F3) + Ki * errorFLOW0_F3 + Kd * ( errorFLOW0_F3 - 2*errorFLOW1_F3 + errorFLOW2_F3));	//	
	errorFLOW2_F3 = errorFLOW1_F3;
	errorFLOW1_F3 = errorFLOW0_F3;

	if ( (ratioWaterFertDutyCycle_F3  - ratioWaterFertDutyCycle_OLD_F3) > 9 ){ //soft start, da pumpa ne dobije slucajno preveliku vrednos upravljackog signala.
		ratioWaterFertDutyCycle_F3 = ratioWaterFertDutyCycle_OLD_F3 + 9;
	}

	if ( ( ratioWaterFertDutyCycle_OLD_F3 - ratioWaterFertDutyCycle_F3 ) > 9 ){
		ratioWaterFertDutyCycle_F3 = ratioWaterFertDutyCycle_OLD_F3 - 9;
	}

	if (ratioWaterFertDutyCycle_F3 > maxDutyCycle) { ratioWaterFertDutyCycle_F3 = maxDutyCycle;} // Ograniciti struju Io = 20mA  max
	if (ratioWaterFertDutyCycle_F3 < minDutyCycle) { ratioWaterFertDutyCycle_F3 = minDutyCycle;} // 4mA, podesiti pumpu za doziranje tako da ne bude premala vrednost, jer bi predugo trajalo djubrenje.
	}
	else{
#ifdef DEBUG_FertProportional
	Serial.println(F("LOOP Locked "));
#endif	
	}
 CurrentDriverSet(ratioWaterFertDutyCycle_F3 , CurrDrvCH3); 
	
	
#ifdef DEBUG_FertProportional
  Serial.println(F("------------------------------------"));  
  Serial.print(F("FERT_3"));  Serial.print(F(" Flow "));Serial.print(FlowSensorTemp); Serial.print(F(" L/s"));Serial.print(F(", Water Flow"));Serial.print(FlowSensorCH5temp);Serial.println(F(" L/s")); 
  Serial.print(F("Water/FERT Ratio = ")); Serial.print(fertilizations[FlowSensorCH3].ratioWaterFert); Serial.print(F(", Measured= ")); Serial.print(ratioWaterFertTemp);  Serial.print(F(", Error= ")); Serial.println(errorFLOW0_F3);   
   Serial.print(F("Current CH3")); Serial.print(F("= ")); Serial.print(0.24 * ratioWaterFertDutyCycle_F3 + 0.13 );Serial.print(F("mA  ("));   Serial.print(ratioWaterFertDutyCycle_F3);Serial.println(F("%)"));
  
#endif 	
}

void setRatioWaterFert_F4(void){
byte Kp = 5;
byte Ki = 4;
byte Kd = 2;

float FlowSensorCH5temp;
float FlowSensorTemp;																					             
float ratioWaterFertTemp =0;



// kalulacija odnosa protoka sa glavnog protokomera i FERT[x] protokomera
 FlowSensorCH5temp = getCurrentFlow(FlowSensorCH5);
 FlowSensorTemp    = getCurrentFlow(FlowSensorCH4);
 
 //ratioWaterFertTemp = 1 + FlowSensorCH5temp / ( FlowSensorTemp + 0.02);// protok na glavnom meracu je zbir protoka vode i prihrane ! (A+B)/B = 1+ A/B . LSB = 0.02 - zastita od delenja s 0
 ratioWaterFertTemp =  FlowSensorCH5temp / ( FlowSensorTemp + 0.01);// 0.01 - zastita od delenja s 0
         errorFLOW0_F4 =  ratioWaterFertTemp - fertilizations[FlowSensorCH4].ratioWaterFert ; // racunanje kolika je greska zadatog odnosa protoka vode i fert-a i dostignutog odnosa protoka... 


if(  ( errorFLOW0_F4 < -ratioFLOW_error )  || (errorFLOW0_F4 > ratioFLOW_error)    ){ // ako greska NIJE zanemarljiva -> procesuiraj

	ratioWaterFertDutyCycle_OLD_F4 = ratioWaterFertDutyCycle_F4;																						
	ratioWaterFertDutyCycle_F4 = (ratioWaterFertDutyCycle_F4 + Kp * ( errorFLOW0_F4 - errorFLOW1_F4) + Ki * errorFLOW0_F4 + Kd * ( errorFLOW0_F4 - 2*errorFLOW1_F4 + errorFLOW2_F4));	//	
	errorFLOW2_F4 = errorFLOW1_F4;
	errorFLOW1_F4 = errorFLOW0_F4;

	if ( (ratioWaterFertDutyCycle_F4  - ratioWaterFertDutyCycle_OLD_F4) > 9 ){ //soft start, da pumpa ne dobije slucajno preveliku vrednos upravljackog signala.
		ratioWaterFertDutyCycle_F4 = ratioWaterFertDutyCycle_OLD_F4 + 9;
	}

	if ( ( ratioWaterFertDutyCycle_OLD_F4 - ratioWaterFertDutyCycle_F4 ) > 9 ){
		ratioWaterFertDutyCycle_F4 = ratioWaterFertDutyCycle_OLD_F4 - 9;
	}

	if (ratioWaterFertDutyCycle_F4 > maxDutyCycle) { ratioWaterFertDutyCycle_F4 = maxDutyCycle;} // Ograniciti struju Io = 20mA  max
	if (ratioWaterFertDutyCycle_F4 < minDutyCycle) { ratioWaterFertDutyCycle_F4 = minDutyCycle;} // 4mA, podesiti pumpu za doziranje tako da ne bude premala vrednost, jer bi predugo trajalo djubrenje.
	}
	else{
#ifdef DEBUG_FertProportional
	Serial.println(F("LOOP Locked "));
#endif	
	}
 CurrentDriverSet(ratioWaterFertDutyCycle_F4 , CurrDrvCH4); 
	
	
#ifdef DEBUG_FertProportional
  Serial.println(F("------------------------------------"));  
  Serial.print(F("FERT_4"));  Serial.print(F(" Flow "));Serial.print(FlowSensorTemp); Serial.print(F(" L/s"));Serial.print(F(", Water Flow"));Serial.print(FlowSensorCH5temp);Serial.println(F(" L/s")); 
  Serial.print(F("Water/FERT Ratio = ")); Serial.print(fertilizations[FlowSensorCH4].ratioWaterFert); Serial.print(F(", Measured= ")); Serial.print(ratioWaterFertTemp);  Serial.print(F(", Error= ")); Serial.println(errorFLOW0_F4);   
   Serial.print(F("Current CH4")); Serial.print(F("= ")); Serial.print(0.24 * ratioWaterFertDutyCycle_F4 + 0.13 );Serial.print(F("mA  ("));   Serial.print(ratioWaterFertDutyCycle_F4);Serial.println(F("%)"));
  
#endif 	
}


#endif

// FertMode = 2
// ZA fiksno podesavanje rada FERT pumpe
// convPercentToDutyCycle: [17% - 84%] = [0% - 100%] = [4mA - 20mA]

void setCurrentDriverPumpFert(byte convPercentToDutyCycle, byte CurrDrvChanPin){	

CurrentDriverSet(convPercentToDutyCycle, CurrDrvChanPin);	
#ifdef DEBUG_FertMode_2 
Serial.print(F("Set Current Fert: ")); Serial.print(CurrDrvChanPin); Serial.print(F("   ")); Serial.print((0.24 * (float)convPercentToDutyCycle + 0.13) );Serial.print(F("mA , ")); Serial.print(convPercentToDutyCycle); Serial.println(F(" %")); 
#endif 		
}
void stopCurrentDriverPumpFert(byte CurrDrvChanPin){
	
StopCurrentDriver(CurrDrvChanPin);
	
}
