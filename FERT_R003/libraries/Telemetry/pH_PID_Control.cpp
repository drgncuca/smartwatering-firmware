#include <Arduino.h>
#include "Telemetry.h"


#ifdef fertPIDregulation

#define dutyKoeficient 5  // definise koliki je duty PWMa u funkciji razlike zadatog pH i izmerenog.
#define dutyCycle_for_ZeroErrorPh 22 //

#define pH_error 0.18
int pH_PIDloopTimeDelay = 100; 

byte dutyCycle = 17;


extern pH_struct pHmeasuring; 
	 // pH sensor parametri & istorija (dinamika ) pH greske.
		 float errorPh0 = 0; // greska pH vrednosti u trenutoj iteraciji (LOOP-u)
		 float errorPh1 = 0; // greska pH vrednosti u prethodnoj iteraciji (LOOP-u)
		 float errorPh2 = 0;// greska pH vrednosti pre dve iteracije (LOOP-u) .
  extern float PID_dutyCycle=40; //[%]
  extern float PID_dutyCycle_OLD = 40;

float gerDutyCycle_fertPh_PID(float errorPh0){

float Ki = pHmeasuring.ferKi;
float Kd = pHmeasuring.ferKd;
float Kp = pHmeasuring.ferKp;  

	if(  ( - pH_error < errorPh0)  && (errorPh0<pH_error)    ){ //kad je greska zanemarljiva, ne babraj nista.
#ifdef DEBUG_Fertilisation_PID	
		 Serial.println(F("pH PID LOCKED"));
#endif
		return PID_dutyCycle;		
	}
	
	PID_dutyCycle_OLD = PID_dutyCycle;																						
	PID_dutyCycle = (PID_dutyCycle + Kp * ( errorPh0 - errorPh1) + Ki * errorPh0 + Kd * ( errorPh0 - 2*errorPh1 + errorPh2));	//	
	errorPh2 = errorPh1;
	errorPh1 = errorPh0;

	if ( (PID_dutyCycle  - PID_dutyCycle_OLD) > 8 ){ //soft start, da pumpa ne dobije slucajno preveliku vrednos upravljackog signala.
		PID_dutyCycle = PID_dutyCycle_OLD + 8;
	}

	if ( ( PID_dutyCycle_OLD - PID_dutyCycle ) > 8 ){
		PID_dutyCycle = PID_dutyCycle_OLD - 8;
	}

	if (PID_dutyCycle > maxDutyCycle) { PID_dutyCycle = maxDutyCycle;} //ograniciti struju Io = 25mA  max
	if (PID_dutyCycle < minDutyCycle) { PID_dutyCycle = minDutyCycle;} // 4mA, podesiti pumpu za doziranje tako da ne bude premala vrednost, jer bi predugo trajalo djubrenje.
#ifdef DEBUG_Fertilisation_PID	
		 Serial.print(F("pH PID_dutyCycle "));Serial.print(PID_dutyCycle);Serial.print(F(", pH error "));Serial.println(errorPh0);
#endif
	return PID_dutyCycle;
}

//----------------------------------------------------------------------------------------
						//    zeljeni pH
void fertilizationCheck( float pHtarget){ 

float errorPh;

byte loop =1;
while(loop--){

pHmeasuring.pH = getPh(); 

errorPh =  pHmeasuring.pH - pHtarget; // errorPh je u opsegu od -1 do 14.

#ifndef  fertPIDregulation
		 if (errorPh < -1.4  ){   //ako je izmereni pH veci od zadatog za vise od 1.4 ( 10%) odrzi drajver na min upravljanja, 4mA     
		  pinMode(CurrDrvCH1, OUTPUT);
		  digitalWrite(CurrDrvCH1, LOW);	//iskljuci drajver Fertn. i stavi drajver u Hi-Z i
												//izadji iz funkcije
		  dutyCycle = 17; //drzi drajver na 4mA
		  return; // ako je dosegnut nivo pHmeasuring.pH koji je zadat zaustavlja se drajver pumpe i izlazi iz fukncije u nekom narednom ciklusu kad i ako se promeni pHmeasuring.pH nastavice nadjubravanjem.
		}
#endif


	// Samo se za jedan strujni drajver racuna dutyCycle (jedno je pH merenje). 
	    
#ifdef  fertPIDregulation //Ako je ukljucena PID regulacija, izracunava novi faktor ispune za strujni drajver 
	if( ! ((-pH_error < errorPh)  && (errorPh < pH_error))    ){
		dutyCycle = gerDutyCycle_fertPh_PID( errorPh );  
	}

#else //Ako nije ukljucen  PID regulacija																											// 5 
	dutyCycle =  dutyKoeficient * errorPh + dutyCycle_for_ZeroErrorPh ; //value in percente.	
if (dutyCycle > maxDutyCycle) dutyCycle = maxDutyCycle;  //ograniciti struju Io = 20mA  max
if (dutyCycle < minDutyCycle) dutyCycle = minDutyCycle;    // podesiti pumpu za doziranje tako da ne bude premala vrednost, jer bi predugo trajalo djubrenje.
#endif	
	
CurrentDriverSet( dutyCycle, CurrDrvCH1) ;

	if( ! ((-pH_error < errorPh)  && (errorPh < pH_error))    ){	
		delay( pH_PIDloopTimeDelay );
	}
	else{
	pH_PIDloopTimeDelay = 100 ;	
	}

}

#ifdef DEBUG_Fertilisation  
  Serial.println(F("------------------------------------"));  
  Serial.print(F("pH Measured = ")); Serial.print(pHmeasuring.pH); Serial.print(F(", pH Desire  = ")); Serial.print(pHtarget);  Serial.print(F(", Error = ")); Serial.println(errorPh);  
//  Serial.print("Kp = ");Serial.print(pHmeasuring.ferKp);Serial.print(F(" Ki = "));Serial.print(pHmeasuring.ferKi);Serial.print(" Kd = ");Serial.println(pHmeasuring.ferKd);   
  if(  ((-pH_error < errorPh)  && (errorPh < pH_error))    )  Serial.println("pH PID Loop LOCKED");
  Serial.print(F("Current CH1 = ")); Serial.print(0.24 * dutyCycle +0.13 );Serial.print("mA, DutyCycle ");   Serial.print(dutyCycle);Serial.println("%"); 
#endif 
}

#endif