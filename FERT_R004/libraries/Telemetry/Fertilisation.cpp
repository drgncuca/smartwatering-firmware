#include <Arduino.h>
#include "Telemetry.h"

extern pH_struct pHmeasuring;

void valve_stop_fertilization(byte fertnum){
  	
StopCurrentDriver( fertnum);
	  
     digitalWrite(fertilizations[fertnum-1].fertilization_pin, relay_stop); 
				  fertilizations[fertnum-1].fertilization_status = 0;
				  fertilizations[fertnum-1].fertilization_duration = 0;
				  fertilizations[fertnum-1].fertilization_started_time = 0L;  
				  fertilizations[fertnum-1].fertilization_id = "";
				  fertilizations[fertnum-1].fertilization_irrigation_id = ""; 
				  fertilizations[fertnum-1].ratioWaterFert    = 0;
				  fertilizations[fertnum-1].pumpPrcntCurrFert = 0;	  

#ifdef Sensor_EC_PID
	 if (fertnum == 2){
		ECmeasuring.CH2_Curr_Strenght = 0;
	
	 }
	 if (fertnum == 3){
		ECmeasuring.CH3_Curr_Strenght = 0;

	 }
	 if (fertnum == 4){
		ECmeasuring.CH4_Curr_Strenght = 0;

	 }
	/*if (fertnum == 5){
		ECmeasuring.CH5_Curr_Strenght = 0;
	
	 }*/
#endif 
}

#ifdef Fertilisation

void valve_start_fertilization(byte fertnum){  
    pinMode(fertilizations[fertnum-1].fertilization_pin, OUTPUT);   
	digitalWrite(fertilizations[fertnum-1].fertilization_pin, HIGH  );
}


void initialiseFertilisationParameters(void){

fertilizations[0].fertilization_pin =30 ;
fertilizations[1].fertilization_pin =41 ;
fertilizations[2].fertilization_pin =40 ;
fertilizations[3].fertilization_pin =39 ;	
	
	for (byte i=0;  i<4 ; i++){
	  
	  fertilizations[i].fertilization_status = 0; // 0 = OFF, 1 = ON
	  fertilizations[i].fertilization_id="";
	  fertilizations[i].fertilization_irrigation_id="";
	  fertilizations[i].fertilization_duration = 0;
	  fertilizations[i].fertilization_started_time = 0;	 	  	 

	  pinMode(fertilizations[i].fertilization_pin, OUTPUT); 
	  digitalWrite(fertilizations[i].fertilization_pin, LOW); 	  

    fertilizations[i].ratioWaterFert    = 0;
	fertilizations[i].pumpPrcntCurrFert	        = 0;											
	}

	
	pHmeasuring.rawPh     = 400; // neka RAW vrednost pH=7
	pHmeasuring.desiredPh = 5.5;//
	pHmeasuring.ferphK    = 8.81;  // Y = k*X + M
	pHmeasuring.ferphM    =-9.87;
	pHmeasuring.ferKp = 4.0;
	pHmeasuring.ferKd = 2.5;
	pHmeasuring.ferKi = 1.5;
	
}


#endif
