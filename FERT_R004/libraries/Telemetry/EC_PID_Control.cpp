#include <Arduino.h>
#include "Telemetry.h"

#ifdef Sensor_EC_PID

#define EC_error 0.09
#define maxCurrent 21  //21mA
#define minCurrent 5   //5mA
//#define ECoffsetCorectiveValue 0.07  //bilo 0.1, Korektivna vrednost EC-a koja se dodata na zadatu vrednost od strane korisnika. Vrednost EC-a na sondi i nakraju napojne cevni nije isti. Verovatno trebaneko vreme da se EC ustabili u sistemu

// EC sensor parametri & istorija (dinamika ) EC greske.
 float errorEC0 = 0;
 float errorEC1 = 0;
 float errorEC2 = 0;
 float EC_PID_current     = 11; //11mA
 float EC_PID_current_OLD = 11;
 
 extern EC_struct ECmeasuring;
 
void EC_PID_Control_Checking(float  ECdesired){	

float Ki = ECmeasuring.ecKi;
float Kd = ECmeasuring.ecKd;
float Kp = ECmeasuring.ecKp;

	errorEC0 = ECdesired - ECmeasuring.measured;  
//Ako je greska veca od neke zadate minimalne dozvoljene vrednosto, procesuiraj PID regulaciju.
	if( ! ((-EC_error < errorEC0)  && (errorEC0 < EC_error))    ){ 

		EC_PID_current_OLD = EC_PID_current;																						
		EC_PID_current = (EC_PID_current + Kp * ( errorEC0 - errorEC1) + Ki * errorEC0 + Kd * ( errorEC0 - 2*errorEC1 + errorEC2));	//	
		errorEC2 = errorEC1;
		errorEC1 = errorEC0;

		if ( (EC_PID_current  - EC_PID_current_OLD) > 4 ){ //soft start, da pumpa ne dobije slucajno preveliku vrednos upravljackog signala.
			EC_PID_current = EC_PID_current_OLD + 4;
		}

		if ( ( EC_PID_current_OLD - EC_PID_current ) > 4 ){
			EC_PID_current = EC_PID_current_OLD - 4;
		}

		if (EC_PID_current > maxCurrent) { EC_PID_current = maxCurrent;} //ograniciti struju Io = 20mA  max
		if (EC_PID_current < minCurrent) { EC_PID_current = minCurrent;} // 4mA, podesiti pumpu za doziranje tako da ne bude premala vrednost, jer bi predugo trajalo djubrenje.

	}

	//CH2
	if (fertilizations[1].fertilization_status == 1) CurrentDriverSet(EC_PID_current * ECmeasuring.CH2_Curr_Strenght, CurrDrvCH2); //  upravljacki strujni drajver za elekto ventil ( FERT 2)
	//CH3
	if (fertilizations[2].fertilization_status == 1) CurrentDriverSet(EC_PID_current * ECmeasuring.CH3_Curr_Strenght, CurrDrvCH3); // ( FERT 3)
	//CH4
	if (fertilizations[3].fertilization_status == 1) CurrentDriverSet(EC_PID_current * ECmeasuring.CH4_Curr_Strenght, CurrDrvCH4); // ( FERT 4)
	//CH5
   //if(fertilizations[3].fertilization_status == 1) CurrentDriverSet(EC_PID_current * ECmeasuring.CH5_Curr_Strenght, CurrDrvCH5); // ( FERT 5)
	
	ECmeasuring.measured = getELconductance();

#ifdef DEBUG_EC_PID

if (fertilizations[0].fertilization_status == 0) {
Serial.println(F("------------------------------------"));
}
Serial.print(F("EC = ")); Serial.print(ECmeasuring.measured); Serial.print(F(", EC Set = ")); Serial.print(ECmeasuring.desired);Serial.print(F(", EC Error = ")); Serial.println( ECmeasuring.desired - ECmeasuring.measured );
if (((-EC_error < errorEC0)  && (errorEC0 < EC_error)))  {Serial.println("EC PID Loop LOCKED");}
Serial.print(F("Current CH2 = "));  if (fertilizations[1].fertilization_status == 1)		{	Serial.print(ECmeasuring.CH2_Curr_Strenght * (EC_PID_current) );Serial.println(F("mA"));	}   else {	Serial.println(F("OFF")); }
Serial.print(F("Current CH3 = "));  if (fertilizations[2].fertilization_status == 1)		{	Serial.print(ECmeasuring.CH3_Curr_Strenght * (EC_PID_current) );Serial.println(F("mA")); 	}	else {	Serial.println(F("OFF")); }
Serial.print(F("Current CH4 = "));  if (fertilizations[3].fertilization_status == 1)		{	Serial.print(ECmeasuring.CH4_Curr_Strenght * (EC_PID_current) );Serial.println(F("mA"));	}	else {	Serial.println(F("OFF")); }

#endif	
}


#endif