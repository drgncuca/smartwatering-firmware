#include <Arduino.h>
#include "Telemetry.h"

//struct irrigation_struct_FlowMetering  irrigationFlowMetering[5];

#ifdef FLOW_100L_imp

byte  flowImplseTick100L = 0;
byte  flowImplseTick100L_old = 0;
float deltaTime = 0.0; 
unsigned long   oldTime = 0L; 
extern byte   loopsCounter     = 0;


void flowImplseTick100L_Procesing(void)
{
	
	flowImplseTick100L = flowImplseTick100L + 1; 
	deltaTime = (float)(millis() - oldTime) / 1000.0 ; // u 10 miliSekundama
	oldTime   = millis();
}


float getCurrentVOLtick100L(void){
byte temp = (flowImplseTick100L - flowImplseTick100L_old);	
if (  temp  > 0  ){	
		flowImplseTick100L_old = flowImplseTick100L;		
		return 100.0 * temp;		
}
else{
	return 0.01;
}

}
/*
unsigned int getFlowImplseTick100L(void)
{
	return flowImplseTick100L;
}
*/

// protok L/min za protokomere od impuls/100L
float getFlow100L(void){
	//Serial.print(F("Delta Time "));Serial.println(deltaTime);
	if (deltaTime !=0.0 ) return 6000.0/deltaTime; // 60*100 = 60s*100L	
	else return 0.0;
}

void resetFlowImpulseTick100L(void){
	flowImplseTick100L = 0;
	deltaTime = 0.0; 
	oldTime = 0L; 	
}
#endif

void resetFlowMetVolume(byte flowmeter_sensor){

		irrigationFlowMetering[flowmeter_sensor].irrigationFlowVolume = 0.00;
			
}

void initialiseIrrFlowMet(void){
	int i = 0;
	while(i < 5)// 5  kanala za merenje protoka
	{
		irrigationFlowMetering[i].irrigationFlowVolume = 0.00;
		irrigationFlowMetering[i].ElapsedTime=0.00;
	#ifdef DEBUG_FLOW 	
		irrigationFlowMetering[i].flow = 0.00;
	#endif
		irrigationFlowMetering[i].flowMeterCalibKons =1.00;
		i++;		
	}			
}
void resetIrrFlowMet(void){
	int i = 0;
	while(i < 5)// 5  kanala za merenje protoka
	{
		irrigationFlowMetering[i].irrigationFlowVolume = 0.00;
		irrigationFlowMetering[i].ElapsedTime=0.00;
		i++;		
	}			
}						   
//-----------------------------------------------------------------------
// vraca proteklu kolicinu vode u litrima [L], SAMO  u jednom loop-u !
float getCurrentVOL(byte flowmeter_sensor){

unsigned long currentTime = millis();

unsigned long samplElapsedTime = currentTime  - irrigationFlowMetering[flowmeter_sensor].ElapsedTime; // proteklo vreme od zadnjeg semplovanja

if (samplElapsedTime < 0) {samplElapsedTime = 0;} //kad miles() funkcija dodje do kraja i pocne ispoceta da broji, prethodna jednacina bi imala ogromanu negativnu vrednost		
irrigationFlowMetering[flowmeter_sensor].ElapsedTime = currentTime;

//integralenje protoka po vremenu: Protok * Vreme = ZAPREMINA.	
float  deltaVolume = getCurrentFlow(flowmeter_sensor)  *  ( (float)(samplElapsedTime) ) /1000.0; //   zapremina = protok * vreme,  millis() vraca u mS vreme.

#ifdef DEBUG_FLOW 	
	irrigationFlowMetering[flowmeter_sensor].flow = getCurrentFlow(flowmeter_sensor);
#endif

//Kad ce resetovati ova promenljiva ? ? ?  
irrigationFlowMetering[flowmeter_sensor].irrigationFlowVolume = irrigationFlowMetering[flowmeter_sensor].irrigationFlowVolume + deltaVolume;

return deltaVolume;	
}
// vraca ukupnu proteklu kolicinu vode [L]
float getTotalVolume(byte flowmeter_sensor){

return irrigationFlowMetering[flowmeter_sensor].irrigationFlowVolume;


}
//-------------------------------------------------------	
								// od 0 do  4
float getCurrentFlow(byte flowmeter_sensor){  //vraca vrednost protoka [L/S]

int flowIDchanel ;
float tempFlow=0;
 
switch (flowmeter_sensor){
	 case 0:flowIDchanel = A3; break;
	 case 1:flowIDchanel = A1; break;
	 case 2:flowIDchanel = A0; break;
	 case 3:flowIDchanel = A4; break;
	 case 4:flowIDchanel = A5; break;	 
	 default : break;
}
  
  int i = 0;
  int sum = 0;
  float flowSensValue = 0;
  
  //usrednjavanje signala sa ADC-a
  while (i < 4)
  {
    sum = sum + analogRead(flowIDchanel);
    delay(2);
    i++;
  }
  flowSensValue = (float)sum * 0.25;//averaging
  
  int tmp_FlowConstImpulsPerSecond = 117;
  switch (flowmeter_sensor) {
	 case 0 : tmp_FlowConstImpulsPerSecond = flowCH1_ConstantImpuls33PerSecond;break; 
	 case 1 : tmp_FlowConstImpulsPerSecond = flowCH2_ConstantImpuls33PerSecond;break; 
	 case 2 : tmp_FlowConstImpulsPerSecond = flowCH3_ConstantImpuls33PerSecond;break; 
	 case 3 : tmp_FlowConstImpulsPerSecond = flowCH4_ConstantImpuls33PerSecond;break; 
	 case 4 : tmp_FlowConstImpulsPerSecond = flowCH5_ConstantImpuls33PerSecond;break; 
	 default: break; 
  }
  
  // vraca vrednost Litara/Sekundi, za sistem od 33imp/L 
  tempFlow = irrigationFlowMetering[flowmeter_sensor].flowMeterCalibKons * flowSensValue / tmp_FlowConstImpulsPerSecond;  
  if (tempFlow<0.006) {tempFlow = 0;} //  pojavljuje se gresa/shum pri nekoj matematici.
  if (tempFlow > 8) {tempFlow = 0;} //  Ako je protok veci od 8L/S (480L/min) -> ogromna vrednost, nemoguce da je toliki.																														 
  
  return tempFlow;
 
}
 //-------------------------------------------------------------------------------------
#ifdef DEBUG_FLOW    //ChanelS = [0..4]
void FlowDebug(byte flowmeter_sensor){ // funkcija za debagovanje 			 
	  Serial.println(F("------------------------------------"));
	  Serial.print(F("FLOW CH-"));Serial.print(flowmeter_sensor+1); 
//		Serial.print(F(", dV: "));           Serial.print(getCurrentVOL( flowmeter_sensor));
		Serial.print(F(", FLOW: ")); Serial.print(irrigationFlowMetering[flowmeter_sensor].flow,3);Serial.print(F("L/s"));	 	
		Serial.print(F(", V: "));    Serial.print(irrigationFlowMetering[flowmeter_sensor].irrigationFlowVolume); Serial.print(F("L,Time: ")); Serial.print(((irrigationFlowMetering[flowmeter_sensor].ElapsedTime)/1000.0) );Serial.println(F("S."));
	  
}
#endif


