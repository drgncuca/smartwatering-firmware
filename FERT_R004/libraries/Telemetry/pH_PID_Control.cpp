#include <Arduino.h>
#include "Telemetry.h"


#ifdef fertPIDregulation

#define pH_Error 0.2
#define maxCurrent 21  //21mA
#define minCurrent 5   //5mA
 
extern pH_struct pHmeasuring;
//extern float fertCurrentDriverCoeff ;

extern pH_struct pHmeasuring; 
// pH sensor parametri & istorija (dinamika ) pH greske.
float errorPh0 = 0; // greska pH vrednosti u trenutoj iteraciji (LOOP-u)
float errorPh1 = 0; // greska pH vrednosti u prethodnoj iteraciji (LOOP-u)
float errorPh2 = 0;// greska pH vrednosti pre dve iteracije (LOOP-u) .
float errorPh = 0;
byte PID_Current     =12; //12mA
byte PID_Current_OLD = 12;

//poboljsati merenje pH da stabilnije meri, isfiltrirati vrednosti.....
float getPh()
{
  byte i = 0;
  unsigned int sum = 0;
  unsigned int tmp = 0;

  while (i < 10)
  {
	tmp = analogRead(ph_sensor_pin);   
    sum = sum + tmp;
    delay(20);
    i++;
//	Serial.print("pH =");Serial.println(tmp);
  }
 	//RAW pH
	pHmeasuring.rawPh =  sum / 10;
	//Serial.print("RAW pH =");Serial.println(pHmeasuring.rawPh);						
  return  (pHmeasuring.rawPh) *  pHmeasuring.ferphK   / 204.6  + pHmeasuring.ferphM;	  //(ADDC * 5/1023) = 1/204.6 konverzija  iz digitalne vrednosti u realnu vrednost napona.
}

float gerCurrent_fertPh_PID(float errorPh0){

float Ki = pHmeasuring.ferKi;
float Kd = pHmeasuring.ferKd;
float Kp = pHmeasuring.ferKp;  

	if(  ( - pH_Error < errorPh0)  && (errorPh0<pH_Error)    ){ //kad je greska zanemarljiva, ne babraj nista.
#ifdef DEBUG_Fertilisation_PID	
		 Serial.println(F("pH PID LOCKED"));
#endif
		return PID_Current;		
	}
	
	PID_Current_OLD = PID_Current;																						
	PID_Current = (PID_Current + Kp * ( errorPh0 - errorPh1) + Ki * errorPh0 + Kd * ( errorPh0 - 2*errorPh1 + errorPh2));	//	
	errorPh2 = errorPh1;
	errorPh1 = errorPh0;

	if ( (PID_Current  - PID_Current_OLD) > 4 ){ //soft start, da pumpa ne dobije slucajno preveliku vrednos upravljackog signala.
		PID_Current = PID_Current_OLD + 4;
	}

	if ( ( PID_Current_OLD - PID_Current ) > 4 ){
		PID_Current = PID_Current_OLD - 4;
	}

	if (PID_Current > maxCurrent) { PID_Current = maxCurrent;} //ograniciti struju Io = 20mA  max
	if (PID_Current < minCurrent) { PID_Current = minCurrent;} // 5mA, podesiti pumpu za doziranje tako da ne bude premala vrednost, jer bi predugo trajalo djubrenje.

	return PID_Current;
}

//----------------------------------------------------------------------------------------
						//    zeljeni pH
void fertilizationCheck( float pHtarget){ 

byte current;

pHmeasuring.pH = getPh(); 

errorPh =  pHmeasuring.pH - pHtarget; // errorPh je u opsegu od 1 do 14.
	
	if( ! ((-pH_Error < errorPh)  && (errorPh < pH_Error))    ){
		current = gerCurrent_fertPh_PID( errorPh );  

	}

CurrentDriverSet( current, CurrDrvCH1) ;

#ifdef DEBUG_Fertilisation_PID	
Serial.println(F("------------------------------------"));  
Serial.print(F("pH = ")); Serial.print(pHmeasuring.pH); Serial.print(F(", pH Set = ")); Serial.print(pHtarget);  Serial.print(F(",pH Error = ")); Serial.println(errorPh);  
Serial.print(F("Current CH1 = "));  if (fertilizations[0].fertilization_status == 1){	Serial.print(PID_Current);Serial.println(F("mA"));	} else {	Serial.println(F("OFF")); }
if(  ((-pH_Error < errorPh)  && (errorPh < pH_Error))    ){Serial.println("pH PID Loop LOCKED");}
Serial.println(); 
#endif

}

byte pH_PID_Locked(void){
if(  ((-pH_Error < errorPh)  && (errorPh < pH_Error))    ){return 1;}
else {return 0;}	
	
	
}

#endif