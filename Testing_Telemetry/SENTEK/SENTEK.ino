#include <Arduino.h>
#include "Telemetry.h"

void setup() {
   RS485setup(); 
  Serial.begin(115200); 
  Serial.println(CLS);
  Serial.println("(RE)start uC");
  
  setSANTEKsensors();  

reqSENTEKpresetHoldReg_Temperature();delay(40);
}

void loop() {

reqSENTEKpresetHoldReg_Moisture();delay(40);
reqSENTEKpresetHoldReg_Salinity(); delay(40); 
 
//reqSENTEKpresetHoldReg_Temperature();delay(400);
Serial.print("Temp      :");Serial.print(getSENTEKtemperature(1),2);  delay(40);Serial.print(" , ");Serial.print(getSENTEKtemperature(2),2);delay(40);Serial.print(" , ");Serial.println(getSENTEKtemperature(3),2); 

// reqSENTEKpresetHoldReg_Moisture();delay(400);
Serial.print("Moisture  :");Serial.print(getSENTEKmoisture(1),2); delay(400);Serial.print(" , ");Serial.print(getSENTEKmoisture(2),2);delay(400);Serial.print(" , ");Serial.println(getSENTEKmoisture(3),2);

//reqSENTEKpresetHoldReg_Salinity();
delay(40);
Serial.print("Salinity  :");Serial.print(getSENTEKsalinity(1),2);delay(40);Serial.print(" , ");Serial.print(getSENTEKsalinity(2),2);delay(40);Serial.print(" , ");Serial.println(getSENTEKsalinity(3),2);
 

 
 //Serial.println(CLS);
 //Serial.println("\033[0;0H");
}
