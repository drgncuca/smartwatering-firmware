#include "EEPROMAnything.h"
#include <SoftwareSerial.h>
#include <Arduino.h>
#include <Wire.h>
#include "inetGSM.h"
#include "SMS.h"
#include <TimeLib.h>
#include "DS3231M.h"

#include "Telemetry.h"
#include <SPI.h>

#define CLS          "\033[2J"  

SMSGSM sms;
extern InetGSM inet;
DS3231M_Class DS3231M;
DateTime RTCnowTime;

#include <Adafruit_MAX31865.h>
Adafruit_MAX31865 TemperaturePT1000 = Adafruit_MAX31865(49);

SHT1x SHT1x_sensor(   20  ,   21);
float temperature;
//MD_AD9833 AD(FSYNC); // Hardware SPI

boolean gsm_started = false;
carrier carrierdata;
struct_operater_vip operaterVIP;
struct_operater_MTS operaterMTS;
struct_operater_Telenor operaterTelenor;

#ifdef Sensor_EC
 EC_struct ECmeasuring;
#endif

float fertCurrentDriverCoeff ;

pH_struct pHmeasuring;
irrigation_struct_FlowMetering  irrigationFlowMetering[5];

fert_struct fertilizations[4];
int valves_fert[4] ;

unsigned long currentTimeMilles;


   
#ifdef  BOARD_SENS_R001
void setup() {
  // initialize digital pin LED_BUILTIN as an output. 
    
  pinMode(24, OUTPUT);//Port PA
  pinMode(25, OUTPUT);
  pinMode(26, OUTPUT);
 
  pinMode(33, OUTPUT);//Port PC
  pinMode(32, OUTPUT);
  pinMode(31, OUTPUT);
  pinMode(30, OUTPUT);  

  pinMode(41, OUTPUT); //Port PG
  pinMode(40, OUTPUT);
  pinMode(39, OUTPUT);   
  pinMode(75, OUTPUT);
  DDRG  = DDRG  | 0x08; // Relay DC4_N <OUT>  
  PORTG = PORTG & 0xF7; // Relay DC4_N <LOW> 
  pinMode(76, OUTPUT); //RTC reset


  pinMode(7, OUTPUT); //GPRS RST  
  pinMode(8, OUTPUT); //GPRS_PWR 
  
  pinMode(4, OUTPUT);  //PWM
  Serial.begin(115200);

  digitalWrite(7, LOW); 
  
while (!DS3231M.begin()) // Initialize RTC communications
  {
    Serial.println(F("Unable to find DS3231MM. Checking again in 3s."));
    delay(3000);
  }
RTCnowTime = DS3231M.now(); // get the current time from device
}
#endif

#if defined(BOARD_FERT_R001) || defined(BOARD_FERT_R002)
void setup() {

  pinMode(22, OUTPUT); //Port PA
  pinMode(23, OUTPUT);
  pinMode(24, OUTPUT);
  pinMode(25, OUTPUT);
  pinMode(26, OUTPUT);
  pinMode(27, OUTPUT);  
  pinMode(28, OUTPUT); 
  pinMode(29, OUTPUT);

  pinMode(37, OUTPUT); //Port PC
  pinMode(36, OUTPUT);
  pinMode(35, OUTPUT);
  pinMode(34, OUTPUT);
  pinMode(33, OUTPUT);
  pinMode(32, OUTPUT);
  pinMode(31, OUTPUT);
  pinMode(30, OUTPUT);  

  pinMode(41, OUTPUT); //Port PG
  pinMode(40, OUTPUT);
  pinMode(39, OUTPUT);   
  pinMode(75, OUTPUT);
  DDRG  = DDRG  | 0x08; 
  pinMode(76, OUTPUT); //RTC reset

  //--- Current OUT PWM------// 
  pinMode(5, OUTPUT);  //CH1
  pinMode(3, OUTPUT);  //CH2
  pinMode(6, OUTPUT);  //CH3
  pinMode(9, OUTPUT);  //CH4
  pinMode(8, OUTPUT);  //CH5

  Serial.begin(115200);
  
  pinMode(7, OUTPUT); //GSM - reset
  digitalWrite(7, LOW);   

 
  
}
#endif  

#ifdef  BOARD_SENS_R001
void Relays_OneByOne(void){
Serial.println(CLS);
Serial.println("\n Relays_OneByOne test ");
char x;

  Serial.print("DC1P");digitalWrite(33, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC1N");digitalWrite(32, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC2P");digitalWrite(31, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC2N");digitalWrite(30, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC3P");digitalWrite(41, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC3N");digitalWrite(40, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC4P");digitalWrite(39, HIGH);while(!Serial.available()){}x = Serial.read();
  //Serial.print("DC4N");digitalWrite(75, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC4N");PORTG = PORTG | 0x08; while(!Serial.available()){}x = Serial.read();

  digitalWrite(33, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(32, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(31, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(30, LOW);while(!Serial.available()){}x = Serial.read();

  digitalWrite(41, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(40, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(39, LOW);while(!Serial.available()){}x = Serial.read();  
  //digitalWrite(75, LOW);while(!Serial.available()){}x = Serial.read();
  PORTG = PORTG & 0xF7; while(!Serial.available()){}x = Serial.read();

  
}
#endif

#if defined(BOARD_FERT_R001) || defined(BOARD_FERT_R002)
void Relays_OneByOne(void){
Serial.println(CLS);
Serial.println("\n Relays_OneByOne test ");
char x;

while(!Serial.available()){}x = Serial.read();Serial.print("AC1");digitalWrite(24, HIGH);
  while(!Serial.available()){}x = Serial.read();Serial.print("AC2");digitalWrite(23, HIGH);
  while(!Serial.available()){}x = Serial.read();Serial.print("AC3");digitalWrite(22, HIGH);
  while(!Serial.available()){}x = Serial.read();Serial.print("AC4");digitalWrite(25, HIGH);
  while(!Serial.available()){}x = Serial.read();Serial.print("AC5");digitalWrite(26, HIGH);
  while(!Serial.available()){}x = Serial.read();Serial.print("AC6");digitalWrite(27, HIGH);
  while(!Serial.available()){}x = Serial.read();Serial.print("AC7");digitalWrite(28, HIGH);
  while(!Serial.available()){}x = Serial.read();Serial.print("AC8");digitalWrite(29, HIGH);
                    
  while(!Serial.available()){}x = Serial.read();Serial.print("AC9") ;digitalWrite(37, HIGH);
  while(!Serial.available()){}x = Serial.read();Serial.print("AC10");digitalWrite(36, HIGH);
  while(!Serial.available()){}x = Serial.read();Serial.print("AC11");digitalWrite(35, HIGH);
  while(!Serial.available()){}x = Serial.read();Serial.print("AC12");digitalWrite(34, HIGH);
                    
  while(!Serial.available()){}x = Serial.read();Serial.print("AC13");digitalWrite(33, HIGH);
  while(!Serial.available()){}x = Serial.read();Serial.print("AC14");digitalWrite(32, HIGH);
  while(!Serial.available()){}x = Serial.read();Serial.print("AC15");digitalWrite(31, HIGH);
  while(!Serial.available()){}x = Serial.read();Serial.print("AC16");digitalWrite(30, HIGH);
  while(!Serial.available()){}x = Serial.read();Serial.print("AC17");digitalWrite(41, HIGH);
  while(!Serial.available()){}x = Serial.read();Serial.print("AC18");digitalWrite(40, HIGH);
  while(!Serial.available()){}x = Serial.read();Serial.print("AC19");digitalWrite(39, HIGH);
  while(!Serial.available()){}x = Serial.read();Serial.print("AC20");PORTG = PORTG | 0x08  ;

  delay(50); 

  while(!Serial.available()){}x = Serial.read();digitalWrite(24, LOW);Serial.print("AC1");
  while(!Serial.available()){}x = Serial.read();digitalWrite(23, LOW);Serial.print("AC2");
  while(!Serial.available()){}x = Serial.read();digitalWrite(22, LOW);Serial.print("AC3");
  while(!Serial.available()){}x = Serial.read();digitalWrite(25, LOW);Serial.print("AC4");
  while(!Serial.available()){}x = Serial.read();digitalWrite(26, LOW);Serial.print("AC5");
  while(!Serial.available()){}x = Serial.read();digitalWrite(27, LOW);Serial.print("AC6");
  while(!Serial.available()){}x = Serial.read();digitalWrite(28, LOW);Serial.print("AC7");
  while(!Serial.available()){}x = Serial.read();digitalWrite(29, LOW);Serial.print("AC8");
                                          
  while(!Serial.available()){}x = Serial.read();digitalWrite(37, LOW);Serial.print("AC9") ;
  while(!Serial.available()){}x = Serial.read();digitalWrite(36, LOW);Serial.print("AC10");
  while(!Serial.available()){}x = Serial.read();digitalWrite(35, LOW);Serial.print("AC11");
  while(!Serial.available()){}x = Serial.read();digitalWrite(34, LOW);Serial.print("AC12");
                                          
  while(!Serial.available()){}x = Serial.read();digitalWrite(33, LOW);Serial.print("AC13");
  while(!Serial.available()){}x = Serial.read();digitalWrite(32, LOW);Serial.print("AC14");
  while(!Serial.available()){}x = Serial.read();digitalWrite(31, LOW);Serial.print("AC15");
  while(!Serial.available()){}x = Serial.read();digitalWrite(30, LOW);Serial.print("AC16");                    
  while(!Serial.available()){}x = Serial.read();digitalWrite(41, LOW);Serial.print("AC17");
  while(!Serial.available()){}x = Serial.read();digitalWrite(40, LOW);Serial.print("AC18");
  while(!Serial.available()){}x = Serial.read();digitalWrite(39, LOW);Serial.print("AC19");  
  while(!Serial.available()){}x = Serial.read();PORTG = PORTG & 0xF7 ;Serial.print("AC20");
}
#endif
  
void RTC(void){
char output_buffer[32]; ///< Temporary buffer for sprintf()
Serial.println(CLS);
Serial.println("\nReal Time Clock Test :");
Serial.println("for EXIT pres 0\n");

while (!DS3231M.begin()) // Initialize RTC communications
  {
    Serial.println(F("Unable to find DS3231MM. Checking again in 3s."));
    delay(3000);
  } // of loop until device is located
//DS3231M.adjust(DateTime(2020,03,25,18,42,55));
DS3231M.pinSquareWave(); // Make INT/SQW pin toggle at 1Hz  
static uint8_t secs = 0;
DateTime now = DS3231M.now(); // get the current time from device

while((Serial.read() != '0')){
  if (secs != now.second())     // Output if seconds have changed
  {
    sprintf(output_buffer,"%04d-%02d-%02d %02d:%02d:%02d", now.year(), now.month(),  now.day(), now.hour(), now.minute(), now.second());
    Serial.print("DATE and TIME : ");Serial.print(output_buffer);
    Serial.write(cr);    
    secs = now.second(); // Set the counter variable    
  } // of if the seconds have changed
now = DS3231M.now();  
}
}

void PWM(void){
  Serial.print(CLS);
  Serial.println("PWM Generator....");
int td = 1;
char x = 't';
  while ( ( x != '0')){
    
    if(x == 't') {
      td++; Serial.print(CLS); Serial.write(cr);Serial.print("td = ");Serial.print(td);
      switch (td){
        case 1:Serial.print(492); break;
        case 2:Serial.print(", f = ");Serial.print(248); break;
        case 3:Serial.print(", f = ");Serial.print(166); break;
        case 4:Serial.print(", f = ");Serial.print(124); break;
        case 5:Serial.print(", f = ");Serial.print(100); break;
        case 6:Serial.print(", f = ");Serial.print(83); break;
        case 7:Serial.print(", f = ");Serial.print(71); break;
        case 8:Serial.print(", f = ");Serial.print(62); break;
        case 9:Serial.print(", f = ");Serial.print(55); break;
        case 10:Serial.print(", f = ");Serial.print(50); break;
        case 11:Serial.print(", f = ");Serial.print(45); break;
        case 12:Serial.print(", f = ");Serial.print(41); break;
        case 13:Serial.print(", f = ");Serial.print(38); break;
        case 14:Serial.print(", f = ");Serial.print(35); break;
        case 15:Serial.print(", f = ");Serial.print(33); break;
        case 16:Serial.print(", f = ");Serial.print(31); break;
        case 17:Serial.print(", f = ");Serial.print(29); break;
        case 18:Serial.print(", f = ");Serial.print(27); break;
        case 19:Serial.print(", f = ");Serial.print(26); break;
        case 24:Serial.print(", f = ");Serial.print(20); break;
        case 26:Serial.print(", f = ");Serial.print(19); break;
        default: break;
        }
    }
    if(x == 'T' && td !=1) {td--;}
    digitalWrite(4,HIGH);
    delay(td);
    digitalWrite(4,LOW); 
    delay(td);  

    x = Serial.read();
    
}  
}

#ifdef  BOARD_SENS_R001
void Flow(void){
 int sensorValue = 0;  // variable to store the value coming from the sensor
 int i = 40;

 float val;
 char buff[3];

while ( (Serial.read() != '0')){
  Serial.print(CLS);
  Serial.println("Flow Measuring Test: \n");
  Serial.println("Trimovati potencimetar PT2, tako da za 270Hz se dobije protok od 8.2 L/s!");
  Serial.println("      ILI                 ,         za 250Hz se dobije protok od 7.5 L/s!");
  sensorValue = analogRead(A5);
  val = ((float)sensorValue * 5) / 1023 ;
  dtostrf(val, 1, 2, buff);
  Serial.print("ADC5 Voltage : "); Serial.print(val);Serial.print("V");Serial.print(" Flow :"); val = (float)sensorValue / 88;  dtostrf(val, 1, 2, buff); Serial.print(val);Serial.println("[L/s]");  
  delay(1000);
 }  
}
#endif

#if defined(BOARD_FERT_R001) || defined(BOARD_FERT_R002)
void Flow(void){
 int sensorValue = 0;  // variable to store the value coming from the sensor
 int i = 40;
 float val;
 char buff[3];

while ( (Serial.read() != '0')){
  Serial.print(CLS);
  Serial.println("Flow Measuring Test: \n");
  Serial.println("Trimovati potencimetar PT2, tako da za 270Hz se dobije protok od 8.2 L/s!");
  Serial.println("      ILI                 ,         za 250Hz se dobije protok od 7.5 L/s!");
  
  sensorValue = analogRead(A3);
  val = ((float)sensorValue * 5) / 1023 ;
  dtostrf(val, 1, 2, buff);
  Serial.print("CH1(ADC3) Voltage : "); Serial.print(val);Serial.print("V");Serial.print(" Flow :"); val = (float)sensorValue / 88;  dtostrf(val, 1, 2, buff); Serial.print(val);Serial.println("[L/s]");
  
  sensorValue = analogRead(A1);
  val = ((float)sensorValue * 5) / 1023 ;
  dtostrf(val, 1, 2, buff);
  Serial.print("CH2(ADC1) Voltage : "); Serial.print(val);Serial.print("V");Serial.print(" Flow :"); val = (float)sensorValue / 88;  dtostrf(val, 1, 2, buff); Serial.print(val);Serial.println("[L/s]");
  
  sensorValue = analogRead(A0);
  val = ((float)sensorValue * 5) / 1023 ;
  dtostrf(val, 1, 2, buff);
  Serial.print("CH31(ADC0) Voltage : "); Serial.print(val);Serial.print("V");Serial.print(" Flow :"); val = (float)sensorValue / 88;  dtostrf(val, 1, 2, buff); Serial.print(val);Serial.println("[L/s]");
  
  sensorValue = analogRead(A4);
 val = ((float)sensorValue * 5) / 1023 ;
  dtostrf(val, 1, 2, buff);
  Serial.print("CH4(ADC4) Voltage : "); Serial.print(val);Serial.print("V");Serial.print(" Flow :"); val = (float)sensorValue / 88;  dtostrf(val, 1, 2, buff); Serial.print(val);Serial.println("[L/s]");
  
  sensorValue = analogRead(A5);
  val = ((float)sensorValue * 5) / 1023 ;
  dtostrf(val, 1, 2, buff);
  Serial.print("CH5(ADC5) Voltage : "); Serial.print(val);Serial.print("V");Serial.print(" Flow :"); val = (float)sensorValue / 88;  dtostrf(val, 1, 2, buff); Serial.print(val);Serial.println("[L/s]");
  
  delay(1500);
}  
}
#endif

void FlowPWM(void){
Serial.print(CLS);
Serial.println("PWM generator + FLOW testing for CH5. With 't' and 'T' +/- PWM freq.");
initialiseIrrFlowMet();
int cnt = 0;
int  td = 1;
char x = ' ';
  while ( ( x != '0')){
    x = Serial.read();
    if(x == 't') {td++;}
    if(x == 'T') {td--;}
    digitalWrite(4,HIGH);
    delay(td);
    digitalWrite(4,LOW); 
    delay(3);
    cnt++;
    if (cnt == 2500) {
      Serial.print(CLS);                                        //FlowSensorCH5
      Serial.print("Flow [L/min] :"); Serial.println(getCurrentFlow(4)*60 );
      Serial.print("Volume   [L] :");Serial.println(getCurrentVOL(4));
      Serial.print("td : "); Serial.println(td);
      cnt =0;
    }
   }
}

void pH(void){
 int sensorValue = 0;  // variable to store the value coming from the sensor
 float val;
 char buff[3];
 
 Serial.print(CLS);
 Serial.println("pH  Test, Calibrate PT1=10kohm, for 0Vin, ADC = 1.94V. With C enter in calibration mode\n");
 Serial.println("ADC reading:");

 pHmeasuring.ferphK      = 8.81;  // Y = k*X + M
 pHmeasuring.ferphM     =-9.87; 
char x='x';
String tmp="111";  
while ( x != '0'){
  x = Serial.read();  
  sensorValue = analogRead(A2);
  val = ((float)sensorValue * 5) / 1023 ;
  dtostrf(val, 1, 2, buff);
  Serial.write(cr); Serial.print("ADC "); Serial.print(sensorValue); Serial.print(" -> ");Serial.print(val);Serial.print("V,  pH = ");Serial.print(getPh()); Serial.print(" [K,M] "); Serial.print(pHmeasuring.ferphK );Serial.print(pHmeasuring.ferphM );
  delay(1000);
  if(x == 'c'){
    Serial.setTimeout(3500);
    Serial.print(CLS);
    Serial.write(cr); 
    
    Serial.print("factor K:");  
    tmp = Serial.readStringUntil('\n'); 
    Serial.println(tmp);
    pHmeasuring.ferphK = tmp.toFloat(); 
    
    Serial.print("factor M:");  
    tmp = Serial.readStringUntil('\n') ;
    Serial.println(tmp); 
    pHmeasuring.ferphM = tmp.toFloat(); 
  }
}
}

#ifdef  BOARD_SENS_R001
void RS485(void){
 char x = 'x';
 Serial.print(CLS);
 Serial.println("RS485 test :\n");
 Serial.println("Chose chanel from 1 to 4, or 0 for EXIT :");
 Serial2.begin(9600);
 RS485setup();
     
 while ( x != '0'){
  x = Serial.read();
 RS485writeStringLN("ABCD____EFG");
  delay(800);
  Serial.println(RS485readString(1));
  delay(800);
 } 
}
#endif

#if defined(BOARD_FERT_R001) || defined(BOARD_FERT_R002) 
void RS485(void){
 char x = 'x';
 Serial.print(CLS);
 Serial.println("RS485 test :\n");
 Serial.println("Chose chanel from 1 to 4, or 0 for EXIT :");
 Serial2.begin(115200);

// CH1 is TX, data enable transmit
DDRH  = DDRH  | 0x04; //PH2 is out.
PORTH = PORTH | 0x04; //PH2 is HIGH.

// CH1  RX,
DDRD  = DDRD  | 0x10; //PD4 is out.
//PORTD = PORTD & 0xEF; //PD4 is LOW  -> Enable receiv data.
PORTD = PORTD | 0x10;   //PD4 is HIGH -> Disable receiv data.


// CH2  RX,
DDRD  = DDRD  | 0x80;  //PD7 is out.
PORTD = PORTD | 0x80;  //PD7 is HIGH -> Disable receiv data.

// CH3  RX,
DDRD  = DDRD  | 0x40;  //PD6 is out.
PORTD = PORTD | 0x40;  //PD6 is HIGH -> Disable receiv data

// CH4  RX,
DDRD  = DDRD  | 0x20;  //PD5 is out.
PORTD = PORTD | 0x20;  //PD5 is HIGH -> Disable receiv data

 while ( x != '0'){
  x = Serial.read();
  if(x == '1'){
    Serial.println("Chanel 1:");
     PORTD = PORTD & 0xEF;//PD4 is LOW
     PORTD = PORTD | 0x80;
     PORTD = PORTD | 0x40;
     PORTD = PORTD | 0x20;
    }
   if(x == '2'){
    Serial.println("Chanel 2:");
     PORTD = PORTD | 0x10;
     PORTD = PORTD & 0x7F;
     PORTD = PORTD | 0x40;
     PORTD = PORTD | 0x20;
    }
    if(x == '3'){
      Serial.println("Chanel 3:");
     PORTD = PORTD | 0x10;
     PORTD = PORTD | 0x80;
     PORTD = PORTD & 0xBF;
     PORTD = PORTD | 0x20;
    }
    if(x == '4'){
      Serial.println("Chanel 4:");
     PORTD = PORTD | 0x10;
     PORTD = PORTD | 0x80;
     PORTD = PORTD | 0x40;
     PORTD = PORTD & 0xDF;
    } 
  Serial2.print("ABCD"); 
  Serial.println(Serial2.read());
  delay(1000);
 } 
}
#endif

#if defined(BOARD_FERT_R001) || defined(BOARD_FERT_R002)
void CurrentIN_OUT(void){
int pulse = 25;
int pause = 75;  //frequency 100Hz.
int sensorValue;
char x = 'x';
char buff[3];
float val;
int i=0;
  
Serial.println(CLS);
Serial.println("Current OUT test:"); Serial.println("Pres 4 and 6 for decrease and increase duty cycle\n\n");
Serial.print("Pulse ");Serial.print(pulse); Serial.print(" mS");Serial.print(" Pause ");Serial.print(pause);Serial.print(" mS");
Serial.print("  Current OUT = ");Serial.print((float)pulse*0.275);Serial.print(" mA");

 pinMode(5, OUTPUT);  //CH1
  pinMode(3, OUTPUT);  //CH2
  pinMode(6, OUTPUT);  //CH3
  pinMode(9, OUTPUT);  //CH4
  pinMode(8, OUTPUT);  //CH5

while ( x != '0'){ 

  x =  Serial.read();
  if (x == '6') {
    pulse++; pause--;
  }
   if (x == '4') {
    pulse--; pause++;
  }

  i++;
  if (i == 10){ // refresh reading every 1s.    
        i=0;  
        Serial.println(CLS);
        Serial.print("Pulse ");Serial.print(pulse); Serial.print(" mS");Serial.print(" Pause ");Serial.print(pause);Serial.print(" mS");
        Serial.print("  Current OUT = ");Serial.print((float)pulse*0.275);Serial.println(" mA");
      
        sensorValue = analogRead(A12);  //CH1
        val = (float)sensorValue / 25 ;
        dtostrf(val, 2, 1, buff);
        Serial.print("Current IN on CH1 = ");Serial.print(val);Serial.println(" mA");
      
        sensorValue = analogRead(A13);  //CH2
        val = (float)sensorValue / 25 ;
        dtostrf(val, 2, 1, buff);
        Serial.print("Current IN on CH2 = ");Serial.print(val);Serial.println(" mA");
        
        sensorValue = analogRead(A11);  //CH3
        val = (float)sensorValue / 25 ;
        dtostrf(val, 2, 1, buff);
        Serial.print("Current IN on CH3 = ");Serial.print(val);Serial.println(" mA");
        
        sensorValue = analogRead(A10);  //CH4
        val = (float)sensorValue / 25 ;
        dtostrf(val, 2, 1, buff);
        Serial.print("Current IN on CH4 = ");Serial.print(val);Serial.println(" mA");
        
        sensorValue = analogRead(A9);  //CH5
        val = (float)sensorValue / 25 ;
        dtostrf(val, 2, 1, buff);
        Serial.print("Current IN on CH5 = ");Serial.print(val);Serial.println(" mA");
  }  
  
  delay(pause);
  
  digitalWrite(5, HIGH);
  digitalWrite(3, HIGH);
  digitalWrite(6, HIGH);
  digitalWrite(9, HIGH);
  digitalWrite(8, HIGH);
  
  delay(pulse);
  
  digitalWrite(5, LOW);
  digitalWrite(3, LOW);
  digitalWrite(6, LOW);
  digitalWrite(9, LOW);
  digitalWrite(8, LOW);  
}  

  pinMode(5, INPUT);  //CH1
  pinMode(3, INPUT);  //CH2
  pinMode(6, INPUT);  //CH3
  pinMode(9, INPUT);  //CH4
  pinMode(8, INPUT);  //CH5
}
#endif

#ifdef  BOARD_SENS_R001
void CurrentIN_OUT(void){
int pulse = 25;
int pause = 75;  //frequency 100Hz.
int sensorValue;
char x = 'x';
char buff[3];
float val;
int i=0;
  
Serial.println(CLS);
Serial.println("Current IN test. Set some Current source from another board."); 
 
while (Serial.read() != '0'){
     
        i=0;  
        Serial.println(CLS);
          
        sensorValue = analogRead(A12);  //CH1
        val = (float)sensorValue / 25 ;
        dtostrf(val, 2, 1, buff);
        Serial.print("Current IN on CH1 = ");Serial.print(val);Serial.println(" mA");
      
        sensorValue = analogRead(A13);  //CH2
        val = (float)sensorValue / 25 ;
        dtostrf(val, 2, 1, buff);
        Serial.print("Current IN on CH2 = ");Serial.print(val);Serial.println(" mA");        

        delay(1000);
  
}  
}
#endif

#if defined(BOARD_FERT_R001) || defined(BOARD_FERT_R002)
void AnalogIN(void){
int sensorValue;
char x = 'x';
char buff[3];
float val; 

while ( (Serial.read() != '0')){
        Serial.println(CLS);
        Serial.print("Analog INUP Test CH1, CH2, CH3 and CH4 :\n\n\n");
        
        sensorValue = analogRead(A15);  //CH1 
        val = (float)sensorValue*5 / 2048 ;
        dtostrf(val, 1, 2, buff);
        Serial.write(cr);Serial.print("Analog IN CH1 = ");Serial.print(val);Serial.println("V");

        sensorValue = analogRead(A7);  //CH2 
        val = (float)sensorValue*5 / 2048 ;
        dtostrf(val, 1, 2, buff);
        Serial.write(cr);Serial.print("Analog IN CH2 = ");Serial.print(val);Serial.println("V");
        
        sensorValue = analogRead(A14);  //CH3 
        val = (float)sensorValue*5 / 2048 ;
        dtostrf(val, 1, 2, buff);
        Serial.write(cr);Serial.print("Analog IN CH3 = ");Serial.print(val);Serial.println("V");

        sensorValue = analogRead(A8);  //CH4 - jedini trenutno funkcionalan
        val = (float)sensorValue*5 / 2048 ;
        dtostrf(val, 1, 2, buff);
        Serial.write(cr);Serial.print("Analog IN CH4 = ");Serial.print(val);Serial.println("V");      
               
        delay(1500);
}  
}
#endif


void Temperature(void){

TemperaturePT1000.begin(MAX31865_3WIRE);  // set to 2WIRE or 4WIRE as necessary
Serial.println(CLS);

while(Serial.read() != '0'){

   Serial.println("\033[0;0H");
   Serial.println("Temperature Test :\n");
  
   uint16_t rtd = TemperaturePT1000.readRTD();
 // Serial.print("RTD value: "); Serial.println(rtd); 
  float ratio = rtd;
  ratio /= 32768;
  //Serial.print("Ratio = "); Serial.println(ratio,2);
  Serial.print("Resistance = "); Serial.print(RREF*ratio,1);Serial.println(" ohm");
  Serial.print("Temperature = "); Serial.print(TemperaturePT1000.temperature(RNOMINAL, RREF));Serial.println(" Celsus");

  // Check and print any faults
  uint8_t fault = TemperaturePT1000.readFault();
  if (fault) {
    Serial.print("Fault 0x"); Serial.println(fault, HEX);
    if (fault & MAX31865_FAULT_HIGHTHRESH) {
      Serial.println("RTD High Threshold"); 
    }
    if (fault & MAX31865_FAULT_LOWTHRESH) {
      Serial.println("RTD Low Threshold"); 
    }
    if (fault & MAX31865_FAULT_REFINLOW) {
      Serial.println("REFIN- > 0.85 x Bias"); 
    }
    if (fault & MAX31865_FAULT_REFINHIGH) {
      Serial.println("REFIN- < 0.85 x Bias - FORCE- open"); 
    }
    if (fault & MAX31865_FAULT_RTDINLOW) {
      Serial.println("RTDIN- < 0.85 x Bias - FORCE- open"); 
    }
    if (fault & MAX31865_FAULT_OVUV) {
      Serial.println("Under/Over voltage"); 
    }
    TemperaturePT1000.clearFault();
  }
  Serial.println();
  delay(500);  
  }  
}

void Humidity_Temperature(void){

  float h,t;
  while((Serial.read() != '0')){ 
    h = SHT1x_sensor.readHumidity();
    t = SHT1x_sensor.readTemperatureC();  
    Serial.println(CLS);     
    Serial.print("SHT1x Temp: ");Serial.print(t ); Serial.print(", Humidity : ");Serial.print( h );
      
   delay(1000);  
  } 
}

void Conductivity(void){
#ifdef Sensor_EC   

Serial.println(CLS);
char x = 'x';

  setConductivityDriver(40, 1);
  
  setupTemperaturePT1000();
  temperature = SHT1x_sensor.readTemperatureC(); 
 Serial.println("press 'O' for Offset Calibration. Triming until ADC ==496 (2.41V)");
 int cnt = 0;
 while(!Serial.available() && (cnt++<25)) {delay(100);}
 x = Serial.read(); 
  if(x == 'o'){
       while(Serial.read() != '0'  ){
          Serial.write(cr); Serial.print("ADC6 : ");Serial.print(analogRead(A6));
          delay(200);
      }    
    }
 Serial.println(CLS); 
 
 while(Serial.read() != '0'){
   Serial.println("\033[0;0H");  
   getELconductance();   
   //delay(4000);
  }   
#endif   
}


void UART3(void){
  
 char x = 'x', z='z';
 Serial.print(CLS);
 Serial.println("UART3 LoopBack Test :\n");
 Serial.println("Short Connect RX and TX on Blue Connector");
 
 Serial3.begin(115200); 
 
  while((x != '0')){
     while(!Serial.available()){}
     x = Serial.read();
     Serial3.print(x); 
     delay(20); 
     z = Serial3.read();
     delay(20);
     Serial.print(z);  
     delay(20);  
  } 
}

#ifdef  BOARD_SENS_R001
void charging(void){
  int sensorValue =0;
 char x = 'x', z='z';
 Serial.print(CLS);

 while((Serial.read() != '0')){
   Serial.print("\033[0;0H");
   Serial.println("Charger test, c-charging C-discharging, b-battery status \n");
   sensorValue = analogRead(A8); Serial.print("Battery voltage: "); Serial.print((float)sensorValue *0.01472 , 3 );Serial.println(" V");
   sensorValue = analogRead(A9); Serial.print("Solar   voltage: "); Serial.print((float)sensorValue *0.0280  , 3 );Serial.println(" V");
   delay(1000);
   x = Serial.read();
   if (x=='c'){
      digitalWrite(26, HIGH);
      Serial.println("Charging.....");
    }
     if (x=='C'){
      digitalWrite(26, LOW);
      Serial.println("DisCharging.....");
    }
      if (x=='b'){
       digitalWrite(26, LOW);
       Serial.println("Battery status :"); 
      while ((Serial.read() != '0')){      
        Serial.write(cr);
        Serial.print((float)analogRead(A8) *0.01472,3); Serial.print(" V");
        delay(800);
        }
    }     
 }
 digitalWrite(26, LOW);
}
#endif


void GSMsetReset(void){
Serial.println("GSM Set/Restet with H & L ");  
char x='x';
while((x != '0')){ 
x = Serial.read(); 
if(x == 'h') {digitalWrite(7, HIGH); Serial.println("GSM Reset = H"); }   
if(x == 'l') {digitalWrite(7, LOW);  Serial.println("GSM Reset = L");}

delay(700);
}

}

void SMS_test(void) {  
bool gsm_started = false;
char x = 'x';
char c;
Serial.println(CLS);
Serial.println("SMS Test");
  if (gsm.begin(9600))
  {
    Serial.println("\nGSM READY:\n");
    inet.attachGPRS("prepaidnet","mts","gprs");  
    Serial.println("Sending SMS: 0 for NO, 1 for YES:");
    while(!Serial.available()){}
    x = Serial.read(); 
    if (x == '1'){sms.SendSMS("0646116952", "SmartWatering Controler A005");}   
    if (x == '0'){return;}
  }
  else
  {
    Serial.println(F("\nstatus=IDLE"));
  }
    while (c = gsm.read()) {
      Serial.print(c);
    }
}

void OperaterChanging(void){
 Serial.println(CLS);
 Serial.println(F("Operater Detecting Test")); 
 Serial.println(F("GSM connecting.....")); 
 if (gsm.begin(9600)) 
  {
    gsm_started = true;
    Serial.println(F("\GSM = READY"));
    }
  else
  {
    gsm_started = false;
    Serial.println(F("\GSM = IDLE")); 
  }

doesOperaterChanging();

EEPROM_readAnything(EERPOM_mem_loc_CarrierData, carrierdata);
    char tmp[2]="00"; 
    Serial.println("\n---------------------------");    
    Serial.println("Sadrzaj EEPROM-a : ");   
    EEPROM_readAnything(64, tmp);
    Serial.println(String(tmp).c_str());     
    Serial.println("Data in the Carrier Data Structure");
    Serial.println(carrierdata.apnname);
    Serial.println(carrierdata.apnuser);
    Serial.println(carrierdata.apnpass);

delay(1500);
}

void GPRS_Connecting(void){ 
 Serial.println(CLS);
 Serial.println(F("GPRS Connecting Test"));
unsigned long tmp =  currentTime();

 if (gsm.begin(9600)) 
  {
    gsm_started = true;
    Serial.println(F("\GSM = READY"));
    }
  else
  {
    gsm_started = false;
    Serial.println(F("\GSM = IDLE")); 
  }
  
if (gsm_started)
  {
    //GPRS attach, put in order APN, username and password.
    if (inet.attachGPRS("internet", "telenor", "gprs"))
//  if (inet.attachGPRS(String(carrierdata.apnname).c_str(), String(carrierdata.apnuser).c_str(), String(carrierdata.apnpass).c_str()))
    {
     Serial.println(F("GPRS = ATTACHED"));     
     gsm.SimpleWriteln("AT+CIFSR");//Read IP address.
     delay(5000);     
     gsm.WhileSimpleRead();//Read until serial buffer is empty
    }
    else
    {
      Serial.println(F("GPRS = ERROR"));
    }
    Serial.print("GSM/GPRS Wake UP :");   Serial.println( currentTime() - tmp);
    delay(3000);
  }
else
  {
   Serial.println(F("GSM&GPRS=NOTstarted"));
  }
delay(2500);  
}

void GPRSdataExchange(void){  
 Serial.println(CLS);
 Serial.println(F("GPRS Data TX/RX test...."));   
char msg[1000];

while ((Serial.read() != '0')){ 
unsigned long tmp =  currentTime();  
  memset(msg, 0, sizeof(msg));                                            //Tresnja Zones
  inet.httpPOST("app.smartwatering.rs", 80, "/api/post/sync_device.php", "did=rjTXqd&t1=1.11&t2=2.22&t3=3.33&m1=0.00&m2=0.00&m3=0.00&mid=&fid=&ferph=174&cfl=2.22&tfv=3.33&ecv=4.44&iids=", msg, 1000);  
  Serial.println(msg);
  Serial.print("Sync Time: ");Serial.println(currentTime() - tmp);   
  delay(4000);
}
}


void ArduinoStandBy(void){
#ifdef Power_Saving  
  PowerStandBy(10); // 10sekundi 
#endif  
}
//--------------------------------------------------------------------------------------------
void sentek(void){

#ifdef SENTEC_Include
  RS485setup();
  setSANTEKsensors();  
Serial.println(CLS);
 
while(Serial.read() != '0'){  
   
  reqSENTEKpresetHoldReg_Temperature();delay(200);
  Serial.print("Temp     :");Serial.print(getSENTEKtemperature(1),2);  delay(50);Serial.print(" , ");Serial.print(getSENTEKtemperature(2),2);delay(50);Serial.print(" , ");Serial.println(getSENTEKtemperature(3),2); 
//  Serial.print("          ");Serial.print(getSENTEKtemperature(4),2);  delay(200);Serial.print(" , ");Serial.print(getSENTEKtemperature(5),2);delay(200);Serial.print(" , ");Serial.println(getSENTEKtemperature(6),2); 
  delay(50);
  reqSENTEKpresetHoldReg_Moisture();
  delay(800);
//  getSENTEKmoisture(1);
  Serial.print("Moisture :");Serial.print(getSENTEKmoisture(1),2); delay(50);Serial.print(" , ");Serial.print(getSENTEKmoisture(2),2);delay(50);Serial.print(" , ");Serial.println(getSENTEKmoisture(3),2);
//  Serial.print("          ");Serial.print(getSENTEKmoisture(4),2); delay(200);Serial.print(" , ");Serial.print(getSENTEKmoisture(5),2);delay(200);Serial.print(" , ");Serial.println(getSENTEKmoisture(6),2);
//  delay(200);
//  reqSENTEKpresetHoldReg_Salinity();  delay(400);
//  Serial.print("Salinity :");Serial.print(getSENTEKsalinity(1),2);delay(40);Serial.print(" , ");Serial.print(getSENTEKsalinity(2),2);delay(40);Serial.print(" , ");Serial.println(getSENTEKsalinity(3),2);  
//  Serial.print("          ");Serial.print(getSENTEKsalinity(4),2);delay(40);Serial.print(" , ");Serial.print(getSENTEKsalinity(5),2);delay(40);Serial.print(" , ");Serial.println(getSENTEKsalinity(6),2);   

  delay(1000);
  }
#endif
}

#if (defined  (BOARD_FERT_R001) || defined  (BOARD_FERT_R002)) && (defined (Fertilisation))

#ifdef Sensor_EC_PID
void FertilisationEC_PID_Test(void){
  
char x = 'x';

setConductivityDriver(40,1);
Serial.println(CLS);
 
Serial.println("Sa 4 i 6 menja se EC"); 
Serial.println("sa P menja se pumpa ");
Serial.println("sa O - ON, F- OFF");

delay(2000);

while(x != '0'){
   getELconductance();
   x =  Serial.read();
  if (x == '6') {
    ECmeasuring.desired+=0.1;
  }
   if (x == '4') {
    ECmeasuring.desired-=0.1;
  }
    if (x == '9') {
    ECmeasuring.desired+=0.4;
  }
   if (x == '7') {
    ECmeasuring.desired-=0.4;
  }
 if (x == 'P') {
    ECmeasuring.ecKp+=0.6;
  }
   if (x == 'p') {
    ECmeasuring.ecKp-=0.6;
  }
   if (x == 'I') {
    ECmeasuring.ecKi+=0.4;
  }
   if (x == 'i') {
    ECmeasuring.ecKi-=0.4;
  }
   if (x == 'D') {
    ECmeasuring.ecKd+=0.4;
  }
   if (x == 'd') {
    ECmeasuring.ecKd-=0.4;
  }
                                         
   EC_PID_Control_Checking( );
  delay(3000);
  Serial.println("\033[0;0H");
}   
}
#endif

void FertilisationTest(void){
  
float pH = 5.5;

char x = 'x';

initialiseFertilisationParameters();
fertilizations[0].fertilization_status = 1;
 
Serial.println("Sa 4 i 6 menja se pH"); 
Serial.println("sa P menja se pumpa a sa H se menja pH=15 ");
Serial.println("sa O - ON, F- OFF");

//delay(2000);
Serial.println(CLS);
while(x != '0'){
  
   x =  Serial.read();
  if (x == '6') {
    pH+=0.15;
  }
   if (x == '4') {
    pH-=0.15;
  }
    if (x == '9') {
    pH+=2;
  }
   if (x == '7') {
    pH-=2;
  }
 if (x == 'P') {
    pHmeasuring.ferKp+=0.4;
  }
   if (x == 'p') {
    pHmeasuring.ferKp-=0.4;
  }
   if (x == 'I') {
    pHmeasuring.ferKi+=0.4;
  }
   if (x == 'i') {
    pHmeasuring.ferKi-=0.4;
  }
   if (x == 'D') {
    pHmeasuring.ferKd+=0.4;
  }
   if (x == 'd') {
    pHmeasuring.ferKd-=0.4;
  }
  
  fertilizationCheck( CurrDrvCH1, pH);
  delay(900);
  //Serial.println(CLS);
  Serial.println("\033[0;0H");
}   
  
  
}
#endif


void Power_StandBy_Testing(void){
#ifdef Power_Saving
Serial.println(CLS);
Serial.println(F("Power Sleep Test.")); delay(200);
PowerStandBy(9);
PowerWakeUP();
Serial.print(F("Press any Key."));
while (Serial.available()==0) {}
  
 
//#define SleepTime 60   
//   Serial.println(CLS);
//   Serial.println(F("Power Handling.")); 
//   Serial.println(F("d) uC power down"));
//   Serial.println(F("u) power up "));
//   Serial.println(F("g) GSM power down"));
//   Serial.println(F("G) GSM power UP"));
//   Serial.println(F("p) Power of GSM down"));
//   Serial.println(F("P) Power of GSM UP"));
//   char x = 'x';
//   unsigned long temp =  currentTime();
//   while ((x != '0')){ 
//      x = Serial.read();
//      if (x == 'd') { temp = currentTime(); PowerStandBy(SleepTime +1); Serial.print("StandBY time:");Serial.println( currentTime() - temp ); } 
//      if (x == 'u') { PowerWakeUP();} 
//      if (x == 'p') { DDRE  = DDRE  | 0x04; PORTE = PORTE & 0xFB;   } 
//      if (x == 'P') { DDRE  = DDRE  | 0x04; PORTE = PORTE | 0x04;   } 
//      if (x == 'g') { DDRH  = DDRH  | 0x20; PORTH = PORTH & 0xDF;} 
//      if (x == 'G') { DDRH  = DDRH  | 0x20; PORTH = PORTH | 0x20;}       
//      if (x == '0') { return;} 
//      delay(100); 
//    }    
#endif  
}
//------------------------------------------------------------------------------------------------------------
void loop(){  
 char x = 'x';

Serial.println(CLS);

#ifdef BOARD_SENS_R001
  Serial.println(F("\n Testing SENS board\n\n"));
#endif
#if defined(BOARD_FERT_R001)
  Serial.println(F("\n Testing FERT R001 board\n\n"));
#endif
#if defined(BOARD_FERT_R002)
  Serial.println(F("\n Testing FERT R002 board\n\n"));
#endif

Serial.println(F("1) RTC"));
Serial.println(F("2) Flow"));
Serial.println(F("3) Flow  with PWM"));
Serial.println(F("4) PWM generate"));
Serial.println(F("5) pH"));
Serial.println(F("6) RS485"));
Serial.println(F("7) Current IN/OUT"));
Serial.println(F("8) GSM Set Reset"));
Serial.println(F("9) SMS"));
Serial.println(F("o) Operater Detecting"));
Serial.println(F("c) GPRS Connecting"));
Serial.println(F("d) GPRS Data TX/RX"));
Serial.println(F("h) Humidity and Temp  I2C"));
Serial.println(F("t) Temperature"));
Serial.println(F("e) EC"));
Serial.println(F("s) SENTEK "));
Serial.println(F("u) UART3-Loopaback"));
Serial.println(F("r) Relays ObeByOne"));
Serial.println(F("a) Arduino Stand By"));
Serial.println(F("w) Power Stand-by"));
Serial.println(F("i) Charging"));
Serial.println(F("f) Fertilisation"));
Serial.println(F("l)Fert EC_PID_Test"));

 while(!Serial.available()){}
 x = Serial.read();

     switch (x){  
       case '1':RTC(); break;
       case '2':Flow();break; 
       case '3':FlowPWM();break; 
       case '4':PWM();break;
       case '5':pH();break;
       case '6':RS485();break;           
       case '7':CurrentIN_OUT();break;
       case '8':GSMsetReset();break;
       case '9':SMS_test();break;          
       case 'o':OperaterChanging();break;
       case 'c':GPRS_Connecting();break;
       case 'd':GPRSdataExchange();break;           
       case 'h':Humidity_Temperature();break;
       case 't':Temperature();break;       
       case 's':sentek();break;
       case 'e':Conductivity();break;
       case 'u':UART3();break;           
       case 'r':Relays_OneByOne();
       case 'a':ArduinoStandBy();break;
       case 'w':Power_StandBy_Testing();break;
#if (defined  (BOARD_FERT_R001) || defined  (BOARD_FERT_R002)) && (defined (Fertilisation))      
       case 'f':FertilisationTest();break;
    #ifdef Sensor_EC_PID 
       case 'l':FertilisationEC_PID_Test();break;
    #endif
#endif       
#ifdef BOARD_SENS_R001
       case 'i':charging();break;
#endif               
       default:break;               
     }             
 
}

static uint8_t currentTime()
{
  RTCnowTime = DS3231M.now(); 
return RTCnowTime.second();
}
