#include <Arduino.h>
#include <FlowMeter.h>
#include "Telemetry.h"

  // fmeter sensor
  float flowmeter_capacity = 30;
  float flowmeter_kfactor = 4.25;
  float flowmeter_mfactor = 1;

  FlowSensorProperties flowmeter_sensor = {flowmeter_capacity, flowmeter_kfactor, {flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor}};
  FlowMeter Meter = FlowMeter(2, flowmeter_sensor); //19-PD2 INT2
  long flowmeter_period = 1000; // one second (in milliseconds)
  long flowmeter_lasttime = 0;

void MeterISR()
{  
  Meter.count();// let our flow meter count the pulses
}

struct flowmeter_struct
{
  float currentflow;
  float currentvolume;
};

void setup() {
   RS485setup(); 
  Serial.begin(115200); 
  Serial.println(CLS);
  Serial.println("(RE)start uC");
  
  setSANTEKsensors();  
  //Serial.print("Echo:");echoSENTEKtoMaster();
  //Serial.print("Read Hold Reg");reqSENTEKreadingHoldReg();
  //SENTEKslaveID();
  //Serial.print("Preset Hold Reg");
// Serial.print("Present of Humidity  at  0x30023 "); readSENTEKinputReg(":010400160001E4");//Humidity
// Serial.print("Present of Temperatur at 0x30013 "); readSENTEKinputReg(":0104000C0001EE");//temp
// Serial.print("Present of Moisture   at 0x30009 "); readSENTEKinputReg(":010400080001F2");//moisture
//  reqSENTEKpresetHoldReg_Moisture();
  //delay(5000);
  //Serial.print("Moisture");getSENTEKmoisture(1);
  
  // enable a call to the 'interrupt service handler' (ISR) on every rising edge at the interrupt pin
  attachInterrupt(INT7, MeterISR, RISING);
  // sometimes initializing the gear generates some pulses that we should ignore
  Meter.reset();
  
}

void loop() {
 
struct flowmeter_struct flow = checkFlowMeter(); //svako malo azurira vrednost protoka i zapremine
 
reqSENTEKpresetHoldReg_Temperature();delay(0);
Serial.print("Temp      :");Serial.print(getSENTEKtemperature(1),2);  delay(0);Serial.print(" , ");Serial.print(getSENTEKtemperature(2),2);delay(0);Serial.print(" , ");Serial.println(getSENTEKtemperature(3),2); 
 reqSENTEKpresetHoldReg_Moisture();//delay(40);
Serial.print("Moisture  :");Serial.print(getSENTEKmoisture(1),2); delay(0);Serial.print(" , ");Serial.print(getSENTEKmoisture(2),2);delay(0);Serial.print(" , ");Serial.println(getSENTEKmoisture(3),2);
reqSENTEKpresetHoldReg_Salinity();
Serial.print("Salinity  :");Serial.print(getSENTEKsalinity(1),2);Serial.print(" , ");Serial.print(getSENTEKsalinity(2),2);Serial.print(" , ");Serial.println(getSENTEKsalinity(3),2);
 

 
 //Serial.println(CLS);
 //Serial.println("\033[0;0H");
}

struct flowmeter_struct checkFlowMeter()
{

struct flowmeter_struct flow_tmp;


    // process the (possibly) counted ticks
    Meter.tick(flowmeter_period);  
//  #ifdef DEBUG_Telemetry  
   
//  #endif
  
  flow_tmp.currentflow = Meter.getCurrentFlowrate();
  flow_tmp.currentvolume = Meter.getCurrentVolume();
  Meter.reset();
   Serial.println("Currently " + String(flow_tmp.currentflow) + " L/min, " + String(flow_tmp.currentvolume)+ " L total.");
  return flow_tmp;
}
