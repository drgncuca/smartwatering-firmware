
#include <SoftwareSerial.h>
#include <Arduino.h>




  byte valves_on[]  = {33, 31, 41, 39};
                   //DC1_N  2   3   4
  byte valves_off[] = {32, 30, 40, 75};

  byte delayValve = 50; 

void setup() {
  // initialize digital pin LED_BUILTIN as an output. 
    
  pinMode(24, OUTPUT);//Port PA
  pinMode(25, OUTPUT);
  pinMode(26, OUTPUT);
 
  pinMode(33, OUTPUT);//Port PC
  pinMode(32, OUTPUT);
  pinMode(31, OUTPUT);
  pinMode(30, OUTPUT);  

  pinMode(41, OUTPUT); //Port PG
  pinMode(40, OUTPUT);
  pinMode(39, OUTPUT);   
  pinMode(75, OUTPUT);
  DDRG  = DDRG  | 0x08; // Relay DC4_N <OUT>  
  PORTG = PORTG & 0xF7; // Relay DC4_N <LOW> 
  pinMode(76, OUTPUT); //RTC reset


  pinMode(7, OUTPUT); //GPRS RST  
  pinMode(8, OUTPUT); //GPRS_PWR 
  digitalWrite(8, HIGH); 
  digitalWrite(7, HIGH);  

  pinMode(23, OUTPUT); //5V0_I2C_PWR_EN
  digitalWrite(23, LOW); 

   pinMode(29, OUTPUT); //PWR_LED
  digitalWrite(29, HIGH);  
      
  pinMode(4, OUTPUT);  //PWM
  Serial.begin(115200);   


Serial.println("---------------------------");

}




void Relays_OneByOne(void){

Serial.println("\n Relays_OneByOne test ");
char x;

  Serial.print("DC1P");digitalWrite(33, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC1N");digitalWrite(32, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC2P");digitalWrite(31, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC2N");digitalWrite(30, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC3P");digitalWrite(41, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC3N");digitalWrite(40, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC4P");digitalWrite(39, HIGH);while(!Serial.available()){}x = Serial.read();
  //Serial.print("DC4N");digitalWrite(75, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC4N");PORTG = PORTG | 0x08; while(!Serial.available()){}x = Serial.read();

  digitalWrite(33, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(32, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(31, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(30, LOW);while(!Serial.available()){}x = Serial.read();

  digitalWrite(41, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(40, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(39, LOW);while(!Serial.available()){}x = Serial.read();  
  //digitalWrite(75, LOW);while(!Serial.available()){}x = Serial.read();
  PORTG = PORTG & 0xF7; while(!Serial.available()){}x = Serial.read();

  
}

void startValve(byte zone) {	
    
    if (zone == 3) { 
		PORTG = PORTG & 0xFB; //Reley za P na '0'
		PORTG = PORTG | 0x08; //Relay za N na '1' 	
		delay(delayValve);		
		PORTG = PORTG & 0xF7; //Relay za N na '0'
	}
	else{		
		digitalWrite(valves_on[zone], LOW);  // Releji za P (pozitivan) prikljucak vential 
		digitalWrite(valves_off[zone], HIGH);// Releji za N (negativan) prikljucak ventila
						  
		delay(delayValve);
		digitalWrite(valves_off[zone], LOW);			
		
	}
}
void Zone_OneByOne(void){


Serial.println("Zone_ON");
char x;
byte zone = 0;

while(x != 'x'){
  while(!Serial.available());
  Serial.print("Zone : ");Serial.println(zone+1);
  startValve(zone++);
  if (zone==4) return;
  x = Serial.read();
}

}

void Zone_OFF_OneByOne(void ){

Serial.println("Zone_OFF ");
char x;
byte zone = 0;

while(x != 'x'){
  while(!Serial.available());
  Serial.print("Zone : ");Serial.println(zone+1);
  stopValve(zone++);
  if (zone==4) return;
  x = Serial.read();
}



}

void stopValve(byte zone) {	
     
    if (zone == 3) {
		PORTG = PORTG & 0xF7;  //Relay za N na '0'
		PORTG = PORTG | 0x04;  //Reley za P na '1'
		delay(delayValve);
		PORTG = PORTG & 0xFB;  //Reley za P na '0'
	}
	else{
		digitalWrite(valves_off[zone], LOW);   //Releji za N (negativan) prikljucak ventila	
 
		digitalWrite(valves_on[zone], HIGH);  // Releji za P (pozitivan) prikljucak vential   
		delay(delayValve);
		digitalWrite(valves_on[zone], LOW); 		
	}

pinMode(34, OUTPUT);  //12V_SYS_PWR_EN
digitalWrite(34,HIGH);//ON

}
void Zone_ON_OFF_Auto(void){

Serial.println("Zone_Auot ON OFF ");
char x;

while(x != 'x'){
 
startValve(0);
delay(1000);
stopValve(0);
delay(1000);

}

}
//------------------------------------------------------------------------------------------------------------
void loop(){  
 char x = 'x';
Zone_ON_OFF_Auto();
Serial.println(F("r) Relays ObeByOne"));
Serial.println(F("z) Zone ON ObeByOne"));
Serial.println(F("o) Zone OFF ObeByOne"));
Serial.println(F("a) Zone ON/OFF Automatic"));

 while(!Serial.available()){}
 x = Serial.read();

     switch (x){  
           
       case 'r':Relays_OneByOne();
       case 'z':Zone_OneByOne();
       case 'o':Zone_OFF_OneByOne();
      case 'a':Zone_ON_OFF_Auto();
 
             
       default:break;               
     }             
 
}


