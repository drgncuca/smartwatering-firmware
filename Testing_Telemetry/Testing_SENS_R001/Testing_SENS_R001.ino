#include "EEPROMAnything.h"
#include <SoftwareSerial.h>
#include <Arduino.h>
#include <Wire.h>
#include "inetGSM.h"
#include "SMS.h"
#include <TimeLib.h>
#include "DS3231M.h"

#include "Telemetry.h"
#include <SPI.h>

#define CLS          "\033[2J"  

SMSGSM sms;
extern InetGSM inet;
DS3231M_Class DS3231M;
DateTime RTCnowTime;

#include <Adafruit_MAX31865.h>
Adafruit_MAX31865 TemperaturePT1000 = Adafruit_MAX31865(49);

SHT1x SHT1x_sensor(   20  ,   21);
float temperature;
//MD_AD9833 AD(FSYNC); // Hardware SPI

boolean gsm_started = false;
carrier carrierdata;
struct_operater_vip operaterVIP;
struct_operater_MTS operaterMTS;
struct_operater_Telenor operaterTelenor;

#ifdef Sensor_EC
// EC_struct ECmeasuring;
#endif

float fertCurrentDriverCoeff ;

pH_struct pHmeasuring;
irrigation_struct_FlowMetering  irrigationFlowMetering[5];


int valves_fert[4] ;

unsigned long currentTimeMilles;


   

void setup() {
  // initialize digital pin LED_BUILTIN as an output. 
    
  pinMode(24, OUTPUT);//Port PA
  pinMode(25, OUTPUT);
  pinMode(26, OUTPUT);
 
  pinMode(33, OUTPUT);//Port PC
  pinMode(32, OUTPUT);
  pinMode(31, OUTPUT);
  pinMode(30, OUTPUT);  

  pinMode(41, OUTPUT); //Port PG
  pinMode(40, OUTPUT);
  pinMode(39, OUTPUT);   
  pinMode(75, OUTPUT);
  DDRG  = DDRG  | 0x08; // Relay DC4_N <OUT>  
  PORTG = PORTG & 0xF7; // Relay DC4_N <LOW> 
  pinMode(76, OUTPUT); //RTC reset


  pinMode(7, OUTPUT); //GPRS RST  
  pinMode(8, OUTPUT); //GPRS_PWR 
   digitalWrite(8, HIGH); 
    digitalWrite(7, HIGH);  
      
  pinMode(4, OUTPUT);  //PWM
  Serial.begin(115200);   
  
while (!DS3231M.begin()) // Initialize RTC communications
  {
    Serial.println(F("Unable to find DS3231MM. Checking again in 3s."));
    delay(3000);
  }
RTCnowTime = DS3231M.now(); // get the current time from device
}




void Relays_OneByOne(void){
Serial.println(CLS);
Serial.println("\n Relays_OneByOne test ");
char x;

  Serial.print("DC1P");digitalWrite(33, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC1N");digitalWrite(32, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC2P");digitalWrite(31, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC2N");digitalWrite(30, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC3P");digitalWrite(41, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC3N");digitalWrite(40, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC4P");digitalWrite(39, HIGH);while(!Serial.available()){}x = Serial.read();
  //Serial.print("DC4N");digitalWrite(75, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("DC4N");PORTG = PORTG | 0x08; while(!Serial.available()){}x = Serial.read();

  digitalWrite(33, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(32, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(31, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(30, LOW);while(!Serial.available()){}x = Serial.read();

  digitalWrite(41, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(40, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(39, LOW);while(!Serial.available()){}x = Serial.read();  
  //digitalWrite(75, LOW);while(!Serial.available()){}x = Serial.read();
  PORTG = PORTG & 0xF7; while(!Serial.available()){}x = Serial.read();

  
}



  
void RTC(void){
char output_buffer[32]; ///< Temporary buffer for sprintf()
Serial.println(CLS);
Serial.println("\nReal Time Clock Test :");
Serial.println("for EXIT pres 0\n");

while (!DS3231M.begin()) // Initialize RTC communications
  {
    Serial.println(F("Unable to find DS3231MM. Checking again in 3s."));
    delay(3000);
  } // of loop until device is located
//DS3231M.adjust(DateTime(2020,03,25,18,42,55));
DS3231M.pinSquareWave(); // Make INT/SQW pin toggle at 1Hz  
static uint8_t secs = 0;
DateTime now = DS3231M.now(); // get the current time from device

while((Serial.read() != '0')){
  if (secs != now.second())     // Output if seconds have changed
  {
    sprintf(output_buffer,"%04d-%02d-%02d %02d:%02d:%02d", now.year(), now.month(),  now.day(), now.hour(), now.minute(), now.second());
    Serial.print("DATE and TIME : ");Serial.print(output_buffer);
    Serial.write(cr);    
    secs = now.second(); // Set the counter variable    
  } // of if the seconds have changed
now = DS3231M.now();  
}
}

void PWM(void){

pinMode(8, OUTPUT);


Serial.println("\nPWM =31Hz-a on Current CH5\n");
 TCCR4B &= ~7;
 TCCR4B |= 5;  
 analogWrite(8, 63);

delay(2000);

 analogWrite(8, 63);

 Serial.print(3);
   analogWrite(8, 63);
 TCCR4B &= ~7;
 TCCR4B |= 3;  
 delay(4000);

Serial.print(2);
    analogWrite(8, 63);
 TCCR4B &= ~7;
 TCCR4B |= 2;  
 delay(4000);
 
Serial.print(1);
    analogWrite(8, 63);
 TCCR4B &= ~7;
 TCCR4B |= 1;  
 delay(4000);
}


void Flow(void){
float sensorValue = 0.01;  // variable to store the value coming from the sensor
initialiseIrrFlowMet();

while ( (Serial.read() != '0')){
  Serial.print(CLS);
  Serial.println("Flow Measuring Test: \n");
  Serial.println("Trimovati potencimetar PT2, za 270Hz se dobije protok od 8.2 L/s!");
  Serial.println("      ILI                 , za 250Hz se dobije protok od 7.5 L/s!");
  Serial.println("                          , za 31Hz se dobije protok od 0.93 L/s!");

    
 sensorValue = getCurrentFlow( FlowSensorCH5);
Serial.print(F("FLOW: ")); Serial.print(sensorValue);Serial.print(F("L/s")); 
delay(1000);
 }  
}


void FlowPWM(void){
Serial.print(CLS);
Serial.println("PWM generator + FLOW testing for CH5. With 't' and 'T' +/- PWM freq.");
initialiseIrrFlowMet();
int cnt = 0;
int  td = 1;
char x = ' ';
  while ( ( x != '0')){
    x = Serial.read();
    if(x == 't') {td++;}
    if(x == 'T') {td--;}
    digitalWrite(4,HIGH);
    delay(td);
    digitalWrite(4,LOW); 
    delay(3);
    cnt++;
    if (cnt == 2500) {
      Serial.print(CLS);                                        //FlowSensorCH5
      Serial.print("Flow [L/min] :"); Serial.println(getCurrentFlow(4)*60 );
      Serial.print("Volume   [L] :");Serial.println(getCurrentVOL(4));
      Serial.print("td : "); Serial.println(td);
      cnt =0;
    }
   }
}

void pH(void){
 int sensorValue = 0;  // variable to store the value coming from the sensor
 float val;
 char buff[3];
 
 Serial.print(CLS);
 Serial.println("pH  Test, Calibrate PT1=10kohm, for 0Vin, ADC = 1.94V. With C enter in calibration mode\n");
 Serial.println("ADC reading:");

 pHmeasuring.ferphK      = 8.81;  // Y = k*X + M
 pHmeasuring.ferphM     =-9.87; 
char x='x';
String tmp="111";  
while ( x != '0'){
  x = Serial.read();  
  sensorValue = analogRead(A2);
  val = ((float)sensorValue * 5) / 1023 ;
  dtostrf(val, 1, 2, buff);
  Serial.write(cr); Serial.print("ADC "); Serial.print(sensorValue); Serial.print(" -> ");Serial.print(val);Serial.print("V,  pH = ");Serial.print(getPh()); Serial.print(" [K,M] "); Serial.print(pHmeasuring.ferphK );Serial.print(pHmeasuring.ferphM );
  delay(1000);
  if(x == 'c'){
    Serial.setTimeout(3500);
    Serial.print(CLS);
    Serial.write(cr); 
    
    Serial.print("factor K:");  
    tmp = Serial.readStringUntil('\n'); 
    Serial.println(tmp);
    pHmeasuring.ferphK = tmp.toFloat(); 
    
    Serial.print("factor M:");  
    tmp = Serial.readStringUntil('\n') ;
    Serial.println(tmp); 
    pHmeasuring.ferphM = tmp.toFloat(); 
  }
}
}


void RS485(void){
 char x = 'x';
 Serial.print(CLS);
 Serial.println("RS485 test :\n");
 Serial.println("Chose chanel from 1 to 4, or 0 for EXIT :");
 Serial2.begin(9600);
 RS485setup();
     
 while ( x != '0'){
  x = Serial.read();
 RS485writeStringLN("ABCD____EFG");
  delay(800);
  Serial.println(RS485readString());
  delay(800);
 } 
}




void CurrentIN_OUT(void){
int pulse = 25;
int pause = 75;  //frequency 100Hz.
int sensorValue;
char x = 'x';
char buff[3];
float val;
int i=0;
  
Serial.println(CLS);
Serial.println("Current IN test. Set some Current source from another board."); 
 
while (Serial.read() != '0'){
     
        i=0;  
        Serial.println(CLS);
          
        sensorValue = analogRead(A12);  //CH1
        val = (float)sensorValue / 25 ;
        dtostrf(val, 2, 1, buff);
        Serial.print("Current IN on CH1 = ");Serial.print(val);Serial.println(" mA");
      
        sensorValue = analogRead(A13);  //CH2
        val = (float)sensorValue / 25 ;
        dtostrf(val, 2, 1, buff);
        Serial.print("Current IN on CH2 = ");Serial.print(val);Serial.println(" mA");        

        delay(1000);
  
}  
}





void Temperature(void){

TemperaturePT1000.begin(MAX31865_3WIRE);  // set to 2WIRE or 4WIRE as necessary
Serial.println(CLS);

while(Serial.read() != '0'){

   Serial.println("\033[0;0H");
   Serial.println("Temperature Test :\n");
  
   uint16_t rtd = TemperaturePT1000.readRTD();
 // Serial.print("RTD value: "); Serial.println(rtd); 
  float ratio = rtd;
  ratio /= 32768;
  //Serial.print("Ratio = "); Serial.println(ratio,2);
  Serial.print("Resistance = "); Serial.print(RREF*ratio,1);Serial.println(" ohm");
  Serial.print("Temperature = "); Serial.print(TemperaturePT1000.temperature(RNOMINAL, RREF));Serial.println(" Celsus");

  // Check and print any faults
  uint8_t fault = TemperaturePT1000.readFault();
  if (fault) {
    Serial.print("Fault 0x"); Serial.println(fault, HEX);
    if (fault & MAX31865_FAULT_HIGHTHRESH) {
      Serial.println("RTD High Threshold"); 
    }
    if (fault & MAX31865_FAULT_LOWTHRESH) {
      Serial.println("RTD Low Threshold"); 
    }
    if (fault & MAX31865_FAULT_REFINLOW) {
      Serial.println("REFIN- > 0.85 x Bias"); 
    }
    if (fault & MAX31865_FAULT_REFINHIGH) {
      Serial.println("REFIN- < 0.85 x Bias - FORCE- open"); 
    }
    if (fault & MAX31865_FAULT_RTDINLOW) {
      Serial.println("RTDIN- < 0.85 x Bias - FORCE- open"); 
    }
    if (fault & MAX31865_FAULT_OVUV) {
      Serial.println("Under/Over voltage"); 
    }
    TemperaturePT1000.clearFault();
  }
  Serial.println();
  delay(500);  
  }  
}

void Humidity_Temperature(void){
char x = 'x';
  float h,t;
  
while((x != '0')){
   Serial.println("\033[0;0H");
     Wire.end();  // u konfliktu dva I2C-a RTC i Temp-Humid senzor
     t = SHT1x_sensor.readTemperatureC();  
     h = SHT1x_sensor.readHumidity();
     Wire.begin();   
     Serial.print("Temperature:");   Serial.print(t,2); Serial.println("°C");
     Serial.print("humidity   :");   Serial.print(h,2); Serial.println(" %");
     x = Serial.read();
     delay(1500);  
  } 
}

void Conductivity(void){
#ifdef Sensor_EC   

Serial.println(CLS);
char x = 'x';

  setConductivityDriver(40, 1);
  
  setupTemperaturePT1000();
  temperature = SHT1x_sensor.readTemperatureC(); 
 Serial.println("press 'O' for Offset Calibration. Triming until ADC ==496 (2.41V)");
 int cnt = 0;
 while(!Serial.available() && (cnt++<25)) {delay(100);}
 x = Serial.read(); 
  if(x == 'o'){
       while(Serial.read() != '0'  ){
          Serial.write(cr); Serial.print("ADC6 : ");Serial.print(analogRead(A6));
          delay(200);
      }    
    }
 Serial.println(CLS); 
 
 while(Serial.read() != '0'){
   Serial.println("\033[0;0H");  
   getELconductance();   
   //delay(4000);
  }   
#endif   
}


void UART3(void){
  
 char x = 'x', z='z';
 Serial.print(CLS);
 Serial.println("UART3 LoopBack Test :\n");
 Serial.println("Short Connect RX and TX on Blue Connector");
 
 Serial3.begin(115200); 
 
  while((x != '0')){
     while(!Serial.available()){}
     x = Serial.read();
     Serial3.print(x); 
     delay(20); 
     z = Serial3.read();
     delay(20);
     Serial.print(z);  
     delay(20);  
  } 
}


void charging(void){
  int sensorValue =0;
 char x = 'x', z='z';
 Serial.print(CLS);

 while((Serial.read() != '0')){
   Serial.print("\033[0;0H");
   Serial.println("Charger test, c-charging C-discharging, b-battery status \n");
   sensorValue = analogRead(A8); Serial.print("Battery voltage: "); Serial.print((float)sensorValue *0.01472 , 3 );Serial.println(" V");
   sensorValue = analogRead(A9); Serial.print("Solar   voltage: "); Serial.print((float)sensorValue *0.0280  , 3 );Serial.println(" V");
   delay(1000);
   x = Serial.read();
   if (x=='c'){
      digitalWrite(26, HIGH);
      Serial.println("Charging.....");
    }
     if (x=='C'){
      digitalWrite(26, LOW);
      Serial.println("DisCharging.....");
    }
      if (x=='b'){
       digitalWrite(26, LOW);
       Serial.println("Battery status :"); 
      while ((Serial.read() != '0')){      
        Serial.write(cr);
        Serial.print((float)analogRead(A8) *0.01472,3); Serial.print(" V");
        delay(800);
        }
    }     
 }
 digitalWrite(26, LOW);
}



void GSMsetReset(void){
Serial.println("GSM Set/Restet with H & L ");  
char x='x';
while((x != '0')){ 
x = Serial.read(); 
if(x == 'h') {digitalWrite(7, HIGH); Serial.println("GSM Reset = H"); }   
if(x == 'l') {digitalWrite(7, LOW);  Serial.println("GSM Reset = L");}

delay(700);
}

}

void SMS_test(void) {  
bool gsm_started = false;
char x = 'x';
char c;
Serial.println(CLS);
Serial.println("SMS Test");
  if (gsm.begin(9600))
  {
    Serial.println("\nGSM READY:\n");
    inet.attachGPRS("prepaidnet","mts","gprs");  
    Serial.println("Sending SMS: 0 for NO, 1 for YES:");
    while(!Serial.available()){}
    x = Serial.read(); 
    if (x == '1'){sms.SendSMS("0646116952", "SENS Controler Sendign SMS");}   
    if (x == '0'){return;}
  }
  else
  {
    Serial.println(F("\nstatus=IDLE"));
  }
    while (c = gsm.read()) {
      Serial.print(c);
    }
}

void OperaterChanging(void){
 Serial.println(CLS);
 Serial.println(F("Operater Detecting Test")); 
 Serial.println(F("GSM connecting.....")); 
 if (gsm.begin(9600)) 
  {
    gsm_started = true;
    Serial.println(F("\GSM = READY"));
    }
  else
  {
    gsm_started = false;
    Serial.println(F("\GSM = IDLE")); 
  }

doesOperaterChanging();

EEPROM_readAnything(EERPOM_mem_loc_CarrierData, carrierdata);
    char tmp[2]="00"; 
    Serial.println("\n---------------------------");    
    Serial.println("Sadrzaj EEPROM-a : ");   
    EEPROM_readAnything(64, tmp);
    Serial.println(String(tmp).c_str());     
    Serial.println("Data in the Carrier Data Structure");
    Serial.println(carrierdata.apnname);
    Serial.println(carrierdata.apnuser);
    Serial.println(carrierdata.apnpass);

delay(1500);
}

void GSM_Connecting(void){ 
  byte sim_restart_pin = 7;
 Serial.println(CLS);
 Serial.println(F("GSM/GPRS Connecting Test"));

  pinMode(sim_restart_pin, OUTPUT);
  digitalWrite(sim_restart_pin, HIGH);
  delay(500);
  digitalWrite(sim_restart_pin, LOW);
  delay(500);
  digitalWrite(sim_restart_pin, HIGH);

  Serial.println(F("Please Wait"));
  unsigned long tmp =  currentTime();
 if (gsm.begin(9600)) 
  {
    gsm_started = true;
    Serial.println(F("\GSM = READY"));
    }
  else
  {
    gsm_started = false;
    Serial.println(F("\GSM = IDLE")); 
  }
  
if (gsm_started)
  {
    //GPRS attach, put in order APN, username and password.
    if (inet.attachGPRS("gprswap", "mts", "064"))
//  if (inet.attachGPRS("internet", "telenor", "gprs"))
//  if (inet.attachGPRS(String(carrierdata.apnname).c_str(), String(carrierdata.apnuser).c_str(), String(carrierdata.apnpass).c_str()))
    {
     Serial.println(F("GPRS = ATTACHED"));     
     gsm.SimpleWriteln("AT+CIFSR");//Read IP address.
     delay(5000);     
     gsm.WhileSimpleRead();//Read until serial buffer is empty
    }
    else
    {
      Serial.println(F("GPRS = ERROR"));
    }
     
    
  }
else
  {
   Serial.println(F("GSM&GPRS=NOTstarted"));
  }
 Serial.println( currentTime() - tmp);  
char x='x';  
while((x != '0')){ x = Serial.read(); }
 

}


void GPRSdataExchange(void){  
 Serial.println(CLS);
 Serial.println(F("GPRS Data Exchange with WEB (Tresnja Zones)"));   
char msg[1000];

while ((Serial.read() != '0')){ 
unsigned long tmp =  currentTime();  
  memset(msg, 0, sizeof(msg));                                            //Tresnja Zones
  inet.httpPOST("app.smartwatering.rs", 80, "/api/post/sync_device.php", "did=rjTXqd&t1=1.11&t2=2.22&t3=3.33&m1=0.00&m2=0.00&m3=0.00&mid=&fid=&ferph=174&cfl=2.22&tfv=3.33&ecv=4.44&iids=", msg, 1000);  
  Serial.println(msg);
  Serial.print("Sync Time: ");Serial.println(currentTime() - tmp);   

}
}


void ArduinoStandBy(void){
#ifdef Power_Saving  
  PowerStandBy(10); // 10sekundi 
#endif  
}
//--------------------------------------------------------------------------------------------
void sentek(void){


  RS485setup();
  setSANTEKsensors();  

Serial.println(CLS); 
while(Serial.read() != '0'){  

  reqSENTEKpresetHoldReg_Temperature();
  delay(150);
  Serial.print("Temp     :");Serial.print(getSENTEKtemperature(1),2);  Serial.print(" , ");Serial.print(getSENTEKtemperature(2),2);Serial.print(" , ");Serial.println(getSENTEKtemperature(3),2); 
//  Serial.print("          ");Serial.print(getSENTEKtemperature(4),2);  delay(200);Serial.print(" , ");Serial.print(getSENTEKtemperature(5),2);delay(200);Serial.print(" , ");Serial.println(getSENTEKtemperature(6),2); 
//  delay(50);

  reqSENTEKpresetHoldReg_Moisture();
  delay(800);
  Serial.print("Moisture :");Serial.print(getSENTEKmoisture(1),2); Serial.print(" , ");Serial.print(getSENTEKmoisture(2),2);Serial.print(" , ");Serial.println(getSENTEKmoisture(3),2);
//  Serial.print("          ");Serial.print(getSENTEKmoisture(4),2); delay(200);Serial.print(" , ");Serial.print(getSENTEKmoisture(5),2);delay(200);Serial.print(" , ");Serial.println(getSENTEKmoisture(6),2);


//  delay(200);
  reqSENTEKpresetHoldReg_Salinity();
  delay(800);
  Serial.print("Salinity :");Serial.print(getSENTEKsalinity(1),2);Serial.print(" , ");Serial.print(getSENTEKsalinity(2),2);Serial.print(" , ");Serial.println(getSENTEKsalinity(3),2);  
// Serial.print("          ");Serial.print(getSENTEKsalinity(4),2);delay(40);Serial.print(" , ");Serial.print(getSENTEKsalinity(5),2);delay(40);Serial.print(" , ");Serial.println(getSENTEKsalinity(6),2);   


  }

}


void Power_StandBy_Testing(void){
#ifdef Power_Saving
Serial.println(CLS);
Serial.println(F("Power Sleep Test.")); delay(200);
PowerStandBy(9);
PowerWakeUP();
Serial.print(F("Press any Key."));
while (Serial.available()==0) {}
  
 
//#define SleepTime 60   
//   Serial.println(CLS);
//   Serial.println(F("Power Handling.")); 
//   Serial.println(F("d) uC power down"));
//   Serial.println(F("u) power up "));
//   Serial.println(F("g) GSM power down"));
//   Serial.println(F("G) GSM power UP"));
//   Serial.println(F("p) Power of GSM down"));
//   Serial.println(F("P) Power of GSM UP"));
//   char x = 'x';
//   unsigned long temp =  currentTime();
//   while ((x != '0')){ 
//      x = Serial.read();
//      if (x == 'd') { temp = currentTime(); PowerStandBy(SleepTime +1); Serial.print("StandBY time:");Serial.println( currentTime() - temp ); } 
//      if (x == 'u') { PowerWakeUP();} 
//      if (x == 'p') { DDRE  = DDRE  | 0x04; PORTE = PORTE & 0xFB;   } 
//      if (x == 'P') { DDRE  = DDRE  | 0x04; PORTE = PORTE | 0x04;   } 
//      if (x == 'g') { DDRH  = DDRH  | 0x20; PORTH = PORTH & 0xDF;} 
//      if (x == 'G') { DDRH  = DDRH  | 0x20; PORTH = PORTH | 0x20;}       
//      if (x == '0') { return;} 
//      delay(100); 
//    }    
#endif  
}

//------------------------------------------------------------------------------------------------------------
void loop(){  
 char x = 'x';


Serial.println(CLS);

Serial.println(F("\n Testing SENS R001 board\n\n"));


Serial.println(F("1) RTC"));
Serial.println(F("2) Flow"));
Serial.println(F("3) Flow  with PWM"));
Serial.println(F("4) PWM generate"));
Serial.println(F("5) pH"));
Serial.println(F("6) RS485"));
Serial.println(F("7) Current IN/OUT"));
Serial.println(F("8) GSM Set Reset"));
Serial.println(F("9) SMS"));
Serial.println(F("o) Operater Detecting"));
Serial.println(F("c) GSM Connecting"));
Serial.println(F("d) GPRS Data Exchange"));
Serial.println(F("h) Humidity and Temp  I2C"));
Serial.println(F("t) Temperature"));
Serial.println(F("e) EC"));
Serial.println(F("s) SENTEK "));
Serial.println(F("u) UART3-Loopaback"));
Serial.println(F("r) Relays ObeByOne"));
Serial.println(F("a) Arduino Stand By"));
Serial.println(F("b) Li-Po Charger"));
Serial.println(F("w) Power Stand-by"));
Serial.println(F("i) Charging"));
Serial.println(F("f) Fertilisation"));
Serial.println(F("l)Fert EC_PID_Test"));

 while(!Serial.available()){}
 x = Serial.read();

     switch (x){  
       case '1':RTC(); break;
       case '2':Flow();break; 
       case '3':FlowPWM();break; 
       case '4':PWM();break;
       case '5':pH();break;
       case '6':RS485();break;           
       case '7':CurrentIN_OUT();break;
       case '8':GSMsetReset();break;
       case '9':SMS_test();break;          
       case 'o':OperaterChanging();break;
       case 'c':GSM_Connecting();break;
       case 'd':GPRSdataExchange();break;           
       case 'h':Humidity_Temperature();break;
       case 't':Temperature();break;       
       case 's':sentek();break;
       case 'e':Conductivity();break;
       case 'u':UART3();break;           
       case 'r':Relays_OneByOne();
       case 'a':ArduinoStandBy();break;
       case 'w':Power_StandBy_Testing();break;

       case 'i':charging();break;
             
       default:break;               
     }             
 
}

static uint8_t currentTime()
{
  RTCnowTime = DS3231M.now(); 
return RTCnowTime.second();
}
