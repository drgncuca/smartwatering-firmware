#include <EEPROM.h>
#include "EEPROMAnything.h"
#include <SoftwareSerial.h>
#include <Wire.h>
#include <TimeLib.h>
#include <ArduinoJson.h>
#include "SIM900.h"
#include "ds3231.h"
#include "inetGSM.h"
#include "sms.h"
#include <String.h>
#include "Telemetry.h"					
#include <SPI.h>
#include <Adafruit_MAX31865.h>

#define FirwareVersion "FV107"

#ifdef Sensor_EC
  Adafruit_MAX31865 TemperaturePT1000 = Adafruit_MAX31865(49);
#endif

#ifdef INTERRUPTFLOW
  #include <FlowMeter.h>
#endif  

InetGSM inet;
//TODO Check max size of buffer
StaticJsonBuffer<650> jsonBuffer;
int numdata;
boolean gsm_started = false;


bool isperm3 = false;
bool isautomode = false;
bool is_some_irrigation_on = false;

char server[] = "app.smartwatering.rs";

//char parcel_id[] = "rjTXqd"; // Testing Single Device FERT+Zone, Kruska NS Dragan Cuca!!
  
//char parcel_id[] = "MGTnV5"; //FERT DID for TESTING  (DID) // Testing multi device
//char parcel_id[] = "NmL73F"; //Zone DID for TESTING  (DID) // Testing multi device

//char parcel_id[] = "A4dgm";  // Lesnik Zvornik  (PID)
//char parcel_id[] = "yuIFb";  //seskejabuke
//char parcel_id[] = "HxK8jk"; //Aleksandar Rancic, Borovnice, FERT 
//char parcel_id[] = "TRZyr";  //Darko Ristic, Borovnica Ub, PID
//char parcel_id[] = "HIsWGI"; //Ivica Todorovic Borovnica. DID
//char parcel_id[] = "IS7Zj"; //Delta Agrar, Jabuka, Tornjas. PID , DC-ventili
//char parcel_id[] = "qWJdQZ"; //Borovnica Trlić FERT 10 Zones
//char parcel_id[] = "LgomjY"; //Borovnica Trlić Meteo 1 : 
//char parcel_id[] = "tCHkuC"; //Borovnica Trlić Meteo 2 :  
//char parcel_id[] = "JXUnNw"; //Zemlja i Sunce - Parcela Ivan
//char parcel_id[] = "gHduMG";//Borovnica Aleksandar Uzice, DC Relays permanent ON/OFF [DEL-13]
//char parcel_id[] = "sfWSqk";//Kupina Andrija Jelinac 4 zone AC [DEL-5]
//char parcel_id[] = "LgomjY";//Trlic FERT [DEL-1] pH, EC, SENTEK, STH20.
//char parcel_id[] = "tCHkuC";//Trlic FERT [DEL-1] SENTEK, STH20.
//char parcel_id[] = "aQjkKo";// Tornjos II
//char parcel_id[] = "ffCIYQ";//Darko Smiljkovski - Lesnik
//char parcel_id[] = "eD1BdU";//Sava Coop testna stanica
//char parcel_id[] = "8taxAn";//Damir Bulj
//char parcel_id[] = "8YOyxi";//Vladan Redzic Ecoland JAbuka  (Vlaški do)
//char parcel_id[] = "7aCids";//Radoica Radomirovic - Borovnica (Leposavic, Kosovo)
//char parcel_id[] = "B7FmJb";//Marko Smederevo : Zone 1-4
//char parcel_id[] = "M9xswP";//Marko Smederevo : Zone 5-8 
//char parcel_id[] = "9YyhWN";//Milos Aleksic Lesnik 
//char parcel_id[] = "7m4wBq";//Aleksandar Rancic, Borovnica, Zones 
//char parcel_id[] = "uPt2he";//Moshe Lifhitz, Borovnica, SENS
//char parcel_id[] = "tIzUOI";//Ivica Donji Racnik
//char parcel_id[] = "OqLzTt";//Ivica Donji Racnik
//char parcel_id[] = "qTY5yN";//DEL-28
//char parcel_id[] = "s4IUok";//Strahinja Malin START Airplant Cortanovci
//char parcel_id[] = "TkEoPS";//Poljoprivredni Fax

//char parcel_id[] = "zGEuDZ";//Delta Agrar Sveti Nikola Sečanj DEVICE 1 //R002 A003 deviceid=107
//char parcel_id[] = "vjo263";//Delta Agrar Sveti Nikola Sečanj DEVICE 2 //A005
//char parcel_id[] = "rY1mCn";//Delta Agrar Sveti Nikola Sečanj DEVICE 3 //A006
//char parcel_id[] = "Lt9d7b";//Delta Agrar Sveti Nikola Sečanj DEVICE 4 //A008
//char parcel_id[] = "boNWoI";//Delta Agrar Sveti Nikola Sečanj DEVICE 5 //A009
//char parcel_id[] = "zIFTit";//Delta Agrar Sveti Nikola Sečanj DEVICE 6 //A010
//char parcel_id[] = "Uga2OJ";//Delta Agrar Sveti Nikola Sečanj DEVICE 7 //A011
//char parcel_id[] = "Mbcive";//Delta Agrar Sveti Nikola Sečanj DEVICE 8 //A012
//char parcel_id[] = "kfmwh9";//Delta Agrar Sveti Nikola Sečanj DEVICE 9 //A014  

//char parcel_id[] = "XdK9h4";//Marko Radovanovic Kragujevac    
//char parcel_id[] = "GrcTsk";//Petar Zuvić 
char parcel_id[] = "JxRKeA";//Test Veternik SENS1
//char parcel_id[] = "1Ua2n1";//Test Veternik SENS2  
//char parcel_id[] = "yjNu3H";//Marija Mijailović
//char parcel_id[] = "fjCK1j";//Nenad Čanić
//char parcel_id[] = "tXebTy";//AQ Manojlović Duško Manojlović
//char parcel_id[] = "dvVKzZ";//AQ Manojlović Duško Manojlović

//char parcel_id[] = "95CgxD";//Goran Miljkovic Migor Speacers
//char parcel_id[] = "PPHt5z";//Goran Miljkovic Migor Speacers
//char parcel_id[] = "QjRjN7";//Goran Miljkovic Migor Speacers

//char parcel_id[] = "krxA4Y";//Delta Agrar Čelarevo
//char parcel_id[] = "lzk0YK";//Delta Agrar Čelarevo
//char parcel_id[] = "kOCZrz";//Delta Agrar Čelarevo
//char parcel_id[] = "Lqrofw";//Delta Agrar Čelarevo

//char parcel_id[] = "JKrY3o";//Nemanja Jovanović

//char parcel_id[] = "U8a9XV";//A001 Goran Jašin MK Group PIK Bečej R003 A001
//char parcel_id[] = "hfN3iI";//A002 Goran Jašin MK Group PIK Bečej R003 A002 
//char parcel_id[] = "D73Jl0";//A003 Goran Jašin MK Group PIK Bečej R003 A003
//char parcel_id[] = "HxKErO";//A004 Goran Jašin MK Group PIK Bečej R003 A004
//char parcel_id[] = "7PatOa";//A005 Goran Jašin MK Group PIK Bečej R003 A005
//char parcel_id[] = "y1O0PF";//A006 Goran Jašin MK Group PIK Bečej R003 A006
//char parcel_id[] = "YzN8jm";//A007 Goran Jašin MK Group PIK Bečej R003 A007
//char parcel_id[] = "cR1PYT";//A008 Goran Jašin MK Group PIK Bečej R003 A008
//char parcel_id[] = "Zr28Mq";//A009 Goran Jašin MK Group PIK Bečej R003 A009
//char parcel_id[] = "qrevcX";//A010 Goran Jašin MK Group PIK Bečej R003 A010
//char parcel_id[] = "Mux6am";//A011 Goran Jašin MK Group PIK Bečej R003 A011
//char parcel_id[] = "kjWNOY";//A012 Goran Jašin MK Group PIK Bečej R003 A012
//char parcel_id[] = "0RGhXy";//A013 Goran Jašin MK Group PIK Bečej R003 A013
//char parcel_id[] = "6xkCW2";//A014 Goran Jašin MK Group PIK Bečej R003 A014

//char parcel_id[] = "ortwRO";//BBerry Tomislav Žarkovac	
//char parcel_id[] = "epJtIi";//BBerry Tomislav Žarkovac	
//char parcel_id[] = "JKrY3o";//Nemanja Jovanović
//char parcel_id[] = "s3FShj";//Srdjan Božović
//char parcel_id[] = "BahMAH";//Selma Matanovic
//char parcel_id[] = "ir9FQo";//Aleksandar Milic

//char parcel_id[] = "luk6Kj";//Matijevic doo Novo Orahovo
//char parcel_id[] = "Le1mYx";//Matijevic doo Novo Orahovo
//char parcel_id[] = "609S2Y";//Matijevic doo Novo Orahovo
//char parcel_id[] = "xSgGWi";//Matijevic doo Novo Orahovo
//char parcel_id[] = "lWQ4ym";//Matijevic doo Novo Orahovo
//char parcel_id[] = "gjtYi4";//Matijevic doo Novo Orahovo						

byte number_of_zones = 4;//

#ifdef Power_Saving
byte PowerStandByTimeInMin = 0;
#endif

#ifdef LiPo_Charger
byte BatterySupplying = 0;
float BatteryVoltage = 0;
byte BattCharging = 0; // Status  baterije
int BatteryCChargingCurrent = 0;
#endif

struct irrigation_struct
{
  unsigned int duration = 0; // vreme trajanja navodnjavanja u sekundama
  unsigned long amount = 0L; // Kolicina vode u litrima
  byte status[4] = {0, 0, 0, 0}; // 0 = off, 1 = on
  unsigned long started_time =  0L;// u sekundama
//  unsigned int max_duration = 0; // max vreme navodnjavanja po zapremini, zastitni parametar.
  String irrigation_id = "";
  float current = 0;// kolicinu vode u litrima.
  //float current = 0L; 
} zonal_irrigation_struct;


//pH global variabla
pH_struct pHmeasuring;

irrigation_struct  zonal_irrigations[NUM_SLOTS];

irrigation_struct_FlowMetering  irrigationFlowMetering ; // Globalna promenljiva koja sadrzi informacije o protekloj kolicini VODE na 5 kanalala flowMeter-a i proteklo vreme.

#ifdef DC_Valves  //DC1_P  2   3   4
  byte valves_on[]  = {33, 31, 41, 39};
                   //DC1_N  2   3   4
  byte valves_off[] = {32, 30, 40, 75};
  int valvedelay = 100; // neka vrednost po difoultu u slucaju da zakaze komunikacija sa serverom 
#endif
 
#ifdef FLOW_Toggling
  byte flow_pin_Toggling = 15;// za FERT -> UART3_RX, na FERT1 PCB-u je oznacen sa: UART3_TX. 
//  byte flow_pin_Toggling = 72;// za SENS -> FLOW.
#endif

float el_conductance = 0;

//----------Temperature and Humidity-----
#ifdef SHT1sensor
  float temperature;
  float humidity;
  SHT1x SHT1x_sensor(   20  ,   21);
#endif

float ph_value = 0;

#define sim_restart_pin  7
#define arduino_restart_pin  12

String params = "";
//TODO: Check max size of the message and reduce it's size accordingly
char msg[MSG_BUF]; 

carrier carrierdata;
struct_operater_vip operaterVIP;
struct_operater_MTS operaterMTS;
struct_operater_Telenor operaterTelenor;
struct_operater_BHmob operaterBHmob;									

// last time successfully contacted server
unsigned long last_seen = 0L;
#ifdef INTERRUPTFLOW
	// fmeter sensor
	double flowmeter_capacity = 30;
	double flowmeter_kfactor = 4.25;
	double flowmeter_mfactor = 1;

	FlowSensorProperties flowmeter_sensor = {flowmeter_capacity, flowmeter_kfactor, {flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor}};
	FlowMeter Meter = FlowMeter(2, flowmeter_sensor);
	long flowmeter_period = 1000; // one second (in milliseconds)
	long flowmeter_lasttime = 0;

void MeterISR()
{  
  Meter.count();// let our flow meter count the pulses
}
#endif

#if defined FLOW || defined INTERRUPTFLOW					   
struct flowmeter_struct
{
  float currentflow;
  float currentvolume;
};
#endif

bool carrierChange = false;

#ifdef TIME_F_Measure
unsigned long currentTimeMilles;
#endif

#ifdef RainFallSensor
volatile unsigned int RainFallCaunterTich = 0;
#endif

//--------------------------------------------------------------------------------------
void setup()
{ 
Serial.begin(115200); // Serial connection. 

initiWD(); //disabled by default
 
#ifdef LiPo_Charger 
  pinMode(36, OUTPUT); //Battery_Charger Supply EN
  digitalWrite(36, HIGH);
#endif
  
  delay(100);
  PORTE = PORTG & 0xFB; // GPRS_PWR_EN <LOW>
  DDRE  = DDRG  | 0x04; // GPRS_PWR_EN <OUT>   
  delay(100);
  PORTE = PORTG | 0x04; // GPRS_PWR_EN <HIGH>
  
  pinMode(26, OUTPUT); //Battery Measure ON/OFF  //  R002:5V0_Relay_PWR_EN
  pinMode(29, OUTPUT); //PWR_LED RED
  pinMode(24, OUTPUT); //5V0_SYS_PWR_EN 
  pinMode(23, OUTPUT); //5V0_I2C_PWR_EN
  digitalWrite(8, LOW); //off GPRS_PWR KEY
  pinMode(8, OUTPUT);  //GPRS_PWR KEY
  pinMode(34, OUTPUT);  //12V_SYS_PWR_EN
  pinMode(27, OUTPUT);  //5Vneg_PWR_EN
  pinMode(22, OUTPUT);  //5V0_LTE_PWR_EN
  pinMode(13, OUTPUT);  //OTA WC
 
  delay(100); 
  digitalWrite(23,LOW); //ON  
  digitalWrite(24,LOW); //ON
  digitalWrite(8, LOW); //off
  digitalWrite(29,HIGH); //ON
  digitalWrite(34,HIGH);//ON
  digitalWrite(27,LOW);//ON
  digitalWrite(22,HIGH); //OFF
  digitalWrite(26,HIGH); //Battery Measure ON
  digitalWrite(13,LOW);  //OTA WC

#ifdef Power_Saving
    pinMode(48, OUTPUT);  //LED1
    pinMode(47, OUTPUT);   
    pinMode(42, OUTPUT);  
    pinMode(17, OUTPUT); //LED4   
   
#endif

  digitalWrite(arduino_restart_pin, HIGH);
  pinMode(arduino_restart_pin, OUTPUT);
  
  pinMode(sim_restart_pin, OUTPUT);
  digitalWrite(sim_restart_pin, HIGH);  
  
  delay(100);  
  digitalWrite(8, HIGH); //on 
  delay(1050);  
  digitalWrite(8, LOW); //off
  delay(100);
  digitalWrite(sim_restart_pin, LOW);
  delay(150);
  digitalWrite(sim_restart_pin, HIGH);
  delay(500);

 #ifdef DC_Valves
   for (byte i=0; i<3;i++ )
  {
    pinMode(valves_on[i], OUTPUT);
    digitalWrite(valves_on[i],LOW);
    
    pinMode(valves_off[i], OUTPUT);
    digitalWrite(valves_off[i],LOW);
    
  }
  pinMode(39, OUTPUT);  // Relay DC4_P <OUT>
  digitalWrite(39,LOW); // Relay DC4_P <LOW>
	
  DDRG  = DDRG  | 0x08; // Relay DC4_N <OUT>  
  PORTG = PORTG & 0xF7; // Relay DC4_N <LOW>

#endif


#ifdef SENTEC_Include   
  RS485setup();  
  setSANTEKsensors(); 
  sensorSampledMask();	
 
#ifdef DEBUG_SENTEK_Write_to_Excel
  pinMode(6, INPUT_PULLUP); // JUMPER Near Relay and GND test point
#ifdef DEBUG_BASIC
    Serial.println(F("ADD Jumper to RUN Mode: SENTEK Write in Excel"));
#endif
    if (digitalRead(6) == 0) {
      goto SENTEK_GoTo;
    }
#endif
  
#endif

#ifdef DEBUG_BASIC
  Serial.print(F("\n\n(RE)start uC - FW Version : ")); Serial.println(FirwareVersion);
#endif 

  initialiseIrrFlowMet(); //REsetovanje Zapremine i globalnog proteklog vremena na 0 vrednosti.
  
   Wire.begin();
   delay(100);
#ifdef LiPo_Charger   
   LiPoChargeInit();  
   LiPoChargeManager();
#endif
       
#ifdef Sensor_EC  
  setConductivityDriver(70, 1); // setovanje SINusnog generatora: 70Hz, 1-> sinus.  
  setupTemperaturePT1000();// Setovanje temperaturnog senzora za PT1000.
#endif
//Parametri za merenje pH zemljista
pHmeasuring.ferphK = 1.75;
pHmeasuring.ferphM = 3.91;								   

  struct ts t;

  DS3231_get(&t); 

#ifdef WhatchDog
    if ( EEPROM.read(127) == 0){//Ako je pri navodnjavanju WD resetovao uC-e, u EERPOMu na lokaciji 127 bice vrednost 0.
	stopAll();
	Serial.println(F("WD resetovao uC-a, gasim zaostala navodnjavanja"));
	EEPROM.write(127, 255);// Nema zadatih navodnjavanja.....
	}
	#ifdef DEBUG_WhatchDog
	  pinMode(6, INPUT_PULLUP); // JUMPER Near Relay and GND test point
	#endif
#endif  

#ifdef DEBUG_BASIC
  Serial.print(F("GSM="));
#endif 
  
  if (gsm.begin(9600))
  {
    gsm_started = true;    
#ifdef DEBUG_BASIC
    Serial.println(F("READY"));
//	  gsm.SimpleWriteln(F("AT+CGMR="));//Firmware version
//	  gsm.WhileSimpleRead();
#endif 

//Pri Debagovanju, preskoci jedan deo koda, da ubrza startovanje uC-a
#ifdef SKIP_Operater_Detecting
    goto skip_operater_detecting;
#endif

  }
  else
  {
    gsm_started = false;
#ifdef DEBUG_BASIC
    Serial.println(F("IDLE"));
#endif
  
  }

doesOperaterChanging();// Promena operatera

EEPROM_readAnything(EERPOM_mem_loc_CarrierData, carrierdata);

#ifdef DEBUG_Operater 
    char tmp[2]="00"; 
    Serial.println("\n---------------------------");    
    Serial.println("Sadrzaj EEPROM-a : ");   
    EEPROM_readAnything(64, tmp);
    Serial.println(String(tmp).c_str());     
    Serial.println("Data in the Carrier Data Structure");
    Serial.println(carrierdata.apnname);
    Serial.println(carrierdata.apnuser);
    Serial.println(carrierdata.apnpass);        
#endif

if (gsm_started)
  {
    Serial.print(F("GPRS="));    
//Pri Debagovanju, preskoci jedan deo koda, da ubrza startovanje uC-a    
#ifdef SKIP_Operater_Detecting  
    skip_operater_detecting:
    Serial.print(F("GPRS="));  
    if (inet.attachGPRS("internet", "internet", "internet"))
    {
#else
    if (inet.attachGPRS(String(carrierdata.apnname).c_str(), String(carrierdata.apnuser).c_str(), String(carrierdata.apnpass).c_str()))
    {
#endif    
    
#ifdef DEBUG_BASIC
      Serial.println(F("ATTACHED")); 
#endif
#ifdef DEBUG
     gsm.SimpleWriteln("AT+CIFSR");//Read IP address.
     delay(5000);     
     gsm.WhileSimpleRead();//Read until serial buffer is empty

#endif
    }
    else
    {
#ifdef DEBUG_BASIC
      Serial.println(F("ERROR"));
#endif
    }
    delay(1000); 
  }
else
  {
#ifdef DEBUG_BASIC    
   Serial.println(F("GSM&GPRS=NOTstarted"));
#endif  
  }  
  
#ifdef WhatchDog
	enableWD(1); //E N A B L E    WatchDog
#endif

Serial.println(F("GSM time"));
gsm.SimpleWriteln("AT+CCLK?");
delay(1000);
gsm.WhileSimpleRead();

Serial.println(F("GPRS time"));
gsm.SimpleWriteln("AT+CIPGSMLOC=2,1");
delay(8000);
gsm.WhileSimpleRead();

Serial.println(F("FV"));
gsm.SimpleWriteln("AT+CGMR");
delay(8000);
gsm.WhileSimpleRead();
 
#ifdef FLOW_Toggling
   pinMode(flow_pin_Toggling, INPUT);
//   DDRE  = DDRE & 0xEF;  // Za SENS - FLOW input 
#endif
											  
#ifdef DEBUG_SENTEK_Write_to_Excel
   SENTEK_GoTo:
   // za SENTEK Kalibraciju MOD, gde se vrti samo petlja za uzorkovanje Salinity-a. Jumperom se prespoji UART3_RX na GND.
#endif	
											
#ifdef SENTEC_Include         
  reqSENTEKpresetHoldReg_Temperature();delay(400); 
  reqSENTEKpresetHoldReg_Moisture();delay(400); 
#endif
#ifdef SENTEK_Salinity   
  reqSENTEKpresetHoldReg_Salinity();delay(400); 
#endif	

#ifdef RainFallSensor
attachInterrupt(digitalPinToInterrupt(2), RainFallCaunter, RISING);
RainFallCaunterTich = 0;
#endif

#ifdef INTERRUPTFLOW
  // enable a call to the 'interrupt service handler' (ISR) on every rising edge at the interrupt pin
  attachInterrupt(INT7, MeterISR, RISING);
  // sometimes initializing the gear generates some pulses that we should ignore
  Meter.reset();
#endif

#ifdef FLOW_100L_imp
  // enable a call to the 'interrupt service handler' (ISR) on every rising edge at the interrupt pin
  attachInterrupt(INT7, flowImplseTick100L_Procesing, RISING);
#endif
setRTCtime();

};
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void loop()
{
#ifdef DEBUG_BASIC
  Serial.println(F("Usao u loop"));
#endif

#ifdef WhatchDog
	togleWD(); //togle WD
#endif


#ifdef LiPo_Charger 
  LiPoChargeManager(); 
#endif

  params = "";  
  params.concat("did=" + String(parcel_id));
//params.concat("pid=" + String(parcel_id));
  params.concat("&" + String(FirwareVersion));

//----------------------- SENTEK sensors 1, 2, 3 -------------------------
#ifdef SENTEC_Include
digitalWrite(34, HIGH);
delay(500);
        reqSENTEKpresetHoldReg_Temperature();
        //delay(200);  
        params.concat("&t1=" + String(getSENTEKtemperature(1))); //temperature
        //delay(200);  
        params.concat("&t2=" + String(getSENTEKtemperature(2)));
        //delay(200);  
        params.concat("&t3=" + String(getSENTEKtemperature(3)));  
        //delay(200);        
        reqSENTEKpresetHoldReg_Moisture();
        //delay(200);
        params.concat("&m1=" + String(getSENTEKmoisture(1))); //moisture
        //delay(200);        
        params.concat("&m2=" + String(getSENTEKmoisture(2)));
        //delay(200);
        params.concat("&m3=" + String(getSENTEKmoisture(3)));
       
#ifdef SENTEK_Salinity    
    #ifdef DEBUG_SENTEK_Write_to_Excel 		
		enableWD(0);//disable WD
    while (digitalRead(6) == 0){ 
    //while (1){         
                //delay(200);
			reqSENTEKpresetHoldReg_Salinity();
			delay(600);
			getSENTEKsalinity(1);//salinity                
			getSENTEKsalinity(2);                
			getSENTEKsalinity(3);                 
#ifdef SENTEK_Include_Extend_Sens
			getSENTEKsalinity(4); 
			getSENTEKsalinity(5); 
			getSENTEKsalinity(6); 
#endif
        }	
    #endif				   
        //delay(200);
        reqSENTEKpresetHoldReg_Salinity();
        //delay(200);
        params.concat("&s1=" + String(getSENTEKsalinity(1)));//salinity
        //delay(200);
        params.concat("&s2=" + String(getSENTEKsalinity(2)));
        //delay(200);
        params.concat("&s3=" + String(getSENTEKsalinity(3)));
        
   #endif  
 
#endif
//----------------------- SENTEK sensors 4, 5, 6 -------------------------
#ifdef SENTEK_Include_Extend_Sens
        reqSENTEKpresetHoldReg_Temperature();
        //delay(200);  
        params.concat("&t4=" + String(getSENTEKtemperature(4))); //temperature
        //delay(200);  
        params.concat("&t5=" + String(getSENTEKtemperature(5)));
        //delay(200);  
        params.concat("&t6=" + String(getSENTEKtemperature(6)));  
        //delay(200);
        reqSENTEKpresetHoldReg_Moisture();
        //delay(800);
        params.concat("&m4=" + String(getSENTEKmoisture(4))); //moisture
        //delay(200);
        params.concat("&m5=" + String(getSENTEKmoisture(5)));
        //delay(200);
        params.concat("&m6=" + String(getSENTEKmoisture(6)));
    #ifdef SENTEK_Salinity        
        delay(600);
        reqSENTEKpresetHoldReg_Salinity();
        //delay(50);
        params.concat("&s4=" + String(getSENTEKsalinity(4)));//salinity
        //delay(50);
        params.concat("&s5=" + String(getSENTEKsalinity(5)));
        //delay(50);
        params.concat("&s6=" + String(getSENTEKsalinity(6)));
    #endif
	
#endif

#ifdef Power_Saving    
  //digitalWrite(34, HIGH);//12V power ON
  #ifndef RainFallSensor // Merenje Inpulsa, pravi lazan interup

  #endif
  //digitalWrite(27, LOW);//-5V power ON
  //delay(1000);

#endif 

#if defined FLOW || defined INTERRUPTFLOW
  struct flowmeter_struct flow = checkFlowMeter(); //azurira vrednost protoka i zapremine
  checkValves(flow.currentvolume);
#else
  checkValves(0);
#endif
 
#ifdef INTERRUPTFLOW  
   Meter.reset();				 
#endif				   
#ifdef FLOW_Toggling 
   bool currentFlowStatus = checkFlow() ? 1 : 0;  // ako ima bilo kakvog protoka vraca '1'
#endif

  if(isautomode) {
    checkLastSeen();
  }

#ifdef Sensor_pH  
  ph_value = getPh();
#endif

#ifdef SHT1sensor
   Wire.end();  // u konfliktu dva I2C-a RTC i Temp-Humid senzor
   temperature = SHT1x_sensor.readTemperatureC();  
   humidity = SHT1x_sensor.readHumidity();
   Wire.begin();    
#endif

#ifdef Sensor_EC
  el_conductance = getELconductance(); 
#endif  

#ifdef Sensor_10HS_1
  params.concat("&h1=" + String(analogRead(A4))); 
#endif  
#ifdef Sensor_10HS_2
  params.concat("&h2=" + String(analogRead(A3)));
#endif  

#ifdef SHT1sensor
  params.concat("&ah=" + String(humidity));
  params.concat("&at=" + String(temperature));
#endif  
//  params.concat("&temp=" + String(0));
	  
#ifdef DC_Power_SENS
  params.concat("&pow=" + String( BatteryVoltage ));// Battery Voltage for SENS board
  params.concat("&sol=" + String(getVoltageSolar() ));// Solar Voltage for SENS board 
  if (BattCharging == 1) params.concat("&cc=" + String(BatteryCChargingCurrent));// Setovana struja punjenja
  else  params.concat("&cc=" + String("0.0"));// Setovana struja punjenja
#endif

#ifdef Sensor_BGT_WSD2
#ifdef Power_Saving  
  delay(5500);
#endif  
    params.concat("&ah=" + String(getBGThumidity()));
    params.concat("&at=" + String(getBGTtemperature()));
#endif  

#ifdef Sensor_pH
  params.concat("&ferph=" + String(pHmeasuring.rawPh));//salje se RAW pH.
#endif
 
#if defined FLOW || defined INTERRUPTFLOW 
  params.concat("&cfl=" + String(flow.currentflow));
  params.concat("&tfv=" + String(flow.currentvolume));  
#endif  

#ifdef FLOW_Toggling  
  params.concat("&cfl=" + String(currentFlowStatus));
#endif    

#ifdef FLOW_100L_imp 
if (isSomeIrigationON() == 1){
  params.concat("&cfl=" + String(getFlow100L()));
  params.concat("&tfv=" + String(getFlowImplseTick100L())); 
}
else {
  params.concat("&cfl=0.0");
  params.concat("&tfv=0.0");	
  resetFlowImpulseTick100L(); 	
}
#endif 


#ifdef Sensor_EC
  params.concat("&ecv=" + String(el_conductance));
//  params.concat("&ect=" + String(tempPT1000)); // Samao za debagovanje. Zakomenentarisati!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#endif

#ifdef WhatchDog
	togleWD(); //togle WD
#endif

#if defined(Power_Saving) && ( defined(WindDurection) || defined(WindSpeed) )
if (BatterySupplying == 1)  delay(1500);
#endif

#ifdef WindDurection
  params.concat("&wd=" + String(analogRead(A0)));
#endif

#ifdef WindSpeed 
int tmp =  analogRead(A1);
if (tmp<4) tmp = 0;
  params.concat("&ws=" + String(tmp));
#endif

#ifdef RainFallSensor
  params.concat("&rf=" + String(RainFallCaunterTich));
  RainFallCaunterTich = 0;
#endif
//------------------------------------------------------------------------------------------- SENSORS Reading END
#ifdef Power_Saving    
  if (BatterySupplying == 1){
  	digitalWrite(34, LOW); //12V_SYS_PWR power OFF
    digitalWrite(29, LOW); // Power LED OFF
	//digitalWrite(27, HIGH);// -5V power OFF
	digitalWrite(17, LOW);// LED Relay-4 OFF
	digitalWrite(42, LOW);// LED Relay-3 OFF
	digitalWrite(47, LOW);// LED Relay-2 OFF
	digitalWrite(48, LOW);// LED Relay-1 OFF
	
  }
  else {
    digitalWrite(29, HIGH); // Power LED ON
  }
#endif

  String activeirrigations;
  for (byte i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id != "")
    {
      activeirrigations.concat("," + String(zonal_irrigations[i].irrigation_id));
    }
  }
  activeirrigations.remove(0, 1);
  params.concat("&iids=" + String(activeirrigations));

#ifdef DEBUG_BASIC  
  Serial.println(params.c_str());
#endif

#ifdef WhatchDog
	togleWD(); //togle WD
#endif

  memset(msg, 0, sizeof(msg)); 

  apiCall(server, 80, "/api/post/sync_device.php", params.c_str(), msg, MSG_BUF);

char *jsonResponse = strstr(msg, "{");
    
#ifdef DEBUG_BASIC   
    Serial.println(jsonResponse);													   
#endif
//--------------------------------------------------------------------------------
  JsonObject &root = jsonBuffer.parseObject(jsonResponse);
//--------------------------------------------------------------------------------

  JsonVariant numberofzones_json = root["zones"];
  if (numberofzones_json.success())
  {
    if (number_of_zones != root["zones"])
    {
      number_of_zones = root["zones"];
    }
  }

if ( (number_of_zones<1) || (number_of_zones > 4)  ) number_of_zones = 4;																		 

#ifdef DC_Valves
  JsonVariant valvedelay_json = root["valvedelay"];
  if (valvedelay_json.success())
  {
    if (valvedelay != root["valvedelay"])
    {
      valvedelay = root["valvedelay"];
    }
  }
  if ( (valvedelay < 10) || (valvedelay > 1000) ) valvedelay =100;																  
#endif
//---------------------------------------------------------------------
  JsonObject &flowmeter_config = root["flowconf"];
  if (flowmeter_config.success())
  {
#ifdef FLOW
    irrigationFlowMetering.flowMeterCalibKons = flowmeter_config["kf"];
#endif

#ifdef INTERRUPTFLOW
bool flow_config_changed = false;
    if (flowmeter_config["cap"] != flowmeter_capacity)
    {
      flowmeter_capacity = flowmeter_config["cap"];
      flow_config_changed = true;
    }
    if (flowmeter_config["kf"] != flowmeter_kfactor)
    {
      flowmeter_kfactor = flowmeter_config["kf"];
      flow_config_changed = true;
    }
    if (flowmeter_config["mf"] != flowmeter_mfactor)
    {
      flowmeter_mfactor = flowmeter_config["mf"];
      flow_config_changed = true;
    }

    if (flow_config_changed)
    {
      FlowSensorProperties flowmeter_sensor = {flowmeter_capacity, flowmeter_kfactor, {flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor}};
      Meter = FlowMeter(2, flowmeter_sensor);
    }
#endif								 
  }

  JsonObject &start_irrigation = root["irr"];
  if (start_irrigation.success())
  {
    // TODO: check if this irrigationid is started
    if (strcmp(start_irrigation["irrigationid"].as<String>().c_str(), root["irr"]["iid"].as<String>().c_str()) != 0)
    {
      isautomode = start_irrigation["automode"];
      if (start_irrigation["zonal"])
      {
        if (start_irrigation["duration"])
        {
          //per time
          isperm3 = false;
          zonal_irrigation_start(start_irrigation["iid"].as<String>(), start_irrigation["duration"], start_irrigation["zones"]);
          //Serial.println("Per Time");
          //zonal_irrigation_start_DBG(start_irrigation["iid"].as<String>(), start_irrigation["duration"], start_irrigation["zones"]);
        }
        else
        {
          //per m3
          isperm3 = true;
          zonal_irrigation_start(start_irrigation["iid"].as<String>(), start_irrigation["amount"], start_irrigation["zones"], start_irrigation["maxduration"]);          
        }
      }
    }
  }

//-----------------------koeficijent za kalibraciju pH senzora-- Y = k*X + M -----------------------//
   JsonVariant ferphK_json = root["ferphk"];
    if (ferphK_json.success())
    {
      if (pHmeasuring.ferphK != root["ferphk"])
      {
          pHmeasuring.ferphK = root["ferphk"];
      }
    }

   JsonVariant ferphM_json = root["ferphm"];
    if (ferphK_json.success())
    {
      if (pHmeasuring.ferphM != root["ferphm"])
      {
          pHmeasuring.ferphM  = root["ferphm"];
      }
    }


//---------EC calbracioni koeficijent EC = k*X + m -----------------------------------------
#ifdef Sensor_EC
 JsonVariant EC_CalibrCoeff_json = root["eck"];
    if (EC_CalibrCoeff_json.success())
    {
      if ( EC_calibr_Coeff != root["eck"])
      {
           EC_calibr_Coeff = root["eck"];
      }
    }

 JsonVariant EC_CalibrCoeff_M_json = root["ecm"];
    if (EC_CalibrCoeff_M_json.success())
    {
      if ( EC_calibr_M_Coeff != root["ecm"])
      {
           EC_calibr_M_Coeff = root["ecm"];
      }
    }
#endif    

 
//-----------------------------------------------------------------------------------------------------//

  JsonVariant stop_irrigation_id_json = root["stopirr"];
  if (stop_irrigation_id_json.success())
  {    
    char irrid[5];
    strcpy(irrid, root["stopirr"].as<const char *>()); 
    stop_irrigation(irrid);    
  }
//--------------------------------------------------------------------------------																				  
#ifdef Power_Saving

 JsonVariant PowerStandByTime_json = root["slp"];
    if (PowerStandByTime_json.success())
    {
      if (PowerStandByTimeInMin != root["slp"])
      {
          PowerStandByTimeInMin = root["slp"];
      }
      if ((PowerStandByTimeInMin <0) || (PowerStandByTimeInMin > 90 )) PowerStandByTimeInMin = 0;
   }
// Serial.print("PowerStandByTimeInMin ");Serial.print(PowerStandByTimeInMin);
// Serial.print(" ,is_some_irrigation_on ");Serial.print(is_some_irrigation_on);  
// Serial.print(" ,BatterySupplying      ");Serial.println(BatterySupplying);
// delay(500);
if (   (isSomeIrigationON() == 0) && ( BatterySupplying == 1) ){  

	if (PowerStandByTimeInMin != 0){  
		PowerStandBy(PowerStandByTimeInMin); 
		PowerWakeUP();
		goto pwrLabel;
	}
	if(BatteryVoltage <10.4){
		enableWD(0);
		Serial.println(F("Go to Sleep 10min"));
		PowerStandBy(10); 
		PowerWakeUP();
		
		goto pwrLabel;
	}
	if(BatteryVoltage <10.8){
		enableWD(0);
		Serial.println(F("Go to Sleep 5min"));
		PowerStandBy(5); 
		PowerWakeUP();
		
		goto pwrLabel;
	}
		if(BatteryVoltage <11.0){
		enableWD(0);	
		Serial.println(F("Go to Sleep 3min"));
		PowerStandBy(3); 
		PowerWakeUP();
		
		goto pwrLabel;
	}
	if(BatteryVoltage <11.2){
		enableWD(0);
		Serial.println(F("Go to Sleep 1min"));
		PowerStandBy(1); 
		PowerWakeUP();
		
		goto pwrLabel;
	}
	
}
pwrLabel:
#endif		

jsonBuffer.clear();

#ifdef DEBUG_WhatchDog
byte counter = 0;
while (digitalRead(6) == 0){
	Serial.print(F("FRIZZ uC for "));Serial.print(counter);Serial.println(F(" s")); 
	delay(1000);
	counter++;
	}
#endif

}//============================= END ====== OF ================  L O O P ( ) =================================================================
//============================================================================================================================================

//per duration
void zonal_irrigation_start(String irrigationid, int duration, JsonArray &zones)
{
  for (byte i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id == "")
    {
	  zonal_irrigations[i].started_time = currentTime(); 																											  
      zonal_irrigations[i].irrigation_id = irrigationid;
#ifdef DEBUG
        Serial.println(F("Palim Zone [per time] :"));
#endif	  
      for (byte zone : zones)
      {
        zonal_irrigations[i].duration = duration * 60L;
        startValve(zone-1);
   		delay(200);
#ifdef DC_Valves_5V
	delay(800);
#endif		
        zonal_irrigations[i].status[zone - 1] = 1;
#ifdef DEBUG
        Serial.print(F("zona : ")); Serial.println(zone);
#endif
      }

#ifdef DEBUG
      String logger = "Started zonal Irrigation [per time] ID " + String(zonal_irrigations[i].irrigation_id);
      Serial.println(logger);
#endif
#ifdef WhatchDog 
    if(EEPROM.read(127) != 0) EEPROM.write(127, 0);//memLoc 127, value=0x00 - Zadato navodnjavanje;
#endif
      params = "";
      memset(msg, 0, sizeof(msg));
      params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));
      apiCall(server, 80, "/api/post/irrigation_started.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif

      return;
    }
  } //for (int i = 0; i < NUM_SLOTS; i++)
}


//per m3 
void zonal_irrigation_start(String irrigationid, long amount, JsonArray &zones, int max_duration){

  for (byte i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id == "")
    {
	  zonal_irrigations[i].started_time = currentTime(); 
      zonal_irrigations[i].current = 0;                  	  
      zonal_irrigations[i].irrigation_id = irrigationid;
      zonal_irrigations[i].amount = amount; // vrednost u litrima.
      zonal_irrigations[i].duration = max_duration * 60L; // vreme u sekundama  
#ifdef DEBUG
        Serial.println(F("Palim zone [per m3] :")); 
#endif	  
      for (byte zone : zones) //ukljucuje zadate zone
      {
        delay(200);
#ifdef DC_Valves_5V
		delay(800);
#endif	
        startValve(zone-1);    
        zonal_irrigations[i].status[zone - 1] = 1;
#ifdef DEBUG
        Serial.print(F("zona : ")); Serial.println(zone);
#endif
      }


#ifdef DEBUG_Extend
      Serial.print(F("Started zonal Irigation [per m3] ID:")); Serial.println(zonal_irrigations[i].irrigation_id);
#endif
#ifdef WhatchDog 
    if(EEPROM.read(127) != 0) EEPROM.write(127, 0);//memLoc 127, value=0x00 - zadato navodnjavnje;
#endif
      params = "";
      memset(msg, 0, sizeof(msg));
      params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));
      apiCall(server, 80, "/api/post/irrigation_started.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG_Extend
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif
      return;
    }
  }
}

void stop_irrigation(String irrigationid)
{
  // stop zonal irrigation
  for (byte i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id == irrigationid)
    {
      // turn of all active zones
      for (byte checking_zone = 0; checking_zone < number_of_zones; checking_zone++)
      {
        if (zonal_irrigations[i].status[checking_zone ] == 1)
        {
			delay(200);
#ifdef DC_Valves_5V
			delay(800);
#endif	
			stopValve(checking_zone);

#ifdef DEBUG_Extend
     Serial.print(F("Gasim zonu "));Serial.println(checking_zone+1);
#endif          
          zonal_irrigations[i].status[checking_zone ] = 0;
        }
        //zonal_irrigations[i].started_time[checking_zone ] = 0L;
      }
      zonal_irrigations[i].irrigation_id = "";
      zonal_irrigations[i].duration = 0;
      zonal_irrigations[i].started_time = 0L;


#ifdef DEBUG
      Serial.print(F("Stopped irrigation ID ")); Serial.println(irrigationid);
#endif

#ifdef WhatchDog 
	if( isSomeIrigationON() == 0) {
		if(EEPROM.read(127) != 255 ) EEPROM.write(127, 255);//memLoc 127, value 255 - nema navodnjavanja  		
	} 
#endif

      params = "";
      params.concat("iid=" + String(irrigationid));
      memset(msg, 0, sizeof(msg));
      apiCall(server, 80, "/api/post/irrigation_stopped.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG_Extend
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif
	return;
    }
  }

}

void checkValves(float deltaVolumePerOneLoop)
{
//--------------------------------------------------------------------
  // zonal check
  if (!isperm3)
  {
    IrigationProcesingBYtime();
  }
  // per m3 check
  else
  {
	IrigationProcesingBYvolumetric( deltaVolumePerOneLoop);
  }
#ifdef WhatchDog  
    if( isSomeIrigationON() == 0)  {
		if(EEPROM.read(127) != 255 ) EEPROM.write(127, 255);//memLoc 127, value 0 - nema navodnjavanja	
	}
#endif
}
//------------------------------------------------------------
void IrigationProcesingBYtime(void){
    for (byte Irigation = 0; Irigation < NUM_SLOTS; Irigation++)
    {
		if (zonal_irrigations[Irigation].irrigation_id != "")
		{
unsigned long tmpTime = (currentTime() - zonal_irrigations[Irigation].started_time);
			if (  tmpTime > zonal_irrigations[Irigation].duration  )
			{  
#ifdef DEBUG
//	Serial.print(F("SLOT [")); Serial.print(Irigation); Serial.print(F("] "));
//	Serial.print(F(" Isteklo Vreme= ")); Serial.print((float)tmpTime/60.0); Serial.print(F(" min"));
	Serial.println(F("Gasim Zone:"));
#endif
				for (byte processing_zone = 0; processing_zone < number_of_zones; processing_zone++)
				{
					if ((zonal_irrigations[Irigation].status[processing_zone] == 1))
					{
						delay(200); 
					#ifdef DC_Valves_5V
						delay(800);
					#endif							
						stopValve(processing_zone);
						zonal_irrigations[Irigation].status[processing_zone] = 0;
#ifdef DEBUG
	Serial.print(F("zona :"));Serial.println(processing_zone+1);
#endif       
					}
				}  
				
				zonal_irrigations[Irigation].duration = 0;   
				zonal_irrigations[Irigation].started_time = 0L;	
		
#ifdef DEBUG
	Serial.print(F("Completed zonal Irigation[by time] ID: ")); Serial.println(zonal_irrigations[Irigation].irrigation_id);
#endif
				params = "";
				params.concat("iid=" + String(zonal_irrigations[Irigation].irrigation_id));
				zonal_irrigations[Irigation].irrigation_id = "";
				memset(msg, 0, sizeof(msg));
				apiCall(server, 80, "/api/post/irrigation_finished.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG_Extend
	char *jsonResponse = strstr(msg, "{");
	Serial.println(jsonResponse);
#endif      
				
				  
			}  
		}//if (zonal_irrigations[Irigation].irrigation_id != "")
	}//for (byte Irigation = 0; Irigation < NUM_SLOTS; Irigation++)	
	
}

void IrigationProcesingBYvolumetric(float deltaVolumePerOneLoop){
				 
    for (byte Irigation = 0; Irigation < NUM_SLOTS; Irigation++)
    {
     
	 if (zonal_irrigations[Irigation].irrigation_id != "")
      {
		zonal_irrigations[Irigation].current += deltaVolumePerOneLoop; 
#ifdef Irigation_Per_m3 
	Serial.print(F("SLOT [")); Serial.print(i); Serial.print(F("],"));
	Serial.print(F(" Isteklo Vode = ")); Serial.print(zonal_irrigations[Irigation].current); Serial.println(F(" L"));
	//Serial.print(F(" Proteklo Vreme = ")); Serial.print(currentTime() - zonal_irrigations[i].started_time); Serial.println(F(" S"));
#endif			
		if (zonal_irrigations[Irigation].current > zonal_irrigations[Irigation].amount)																														 
            {

#ifdef DEBUG
	Serial.println(F("Gasim zone: "));
#endif	

				//gasi zone koje su aktivne za SLOT (zadato navodnjavanje)
				for (byte processing_zone = 0; processing_zone < number_of_zones; processing_zone++)
				{
					delay(200); 
				#ifdef DC_Valves_5V
					delay(800);
				#endif						
					if(zonal_irrigations[Irigation].status[processing_zone ] == 1)
				    {	
					stopValve(processing_zone);	
					zonal_irrigations[Irigation].status[processing_zone ] = 0;					
#ifdef DEBUG        
	Serial.print(F("zona: "));Serial.println(processing_zone+1); 
#endif 
					}  
				}
				// clear slot 
                zonal_irrigations[Irigation].amount = 0L;
                zonal_irrigations[Irigation].duration = 0;
				zonal_irrigations[Irigation].current = 0 ;
				initialiseIrrFlowMet();				

#ifdef DEBUG
	Serial.print(F("Completed zonal Irigation [by m3] ID: ")); Serial.println(zonal_irrigations[Irigation].irrigation_id);
#endif				
                params = "";
                params.concat("iid=" + String(zonal_irrigations[Irigation].irrigation_id));
				zonal_irrigations[Irigation].irrigation_id = "";
                memset(msg, 0, sizeof(msg));
                apiCall(server, 80, "/api/post/irrigation_finished.php", params.c_str(), msg, MSG_BUF);          
#ifdef DEBUG_Extend
    char *jsonResponse = strstr(msg, "{");
    Serial.println(jsonResponse);
#endif	
			}// Za SLOT [i] istekla zadata kolicina vode, ugasene pripadajuce zone i server obaveste.	
	
	    }
		
// ako je za dati SLOT (zadato navodnjavanje) isteklo max vreme -> gasi navodnjavanje		
		if ( (zonal_irrigations[Irigation].duration != 0) && (  (currentTime() - zonal_irrigations[Irigation].started_time)  >  zonal_irrigations[Irigation].duration))
		{
#ifdef Irigation_Per_m3 
	Serial.print(F("SLOT [")); Serial.print(Irigation); Serial.print(F("],"));
	Serial.print(F("Isteklo Zastitno Vreme = ")); Serial.print(zonal_irrigations[Irigation].duration ); Serial.println(F(" S")); 
	
#endif
		Serial.println(F("Gasim zone: "));
				for (byte processing_zone = 0; processing_zone < number_of_zones; processing_zone++)
				{
					delay(150);   
					if(zonal_irrigations[Irigation].status[processing_zone ] == 1)
				    {	
					stopValve(processing_zone);
					zonal_irrigations[Irigation].status[processing_zone ] = 0;					
#ifdef DEBUG_Extend      
	Serial.print(F("zona: "));Serial.println(processing_zone+1); 
#endif 
					}  
				}
#ifdef DEBUG
    Serial.print(F("Stoped zonal irrigation[maxdur] ID ")); Serial.println(zonal_irrigations[Irigation].irrigation_id);

#endif			// clear slot		   
				
				
                zonal_irrigations[Irigation].amount = 0L;
                zonal_irrigations[Irigation].duration = 0;
				zonal_irrigations[Irigation].current = 0 ;
				initialiseIrrFlowMet();
        
				params = "";
                params.concat("iid=" + String(zonal_irrigations[Irigation].irrigation_id));
				zonal_irrigations[Irigation].irrigation_id = "";
                params.concat("&maxduration=1");
                memset(msg, 0, sizeof(msg));
                apiCall(server, 80, "/api/post/irrigation_stopped.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG_Extend
            char *jsonResponse = strstr(msg, "{");
            Serial.println(jsonResponse);
#endif		
			
		}// Isteklo zastitno vreme.			
	} // SLOTS for(....)	
}

time_t currentTime()
{
  struct ts t;
  tmElements_t te;
  DS3231_get(&t);
  //char buff[BUFF_RTC_TIME];
  //snprintf(buff, BUFF_RTC_TIME, "Trenutno vreme %d.%02d.%02d %02d:%02d:%02d", t.year, t.mon, t.mday, t.hour, t.min, t.sec);
  //Serial.println(buff);

  te.Second = t.sec;
  te.Minute = t.min;
  te.Hour = t.hour;
  te.Day = t.mday;
  te.Month = t.mon;
  te.Year = t.year - 1970;

  time_t timeInSeconds = makeTime(te);
  return timeInSeconds;
}
//==================================================================================================================================
int apiCall(const char *server, int port, const char *path, const char *parameters, char *result, int resultlength)
{
  int completed = 0;
  delay(200);
  completed = inet.httpPOST(server, port, path, parameters, result, resultlength);

  if (completed == 0)
  {
#ifdef DEBUG
    Serial.println(F("Dettach GPRS connection..."));
#endif
    inet.dettachGPRS();
    delay(2000);
#ifdef DEBUG
    Serial.println(F("Restarting SIM module..."));
#endif
    digitalWrite(sim_restart_pin, LOW);
    delay(50);
    digitalWrite(sim_restart_pin, HIGH);
    delay(500);
#ifdef DEBUG
    Serial.println(F("GSM Shield testing."));
#endif
    // Start configuration of shield with baudrate.
    // For http uses is recommended to use 4800 or slower.
    if (gsm.begin(9600))
    {
#ifdef DEBUG
      Serial.println(F("\nGSM = READY"));
#endif
      gsm_started = true;
    }
    else
    {
#ifdef DEBUG
      Serial.println(F("\nGSM = IDLE"));
#endif
    }
    if (gsm_started)
    {
#ifdef DEBUG
      Serial.println(F("Attach GPRS connection..."));
#endif
      gsm.SimpleWriteln("AT+CGATT=1");
      delay(3000);

      if (inet.attachGPRS(String(carrierdata.apnname).c_str(), String(carrierdata.apnuser).c_str(), String(carrierdata.apnpass).c_str()))
      {
#ifdef DEBUG
        Serial.println(F("GPRS = ATTACHED"));         
        gsm.SimpleWriteln("AT+CIFSR");
        delay(5000);
      //Read until serial buffer is empty.s
        gsm.WhileSimpleRead();
#endif
      }
      else
      {
#ifdef DEBUG
        Serial.println(F("status=ERROR"));
#endif
      }
    
    }
    delay(1000);
  }

  
  if (completed == 0)
  {
    // check to turn off pump if there is no active irrrigation

    if (!isSomeIrigationON())
    {
#ifdef DEBUG
      Serial.println(F("Reseting Arduino..."));
#endif
      digitalWrite(arduino_restart_pin, LOW); // Reset Arduino
//      delay(5000);
    }
    else
    {
#ifdef DEBUG
      Serial.println(F("Not reseting Arduino beacuse of active irrigation."));
#endif
    }
  }
  return completed;
}

#ifdef FLOW
struct flowmeter_struct checkFlowMeter()
{
  // output some measurement result
  #ifdef  DEBUG_Telemetry                                                                       //merenje protoka vode na kanalu CH = 5
    Serial.println("Currently " + String(getCurrentFlow()*60) + " L/min, " + String(getCurrentVOL(FlowSensorCH5)) + "L total.");
  #endif
  struct flowmeter_struct flow;
  flow.currentflow = getCurrentFlow()*60.0; //getCurrentFlow vraca protok u sekundama, pa *60 dobija se u L/min protok
  flow.currentvolume = getCurrentVOL();  
#ifdef DEBUG_Telemetry
  FlowDebug( FlowSensorCH5);
#endif
  return flow;
}
#endif

#ifdef INTERRUPTFLOW
struct flowmeter_struct checkFlowMeter()
{
struct flowmeter_struct flow_tmp;
    // process the (possibly) counted ticks
  Meter.tick(flowmeter_period); 
  flow_tmp.currentflow = Meter.getCurrentFlowrate();
  flow_tmp.currentvolume = Meter.getCurrentVolume();
  Meter.reset();
  return flow_tmp;
}
#endif

			
void checkLastSeen() {
  if (last_seen > 0 && !(currentTime() - last_seen < 20 * 60L)) {
    stopAll();
  }
}

void stopAll() {

  // stop zonal irrigations
  for (byte checking_slot = 0; checking_slot < NUM_SLOTS; checking_slot++) {
    for (byte checking_zone=0; checking_zone < number_of_zones; checking_zone++) { 
      zonal_irrigations[checking_slot].status[checking_zone] = 0;
      
    }
	zonal_irrigations[checking_slot].current= 0;
    zonal_irrigations[checking_slot].started_time = 0L;
    zonal_irrigations[checking_slot].irrigation_id = "";
    zonal_irrigations[checking_slot].amount = 0L;
    zonal_irrigations[checking_slot].duration = 0;

  }
    for (byte checking_zone=0; checking_zone < number_of_zones; checking_zone++){
		
	stopValve(checking_zone); 
	delay(500);
	}
  
  initialiseIrrFlowMet();
}
//---------------DC Valves permanent ON/OFF-----------------------------------------
#ifdef DC_Valves_Permanent
void startValve(byte zone) {
    digitalWrite(valves_off[zone], HIGH);// Releji za N (negativan) prikljucak ventila
    if (zone == 3) { 
      PORTG = PORTG | 0x08; 
     }

}

void stopValve(byte zone) {
    digitalWrite(valves_off[zone], LOW);   //Releji za N (negativan) prikljucak ventila
    if (zone == 3) { 
      PORTG = PORTG & 0xF7; 
    } 

}
#endif
//------------------------------------------------------------------------
#if defined(DC_Valves) && !defined(DC_Valves_Permanent)
void startValve(byte zone) {	
    
    if (zone == 3) { 
		PORTG = PORTG & 0xFB; //Reley za P na '0'
		PORTG = PORTG | 0x08; //Relay za N na '1' 	
		delay(valvedelay);		
		PORTG = PORTG & 0xF7; //Relay za N na '0'
	}
	else{		
		digitalWrite(valves_on[zone], LOW);  // Releji za P (pozitivan) prikljucak vential 
		digitalWrite(valves_off[zone], HIGH);// Releji za N (negativan) prikljucak ventila
						  
		delay(valvedelay);
		digitalWrite(valves_off[zone], LOW);			
		
	}
 

#ifdef Power_Saving
if (BatterySupplying == 0){ // LEDs
    switch(zone)  {
    case 0: { digitalWrite(48, HIGH); break; }  
    case 1: { digitalWrite(47, HIGH); break; }  
    case 2: { digitalWrite(42, HIGH); break; }  
    case 3: { digitalWrite(17, HIGH); break; }   
      
     }
}   
#endif

}

void stopValve(byte zone) {	
    
    if (zone == 3) {
		PORTG = PORTG & 0xF7;  //Relay za N na '0'
		PORTG = PORTG | 0x04;  //Reley za P na '1'
		delay(valvedelay);
		PORTG = PORTG & 0xFB;  //Reley za P na '0'
	}
	else{
		digitalWrite(valves_off[zone], LOW);   //Releji za N (negativan) prikljucak ventila	
 
		digitalWrite(valves_on[zone], HIGH);  // Releji za P (pozitivan) prikljucak vential   
		delay(valvedelay);
		digitalWrite(valves_on[zone], LOW); 		
	}

#ifdef Power_Saving  
  switch(zone)  { //LEDs
    case 0: {  digitalWrite(48, LOW); break; }  
    case 1: {  digitalWrite(47, LOW); break; }  
    case 2: {  digitalWrite(42, LOW); break; }  
    case 3: {  digitalWrite(17, LOW); break; }      
   }
#endif

}
#endif

#ifdef FLOW_Toggling
bool checkFlow() {
     byte flowreadcount = 0;  
     byte firstflowstate = digitalRead(flow_pin_Toggling);
//     byte firstflowstate = ( PINE & 0x80);
     while (flowreadcount < 30) {
        byte flowstate = digitalRead(flow_pin_Toggling); // Za FERT
//        byte flowstate = ( PINE & 0x80);               // Za SENS
        if (flowstate != firstflowstate) {
          return true;
        }
        delay(50);
        flowreadcount++;
     }  
     return false;
  }
#endif

#ifdef TIME_F_Measure
void funcTimeMeasure(String funcBlock){
    Serial.print("Exe Time of "); Serial.print(funcBlock);Serial.print(" = "); Serial.print((float)(millis()-currentTimeMilles),0); Serial.println(" mS");
    currentTimeMilles = millis();
}
#endif

#ifdef RainFallSensor
  void RainFallCaunter(void){
    #ifdef DEBUG_RainFallSensor
    Serial.println("Rain Fall Tick");  
    #endif
    RainFallCaunterTich++;  
  }
#endif

//bool is_some_irrigation_on = false;
byte isSomeIrigationON(void){
		
	    byte slot_i  = 0;
	    byte count_active_slots  = 0;	
        for (byte slot_i = 0; slot_i < NUM_SLOTS; slot_i++){
		    if (zonal_irrigations[slot_i].irrigation_id != "")	count_active_slots++;		
        }
	    if (count_active_slots == 0)return 0;
		else return 1;		

}
void setRTCtime(void){
  /*
String 	xString;
char x[60];
gsm.SimpleWriteln("AT+CLTS=?"); 	
 delay(2000);//  Add Max Response Time
 gsm.SimpleRead1(x);
 xString = String(x).c_str(); 	
 Serial.print("CCLK: ");Serial.println(xString);
 Serial.print("CCLK: ");Serial.println(x);
	*/
}