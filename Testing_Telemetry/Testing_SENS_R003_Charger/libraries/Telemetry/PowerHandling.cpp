#include <Arduino.h>
#include <SoftwareSerial.h>
#include "SIM900.h"
#include "inetGSM.h"
#include "Telemetry.h"

#ifdef Power_Saving
	#include <avr/sleep.h>
	#include <avr/power.h>
	#include <avr/wdt.h>

	int InterruptWDcauntIvent = 0;
	
extern InetGSM inet;

ISR(WDT_vect) {
 InterruptWDcauntIvent++;
}

void setupWatchDogTimer(void) {
MCUSR &= ~(1<<WDRF); 
WDTCSR |= (1<<WDCE) | (1<<WDE); 
WDTCSR  = (1<<WDP3) | (0<<WDP2) | (0<<WDP1) | (1<<WDP0);  //8s 
//WDTCSR  = (0<<WDP3) | (1<<WDP2) | (1<<WDP1) | (0<<WDP0);  //1s 
WDTCSR |= _BV(WDIE);
}

void enterSleep(void)
{
     set_sleep_mode(SLEEP_MODE_PWR_DOWN);	 
     sleep_enable(); 	 
     sleep_mode();       
  // The program will continue from here after the WDT timeout
  // First thing to do is disable sleep.
     sleep_disable(); 
     
  // Re-enable the peripherals.
	 power_all_enable();
}
	
void PowerStandBy(byte PowerStandByTimeInMin){
int SleepTime_8s = PowerStandByTimeInMin * 6;//bilo je   /10; // Watch Dog Interrupt is 8 seconds !
if (PowerStandByTimeInMin == 1) SleepTime_8s = 3;
#ifdef DEBUG_Power_Saving
	Serial.print(F("SENS Sleep..."));
#endif 

//PWR LED
digitalWrite(29, LOW);

//Power OFF >
//digitalWrite(34,LOW);//12V_SYS_PWR_EN (SENTEK)
digitalWrite(8, HIGH);//GPRS_PWR KEY
digitalWrite(24,HIGH);//5V0_SYS_PWR_EN
digitalWrite(23,HIGH);//5V0_I2C_PWR_EN
digitalWrite(27,HIGH); //-5V0  
digitalWrite(26,LOW); //Battery Measuring OFF

digitalWrite(7, LOW);//GSM RST

//GSM Power down
DDRH  = DDRH  | 0x20; PORTH = PORTH & 0xDF;
delay(10);
DDRE  = DDRE  | 0x04; PORTE = PORTE & 0xFB;
#ifdef DEBUG_Power_Saving
//	SleepTime = currentTime();
#endif
	InterruptWDcauntIvent = 0 ;
	setupWatchDogTimer(); 
    while (InterruptWDcauntIvent < SleepTime_8s) { //ONE while lopp is 8s
        
		enterSleep();
		
#ifdef WhatchDog
		togleWD(); //togle WD
#endif
		
    } // InterruptWDcauntIvent * 8 sekundi

	wdt_disable();
}

void PowerWakeUP(void){
	
#ifdef DEBUG_Power_Saving
	Serial.println(F("...Wake UP"));
#endif 	
//PWR LED
digitalWrite(29, HIGH);

// Power ON>
//digitalWrite(34,HIGH);//12V_SYS_PWR_EN (SENTEK)
digitalWrite(8, LOW);//GPRS_PWR KEY
digitalWrite(24,LOW);//5V0_SYS_PWR_EN
digitalWrite(23,LOW);//5V0_I2C_PWR_EN
digitalWrite(27,LOW); //-5V0
digitalWrite(26,HIGH); //Battery Measuring ON

//GSM Power UP
DDRE  = DDRE  | 0x04; PORTE = PORTE | 0x04;
delay(10);
DDRH  = DDRH  | 0x20; PORTH = PORTH | 0x20;
delay(500);

digitalWrite(7, LOW);
delay(50);
digitalWrite(7, HIGH);
delay(50);

#ifdef DEBUG_Power_Saving
	Serial.print(F("GSM="));
#endif 

bool gsm_started = false;
if (gsm.begin(9600))
  {
    gsm_started = true;    
#ifdef DEBUG_Power_Saving
    Serial.println(F("READY"));
#endif

//Pri Debagovanju, preskoci jedan deo koda, da ubrza startovanje uC-a
#ifdef SKIP_Operater_Detecting
    goto skip_operater_det;
#endif
 
  }
  else
  {
    gsm_started = false;
#ifdef DEBUG_Power_Saving
    Serial.println(F("IDLE"));
#endif
  
  }

if (gsm_started)
  {
#ifdef DEBUG_Power_Saving	  
    Serial.print(F("GPRS WakeUP="));
#endif	

#ifdef SKIP_Operater_Detecting  
    skip_operater_det:
    Serial.print(F("GPRS="));  
    if (inet.attachGPRS("internet", "internet", "internet"))
    {
#else
    if (inet.attachGPRS(String(carrierdata.apnname).c_str(), String(carrierdata.apnuser).c_str(), String(carrierdata.apnpass).c_str()))
    {
#endif  

#ifdef DEBUG_Power_Saving
    Serial.println(F("ATTACHED")); 
     gsm.SimpleWriteln("AT+CIFSR");//Read IP address.
     delay(5000);     
     gsm.WhileSimpleRead();//Read until serial buffer is empty
#endif
    }
    else
    {
#ifdef DEBUG_Power_Saving
      Serial.println(F("ERROR"));
#endif
    }
    delay(1000); 
  }
else
  {
#ifdef DEBUG_Power_Saving    
   Serial.println(F("GSM&GPRS=NOTstarted"));
#endif  
  }  
#ifdef DEBUG_Power_Saving  
//	Serial.print("Sleep time:"); Serial.println( currentTime() - SleepTime );   
#endif 	
}
#endif

// byte - nije bitan je precizan napon solarnog panela
float getVoltageSolar(void){

float solar = 0;
byte cnt = 0;
while (cnt<5){
	solar = solar + analogRead(A9);
	delay(10);
	cnt++;
}
solar = solar / solarConst ;  //163 = 1024 /  (10kohm + 47kohm)/10kohm
return solar;
}

// float - bitan je precizan napon baterije
float getVoltageBatt(void){

float battery = 0;
byte cnt = 0;

while (cnt<5){
	battery = battery + analogRead(A8);
	delay(10);
	cnt++;
}

battery = battery / battConst;  //335 = 1024 /  (402kohm+200kohm)/200kohm;
return battery;
}

#ifdef AC_Power_FERT
// Vraca vrednost mrenzog napona ~230Vac
int getPower(void){
  int i = 0;
  int sensmax = 0;
  int sensmin = 1023;
while (i++<150){  
int  sensorValue = analogRead(A7);
if (sensmax < sensorValue ) sensmax = sensorValue;
if (sensmin > sensorValue ) sensmin = sensorValue;
}
return (sensmax - sensmin)/4.27;
//Serial.println(sensorValue); 
}
#endif