#include <ModbusMaster.h>

#include <Arduino.h>

#define FirmwareVersion "V0.30"

ModbusMaster node;
bool state = true;

void setup() {
  
 RS485setup();
 
 Serial.begin(112500); 
 
 Serial2.begin(9600); 
 
 // Modbus slave ID 1
 node.begin(1, Serial2);
 // Callbacks allow us to configure the RS485 transceiver correctly
  node.preTransmission(preTransmission);
  node.postTransmission(postTransmission);
}

//----Enable TX, Dissable RX----//
void preTransmission()
{
  RS485setup();
 
  PORTH = PORTH | 0x04; //PH2 is HIGH. ->Enable TX
}

void postTransmission(int chanell)
{
  RS485readEnableCH(chanell); // Enabled ONE  RX Chanell.
  PORTH = PORTH & 0xFB; //PH2 is HIGH. ->Disable TX
}   

//-------Disable  RX  AND  TX ---------------------//    
void RS485setup(){       
  
    //TX driver
    DDRH  = DDRH  | 0x04; //PH2 is out.
    PORTH = PORTH & 0xFB; //PH2 is HIGH. ->Disable TX
    
    // set all RX enable control pins, as OUT and non-active.
    // CH1  RX,
    DDRD  = DDRD  | 0x10; //PD4 is out.
    PORTD = PORTD | 0x10;   //PD4 is HIGH -> Disable receiv data.
    
    // CH2  RX,
    DDRD  = DDRD  | 0x80;  //PD7 is out.
    PORTD = PORTD | 0x80;  //PD7 is HIGH -> Disable receiv data.
    
    // CH3  RX,
    DDRD  = DDRD  | 0x40;  //PD6 is out.
    PORTD = PORTD | 0x40;  //PD6 is HIGH -> Disable receiv data
    
    // CH4  RX,
    DDRD  = DDRD  | 0x20;  //PD5 is out.
    PORTD = PORTD | 0x20;  //PD5 is HIGH -> Disable receiv data
    
}

//------Enable ONE  RX  port, Other MUST be disabled------------------------//
void RS485readEnableCH(int chanell){
 
  PORTH = PORTH & 0xFB; //Disable TX
  switch(chanell){
     case 1: { //CH1
         PORTD = PORTD & 0xEF;//PD4 is LOW - > Enable Receive data on CH1
         PORTD = PORTD | 0x80;
         PORTD = PORTD | 0x40;
         PORTD = PORTD | 0x20;
         break;
    }
    case 2: { //CH2
         PORTD = PORTD | 0x10;
         PORTD = PORTD & 0x7F;
         PORTD = PORTD | 0x40;
         PORTD = PORTD | 0x20;
         break;
    }
   case 3:{//CH3
         PORTD = PORTD | 0x10;
         PORTD = PORTD | 0x80;
         PORTD = PORTD & 0xBF;
         PORTD = PORTD | 0x20;
         break;
    }
    case 4:{//CH4
         PORTD = PORTD | 0x10;
         PORTD = PORTD | 0x80;
         PORTD = PORTD | 0x40;
         PORTD = PORTD & 0xDF;
         }
   }//switch()  
}
//---------------Send  STRING------------------///
void RS485writeString(String string){   
    //disable ALL  RX  chanells
     PORTD = PORTD | 0x10;
     PORTD = PORTD | 0x80;
     PORTD = PORTD | 0x40;
     PORTD = PORTD | 0x20;
     
     // enable TX 
     PORTH = PORTH | 0x04; //PH2 is HIGH.
     Serial2.print(string);      
     delayMicroseconds(100*string.length()); // NEED  some Delay !!!!!!
     //Disable TX
     PORTH = PORTH & 0xFB; //PH2 is LOW.
}
//---------Send ONE character---------------------///
void RS485writeChar(char charToSent){   
    //disable ALL  RX  chanells
     PORTD = PORTD | 0x10;
     PORTD = PORTD | 0x80;
     PORTD = PORTD | 0x40;
     PORTD = PORTD | 0x20;
     
     // enable TX 
     PORTH = PORTH | 0x04; //PH2 is HIGH.
     Serial2.print(charToSent);           
     //Disable TX
     delayMicroseconds(100);//  //100uS whait for send data than disable TX driver. 
     PORTH = PORTH & 0xFB; //PH2 is LOW.
}

char RS485readChar(int chanell){
  RS485readEnableCH(chanell);  
  char x = Serial2.read();  
  return x;  
}

void loop() {
  

Serial.println(FirmwareVersion);



 
  static uint32_t i;
  uint8_t j, result;
  uint16_t data[6];
  
  i++;
while(1){   
//  // set word 0 of TX buffer to least-significant word of counter (bits 15..0)
//  node.setTransmitBuffer(0, lowWord(i));
//  
//  // set word 1 of TX buffer to most-significant word of counter (bits 31..16)
//  node.setTransmitBuffer(1, highWord(i));
//  
//  // slave: write TX buffer to (2) 16-bit registers starting at register 0
//  result = node.writeMultipleRegisters(0, 2);
//  
//  // slave: read (6) 16-bit registers starting at register 2 to RX buffer
  result = node.readHoldingRegisters(4001, 2);
  Serial.print(result);
  // do something with data if read is successful
  if (result == node.ku8MBSuccess)
  {
    for (j = 0; j < 6; j++)
    {
      data[j] = node.getResponseBuffer(j);
    }
  }
}
delay(500);
}
