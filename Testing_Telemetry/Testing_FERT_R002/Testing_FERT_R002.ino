#include "EEPROMAnything.h"
#include <SoftwareSerial.h>
#include <Arduino.h>
#include <Wire.h>
#include <Arduino.h>
#include "inetGSM.h"
#include "SMS.h"
//#include <RTC.h>
#include "DS3231M.h"
#include <MAX31855.h> 
#include "Telemetry.h"
#define CLS          "\033[2J"  
#define WirmVer      "Firmware V1.01  (Functional Verification Testing)"
#include <SPI.h>

SMSGSM sms;
InetGSM inet;
DS3231M_Class DS3231M;

#include <Adafruit_MAX31865.h>
Adafruit_MAX31865 TemperaturePT1000 = Adafruit_MAX31865(49);

SHT1x SHT1x_sensor(   20  ,   21);

boolean gsm_started = false;
carrier carrierdata;
struct_operater_vip operaterVIP;
struct_operater_MTS operaterMTS;
struct_operater_Telenor operaterTelenor;

 pH_struct pHmeasuring;
irrigation_struct_FlowMetering  irrigationFlowMetering[5];

void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  
 
    
  pinMode(22, OUTPUT); //Port PA
  pinMode(23, OUTPUT);
  pinMode(24, OUTPUT);
  pinMode(25, OUTPUT);
  pinMode(26, OUTPUT);
  pinMode(27, OUTPUT);  
  pinMode(28, OUTPUT); 
  pinMode(29, OUTPUT);

  pinMode(37, OUTPUT); //Port PC
  pinMode(36, OUTPUT);
  pinMode(35, OUTPUT);
  pinMode(34, OUTPUT);
  pinMode(33, OUTPUT);
  pinMode(32, OUTPUT);
  pinMode(31, OUTPUT);
  pinMode(30, OUTPUT);  

  pinMode(41, OUTPUT); //Port PG
  pinMode(40, OUTPUT);
  pinMode(39, OUTPUT);   
  pinMode(75, OUTPUT);
  DDRG  = DDRG  | 0x08; 
  pinMode(76, OUTPUT); //RTC reset

  //--- Current OUT PWM------// 
  pinMode(5, OUTPUT);  //CH1
  pinMode(3, OUTPUT);  //CH2
  pinMode(6, OUTPUT);  //CH3
  pinMode(9, OUTPUT);  //CH4
  pinMode(8, OUTPUT);  //CH5

  Serial.begin(115200);
  
  pinMode(7, OUTPUT); //GSM - reset
  digitalWrite(7, LOW);   
  
}

//void Relays(void){
//
//Serial.println(CLS);
//Serial.println("\n Relay Test ");
//Serial.println("pres 0 for EXIT\n");
//while((Serial.read() != '0')){
//  
//  digitalWrite(41, HIGH);delay(50);
//  digitalWrite(40, HIGH);delay(50);
//  digitalWrite(39, HIGH);delay(50);
//  //digitalWrite(75, HIGH);delay(50); //ne fercera
//  //PORTG = PORTG | 0x08;  //PG3 is HIGH 
//  digitalWrite(75, HIGH);delay(50);
// 
//  
//  digitalWrite(22, HIGH);delay(50);
//  digitalWrite(23, HIGH);delay(50);
//  digitalWrite(24, HIGH);delay(50);
//  digitalWrite(25, HIGH);delay(50);
//  digitalWrite(26, HIGH);delay(50);
//  digitalWrite(27, HIGH);delay(50);
//  digitalWrite(28, HIGH);delay(50);
//
//  digitalWrite(37, HIGH);delay(50);
//  digitalWrite(36, HIGH);delay(50);
//  digitalWrite(35, HIGH);delay(50);
//  digitalWrite(34, HIGH);delay(50);
//  digitalWrite(33, HIGH);delay(50);
//  digitalWrite(32, HIGH);delay(50);
//  digitalWrite(31, HIGH);delay(50);
//  digitalWrite(30, HIGH);delay(50);
//
//
//  delay(250); 
//
//  digitalWrite(22, LOW);delay(50);
//  digitalWrite(23, LOW);delay(50);
//  digitalWrite(24, LOW);delay(50);
//  digitalWrite(25, LOW);delay(50);
//  digitalWrite(26, LOW);delay(50);
//  digitalWrite(27, LOW);delay(50);
//  digitalWrite(28, LOW);delay(50);
//
//  digitalWrite(37, LOW);delay(50);
//  digitalWrite(36, LOW);delay(50);
//  digitalWrite(35, LOW);delay(50);
//  digitalWrite(34, LOW);delay(50);
//  digitalWrite(33, LOW);delay(50);
//  digitalWrite(32, LOW);delay(50);
//  digitalWrite(31, LOW);delay(50);
//  digitalWrite(30, LOW);delay(50);
//
//  digitalWrite(41, LOW);delay(50);
//  digitalWrite(40, LOW);delay(50);
//  digitalWrite(39, LOW);delay(50);
//  //digitalWrite(75, LOW);delay(50);
//  //PORTG = PORTG & 0xF7;  //PG3 is LOW
//  digitalWrite(75, LOW);delay(50);  
//
//  delay(250); 
//  }
//}

void Relays_OneByOne(void){

Serial.println(CLS);
Serial.println("\n Relays_OneByOne test ");
char x;

  Serial.print("AC13");digitalWrite(33, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("AC14");digitalWrite(32, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("AC15");digitalWrite(31, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("AC16");digitalWrite(30, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("AC17");digitalWrite(41, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("AC18");digitalWrite(40, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("AC19");digitalWrite(39, HIGH);while(!Serial.available()){}x = Serial.read();
//  Serial.print("AC20");digitalWrite(75, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("AC20");PORTG = PORTG | 0x08; while(!Serial.available()){}x = Serial.read();
 //
  
  Serial.print("AC1");digitalWrite(24, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("AC2");digitalWrite(23, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("AC3");digitalWrite(22, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("AC4");digitalWrite(25, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("AC5");digitalWrite(26, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("AC6");digitalWrite(27, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("AC7");digitalWrite(28, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("AC8");digitalWrite(29, HIGH);while(!Serial.available()){}x = Serial.read();
  
  Serial.print("AC9");digitalWrite(37, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("AC10");digitalWrite(36, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("AC11");digitalWrite(35, HIGH);while(!Serial.available()){}x = Serial.read();
  Serial.print("AC12");digitalWrite(34, HIGH);while(!Serial.available()){}x = Serial.read();
  


  delay(250); 

  digitalWrite(22, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(23, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(24, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(25, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(26, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(27, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(28, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(29, LOW);while(!Serial.available()){}x = Serial.read();
  
  digitalWrite(37, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(36, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(35, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(34, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(33, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(32, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(31, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(30, LOW);while(!Serial.available()){}x = Serial.read();

  digitalWrite(41, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(40, LOW);while(!Serial.available()){}x = Serial.read();
  digitalWrite(39, LOW);while(!Serial.available()){}x = Serial.read();  
//  digitalWrite(75, LOW);while(!Serial.available()){}x = Serial.read();
  PORTG = PORTG & 0xF7; while(!Serial.available()){}x = Serial.read();
  //

}

void RTC(void){

char output_buffer[32]; ///< Temporary buffer for sprintf()

Serial.println(CLS);
Serial.println("\nReal Time Clock Test :");
Serial.println("for EXIT pres 0\n");

while (!DS3231M.begin()) // Initialize RTC communications
  {
    Serial.println(F("Unable to find DS3231MM. Checking again in 3s."));
    delay(3000);
  } // of loop until device is located

//DS3231M.adjust(DateTime(2020,03,25,18,42,55));
DS3231M.pinSquareWave(); // Make INT/SQW pin toggle at 1Hz
  
static uint8_t secs = 0;
DateTime now = DS3231M.now(); // get the current time from device



while((Serial.read() != '0')){
  if (secs != now.second())     // Output if seconds have changed
  {
    sprintf(output_buffer,"%04d-%02d-%02d %02d:%02d:%02d", now.year(), now.month(),  now.day(), now.hour(), now.minute(), now.second());
    Serial.print("DATE and TIME : ");Serial.print(output_buffer);
    Serial.write(cr);
    
    secs = now.second(); // Set the counter variable
    
  } // of if the seconds have changed
now = DS3231M.now();
  
}

}

void PWM(void){
  Serial.print(CLS);
  Serial.println("PWM Generator....");
int td = 1;
char x = 't';
  while ( ( x != '0')){
    
    if(x == 't') {
      td++; Serial.print(CLS); Serial.write(cr);Serial.print("td = ");Serial.print(td);
      switch (td){
        case 1:Serial.print(492); break;
        case 2:Serial.print(", f = ");Serial.print(248); break;
        case 3:Serial.print(", f = ");Serial.print(166); break;
        case 4:Serial.print(", f = ");Serial.print(124); break;
        case 5:Serial.print(", f = ");Serial.print(100); break;
        case 6:Serial.print(", f = ");Serial.print(83); break;
        case 7:Serial.print(", f = ");Serial.print(71); break;
        case 8:Serial.print(", f = ");Serial.print(62); break;
        case 9:Serial.print(", f = ");Serial.print(55); break;
        case 10:Serial.print(", f = ");Serial.print(50); break;
        case 11:Serial.print(", f = ");Serial.print(45); break;
        case 12:Serial.print(", f = ");Serial.print(41); break;
        case 13:Serial.print(", f = ");Serial.print(38); break;
        case 14:Serial.print(", f = ");Serial.print(35); break;
        case 15:Serial.print(", f = ");Serial.print(33); break;
        case 16:Serial.print(", f = ");Serial.print(31); break;
        case 17:Serial.print(", f = ");Serial.print(29); break;
        case 18:Serial.print(", f = ");Serial.print(27); break;
        case 19:Serial.print(", f = ");Serial.print(26); break;
        case 24:Serial.print(", f = ");Serial.print(20); break;
        case 26:Serial.print(", f = ");Serial.print(19); break;
        default: break;
        }
    }
    if(x == 'T' && td !=1) {td--;}
    digitalWrite(4,HIGH);
    delay(td);
    digitalWrite(4,LOW); 
    delay(td);  

    x = Serial.read();
    
}  
}

void Flow(void){
 int sensorValue = 0;  // variable to store the value coming from the sensor
 int i = 40;

 float val;
 char buff[3];


while ( (Serial.read() != '0')){
  Serial.print(CLS);
  Serial.println("Flow Measuring Test: \n");
  Serial.println("Trimovati potencimetar PT2, tako da za 270Hz se dobije protok od 8.2 L/s!");
  Serial.println("      ILI                 ,         za 250Hz se dobije protok od 7.5 L/s!");
  
  sensorValue = analogRead(A3);
  val = ((float)sensorValue * 5) / 1023 ;
  dtostrf(val, 1, 2, buff);
  Serial.print("ADC3 Voltage : "); Serial.print(val);Serial.print("V");Serial.print(" Flow :"); val = (float)sensorValue / 88;  dtostrf(val, 1, 2, buff); Serial.print(val);Serial.println("[L/s]");
  
  sensorValue = analogRead(A1);
  val = ((float)sensorValue * 5) / 1023 ;
  dtostrf(val, 1, 2, buff);
  Serial.print("ADC1 Voltage : "); Serial.print(val);Serial.print("V");Serial.print(" Flow :"); val = (float)sensorValue / 88;  dtostrf(val, 1, 2, buff); Serial.print(val);Serial.println("[L/s]");
  
  sensorValue = analogRead(A0);
  val = ((float)sensorValue * 5) / 1023 ;
  dtostrf(val, 1, 2, buff);
  Serial.print("ADC0 Voltage : "); Serial.print(val);Serial.print("V");Serial.print(" Flow :"); val = (float)sensorValue / 88;  dtostrf(val, 1, 2, buff); Serial.print(val);Serial.println("[L/s]");
  
  sensorValue = analogRead(A4);
 val = ((float)sensorValue * 5) / 1023 ;
  dtostrf(val, 1, 2, buff);
  Serial.print("ADC4 Voltage : "); Serial.print(val);Serial.print("V");Serial.print(" Flow :"); val = (float)sensorValue / 88;  dtostrf(val, 1, 2, buff); Serial.print(val);Serial.println("[L/s]");
  
  sensorValue = analogRead(A5);
  val = ((float)sensorValue * 5) / 1023 ;
  dtostrf(val, 1, 2, buff);
  Serial.print("ADC5 Voltage : "); Serial.print(val);Serial.print("V");Serial.print(" Flow :"); val = (float)sensorValue / 88;  dtostrf(val, 1, 2, buff); Serial.print(val);Serial.println("[L/s]");

  
  delay(1500);
}

  
}


void FlowPWM(void){

initialiseIrrFlowMet();

int cnt = 0;
int  td = 1;
char x = ' ';
  while ( ( x != '0')){
    x = Serial.read();
    if(x == 't') {td++;}
    if(x == 'T') {td--;}
    digitalWrite(4,HIGH);
    delay(td);
    digitalWrite(4,LOW); 
    delay(3*td);
    cnt++;
    if (cnt == 400) {
      Serial.print(CLS);                                        //FlowSensorCH5
      Serial.print("Flow [L/min] :"); Serial.println(getCurrentFlow(4)*60 );
      Serial.print("Volume   [L] :");Serial.println(getCurrentVOL(4));
      Serial.print("td : "); Serial.println(td);
      cnt =0;
    }
   }
  

}


void pH(void){
 int sensorValue = 0;  // variable to store the value coming from the sensor
 float val;
 char buff[3];
 
 Serial.print(CLS);
 Serial.println("pH  Test, Calibrate PT1=10kohm, for 0Vin, ADC = 1.94V\n");
 Serial.println("ADC reading : ");

 pHmeasuring.ferphK      = 8.81;  // Y = k*X + M
 pHmeasuring.ferphM     =-9.87;
  
while ( (Serial.read() != '0')){
    
  sensorValue = analogRead(A2);
  val = ((float)sensorValue * 5) / 1023 ;
  dtostrf(val, 1, 2, buff);
  Serial.write(cr); Serial.print(val);Serial.print("V, pH = "); Serial.print( getPh());
 
  delay(500);
}

}

void RS485(void){
 char x = 'x';
 Serial.print(CLS);
 Serial.println("RS485 test :\n");
 Serial.println("Chose chanel from 1 to 4, or 0 for EXIT :");
 Serial2.begin(115200);

// CH1 is TX, data enable transmit
DDRH  = DDRH  | 0x04; //PH2 is out.
PORTH = PORTH | 0x04; //PH2 is HIGH.

// CH1  RX,
DDRD  = DDRD  | 0x10; //PD4 is out.
//PORTD = PORTD & 0xEF; //PD4 is LOW  -> Enable receiv data.
PORTD = PORTD | 0x10;   //PD4 is HIGH -> Disable receiv data.


// CH2  RX,
DDRD  = DDRD  | 0x80;  //PD7 is out.
PORTD = PORTD | 0x80;  //PD7 is HIGH -> Disable receiv data.

// CH3  RX,
DDRD  = DDRD  | 0x40;  //PD6 is out.
PORTD = PORTD | 0x40;  //PD6 is HIGH -> Disable receiv data

// CH4  RX,
DDRD  = DDRD  | 0x20;  //PD5 is out.
PORTD = PORTD | 0x20;  //PD5 is HIGH -> Disable receiv data

 while ( x != '0'){
  x = Serial.read();
  if(x == '1'){
    Serial.println("Chanel 1:");
     PORTD = PORTD & 0xEF;//PD4 is LOW
     PORTD = PORTD | 0x80;
     PORTD = PORTD | 0x40;
     PORTD = PORTD | 0x20;
    }
   if(x == '2'){
    Serial.println("Chanel 2:");
     PORTD = PORTD | 0x10;
     PORTD = PORTD & 0x7F;
     PORTD = PORTD | 0x40;
     PORTD = PORTD | 0x20;
    }
    if(x == '3'){
      Serial.println("Chanel 3:");
     PORTD = PORTD | 0x10;
     PORTD = PORTD | 0x80;
     PORTD = PORTD & 0xBF;
     PORTD = PORTD | 0x20;
    }
    if(x == '4'){
      Serial.println("Chanel 4:");
     PORTD = PORTD | 0x10;
     PORTD = PORTD | 0x80;
     PORTD = PORTD | 0x40;
     PORTD = PORTD & 0xDF;
    }

 
  Serial2.print("ABCD"); 
  Serial.println(Serial2.read());
  delay(1000);
 } 

}

void CurrentIN_OUT(void){
int pulse = 25;
int pause = 75;  //frequency 100Hz.
int sensorValue;
char x = 'x';
char buff[3];
float val;
int i=0;
  
Serial.println(CLS);
Serial.println("Current OUT test:"); Serial.println("Pres 4 and 6 for decrease and increase duty cycle\n\n");
Serial.print("Pulse ");Serial.print(pulse); Serial.print(" mS");Serial.print(" Pause ");Serial.print(pause);Serial.print(" mS");
Serial.print("  Current OUT = ");Serial.print((float)pulse*0.275);Serial.print(" mA");

 pinMode(5, OUTPUT);  //CH1
  pinMode(3, OUTPUT);  //CH2
  pinMode(6, OUTPUT);  //CH3
  pinMode(9, OUTPUT);  //CH4
  pinMode(8, OUTPUT);  //CH5

while ( x != '0'){ 

  x =  Serial.read();
  if (x == '6') {
    pulse++; pause--;
  }
   if (x == '4') {
    pulse--; pause++;
  }

  i++;
  if (i == 10){ // refresh reading every 1s.    
        i=0;  
        Serial.println(CLS);
        Serial.print("Pulse ");Serial.print(pulse); Serial.print(" mS");Serial.print(" Pause ");Serial.print(pause);Serial.print(" mS");
        Serial.print("  Current OUT = ");Serial.print((float)pulse*0.275);Serial.println(" mA");
      
        sensorValue = analogRead(A12);  //CH1
        val = (float)sensorValue / 25 ;
        dtostrf(val, 2, 1, buff);
        Serial.print("Current IN on CH1 = ");Serial.print(val);Serial.println(" mA");
      
        sensorValue = analogRead(A13);  //CH2
        val = (float)sensorValue / 25 ;
        dtostrf(val, 2, 1, buff);
        Serial.print("Current IN on CH2 = ");Serial.print(val);Serial.println(" mA");
        
        sensorValue = analogRead(A11);  //CH3
        val = (float)sensorValue / 25 ;
        dtostrf(val, 2, 1, buff);
        Serial.print("Current IN on CH3 = ");Serial.print(val);Serial.println(" mA");
        
        sensorValue = analogRead(A10);  //CH4
        val = (float)sensorValue / 25 ;
        dtostrf(val, 2, 1, buff);
        Serial.print("Current IN on CH4 = ");Serial.print(val);Serial.println(" mA");
        
        sensorValue = analogRead(A9);  //CH5
        val = (float)sensorValue / 25 ;
        dtostrf(val, 2, 1, buff);
        Serial.print("Current IN on CH5 = ");Serial.print(val);Serial.println(" mA");
  }  
  
  delay(pause);
  
  digitalWrite(5, HIGH);
  digitalWrite(3, HIGH);
  digitalWrite(6, HIGH);
  digitalWrite(9, HIGH);
  digitalWrite(8, HIGH);
  
  delay(pulse);
  
  digitalWrite(5, LOW);
  digitalWrite(3, LOW);
  digitalWrite(6, LOW);
  digitalWrite(9, LOW);
  digitalWrite(8, LOW);  
}  

  pinMode(5, INPUT);  //CH1
  pinMode(3, INPUT);  //CH2
  pinMode(6, INPUT);  //CH3
  pinMode(9, INPUT);  //CH4
  pinMode(8, INPUT);  //CH5
}

void AnalogIN(void){
int sensorValue;
char x = 'x';
char buff[3];
float val; 


while ( (Serial.read() != '0')){
        Serial.println(CLS);
        Serial.print("Analog INUP Test CH1, CH2, CH3 and CH4 :\n\n\n");
        
        sensorValue = analogRead(A15);  //CH1 
        val = (float)sensorValue*5 / 2048 ;
        dtostrf(val, 1, 2, buff);
        Serial.write(cr);Serial.print("Analog IN CH1 = ");Serial.print(val);Serial.println("V");



        sensorValue = analogRead(A7);  //CH2 
        val = (float)sensorValue*5 / 2048 ;
        dtostrf(val, 1, 2, buff);
        Serial.write(cr);Serial.print("Analog IN CH2 = ");Serial.print(val);Serial.println("V");
        
        sensorValue = analogRead(A14);  //CH3 
        val = (float)sensorValue*5 / 2048 ;
        dtostrf(val, 1, 2, buff);
        Serial.write(cr);Serial.print("Analog IN CH3 = ");Serial.print(val);Serial.println("V");

        sensorValue = analogRead(A8);  //CH4 - jedini trenutno funkcionalan
        val = (float)sensorValue*5 / 2048 ;
        dtostrf(val, 1, 2, buff);
        Serial.write(cr);Serial.print("Analog IN CH4 = ");Serial.print(val);Serial.println("V");
        
               
        delay(1500);
}  
}


void Humidity_Temperature(void){
  char x = 'a';
  while((x != '0')){   
    Serial.println(CLS);  
     x = Serial.read();
    //Wire.end();  // u konfliktu dva I2C-a RTC i Temp-Humid senzor
     Serial.print("Temp : ");Serial.println( SHT1x_sensor.readTemperatureC() );  
     Serial.print("Humi : ");Serial.println( SHT1x_sensor.readHumidity() );
    //Wire.begin();  
   delay(800);  
  } 
}


void Temperature(void){

TemperaturePT1000.begin(MAX31865_3WIRE);  // set to 2WIRE or 4WIRE as necessary
Serial.println(CLS);

while(Serial.read() != '0'){

   //
   Serial.println("\033[0;0H");
  Serial.println("Temperature Test :\n");
  
   uint16_t rtd = TemperaturePT1000.readRTD();
 // Serial.print("RTD value: "); Serial.println(rtd); 
  float ratio = rtd;
  ratio /= 32768;
  //Serial.print("Ratio = "); Serial.println(ratio,2);
  Serial.print("Resistance = "); Serial.print(RREF*ratio,1);Serial.println(" ohm");
  Serial.print("Temperature = "); Serial.print(TemperaturePT1000.temperature(RNOMINAL, RREF));Serial.println(" Celsus");

  // Check and print any faults
  uint8_t fault = TemperaturePT1000.readFault();
  if (fault) {
    Serial.print("Fault 0x"); Serial.println(fault, HEX);
    if (fault & MAX31865_FAULT_HIGHTHRESH) {
      Serial.println("RTD High Threshold"); 
    }
    if (fault & MAX31865_FAULT_LOWTHRESH) {
      Serial.println("RTD Low Threshold"); 
    }
    if (fault & MAX31865_FAULT_REFINLOW) {
      Serial.println("REFIN- > 0.85 x Bias"); 
    }
    if (fault & MAX31865_FAULT_REFINHIGH) {
      Serial.println("REFIN- < 0.85 x Bias - FORCE- open"); 
    }
    if (fault & MAX31865_FAULT_RTDINLOW) {
      Serial.println("RTDIN- < 0.85 x Bias - FORCE- open"); 
    }
    if (fault & MAX31865_FAULT_OVUV) {
      Serial.println("Under/Over voltage"); 
    }
    TemperaturePT1000.clearFault();
  }
  Serial.println();
  delay(500);
  
  }

  
}

void sentek(void){
  RS485setup();
  setSANTEKsensors();  

reqSENTEKpresetHoldReg_Temperature();delay(40);  

while(Serial.read() != '0'){  
  reqSENTEKpresetHoldReg_Moisture();delay(40);
  reqSENTEKpresetHoldReg_Salinity(); delay(40); 
   
  //reqSENTEKpresetHoldReg_Temperature();delay(400);
  Serial.print("Temp      :");Serial.print(getSENTEKtemperature(1),2);  delay(40);Serial.print(" , ");Serial.print(getSENTEKtemperature(2),2);delay(40);Serial.print(" , ");Serial.println(getSENTEKtemperature(3),2); 
  
  // reqSENTEKpresetHoldReg_Moisture();delay(400);
  Serial.print("Moisture  :");Serial.print(getSENTEKmoisture(1),2); delay(400);Serial.print(" , ");Serial.print(getSENTEKmoisture(2),2);delay(400);Serial.print(" , ");Serial.println(getSENTEKmoisture(3),2);
  
  //reqSENTEKpresetHoldReg_Salinity();
  delay(40);
  Serial.print("Salinity  :");Serial.print(getSENTEKsalinity(1),2);delay(40);Serial.print(" , ");Serial.print(getSENTEKsalinity(2),2);delay(40);Serial.print(" , ");Serial.println(getSENTEKsalinity(3),2);  
  }

}

void Conductivity(void){
 Serial.println(CLS);
 
  setConductivityDriver(70, 1);
  setupTemperaturePT1000();  

 while(Serial.read() != '0'){
   Serial.println("\033[0;0H");  
   getELconductance();   
   delay(1000);
  }    
}
//void Conductivity(void){
//
//int sensorValue=0, maxval=0;
//int i =10000; //10.000 semls
//int MuxSwitch = 1;
//float Resistance =0;
//char x = 'x'; 
//                       //Rmux  10M  1M  100K 10K
// pinMode(44, OUTPUT);  //A0      0   1    0   1
// pinMode(45, OUTPUT);  //A1      0   0    1   1
// pinMode(53, OUTPUT);  //EN
// 
// 
//
//
//
//Serial.println(CLS);
//Serial.println("Conductivity Test :\n\n\n");
//Serial.println("With 'c' change chanel");
//
//digitalWrite(53, HIGH); //Enable mux
//
//while ( x != '0'){
//  
//  x = Serial.read();
//  
//  if (x == 'c'){
//    
//    switch (MuxSwitch){
//      case 1: digitalWrite(44, LOW);    
//              digitalWrite(45, LOW);Serial.print("Chanel CH");Serial.println(MuxSwitch);MuxSwitch =2 ;  break;
//      case 2: digitalWrite(44, HIGH);    
//              digitalWrite(45, LOW);Serial.print("Chanel CH");Serial.println(MuxSwitch);MuxSwitch =3 ;  break;;
//      case 3: digitalWrite(44, LOW);    
//              digitalWrite(45, HIGH);Serial.print("Chanel CH");Serial.println(MuxSwitch);MuxSwitch =4 ;  break;;
//      case 4: digitalWrite(44, HIGH);    
//              digitalWrite(45, HIGH);Serial.print("Chanel CH");Serial.println(MuxSwitch);MuxSwitch=1;break;       
//    }
//  }
//    i = 3000;
//    maxval = 0;
//    while (i--){
//      sensorValue = analogRead(A6);
//      if (sensorValue > maxval) {maxval = sensorValue;}//searching for max of SINE signal
//    }
//    Serial.write(cr);Serial.print("SINmax = ");Serial.print((float)maxval*5/1023 - 2.5);
//    delay(400);
//}
//
//  
//}


void SDI12(void){

boolean TX = LOW, EN = LOW;
char x = 'x';
  
 DDRK  = DDRK  | 0x40; //PK6 is out.
 PORTK = PORTK & 0xBF; //PK6 is LOW.  - SDI12_Direction
 //PORTK = PORTK | 0x40;   //PK6 is HIGH. - SDI12_Direction
 

 DDRJ  = DDRJ  | 0x80; //PJ7 is OUT.
 PORTJ = PORTJ | 0x80;  //PJ7 is HIGH. - SDI12_TX

 DDRJ  = DDRJ  & 0xFB; //PJ2 is LOW - INput.    - SDI12_RX

  pinMode(42, OUTPUT); //GREEN_LED 
  pinMode(43, OUTPUT); //RED_LED

  Serial.println(CLS);
  Serial.println("SDI 12 Loopback Test");
  Serial.println("TX = RED_LED & RX = GREEN_LED. (inverted) 'e' - Enable/Disable ");     

while ( x != '0' ){

x = Serial.read();

if (x == 'e'){
  if (EN) { EN = LOW;  PORTK = PORTK & 0xBF;  Serial.write(cr);Serial.println("TX Enable");}
  else    { EN = HIGH; PORTK = PORTK | 0x40;  Serial.write(cr);Serial.println("TX Disable");}
  
  }

//----Togling SDI12_TX-------------// 
if ( TX ) {  
  TX = LOW;
  PORTJ = PORTJ & 0x7F;  //TX is LOW.
  digitalWrite(43,LOW);  //RED led 
 
}  
else{
  TX = HIGH;
  PORTJ = PORTJ | 0x80;  //TX HIGH.
  digitalWrite(43,HIGH); //RED led
     
}


if ( (PINJ  & 0x04) == 0x04) {
  digitalWrite(42,HIGH);
}
else {
  digitalWrite(42,LOW);
} 
  
  
delay (1000);  
}
 
}

void UART3(void){
  
 char x = 'x', z='z';
 Serial.print(CLS);
 Serial.println("UART3 LoopBack Test :\n");
 Serial.println("Short Connect RX and TX on Blue Connector");
 
 Serial3.begin(115200); 
 
  while((x != '0')){
     while(!Serial.available()){}
     x = Serial.read();
     Serial3.print(x); 
     delay(20); 
     z = Serial3.read();
     delay(20);
     Serial.print(z);  
     delay(20);  
  }
 
}


void GSMset(void){
  
digitalWrite(7, LOW);
delay(2000);
digitalWrite(7, HIGH); 
Serial.println("GSM restarted");  
delay(2000);
}

void SMS_test(void) {
  
bool gsm_started = false;
char x = 'x';
char c;
Serial.println(CLS);
Serial.println("SMS Test");


  if (gsm.begin(9600))
  {
    Serial.println("\nGSM READY:\n");
    inet.attachGPRS("prepaidnet","mts","gprs");  
    Serial.println("Sending SMS: 0 for NO, 1 for YES:");
    while(!Serial.available()){}
    x = Serial.read(); 
    if (x == '1'){sms.SendSMS("0646116952", "SmartWatering Controler A005");}   
    if (x == '0'){return;}
  }
  else
  {
    Serial.println(F("\nstatus=IDLE"));
  }
    while (c = gsm.read()) {
      Serial.print(c);
    }

}

void OperaterChanging(void){
 Serial.println(CLS);
 Serial.println(F("Operater Detecting Test")); 
 Serial.println(F("GSM connecting.....")); 
 if (gsm.begin(9600)) 
  {
    gsm_started = true;
    Serial.println(F("\GSM = READY"));
    }
  else
  {
    gsm_started = false;
    Serial.println(F("\GSM = IDLE"));
 
  }

doesOperaterChanging();

EEPROM_readAnything(EERPOM_mem_loc_CarrierData, carrierdata);

    char tmp[2]="00"; 
    Serial.println("\n---------------------------");    
    Serial.println("Sadrzaj EEPROM-a : ");   
    EEPROM_readAnything(64, tmp);
    Serial.println(String(tmp).c_str());     
    Serial.println("Data in the Carrier Data Structure");
    Serial.println(carrierdata.apnname);
    Serial.println(carrierdata.apnuser);
    Serial.println(carrierdata.apnpass);        


while ((Serial.read() != '0')){ } 
}

void GPRS_test(void){ 
 Serial.println(CLS);
 Serial.println(F(" GPRS Connecting Test")); 

 if (gsm.begin(9600)) 
  {
    gsm_started = true;
    Serial.println(F("\GSM = READY"));
    }
  else
  {
    gsm_started = false;
    Serial.println(F("\GSM = IDLE"));
 
  }
  
if (gsm_started)
  {
    //GPRS attach, put in order APN, username and password.
    if (inet.attachGPRS("internet", "telenor", "gprs"))
//  if (inet.attachGPRS(String(carrierdata.apnname).c_str(), String(carrierdata.apnuser).c_str(), String(carrierdata.apnpass).c_str()))
    {
#ifdef DEBUG
     Serial.println(F("GPRS = ATTACHED"));     
     gsm.SimpleWriteln("AT+CIFSR");//Read IP address.
     delay(5000);     
     gsm.WhileSimpleRead();//Read until serial buffer is empty
#endif
    }
    else
    {
#ifdef DEBUG
      Serial.println(F("GPRS = ERROR"));
#endif
    }
    delay(1000);
  }
else
  {
#ifdef DEBUG    
   Serial.println(F("GSM&GPRS=NOTstarted"));
#endif  
  }
delay(3000);  
}

void GPRSdataSending(void){  
 Serial.println(CLS);
 Serial.println(F(" GPRS Data TX/RX test...."));   
char msg[1000];
while ((Serial.read() != '0')){   
  memset(msg, 0, sizeof(msg));                                            //Tresnja Zones
  inet.httpPOST("app.smartwatering.rs", 80, "/api/post/sync_device.php", "did=NmL73F&t1=1.11&t2=2.22&t3=3.33&m1=0.00&m2=0.00&m3=0.00&mid=&fid=&ferph=174&cfl=2.22&tfv=3.33&ecv=4.44&iids=", msg, 1000);  
  Serial.println(msg);   
  delay(4000);
}
}


void loop(){
  
 char x = 'x';

while (1){
  
Serial.println(CLS);

Serial.println( WirmVer);

Serial.println("\nTest Menu :\n");


Serial.println("1) RTC   Test");
Serial.println("2) Flow  Test");
Serial.println("3) Flow  with PWM");
Serial.println("4) Flow  Test");
Serial.println("5) pH    Test");
Serial.println("6) RS485 Test");
Serial.println("7) SMS  Test");
Serial.println("8) Current IN/OUT Test");
Serial.println("9) Analog IN      Test");
Serial.println("o) OperaterChanging");
Serial.println("g) GPRS test");
Serial.println("h) Humidity and Temp  I2C Test");
Serial.println("t) Temperature    Test");
Serial.println("e) El. Conductivity  Test");
Serial.println("s) SENTEK Probe   Test");
Serial.println("u) UART3-Loopaback   Test");

Serial.println("b) Relays ObeByOne   Test");

 while(!Serial.available()){}
 x = Serial.read();
 

     switch (x){
           
           case '1':RTC(); break;
           case '2':Flow();break; 
           case '3':FlowPWM();break;
           case '4':break; 
           case '5':pH();break;
           case '6':RS485();break;
           case '7':SMS_test();break;
           case '8':CurrentIN_OUT();break;
           case '9':AnalogIN();break;
           case 'o':OperaterChanging();break;
           case 'g':GPRS_test();break;
           case 'h':Humidity_Temperature();break;
           case 't':Temperature();break;
           case 'p':PWM();break;
           case 's':sentek();break;
           case 'e':Conductivity();break;
           case 'u':UART3();break;
           case 'r':break;
           case 'b':Relays_OneByOne();
           
           default:break;
               
     } 
 
 
            
} 
}
