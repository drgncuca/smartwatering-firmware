#include <Arduino.h>
#include "Telemetry.h"

#ifdef FLOW_100L_imp

unsigned int    flowImplseTick100L = 0;
unsigned long deltaTime = 0L; 
unsigned long   oldTime = 0L; 

void flowImplseTick100L_Procesing(void)
{
	
flowImplseTick100L = flowImplseTick100L + 100; // Svaki Impuls 100L protekle vode
deltaTime = (millis() - oldTime) / 1000.0 ; // u sekundama
oldTime   = millis();
//Serial.print(F("Flow Tick "));    Serial.println(flowImplseTick100L);	
}
unsigned int getFlowImplseTick100L(void)
{
return flowImplseTick100L;
}
// protok L/min za protokomere od impuls/100L
float getFlow100L(void){
//Serial.print(F("Delta Time "));Serial.println(deltaTime);
if (deltaTime !=0 ) return 6000.0/(float)deltaTime; // 60*100 = 60s*100L	
else return 0;
}

void resetFlowImpulseTick100L(void){
flowImplseTick100L = 0;
deltaTime = 0L; 
oldTime = 0L; 	
}
#endif


void initialiseIrrFlowMet(void){
		irrigationFlowMetering.irrigationFlowVolume = 0.00;
		irrigationFlowMetering.ElapsedTime=0.00;
		irrigationFlowMetering.flowMeterCalibKons =1.0;	
	#ifdef DEBUG_FLOW 	
		irrigationFlowMetering[i].flow = 0.00;
	#endif		
}
//-----------------------------------------------------------------------
float getCurrentVOL( ){

unsigned long currentTime = millis();

unsigned long samplElapsedTime = currentTime  - irrigationFlowMetering.ElapsedTime; // proteklo vreme od zadnjeg semplovanja

if (samplElapsedTime < 0) {samplElapsedTime = 0;} //kad miles() funkcija dodje do kraja i pocne ispoceta da broji, prethodna jednacina bi imala ogromanu negativnu vrednost		
irrigationFlowMetering.ElapsedTime = currentTime;

//integralenje protoka po vremenu: Protok * Vreme = ZAPREMINA.	
float tmp = getCurrentFlow();

float  deltaVolume = ( tmp *  (float)(samplElapsedTime)  ) /1000.0; //    protok * vreme = zapremina. millis() vraca u mS vreme.
	
#ifdef DEBUG_FLOW 	
	irrigationFlowMetering.flow = getCurrentFlow();
#endif																				  
//Kad ce resetovati ova promenljiva ? ? ?  
irrigationFlowMetering.irrigationFlowVolume = irrigationFlowMetering.irrigationFlowVolume + deltaVolume;
  
return deltaVolume;//irrigationFlowMetering[].irrigationFlowVolume;		
}
// vraca ukupnu proteklu kolicinu vode [L]
float getTotalVolume(void){

return irrigationFlowMetering.irrigationFlowVolume;


}										  
//-------------------------------------------------------	
								// od 0 do  4
float getCurrentFlow(void){  //vraca vrednost protoka Litara / Sekundi 

float tempFlow=0;  
  byte i = 0;
  int sum = 0;
  float flowSensValue = 0;
  
  //filtriranje signala sa ADC-a
  while (i < 4)
  {
    sum = sum + analogRead(A5);
    delay(5);
    i++;
  }
  if(sum<9) sum = 0;


  flowSensValue = (float)sum * 0.250;//averaging
  // vraca vrednost Litara/Sekundi, za sistem od 33imp/L 
  tempFlow = irrigationFlowMetering.flowMeterCalibKons * flowSensValue / flowConstantImpuls33PerSecond;  
//  if (tempFlow<0.01) {tempFlow = 0.0;} //  pojavljuje se gresa/sum pri nekoj matematici, Istraziti gde je....
  if (tempFlow > 8) {tempFlow = 8.0;} //  Ako je protok veci od 8L/S (480L/min) -> ogromna vrednost, nemoguce da je toliki.																																							


  return tempFlow;
 
}
 //-------------------------------------------------------------------------------------
#ifdef DEBUG_FLOW    //ChanelS = [0..4]
void FlowDebug(){ // funkcija za debagovanje 			 
	  Serial.println(F("------------------------------------"));				   
						 

//		Serial.print(F(", dV: "));           Serial.print(getCurrentVOL( flowmeter_sensor));
		Serial.print(F(", FLOW: ")); Serial.print(irrigationFlowMetering.flow);Serial.print(F("L/s"));	 	
		Serial.print(F(", V: "));    Serial.print(irrigationFlowMetering.irrigationFlowVolume);
		Serial.print(F("L,Time: "));Serial.print(((irrigationFlowMetering.ElapsedTime)/1000.0) );Serial.println(F("S."));
	  
}
#endif


