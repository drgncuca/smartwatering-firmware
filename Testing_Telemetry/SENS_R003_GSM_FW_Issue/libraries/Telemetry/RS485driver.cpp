#include <Arduino.h>
#include "Telemetry.h"


//-------Disable  RX  AND  TX ---------------------//    
void RS485setup(void){       
  
    //TX driver
    DDRH  = DDRH  | 0x04; //PH2 is out.
    PORTH = PORTH & 0xFB; //PH2 is LOW. ->Disable TX
    
    // set all RX enable control pins, as OUT and non-active.
    // CH1  RX,
    DDRD  = DDRD  | 0x10; //PD4 is out.
    PORTD = PORTD | 0x10;   //PD4 is HIGH -> Disable receiv data.
    
    // CH2  RX,
    DDRD  = DDRD  | 0x80;  //PD7 is out.
    PORTD = PORTD | 0x80;  //PD7 is HIGH -> Disable receiv data.
    
    // CH3  RX,
    DDRD  = DDRD  | 0x40;  //PD6 is out.
    PORTD = PORTD | 0x40;  //PD6 is HIGH -> Disable receiv data
    
    // CH4  RX,
    DDRD  = DDRD  | 0x20;  //PD5 is out.
    PORTD = PORTD | 0x20;  //PD5 is HIGH -> Disable receiv data
    
}

//------Enable ONE  RX  port, Other MUST be disabled------------------------//
void RS485readEnableCH(void){
 
  PORTH = PORTH & 0xFB; //PH2=LOW->Disable TX

         PORTD = PORTD & 0xEF;//PD4 is LOW - > Enable Receive data on CH1
         PORTD = PORTD | 0x80;
         PORTD = PORTD | 0x40;
         PORTD = PORTD | 0x20;
}

void RS485writeStringLN(String string){   
    //disable ALL  RX  chanells
     PORTD = PORTD | 0x10;
     PORTD = PORTD | 0x80;
     PORTD = PORTD | 0x40;
     PORTD = PORTD | 0x20;

     // enable TX 
     PORTH = PORTH | 0x04; //PH2 is HIGH.				  

     Serial1.println(string);  

     delay(30); // NEED  some Delay !!!!!!
     //Disable TX
     PORTH = PORTH & 0xFB; //PH2 is LOW.
	 
	 
}

String RS485readString(void){
	RS485readEnableCH(); 

	String str = Serial1.readString();  

  return str; 
  
}


void RS485readCharArray(char *CharArray){
  RS485readEnableCH(); 
  delay(550);// Da li ovo icemu sluzi, pitanje je vel'ko ............
  
  byte i = 0;
do{
	
}while ((Serial1.available() > 0) && (Serial1.read() != ':')); // Iscitava RX buffer sve dok ne dodje do ':'

CharArray[0] = ':' ;
i = 1;
 
while (  (Serial1.available() > 0)  && (i<19) )
  {
    char tmp;
	tmp = Serial1.read(); 
	if (tmp == 13) break; // kraj ASCII paketa.
	CharArray[i] = 	tmp;
	i++;
  } 
 
//Serial.print(" [");Serial.print(CharArray);Serial.print("] ");compareLRC(CharArray);

}


