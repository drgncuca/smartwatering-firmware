#include <Arduino.h>
#include <SoftwareSerial.h>
#include "Telemetry.h"
#include "wire.h"

#ifdef LiPo_Charger

#define PMIC_ADDRESS 0x09

#define MAX_Batt_Voltage 12576  //12.544V

#define BattLOWLevel  9.00  //

#define BattHIGHLevel 12.62

#define TIMEREFRCNST 5 //  25 => 5min Na svaki 5min rekalkulise struju punjenje.

extern byte BattCharging; // Status  baterije
byte BattFullCounter = 0;
byte BattLowCounter = 0;

byte timeRefresCharginCurrent = 0;

unsigned int setChargingCurrent ;
extern float BatteryVoltage;
extern int BatteryCChargingCurrent;

#ifdef Power_Saving
extern	byte PowerStandByTimeInMin;
#endif
extern byte BatterySupplying ;


void LiPoChargeInit(void){
 BattCharging = 0;

 // Ne Otkacuj Bateriju sa sistema. 
 pinMode(37, OUTPUT); //BATTERY_RL_R
 digitalWrite(37, LOW); // 
 
 // Zakaci Bateriju na sistem. 
 pinMode(35, OUTPUT); //BATTERY_RL_S
 digitalWrite(35, HIGH); // 
 delay(100);
 digitalWrite(35, LOW); // 
 pinMode(35, INPUT); //Oslobodi pin da moze da hednla taster na ploci
 
 pinMode(36, OUTPUT); //Battery_CH_EN
 digitalWrite(36, HIGH); // Enejbluje napajanje za Charger
 
 pinMode(28, INPUT);//Battery_CH_ACOK

 delay(800);
 /*
#ifdef DEBUG_LiPo_CHG 
 getLiPoDeviceID();
#endif 
*/
writeRegister(0x12,0x81B3); // Disable Charging 

setLiPoChargeVoltage(MAX_Batt_Voltage); // puni bateriju do 12.57V, 	12.6V je max, i sto je blize toj vrednosti skracuje vek baterije !!!
//setLiPoChargeCurrent(1664); //max struja punjenja
setLiPoChargeCurrent(300); //max struja punjenja
setLiPoINcurrent(2560);	// max ulazna struja.

}

	

//  Struje punjanja su: 64*N [mA]
//================================ 20%    ===== 40%   ================ 80%    ========== 100%   =======================
//================================ 11.20V ===== 11.40 ================ 12.00V ========== 12.60V =======================
//=====================================================================================================================	

void LiPoChargeManager(void){
	
float batt   = 0;
float solar  = getVoltageSolar();

digitalWrite(26,HIGH); //Battery Measure ON

if ( digitalRead(28) == 0 )	delay(2500); //  Ako se napaja s baterije, onda zbog RC filtera treba da saceka nekih 5 * R * C 

	batt = getVoltageBatt();
	BatteryVoltage = batt;


#ifdef DEBUG_LiPo_CHG
	Serial.println("------------------------------------------");
	Serial.print("SOLAR ");  Serial.print(solar); Serial.println("V");
	Serial.print("BATT  ");  Serial.print(batt);  Serial.println("V"); 
//	Serial.print(F("Current OUT:"));Serial.print(getLiPoChargeCurrent());Serial.println(F(" mA"));
#endif

//PUNJENJE optimizacija na svakih ( 100*12s = 20min )pokreni dopunjavanje baterije..
if ( digitalRead(28) == 1 ){ // Ako je napon solara u granicama od 13.5V do 24.5V 
	// Nakaci bateriju na sistem ako ima napona SOLARA
    pinMode(35, OUTPUT); 
	digitalWrite(35, HIGH); // Zakaci Bateriju na sistem. 
	delay(100);
	digitalWrite(35, LOW); // 
	pinMode(35, INPUT); //Oslobodi pin da moze da hednla taster na ploci		
#ifdef DEBUG_LiPo_CHG		
	Serial.println(F("SOLAR Suppling")); // pin 28- Battery_CH_ACOK
#endif	
	if (solar<13.5){
		BattCharging = 0;	
		stopLiPoCharging();	
		timeRefresCharginCurrent = 0;
		BatterySupplying = 0;
#ifdef DEBUG_LiPo_CHG		
	Serial.println(F("SOLAR to LOW for charging")); 
#endif
		return;
	}
	if ( timeRefresCharginCurrent == 0 ){
					 byte CurrentMultiplier = 2;
		if (solar < 16.5) CurrentMultiplier = 1;

		
#ifdef Charging_SENS_R003  // Rsens = 10mohm
		if (batt<10.51) setChargingCurrent = 384 * CurrentMultiplier;
		if (batt<7.5)   setChargingCurrent = 256 * CurrentMultiplier;
		if (batt>10.50) setChargingCurrent = 768 * CurrentMultiplier;	
//		if (batt>12.40) setChargingCurrent = 512;
		startLiPoCharging(setChargingCurrent);		
		BattCharging = 1;
		timeRefresCharginCurrent = TIMEREFRCNST;
		delay(50);		
		BatteryCChargingCurrent = 25 * analogRead(A10);
#endif		
#ifdef Charging_SENS_R002	  // Rsens = 20mohm
		if (batt<10.51) setChargingCurrent = 640 * CurrentMultiplier;
		if (batt<7.5)   setChargingCurrent = 384 * CurrentMultiplier;
		if (batt>10.50) setChargingCurrent = 1024 * CurrentMultiplier;	
//		if (batt>12.40) setChargingCurrent = 704;
		startLiPoCharging(setChargingCurrent);		
		BattCharging = 1;
		timeRefresCharginCurrent = TIMEREFRCNST;
		delay(50);		
		BatteryCChargingCurrent = 4 * analogRead(A10);
#endif

#ifdef DEBUG_LiPo_CHG	
	Serial.print(F("Charging Current Set to ----> ")); Serial.print(setChargingCurrent); Serial.println(F("mA"));
#endif	
	}
	BatterySupplying = 0;
	timeRefresCharginCurrent = timeRefresCharginCurrent - 1 ;
}
else{//Nema Solara da puni bateriju 
	stopLiPoCharging();
	BattCharging = 0; 
	BatterySupplying = 1;
	timeRefresCharginCurrent = 0;
#ifdef DEBUG_LiPo_CHG	
	Serial.println(F("BATTERY  Suppling"));
#endif
		//EXTRIMELY Low Battery 
	if(batt < BattLOWLevel ){	
	#ifdef DEBUG_LiPo_CHG	
			Serial.println(F("Batt Extrimely LOW -> Cutt OFF Instant"));   
	#endif
		stopLiPoCharging();
		delay(100);	
		stopAll();
	// Raskaci bateriju sa sistema, GASI  KONTROLER !!!!!!! 
		digitalWrite(37, HIGH); 
		delay(100);//ovde ce se ugasiti uC posle par uS....
		digitalWrite(37, LOW);
		pinMode(37, INPUT); 	
	}
	stopLiPoCharging();
	digitalWrite(26,LOW); //Battery Measure OFF		
}

// Brute force STOP charging
if (batt > BattHIGHLevel ) {
		stopLiPoCharging();
#ifdef DEBUG_LiPo_CHG	
		Serial.println(F("Brute Force STOP Charging"));
#endif	
	BattFullCounter = 0;
	BattCharging = 0;
}

unsigned  tmp =	readLiPoChargeReg(); // keep alive charging proces, just ping the charger 
if (tmp & 0x01 == 1) {
#ifdef DEBUG_LiPo_CHG
  Serial.println(F("Charging STOPED"));// Charger zaustavio punjenje.
#endif	
	BattCharging = 0; 
}

if ( (tmp !=0x81B2) && (BattCharging ==1) ){
#ifdef DEBUG_LiPo_CHG	
	Serial.print(F("Re-Start charging"));
#endif	
	writeRegister(0x12,0x81B2);	
}

}


//================================================================================================	
void getLiPoDeviceID(void){
//Wire.begin();
Serial.print(F("Read Device ID: "));Serial.println(readRegister(0xFE),HEX);
Serial.print(F("Read Manufa ID: "));Serial.println(readRegister(0xFF),HEX);
//Wire.end();

}
unsigned int getLiPoChargeOption(void){
	return readRegister(0x12);
}

unsigned int readLiPoChargeReg(void){
unsigned int tmp = readRegister(0x12);	
return tmp;
}

unsigned int getLiPoInputCurrent(void){
unsigned int tmp = readRegister(0x3F);	
//Wire.begin();
//Serial.print(F("InputCurrent: "));Serial.print(F(" , "));Serial.print(tmp);Serial.println(F(" mA"));//Serial.print(tmp,HEX);
//Wire.end();
//tmp = tmp>>7;
//tmp = 128 * tmp; // 128mA
//Serial.print(F(" , "));Serial.print(tmp);Serial.println(F(" mA"));
return tmp;
}

unsigned int getLiPoChargeCurrent(void){
unsigned int tmp = readRegister(0x14);	
//Wire.begin();
//Serial.print(F("ChargeCurrent: "));Serial.print(F(" , "));Serial.print(tmp);Serial.println(F(" mA"));//Serial.print(tmp,HEX);
//Wire.end();
//tmp = tmp >> 6;
//tmp = 64 * tmp; // 64mA
//Serial.print(F(" , "));Serial.print(tmp);Serial.println(F(" mA"));
return tmp;
}

unsigned int getLiPoChargeVoltage(void){
unsigned int tmp = readRegister(0x15);	
//Wire.begin();
//Serial.print(F("ChargeVoltage: "));Serial.print(F(" , "));Serial.print(tmp);Serial.println(F(" mV"));//Serial.print(tmp,HEX);
//Wire.end();
//tmp = tmp>>4;
//tmp = 16 * tmp; // 16mV
//Serial.print(F(" , "));Serial.print(tmp);Serial.println(F(" mV"));
return tmp;
}

void setLiPoChargeVoltage( unsigned int Voltage){

Voltage = Voltage & 0x7FF0;
writeRegister(0x15,Voltage);	
}

void setLiPoChargeCurrent( unsigned int Current){

Current = Current & 0x1FC0;
writeRegister(0x14,Current);	
}

void setLiPoINcurrent( unsigned int Current){

Current = Current & 0x1F80;
writeRegister(0x3F,Current);	
}

void stopLiPoCharging(){
unsigned int tmp  = 	getLiPoChargeOption();
tmp = tmp | 0x01;

writeRegister(0x12,tmp);//DIsejbluj punjenje	
}

void startLiPoCharging(unsigned int Current){

//Bilo Orginalno  12560 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
setLiPoChargeVoltage(MAX_Batt_Voltage); // puni bateriju do 12.56V, 	12.6V je max, i sto je blize toj vrednosti skracuje vek baterije !!!
delay(10);
setLiPoChargeCurrent(Current); //max struja punjenja
//delay(10);
//setLiPoINcurrent(2048);	// max ulazna struja.	
delay(10);	
writeRegister(0x12,0x81B2);// start charging


}

void setWdogLiPoCharger(byte timeRange){ // 0 - disable WDOG
										 // 1 - 44s
										 // 2 - 88s		
										 // 3 - 175s
unsigned int tmp  = 	getLiPoChargeOption(); // Charge Options Register			 					
switch(timeRange){
	case 0: tmp = tmp & 0x9FFF; break; 
	case 1: tmp = tmp & 0xBFFF; tmp = tmp | 0x2000; break;
	case 2: tmp = tmp & 0xDFFF; tmp = tmp | 0x4000; break;
	case 3: tmp = tmp | 0x6000; break;		
}
#ifdef DEBUG_LiPo_CHG		
//	Serial.print(F("Write S_REG: ")); Serial.println(tmp,HEX);
#endif
writeRegister(0x12,tmp);

	
}


unsigned int readRegister(byte address) {
    Wire.beginTransmission(PMIC_ADDRESS);
    Wire.write(address);
    if (Wire.endTransmission(true) != 0) {
#ifdef DEBUG_LiPo_CHG		
		//Serial.println(F("error Read CHG"));
#endif		
      return -1;
    }
	
	byte low  ;
	byte high ;
    if (Wire.requestFrom(PMIC_ADDRESS, 2, true) == 2) {
		low  =  Wire.read();
//		Serial.print(F("LOW : "));Serial.println(low,HEX);
		high  = Wire.read();
//		Serial.print(F("HIG : "));Serial.println(high,HEX);		
    }
	
	unsigned int tmp = high;
	tmp = tmp <<8;
	tmp = tmp + low;	
//	Serial.print(F("LOW + HIG : "));Serial.println(tmp,HEX);	
    return tmp;
}

byte writeRegister(byte address, unsigned int val) {
    Wire.beginTransmission(PMIC_ADDRESS); // 0x09
    Wire.write(address);                  //0x15
	
	byte LOWer   = (byte)  (val & 0xFF) ;
	byte HIGHer  = (byte)  ((val >> 8) & 0xFF); 
//	Serial.print(F("Write Registar: ")); Serial.print(val); Serial.print(F(", H")); Serial.print(HIGHer); Serial.print(F(", L")); Serial.println(LOWer);
	
	Wire.write(LOWer);  // LOW Data
    Wire.write(HIGHer); // HIGH Data 

    if (Wire.endTransmission(true) != 0) {
#ifdef DEBUG_LiPo_CHG		
//		Serial.println(F("Error Write to CHG"));
#endif		
        return 0;
    }
//Wire.end(); // pitanje je da li ovo treba......

 //   Wire.beginTransmission(PMIC_ADDRESS); // 0x09
 //   Wire.write(address);                  //0x15
	
	
//	Serial.print(F("Write Registar: ")); Serial.print(val); Serial.print(F(", H")); Serial.print(HIGHer); Serial.print(F(", L")); Serial.println(LOWer);
	
	//Wire.write(LOWer);  // LOW Data
 //   Wire.write(HIGHer); // HIGH Data 

 //   if (Wire.endTransmission(true) != 0) {
//		Serial.print(F("Error III"));
 //       return 0;
 //   }




  return 1;
}



#endif