#include <Arduino.h>
#include <EEPROM.h>
#include "EEPROMAnything.h"
#include <SoftwareSerial.h>
#include "SIM900.h"
#include "inetGSM.h"
#include "sms.h"
#include <String.h>
#include "Telemetry.h"
#include "MemoryFree.h"
#define MSG_BUF 1000


InetGSM inet;

bool gsm_started = false;
bool completed = false;
char msg[MSG_BUF]; 
carrier carrierdata;
struct_operater_vip operaterVIP;
struct_operater_MTS operaterMTS;
struct_operater_Telenor operaterTelenor;






SMSGSM sms;
void setup() {

char tmp[2]="00"; 
  Serial.begin(115200);  
  Serial.println(CLS); 
  Serial.println(("(RE)start uC:"));

  pinMode(7, OUTPUT);
  digitalWrite(7, HIGH);
  //delay(50);  
  digitalWrite(7, LOW);  
  //delay(5000);  
  digitalWrite(7, HIGH); 
  //delay(50); 
   if (gsm.begin(9600))
  {
    Serial.println(("\nGSM status=READY"));
    gsm_started = true;
  }
  else
  {
    Serial.println(("\nGSM status=IDLE"));
  }

   doesOperaterChanging();  
 //Serial3.print("WhileSimpleRead");
 //gsm.WhileSimpleRead();
 //Serial3.print(F("RSSI[dB]: "));Serial.println(getRSSI());
 
  EEPROM_readAnything(64, tmp); 
  EEPROM_readAnything(EERPOM_mem_loc_CarrierData , carrierdata);   
    Serial.println(F("Data Carrier from EEPROM"));
    Serial.println(carrierdata.apnname);
    Serial.println(carrierdata.apnuser);
    Serial.println(carrierdata.apnpass); 

    //GPRS attach, put in order APN, username and password.
    if (inet.attachGPRS(String(carrierdata.apnname).c_str(), String(carrierdata.apnuser).c_str(), String(carrierdata.apnpass).c_str()))    
    {
    Serial.println(F("GPRS = ATTACHED"));
      //Read IP address.
    gsm.SimpleWriteln("AT+CIFSR");
    delay(5000); // cemu ovaj delay ?-> ne moze saban modul da tako brzo odgovori, nabijem ga.
    //Read until serial buffer is empty.s
     Serial.print("IP : ");
    gsm.WhileSimpleRead();
    }
    else
    {

      Serial3.println(F("GPRS = ERROR"));

    }
memset(msg, 0, sizeof(msg));
}


void loop() {

byte sms_count=0;
byte smspos = 0;
  char sms_txt[30];  //30
  char phone_number[20]; //20    
int counterloop =1;

// String xxString; 
// char xx[60]; 
// gsm.SimpleWriteln("AT+CBC"); // meri napon
// delay(2000);//  Add Max Response Time
// gsm.SimpleRead1(xx);
// xxString = String(xx).c_str();  
//Serial.print(F("Voltage:")); Serial.println (xxString);
 
while(1){
 //doesOperaterChanging();  
 //Serial3.print("WhileSimpleRead");
 //gsm.WhileSimpleRead();
 //Serial3.print(F("RSSI[dB]: "));Serial.println(getRSSI());
 
 Serial.print(F("Loop Test : ")); Serial.print(counterloop++);Serial.print(F("  Free memory: loop ")); Serial.println( freeMemory());


//------------------------------------------- 
 
     completed = inet.httpPOST( "app.smartwatering.rs", 80, "/api/post/sync.php", "pid=A4dgm&h1=0&h2=0&mid=&fid=&fid2=&fid3=&fid4=&ferph=430&cfl=0.00&tfv=0.00&ecv=0.80&iids=", msg, 1000);
     if(completed ){
        Serial.println(F("Sync"));
        Serial.println (String(msg).c_str()); 
         Serial.println(F("------------------------------------------------------------------"));         
     }
    
    else
    {
      Serial.println(F("GPRS = ERROR"));
      inet.dettachGPRS();
      delay(2000);
      inet.attachGPRS(String(carrierdata.apnname).c_str(), String(carrierdata.apnuser).c_str(), String(carrierdata.apnpass).c_str());
      delay(2000);
      gsm.SimpleWriteln("AT+CIFSR");
      delay(5000); 
      //Read until serial buffer is empty.s
      gsm.WhileSimpleRead();
    }   
//------------------------------------------------------------
      completed = inet.httpPOST( "app.smartwatering.rs", 80, "/api/post/debug/sync0.php", "pid=A4dgm&h1=0&h2=0&mid=&fid=&fid2=&fid3=&fid4=&ferph=430&cfl=0.00&tfv=0.00&ecv=0.80&iids=", msg, 1000);
     if(completed ){
        Serial.println(F("sync0"));
        Serial.println (String(msg).c_str()); 
         Serial.println(F("------------------------------------------------------------------"));       
     }
    
    else
    {
      Serial.println(F("GPRS = ERROR"));
    }
//-----------------------------------------------------------
      completed = inet.httpPOST( "app.smartwatering.rs", 80, "/api/post/debug/sync100.php", "pid=A4dgm&h1=0&h2=0&mid=&fid=&fid2=&fid3=&fid4=&ferph=430&cfl=0.00&tfv=0.00&ecv=0.80&iids=", msg, 1000);
     if(completed ){
        Serial.println(F("sync100"));
        Serial.println (String(msg).c_str()); 
        Serial.println(F("------------------------------------------------------------------"));          
     }
    
    else
    {
      Serial.println(F("GPRS = ERROR"));
    }
    //-----------------------------------------------------------
      completed = inet.httpPOST( "app.smartwatering.rs", 80, "/api/post/debug/sync200.php", "pid=A4dgm&h1=0&h2=0&mid=&fid=&fid2=&fid3=&fid4=&ferph=430&cfl=0.00&tfv=0.00&ecv=0.80&iids=", msg, 1000);
     if(completed ){
        Serial.println(F("sync200"));
        Serial.println (String(msg).c_str()); 
        Serial.println(F("------------------------------------------------------------------"));          
     }
    
    else
    {
      Serial.println(F("GPRS = ERROR"));
    }
        //-----------------------------------------------------------
      completed = inet.httpPOST( "app.smartwatering.rs", 80, "/api/post/debug/sync400.php", "pid=A4dgm&h1=0&h2=0&mid=&fid=&fid2=&fid3=&fid4=&ferph=430&cfl=0.00&tfv=0.00&ecv=0.80&iids=", msg, 1000);
     if(completed ){
        Serial.println(F("sync400"));
        Serial.println (String(msg).c_str()); 
        Serial.println(F("------------------------------------------------------------------"));          
     }
    
    else
    {
      Serial.println(F("GPRS = ERROR"));
    }
        
    
   
  }// while(1)


  



while(sms_count<3){
  
  smspos = sms.IsSMSPresent(SMS_UNREAD);   
   if(smspos){     
    
     sms.GetSMS(smspos, phone_number, 20, sms_txt, 30);

     Serial.println("SMS :");
    Serial.print("Br. Tel : ");Serial.print(String(phone_number).c_str()); Serial.print(", SMS : ");Serial.println(String(sms_txt).c_str()); 
    delay(1500);
    sms_count++;
   }
 delay(2550);
 

        

  //Serial.print("L o o p "); Serial.println(counterloop++);
}


 Serial.println("Print ALL SMS :");
sms_count=1;
while(sms_count<4){
sms.GetSMS(sms_count, phone_number, 20, sms_txt, 30);  
Serial.print(", SMS : "); Serial.print(sms_count);Serial.println(String(sms_txt).c_str()); 

sms_count++;
}

delay(2500);
Serial.println("Delete ALL SMS :");
 deleteALLsms();
 sms_count=1;
 
while(sms_count<4){
sms.GetSMS(sms_count, phone_number, 20, sms_txt, 30);  
Serial.print(", SMS : "); Serial.print(sms_count);Serial.println(String(sms_txt).c_str()); 

sms_count++;
}

}
