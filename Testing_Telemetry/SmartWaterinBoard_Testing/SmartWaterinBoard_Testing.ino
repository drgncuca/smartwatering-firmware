
#include <Arduino.h>
#include "inetGSM.h"
#include "SMS.h"
//#include <RTC.h>
#include "DS3231M.h"
#include <MAX31855.h> 

#define CLS          "\033[2J"  
#define WirmVer      "Firmware V1.01  (Functional Verification Testing)"
#define SummPassword  17

#include <SPI.h>
#include <MD_AD9833.h>

SMSGSM sms;
InetGSM inet;
DS3231M_Class DS3231M;



// Pins for SPI comm with the AD9833 IC
#define DATA  50  ///< SPI Data pin number
#define CLK   52  ///< SPI Clock pin number
#define FSYNC 53  ///< SPI Load pin number (FSYNC in AD9833 usage)

MD_AD9833 AD(FSYNC); // Hardware SPI



void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  
 
    
  pinMode(22, OUTPUT); //Port PA
  pinMode(23, OUTPUT);
  pinMode(24, OUTPUT);
  pinMode(25, OUTPUT);
  pinMode(26, OUTPUT);
  pinMode(27, OUTPUT);  
  pinMode(28, OUTPUT); 
  pinMode(29, OUTPUT);

  pinMode(37, OUTPUT); //Port PC
  pinMode(36, OUTPUT);
  pinMode(35, OUTPUT);
  pinMode(34, OUTPUT);
  pinMode(33, OUTPUT);
  pinMode(32, OUTPUT);
  pinMode(31, OUTPUT);
  pinMode(30, OUTPUT);  

  pinMode(41, OUTPUT); //Port PG
  pinMode(40, OUTPUT);
  pinMode(39, OUTPUT);   
  pinMode(75, OUTPUT);
   
  pinMode(76, OUTPUT); //RTC reset

  //--- Current OUT PWM------// 
  pinMode(5, OUTPUT);  //CH1
  pinMode(3, OUTPUT);  //CH2
  pinMode(6, OUTPUT);  //CH3
  pinMode(9, OUTPUT);  //CH4
  pinMode(8, OUTPUT);  //CH5

  Serial.begin(115200);
  
  
}
void Relays75_76(void){
while((Serial.read() != '0')){
  digitalWrite(36, HIGH);delay(50);
  digitalWrite(37, HIGH);delay(50);  
  delay(500);  
  digitalWrite(36, LOW);delay(50);
  digitalWrite(37, LOW);delay(50);
  
  }  
  
  
}
void Relays(void){

Serial.println(CLS);
Serial.println("\n Relay Test ");
Serial.println("pres 0 for EXIT\n");
while((Serial.read() != '0')){
  
  digitalWrite(41, HIGH);delay(50);
  digitalWrite(40, HIGH);delay(50);
  digitalWrite(39, HIGH);delay(50);
  //digitalWrite(75, HIGH);delay(50); //ne fercera
  //PORTG = PORTG | 0x08;  //PG3 is HIGH 
  digitalWrite(75, HIGH);delay(50);
 
  
  digitalWrite(22, HIGH);delay(50);
  digitalWrite(23, HIGH);delay(50);
  digitalWrite(24, HIGH);delay(50);
  digitalWrite(25, HIGH);delay(50);
  digitalWrite(26, HIGH);delay(50);
  digitalWrite(27, HIGH);delay(50);
  digitalWrite(28, HIGH);delay(50);

  digitalWrite(37, HIGH);delay(50);
  digitalWrite(36, HIGH);delay(50);
  digitalWrite(35, HIGH);delay(50);
  digitalWrite(34, HIGH);delay(50);
  digitalWrite(33, HIGH);delay(50);
  digitalWrite(32, HIGH);delay(50);
  digitalWrite(31, HIGH);delay(50);
  digitalWrite(30, HIGH);delay(50);


  delay(250); 

  digitalWrite(22, LOW);delay(50);
  digitalWrite(23, LOW);delay(50);
  digitalWrite(24, LOW);delay(50);
  digitalWrite(25, LOW);delay(50);
  digitalWrite(26, LOW);delay(50);
  digitalWrite(27, LOW);delay(50);
  digitalWrite(28, LOW);delay(50);

  digitalWrite(37, LOW);delay(50);
  digitalWrite(36, LOW);delay(50);
  digitalWrite(35, LOW);delay(50);
  digitalWrite(34, LOW);delay(50);
  digitalWrite(33, LOW);delay(50);
  digitalWrite(32, LOW);delay(50);
  digitalWrite(31, LOW);delay(50);
  digitalWrite(30, LOW);delay(50);

  digitalWrite(41, LOW);delay(50);
  digitalWrite(40, LOW);delay(50);
  digitalWrite(39, LOW);delay(50);
  //digitalWrite(75, LOW);delay(50);
  //PORTG = PORTG & 0xF7;  //PG3 is LOW
  digitalWrite(75, LOW);delay(50);
  

  delay(250); 


  }
}

void Leds(void){

  pinMode(42, OUTPUT); //GREEN_LED 
  pinMode(43, OUTPUT); //RED_LED
  
  Serial.println(CLS);
  Serial.println("\nLED Test");
  Serial.println("pres 0 for EXIT\n");



  while((Serial.read() != '0')){
  digitalWrite(43, HIGH);   
  digitalWrite(42, LOW);
  delay(150);  
  digitalWrite(43, LOW);    
  digitalWrite(42, HIGH);
  delay(150);  

  }
}

void GsmGprs(void) {
  
bool gsm_started = false;
char x = 'x';
char c;
Serial.println(CLS);
Serial.println("GPRS Test");

  digitalWrite(7, HIGH);  
  delay(20);
  digitalWrite(7, LOW);
  delay(5000);
  digitalWrite(7, HIGH); //In reset state

  if (gsm.begin(9600))
  {
    Serial.println("\nGSM READY:\n");
    inet.attachGPRS("prepaidnet","mts","gprs");  
    Serial.println("Sending SMS: 0 for NO, 1 for YES:");
    while(!Serial.available()){}
    x = Serial.read(); 
    if (x == '1'){sms.SendSMS("0646116952", "SmartWatering Controler A005");} 
    
    
  }
  else
  {
    Serial.println(F("\nstatus=IDLE"));
  }
    while (c = gsm.read()) {
      Serial.print(c);
    }


}
void RTC(void){

char output_buffer[32]; ///< Temporary buffer for sprintf()

Serial.println(CLS);
Serial.println("\nReal Time Clock Test :");
Serial.println("for EXIT pres 0\n");

while (!DS3231M.begin()) // Initialize RTC communications
  {
    Serial.println(F("Unable to find DS3231MM. Checking again in 3s."));
    delay(3000);
  } // of loop until device is located

//DS3231M.adjust(DateTime(2020,03,25,18,42,55));
DS3231M.pinSquareWave(); // Make INT/SQW pin toggle at 1Hz
  
static uint8_t secs = 0;
DateTime now = DS3231M.now(); // get the current time from device



while((Serial.read() != '0')){
  if (secs != now.second())     // Output if seconds have changed
  {
    sprintf(output_buffer,"%04d-%02d-%02d %02d:%02d:%02d", now.year(), now.month(),  now.day(), now.hour(), now.minute(), now.second());
    Serial.print("DATE and TIME : ");Serial.print(output_buffer);
    Serial.write(cr);
    
    secs = now.second(); // Set the counter variable
    
  } // of if the seconds have changed
now = DS3231M.now();
  
}

}

void Flow(void){
 int sensorValue = 0;  // variable to store the value coming from the sensor
 int i = 40;

 float val;
 char buff[3];


while ( (Serial.read() != '0')){
  Serial.print(CLS);
  Serial.println("Flow Measuring Test: \n");
  Serial.println("Trimovati potencimetar PT2, tako da za 270Hz se dobije protok od 8.2 L/s!");
  Serial.println("for EXIT pres 0\n");
  
  sensorValue = analogRead(A3);
  val = ((float)sensorValue * 5) / 1023 ;
  dtostrf(val, 1, 2, buff);
  Serial.print("ADC3 Voltage : "); Serial.print(val);Serial.print("V");Serial.print(" Flow :"); val = (float)sensorValue / 88;  dtostrf(val, 1, 2, buff); Serial.print(val);Serial.println("[L/s]");
  
  sensorValue = analogRead(A1);
  val = ((float)sensorValue * 5) / 1023 ;
  dtostrf(val, 1, 2, buff);
  Serial.print("ADC1 Voltage : "); Serial.print(val);Serial.print("V");Serial.print(" Flow :"); val = (float)sensorValue / 88;  dtostrf(val, 1, 2, buff); Serial.print(val);Serial.println("[L/s]");
  
  sensorValue = analogRead(A0);
  val = ((float)sensorValue * 5) / 1023 ;
  dtostrf(val, 1, 2, buff);
  Serial.print("ADC0 Voltage : "); Serial.print(val);Serial.print("V");Serial.print(" Flow :"); val = (float)sensorValue / 88;  dtostrf(val, 1, 2, buff); Serial.print(val);Serial.println("[L/s]");
  
  sensorValue = analogRead(A4);
 val = ((float)sensorValue * 5) / 1023 ;
  dtostrf(val, 1, 2, buff);
  Serial.print("ADC4 Voltage : "); Serial.print(val);Serial.print("V");Serial.print(" Flow :"); val = (float)sensorValue / 88;  dtostrf(val, 1, 2, buff); Serial.print(val);Serial.println("[L/s]");
  
  sensorValue = analogRead(A5);
  val = ((float)sensorValue * 5) / 1023 ;
  dtostrf(val, 1, 2, buff);
  Serial.print("ADC5 Voltage : "); Serial.print(val);Serial.print("V");Serial.print(" Flow :"); val = (float)sensorValue / 88;  dtostrf(val, 1, 2, buff); Serial.print(val);Serial.println("[L/s]");

  
  delay(1500);
}

  
}

void pH(void){
 int sensorValue = 0;  // variable to store the value coming from the sensor
 float val;
 char buff[3];
 
 Serial.print(CLS);
 Serial.println("pH  Test, Calibrate Rpot=10kohm, for 0Vin, ADC = 1.94V\n");
 Serial.println("for EXIT pres 0\n");
 Serial.println("Voltage reading:");
  
while ( (Serial.read() != '0')){
    
  sensorValue = analogRead(A2);
  val = ((float)sensorValue * 5) / 1023 ;
  dtostrf(val, 1, 2, buff);
  Serial.write(cr); Serial.print(val);Serial.print("V");
 
  delay(500);
}

}

void RS485(void){
 char x = 'x';
 Serial.print(CLS);
 Serial.println("RS485 test :\n");
 Serial.println("Chose chanel from 1 to 4, or 0 for EXIT :");
 Serial2.begin(115200);

// CH1 is TX, data enable transmit
DDRH  = DDRH  | 0x04; //PH2 is out.
PORTH = PORTH | 0x04; //PH2 is HIGH.

// CH1  RX,
DDRD  = DDRD  | 0x10; //PD4 is out.
//PORTD = PORTD & 0xEF; //PD4 is LOW  -> Enable receiv data.
PORTD = PORTD | 0x10;   //PD4 is HIGH -> Disable receiv data.


// CH2  RX,
DDRD  = DDRD  | 0x80;  //PD7 is out.
PORTD = PORTD | 0x80;  //PD7 is HIGH -> Disable receiv data.

// CH3  RX,
DDRD  = DDRD  | 0x40;  //PD6 is out.
PORTD = PORTD | 0x40;  //PD6 is HIGH -> Disable receiv data

// CH4  RX,
DDRD  = DDRD  | 0x20;  //PD5 is out.
PORTD = PORTD | 0x20;  //PD5 is HIGH -> Disable receiv data

 while ( x != '0'){
  x = Serial.read();
  if(x == '1'){
    Serial.println("Chanel 1:");
     PORTD = PORTD & 0xEF;//PD4 is LOW
     PORTD = PORTD | 0x80;
     PORTD = PORTD | 0x40;
     PORTD = PORTD | 0x20;
    }
   if(x == '2'){
    Serial.println("Chanel 2:");
     PORTD = PORTD | 0x10;
     PORTD = PORTD & 0x7F;
     PORTD = PORTD | 0x40;
     PORTD = PORTD | 0x20;
    }
    if(x == '3'){
      Serial.println("Chanel 3:");
     PORTD = PORTD | 0x10;
     PORTD = PORTD | 0x80;
     PORTD = PORTD & 0xBF;
     PORTD = PORTD | 0x20;
    }
    if(x == '4'){
      Serial.println("Chanel 4:");
     PORTD = PORTD | 0x10;
     PORTD = PORTD | 0x80;
     PORTD = PORTD | 0x40;
     PORTD = PORTD & 0xDF;
    }

 
  Serial2.print("ABCD"); 
  Serial.println(Serial2.read());
  delay(1000);
 } 

}

void CurrentIN_OUT(void){
int pulse = 25;
int pause = 75;  //frequency 100Hz.
int sensorValue;
char x = 'x';
char buff[3];
float val;
int i=0;
  
Serial.println(CLS);
Serial.println("Current OUT test:"); Serial.println("Pres 4 and 6 for decrease and increase duty cycle\n\n");
Serial.print("Pulse ");Serial.print(pulse); Serial.print(" mS");Serial.print(" Pause ");Serial.print(pause);Serial.print(" mS");
Serial.print("  Current OUT = ");Serial.print((float)pulse*0.275);Serial.print(" mA");

 pinMode(5, OUTPUT);  //CH1
  pinMode(3, OUTPUT);  //CH2
  pinMode(6, OUTPUT);  //CH3
  pinMode(9, OUTPUT);  //CH4
  pinMode(8, OUTPUT);  //CH5

while ( x != '0'){ 

  x =  Serial.read();
  if (x == '6') {
    pulse++; pause--;
  }
   if (x == '4') {
    pulse--; pause++;
  }

  i++;
  if (i == 10){ // refresh reading every 1s.    
        i=0;  
        Serial.println(CLS);
        Serial.print("Pulse ");Serial.print(pulse); Serial.print(" mS");Serial.print(" Pause ");Serial.print(pause);Serial.print(" mS");
        Serial.print("  Current OUT = ");Serial.print((float)pulse*0.275);Serial.println(" mA");
      
        sensorValue = analogRead(A12);  //CH1
        val = (float)sensorValue / 25 ;
        dtostrf(val, 2, 1, buff);
        Serial.print("Current IN on CH1 = ");Serial.print(val);Serial.println(" mA");
      
        sensorValue = analogRead(A13);  //CH2
        val = (float)sensorValue / 25 ;
        dtostrf(val, 2, 1, buff);
        Serial.print("Current IN on CH2 = ");Serial.print(val);Serial.println(" mA");
        
        sensorValue = analogRead(A11);  //CH3
        val = (float)sensorValue / 25 ;
        dtostrf(val, 2, 1, buff);
        Serial.print("Current IN on CH3 = ");Serial.print(val);Serial.println(" mA");
        
        sensorValue = analogRead(A10);  //CH4
        val = (float)sensorValue / 25 ;
        dtostrf(val, 2, 1, buff);
        Serial.print("Current IN on CH4 = ");Serial.print(val);Serial.println(" mA");
        
        sensorValue = analogRead(A9);  //CH5
        val = (float)sensorValue / 25 ;
        dtostrf(val, 2, 1, buff);
        Serial.print("Current IN on CH5 = ");Serial.print(val);Serial.println(" mA");
  }  
  
  delay(pause);
  
  digitalWrite(5, HIGH);
  digitalWrite(3, HIGH);
  digitalWrite(6, HIGH);
  digitalWrite(9, HIGH);
  digitalWrite(8, HIGH);
  
  delay(pulse);
  
  digitalWrite(5, LOW);
  digitalWrite(3, LOW);
  digitalWrite(6, LOW);
  digitalWrite(9, LOW);
  digitalWrite(8, LOW);  
}  

  pinMode(5, INPUT);  //CH1
  pinMode(3, INPUT);  //CH2
  pinMode(6, INPUT);  //CH3
  pinMode(9, INPUT);  //CH4
  pinMode(8, INPUT);  //CH5
}

void AnalogIN(void){
int sensorValue;
char x = 'x';
char buff[3];
float val; 


while ( (Serial.read() != '0')){
        Serial.println(CLS);
        Serial.print("Analog INUP Test CH1, CH3 and CH4 :\n\n\n");
        
        sensorValue = analogRead(A15);  //CH1 - ne radi bez pecovanja
        val = (float)sensorValue*5 / 2047 ;
        dtostrf(val, 1, 2, buff);
        Serial.write(cr);Serial.print("Analog IN CH1 = ");Serial.print(val);Serial.println("V");

//        sensorValue = analogRead(A0);  //CH2 - ne radi bez pecovanja
//        val = (float)sensorValue*5 / 2047 ;
//        dtostrf(val, 1, 2, buff);
//        Serial.write(cr);Serial.print("Analog IN CH2 = ");Serial.print(val);Serial.println("V");

        sensorValue = analogRead(A7);  //CH3 - ne radi bez pecovanja
        val = (float)sensorValue*5 / 2047 ;
        dtostrf(val, 1, 2, buff);
        Serial.write(cr);Serial.print("Analog IN CH3 = ");Serial.print(val);Serial.println("V");

        sensorValue = analogRead(A8);  //CH4 - jedini trenutno funkcionalan
        val = (float)sensorValue*5 / 2047 ;
        dtostrf(val, 1, 2, buff);
        Serial.write(cr);Serial.print("Analog IN CH4 = ");Serial.print(val);Serial.println("V");
        
               
        delay(1500);
}  
}

void Temperature(void){
  
  int32_t ambientTemperature;// = MAX31855.readAmbient(); // retrieve MAX31855 die ambient temperature
  int32_t probeTemperature;//   = MAX31855.readProbe();   // retrieve thermocouple probe temp
  uint8_t faultCode;//          = MAX31855.fault();       // retrieve any error codes
  
 Serial.println(CLS);
  Serial.println("Temperature Test :\n"); 
  Serial.println("SPI CLK has to be less than 1MHz\n"); 
   
  
  pinMode(49, OUTPUT);
//----togling CS - some issue on SPI BUS.
  digitalWrite(49,HIGH); //SPI_CS. 
  delay(200);
  digitalWrite(49,LOW);
  delay(200);
  digitalWrite(49,HIGH);
  
  MAX31855_Class MAX31855; 
  
  MAX31855.begin(49); // 49 is CS
  
  delay(300);
  
 
 while(Serial.read() != '0'){
      
    
  ambientTemperature = MAX31855.readAmbient(); // retrieve MAX31855 die ambient temperature
  probeTemperature   = MAX31855.readProbe();   // retrieve thermocouple probe temp
  faultCode          = MAX31855.fault();       // retrieve any error codes
  
      if ( faultCode )                                     // Display error code
      {
        Serial.println(CLS);
        Serial.print("Temperature Test :\n\n\n"); 
        Serial.print("Fault code ");
        Serial.print(faultCode);
        Serial.println(" returned.");
      }
      else
      {
        Serial.write(cr);
        Serial.print("Ambient Temp = ");
        Serial.print((float)ambientTemperature/1000,1);
        //Serial.print(ambientTemperature);
        Serial.print("C ");
        
        Serial.print("Probe Temp = ");
        Serial.print((float)probeTemperature/1000,1);
        //Serial.print(probeTemperature);
        Serial.print("C        ");
      } // of if-then-else an error occurred
  delay(2000);
  }

  
}


void Conductivity(void){

int sensorValue=0, maxval=0;
int i =10000; //10.000 semls
int MuxSwitch = 1;
float Resistance =0;
char x = 'x'; 
                       //Rmux  10M  1M  100K 10K
 pinMode(44, OUTPUT);  //A0      0   1    0   1
 pinMode(45, OUTPUT);  //A1      0   0    1   1
 pinMode(53, OUTPUT);  //EN
 
 

AD.begin();
AD.setFrequency(0, 1000); //CHAN_0 1000Hz
AD.setMode(1); //MODE_OFF = 0
               //MODE_SINE = 1
               //MODE_SQUARE1 =2
               //MODE_SQUARE2 =3
               //MODE_TRIANGLE = 4

Serial.println(CLS);
Serial.println("Conductivity Test :\n\n\n");
Serial.println("With 'c' change chanel");

digitalWrite(53, HIGH); //Enable mux

while ( x != '0'){
  
  x = Serial.read();
  
  if (x == 'c'){
    
    switch (MuxSwitch){
      case 1: digitalWrite(44, LOW);    
              digitalWrite(45, LOW);Serial.print("Chanel CH");Serial.println(MuxSwitch);MuxSwitch =2 ;  break;
      case 2: digitalWrite(44, HIGH);    
              digitalWrite(45, LOW);Serial.print("Chanel CH");Serial.println(MuxSwitch);MuxSwitch =3 ;  break;;
      case 3: digitalWrite(44, LOW);    
              digitalWrite(45, HIGH);Serial.print("Chanel CH");Serial.println(MuxSwitch);MuxSwitch =4 ;  break;;
      case 4: digitalWrite(44, HIGH);    
              digitalWrite(45, HIGH);Serial.print("Chanel CH");Serial.println(MuxSwitch);MuxSwitch=1;break;       
    }
  }
    i = 3000;
    maxval = 0;
    while (i--){
      sensorValue = analogRead(A6);
      if (sensorValue > maxval) {maxval = sensorValue;}//searching for max of SINE signal
    }
    Serial.write(cr);Serial.print("SINmax = ");Serial.print((float)maxval*5/1023 - 2.5);
    delay(400);
}

  
}


void SDI12(void){

boolean TX = LOW, EN = LOW;
char x = 'x';
  
 DDRK  = DDRK  | 0x40; //PK6 is out.
 PORTK = PORTK & 0xBF; //PK6 is LOW.  - SDI12_Direction
 //PORTK = PORTK | 0x40;   //PK6 is HIGH. - SDI12_Direction
 

 DDRJ  = DDRJ  | 0x80; //PJ7 is OUT.
 PORTJ = PORTJ | 0x80;  //PJ7 is HIGH. - SDI12_TX

 DDRJ  = DDRJ  & 0xFB; //PJ2 is LOW - INput.    - SDI12_RX

  pinMode(42, OUTPUT); //GREEN_LED 
  pinMode(43, OUTPUT); //RED_LED

  Serial.println(CLS);
  Serial.println("SDI 12 Loopback Test");
  Serial.println("TX = RED_LED & RX = GREEN_LED. (inverted) 'e' - Enable/Disable ");     

while ( x != '0' ){

x = Serial.read();

if (x == 'e'){
  if (EN) { EN = LOW;  PORTK = PORTK & 0xBF;  Serial.write(cr);Serial.println("TX Enable");}
  else    { EN = HIGH; PORTK = PORTK | 0x40;  Serial.write(cr);Serial.println("TX Disable");}
  
  }

//----Togling SDI12_TX-------------// 
if ( TX ) {  
  TX = LOW;
  PORTJ = PORTJ & 0x7F;  //TX is LOW.
  digitalWrite(43,LOW);  //RED led 
 
}  
else{
  TX = HIGH;
  PORTJ = PORTJ | 0x80;  //TX HIGH.
  digitalWrite(43,HIGH); //RED led
     
}


if ( (PINJ  & 0x04) == 0x04) {
  digitalWrite(42,HIGH);
}
else {
  digitalWrite(42,LOW);
} 
  
  
delay (1000);  
}
 
}

void UART3(void){
  
 char x = 'x', z='z';
 Serial.print(CLS);
 Serial.println("UART3 LoopBack Test :\n");
 Serial.println("Short Connect RX and TX on Blue Connector");
 
 Serial3.begin(115200); 
 
  while((x != '0')){
     while(!Serial.available()){}
     x = Serial.read();
     Serial3.print(x); 
     delay(20); 
     z = Serial3.read();
     delay(20);
     Serial.print(z);  
     delay(20);  
  }
 
}

boolean Loggin(void){
char summ =0;  
Serial.println(CLS);
Serial.println("Enter Password:");
int i =0;
 while((i < 4) ){
   while(!Serial.available()){}
   summ = summ + (Serial.read() -48);  
   i++;  
}


if (summ == SummPassword ){Serial.println("Logging OK");delay(500);return true;}
else {Serial.println("Logging Faild");delay(1500);return false;}

}


  
void loop(){
  
 char x = 'x';

 
digitalWrite(7, LOW); //GPRS In reset state
DDRH  = DDRH  | 0x07; //PH2 is out.
PORTH = PORTH & 0x7F; //PH2 is HIGH.

 
while (1){
  
Serial.println(CLS);

Serial.println( WirmVer);

Serial.println("\nTest Menu :\n");


Serial.println("1) Relay Test");
Serial.println("2) LEDs  Test");
Serial.println("3) RTC   Test");
Serial.println("4) Flow  Test");
Serial.println("5) pH    Test");
Serial.println("6) RS485 Test");
Serial.println("7) GPRS  Test");
Serial.println("8) Current IN/OUT Test");
Serial.println("9) Analog IN      Test");
Serial.println("t) Temperature    Test");
Serial.println("e) El. Conductivity  Test");
Serial.println("s) SDI12-Loopaback   Test");
Serial.println("u) UART3-Loopaback   Test");

 while(!Serial.available()){}
 x = Serial.read();
 

     switch (x){
           
           case '1':Relays();break;
           case '2':Leds();break;
           case '3':RTC(); break;
           case '4':Flow();break; 
           case '5':pH();break;
           case '6':RS485();break;
           case '7':GsmGprs();break;
           case '8':CurrentIN_OUT();break;
           case '9':AnalogIN();break;
           case 't':Temperature();break;
           case 's':SDI12();break;
           case 'e':Conductivity();break;
           case 'u':UART3();break;
           case 'r':Relays75_76();break;
           default:break;
               
     } 
 
 
            
} 
}
