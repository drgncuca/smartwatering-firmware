#include <ModbusMaster.h>

#include <Arduino.h>
#define CLS "\033[2J" 

ModbusMaster node;
bool state = true;

void setup() {
  
 RS485setup();
 
Serial.begin(9600); 
Serial.println(CLS);
 Serial.println("(RE)start uC");
// Serial1.begin(9600,SERIAL_7N2); 
 Serial1.begin(9600);
 // Modbus slave ID 1
 //node.begin(1, Serial1);
 
 // Callbacks allow us to configure the RS485 transceiver correctly
//  node.preTransmission(preTransmission);
//node.postTransmission(postTransmission);
}

//----Enable TX, Dissable RX----//
void preTransmission()
{
  RS485setup();
 
  PORTH = PORTH | 0x04; //PH2 is HIGH. ->Enable TX
}

void postTransmission(int chanell)
{
  RS485readEnableCH(chanell); // Enabled ONE  RX Chanell.
  PORTH = PORTH & 0xFB;       //PH2 is LOW. ->Disable TX
}   

//-------Disable  RX  AND  TX ---------------------//    
void RS485setup(){       
  
    //TX driver
    DDRH  = DDRH  | 0x04; //PH2 is out.
    PORTH = PORTH & 0xFB; //PH2 is LOW. ->Disable TX
    
    // set all RX enable control pins, as OUT and non-active.
    // CH1  RX,
    DDRD  = DDRD  | 0x10; //PD4 is out.
    PORTD = PORTD | 0x10;   //PD4 is HIGH -> Disable receiv data.
    
    // CH2  RX,
    DDRD  = DDRD  | 0x80;  //PD7 is out.
    PORTD = PORTD | 0x80;  //PD7 is HIGH -> Disable receiv data.
    
    // CH3  RX,
    DDRD  = DDRD  | 0x40;  //PD6 is out.
    PORTD = PORTD | 0x40;  //PD6 is HIGH -> Disable receiv data
    
    // CH4  RX,
    DDRD  = DDRD  | 0x20;  //PD5 is out.
    PORTD = PORTD | 0x20;  //PD5 is HIGH -> Disable receiv data
    
}

//------Enable ONE  RX  port, Other MUST be disabled------------------------//
void RS485readEnableCH(int chanell){
 
  PORTH = PORTH & 0xFB; //PH2=LOW->Disable TX
  switch(chanell){
     case 1: { //CH1
         PORTD = PORTD & 0xEF;//PD4 is LOW - > Enable Receive data on CH1
         PORTD = PORTD | 0x80;
         PORTD = PORTD | 0x40;
         PORTD = PORTD | 0x20;
         break;
    }
    case 2: { //CH2
         PORTD = PORTD | 0x10;
         PORTD = PORTD & 0x7F;
         PORTD = PORTD | 0x40;
         PORTD = PORTD | 0x20;
         break;
    }
   case 3:{//CH3
         PORTD = PORTD | 0x10;
         PORTD = PORTD | 0x80;
         PORTD = PORTD & 0xBF;
         PORTD = PORTD | 0x20;
         break;
    }
    case 4:{//CH4
         PORTD = PORTD | 0x10;
         PORTD = PORTD | 0x80;
         PORTD = PORTD | 0x40;
         PORTD = PORTD & 0xDF;
         }
   }//switch()  
}
//---------------Send  STRING------------------///
void RS485writeString(String string){   
    //disable ALL  RX  chanells
     PORTD = PORTD | 0x10;
     PORTD = PORTD | 0x80;
     PORTD = PORTD | 0x40;
     PORTD = PORTD | 0x20;
     
     // enable TX 
     PORTH = PORTH | 0x04; //PH2 is HIGH.
     Serial1.print(string);      
     delay(30); // NEED  some Delay !!!!!!
     //Disable TX
     PORTH = PORTH & 0xFB; //PH2 is LOW.
}
void RS485writeStringLN(String string){   
    //disable ALL  RX  chanells
     PORTD = PORTD | 0x10;
     PORTD = PORTD | 0x80;
     PORTD = PORTD | 0x40;
     PORTD = PORTD | 0x20;
     
     // enable TX 
     PORTH = PORTH | 0x04; //PH2 is HIGH.
     Serial1.println(string);      
     delay(30); // NEED  some Delay !!!!!!
     //Disable TX
     PORTH = PORTH & 0xFB; //PH2 is LOW.
}
//---------Send ONE character---------------------///
void RS485writeChar(char charToSent){   
    //disable ALL  RX  chanells
     PORTD = PORTD | 0x10;
     PORTD = PORTD | 0x80;
     PORTD = PORTD | 0x40;
     PORTD = PORTD | 0x20;
     
     // enable TX 
     PORTH = PORTH | 0x04; //PH2 is HIGH.
     Serial1.print(charToSent);           
     //Disable TX
     delay(2);//  //100uS whait for send data than disable TX driver. 
     PORTH = PORTH & 0xFB; //PH2 is LOW.
}

char RS485readChar(int chanell){
  RS485readEnableCH(chanell);  
  char x = Serial1.read();  
  return x;  
}
String RS485readString(int chanell){
RS485readEnableCH(chanell); 
  
  String str = Serial1.readString();  
  return str; 
  
  }

int calculateLRC(byte* bytes) {
    unsigned int LRC = 0;
    for (int i = 0; i < 14; i++) {
      LRC ^= bytes[i];
      Serial.write(bytes[i]);
    }
    return LRC;
}
    
void loop() {
  
byte bytes[14]={0,1,0,3,0,0,0,0,0,0,0,2,0};

  static uint32_t i;
  uint8_t j, result;
  uint16_t data[6];


while(1){
RS485writeStringLN(":01080000A5371B++++++++++++++++++++++++++++++++++++++++++++++++++++++++");  //simply echoed back to the Master
Serial.print(RS485readString(1));
delay(400);

RS485writeStringLN(":010300000002FA++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");//requests the reading of Holding Registers 40001 through 40002 from probe 1
Serial.print(RS485readString(1));
delay(400);

RS485writeStringLN(":010600000002F7++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");// broadcast command, preset Holding Register 40001 to the value 2, which will start a scan of all moisture sensors
Serial.print(RS485readString(1));
delay(500);

RS485writeStringLN(":010401000002F8");// Moisture value from 0x30257
Serial.print(RS485readString(1));
delay(400);
Serial.println("-------------------------");


 
}


}
