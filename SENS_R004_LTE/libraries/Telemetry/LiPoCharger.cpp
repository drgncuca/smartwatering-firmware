#include <Arduino.h>
#include <SoftwareSerial.h>
#include "Telemetry.h"
#include "wire.h"

#ifdef LiPo_Charger

#define PMIC_ADDRESS 0x09

#define MAX_Batt_Voltage 12576 
//#define MAX_Batt_Voltage 11488
//#define MAX_Batt_Voltage 11232 
//#define MAX_Batt_Voltage 11008
//#define MAX_Batt_Voltage  10752
//#define MAX_Batt_Voltage  10496

#define BattLOWLevel  9.00  

#define BattHIGHLevel 12.62

#define TIMEREFRCNST 7 //  25 => 5min Na svaki 5min rekalkulise struju punjenje.

extern byte BattCharging; // Status  baterije

byte timeRefresCharginCurrent = 0;

extern unsigned int setChargingCurrent ;  //inicijalno je 128mA
extern float BatteryVoltage;
extern int BatteryCChargingCurrent;

#ifdef Power_Saving
extern	byte PowerStandByTimeInMin;
#endif
extern byte BatterySupplying ;


void LiPoChargeInit(void){
 BattCharging = 0;

 // Ne Otkacuj Bateriju sa sistema. 
 pinMode(37, OUTPUT); //BATTERY_RL_R
 digitalWrite(37, LOW); // 
 
 // Zakaci Bateriju na sistem. 
 pinMode(35, OUTPUT); //BATTERY_RL_S
 digitalWrite(35, HIGH); // 
 delay(100);
 digitalWrite(35, LOW); // 
 pinMode(35, INPUT); //Oslobodi pin da moze da hednla taster na ploci
 
 pinMode(36, OUTPUT); //Battery_CH_EN
 digitalWrite(36, HIGH); // Enejbluje napajanje za Charger
 
 pinMode(28, INPUT);//Battery_CH_ACOK
		
 delay(800);
 
#ifdef DEBUG_LiPo_CHG 
 getLiPoDeviceID();
 Serial.print(F("Charge Voltage ")); Serial.println(getLiPoChargeVoltage());
#endif 

//setChargingCurrent = 128;
writeRegister(0x12,0x81B3); // Disable Charging 

setLiPoChargeVoltage(MAX_Batt_Voltage); // puni bateriju do 12.57V, 	12.6V je max, i sto je blize toj vrednosti skracuje vek baterije !!!
//setLiPoChargeCurrent(1664); //max struja punjenja
//setLiPoChargeCurrent(128); //max struja punjenja
#ifdef SOLAR_40W
setLiPoINcurrent(768);	// max ulazna struja.
//setLiPoINcurrent(256);	// max ulazna struja.
#endif

#ifdef SOLAR_20W
setLiPoINcurrent(512);	// max ulazna struja.
#endif

}
//  Struje punjanja su: 64*N [mA]
//================================ 20%    ===== 40%   ================ 80%    ========== 100%   =======================
//================================ 11.20V ===== 11.40 ================ 12.00V ========== 12.60V =======================
void LiPoChargeManager(void){
	
float solar  = getVoltageSolar();

float batt = getVoltageBatt();
unsigned	chgRegister ; 
	BatteryVoltage = batt;
	
	Serial.print(F("BATT="));  Serial.print(batt);  Serial.println(F("V")); 
#ifdef DEBUG_LiPo_CHG
	Serial.print(F("SOLAR "));  Serial.print(solar); Serial.println("V");
	BatteryCChargingCurrent = getChgCurrent();
	Serial.print(F("Current ")); Serial.print(BatteryCChargingCurrent); Serial.println(F(" mA"));
	//Serial.print(F("regs  "));  Serial.println(readLiPoChargeReg(),HEX);  
#endif

//PUNJENJE optimizacija na svakih ( 100*12s = 20min )pokreni dopunjavanje baterije..

if (      doesACOK() == 1 ){
//if ( digitalRead(28) == 1 ){ // Ako je napon solara u granicama od 14V do 22.5V 
	// Nakaci bateriju na sistem ako ima napona SOLARA
    pinMode(35, OUTPUT); 
	digitalWrite(35, HIGH); // Zakaci Bateriju na sistem. 
	delay(100);
	digitalWrite(35, LOW); // 
	pinMode(35, INPUT); //Oslobodi pin da moze da hednla taster na ploci	
	
	if(batt < (BattLOWLevel + 0.5)) {
		delay(3000); // ako je tek baterija nakacena
		batt = getVoltageBatt();
		BatteryVoltage = batt;
	}
	
	if (solar<17.0){
		BattCharging = 0;	
		stopLiPoCharging();	
		timeRefresCharginCurrent = 0;
		BatterySupplying = 1;
#ifdef DEBUG_LiPo_CHG		
	Serial.println(F("SOLAR to LOW for charging/Supplaing")); 
#endif
		return;
	}
	
// Provera statusa napona Baterije-Solara, da li treba i moze li da puni.......
	if ( timeRefresCharginCurrent == 0 ){
				
#ifdef Charging_SENS_R003
		if (batt<10.51) setChargingCurrent = 768;
		if (batt< 7.50) setChargingCurrent = 512;
		if (batt>10.50) setChargingCurrent = 1024;
#endif		

#ifdef Charging_SENS_R002
		if (batt<10.51) setChargingCurrent = 1024;
		if (batt< 7.50) setChargingCurrent = 768;
		if (batt>10.50) setChargingCurrent = 1536;
#endif	

		byte loop = 1;
		while(loop < 6){		
			batt = batt + 0.128;
			//Serial.print(F("Delta Ubatt ")); Serial.println(batt - BatteryVoltage); 
			if( (batt*1000.0) > MAX_Batt_Voltage) {
				setLiPoChargeVoltage(MAX_Batt_Voltage);
				startLiPoCharging(setChargingCurrent);
				//Serial.print(F(", MAX_Batt_Voltage, loop ")); Serial.println(loop); 
				break;
			}
			else  {
				setLiPoChargeVoltage(batt * 1000.0);			
				startLiPoCharging(setChargingCurrent);
			}
			delay(600);
			if ( doesACOK()  == 1) {
				//Serial.println(F("ACOK")); 
				continue;
			}
			else{
				Serial.println(F("ACNoK")); 
				batt = batt - 0.256;
				//delay(100);
				if (  (batt - BatteryVoltage) <0.3) 
				{
					//Serial.println(F("Delta U manji od 200mV"));
					setLiPoChargeVoltage(BatteryVoltage * 1000.0 +128);
					startLiPoCharging(setChargingCurrent);
					delay(1500);
					break;
				}
				setLiPoChargeVoltage(batt * 1000.0);
				startLiPoCharging(setChargingCurrent);				
				delay(1500);
				break;	
			}			
			loop++;	
		}		 
		batt = BatteryVoltage ; 
	
#ifdef DEBUG_LiPo_CHG	
	Serial.print(F("Set "));  Serial.print(getLiPoChargeCurrent()); Serial.print(F(" mA, mesure ")); Serial.print(getChgCurrent()); Serial.print(F(" mA, up to "));Serial.print(getLiPoChargeVoltage());
	Serial.print(F(" ACOK = "));Serial.println(doesACOK() );
				
#endif		

		timeRefresCharginCurrent = TIMEREFRCNST;
		
	}
	
	timeRefresCharginCurrent = timeRefresCharginCurrent - 1 ;
}
else{//Napon Solara nedovoljan za napajanj/punjene  
	stopLiPoCharging();
		//EXTRIMELY Low Battery 
	if(batt < BattLOWLevel ){
		delay(100);
		batt = getVoltageBatt();
		if(batt < BattLOWLevel ){
			delay(100);
			batt = getVoltageBatt();
			if(batt < BattLOWLevel ){
#ifdef DEBUG_LiPo_CHG	
	Serial.print(F("Batt Extrimely LOW = "));  Serial.print(batt); Serial.println(F(" -> Cutt OFF Instant")); 
#endif
				delay(100);	
				stopAll();
			// Raskaci bateriju sa sistema, GASI  KONTROLER !!!!!!! 
				digitalWrite(37, HIGH); 
				delay(100);//ovde ce se ugasiti uC posle par uS....
				digitalWrite(37, LOW);
				pinMode(37, INPUT); 
			}
		}
	}
		
}

BatterySupplying = doesItBatteryPowered();

#ifdef DEBUG_LiPo_CHG
if (BatterySupplying)	Serial.println(F("Battery Supplaing")); 
else Serial.println(F("SOLAR Supplaing")); 
#endif	

// Brute force STOP charging
if (batt > BattHIGHLevel ) {
		stopLiPoCharging();
#ifdef DEBUG_LiPo_CHG	
		Serial.println(F("Brute Force STOP Charging"));
#endif	
	BattCharging = 0;
}

}
//================================================================================================	
void getLiPoDeviceID(void){
//Wire.begin();
Serial.print(F("Read Device ID: "));Serial.println(readRegister(0xFE),HEX);
Serial.print(F("Read Manufa ID: "));Serial.println(readRegister(0xFF),HEX);
//Wire.end();

}
unsigned int getLiPoChargeOption(void){
	return readRegister(0x12);
}

unsigned int readLiPoChargeReg(void){
unsigned int tmp = readRegister(0x12);	
return tmp;
}

unsigned int getLiPoInputCurrent(void){
unsigned int tmp = readRegister(0x3F);	
return tmp;
}

unsigned int getLiPoChargeCurrent(void){
unsigned int tmp = readRegister(0x14);	
return tmp;
}

unsigned int getLiPoChargeVoltage(void){
unsigned int tmp = readRegister(0x15);	
return tmp;
}

void setLiPoChargeVoltage( unsigned int Voltage){

Voltage = Voltage & 0x7FF0;
writeRegister(0x15,Voltage);	
}

void setLiPoChargeCurrent( unsigned int Current){

Current = Current & 0x1FC0;
writeRegister(0x14,Current);	
}

void setLiPoINcurrent( unsigned int Current){
	Current = Current & 0x1F80;
	writeRegister(0x3F,Current);	
}

void stopLiPoCharging(){
	unsigned int tmp  = getLiPoChargeOption();
	tmp = tmp | 0x01;
	writeRegister(0x12,tmp);//DIsejbluj punjenje	
}

void startLiPoCharging(unsigned int Current){
	setLiPoChargeCurrent(Current); //max struja punjenja	
	writeRegister(0x12,0x81B2);// start charging
}

void setWdogLiPoCharger(byte timeRange){ // 0 - disable WDOG
										 // 1 - 44s
										 // 2 - 88s		
										 // 3 - 175s
unsigned int tmp  = 	getLiPoChargeOption(); // Charge Options Register			 					
switch(timeRange){
	case 0: tmp = tmp & 0x9FFF; break; 
	case 1: tmp = tmp & 0xBFFF; tmp = tmp | 0x2000; break;
	case 2: tmp = tmp & 0xDFFF; tmp = tmp | 0x4000; break;
	case 3: tmp = tmp | 0x6000; break;		
}
#ifdef DEBUG_LiPo_CHG		
//	Serial.print(F("Write S_REG: ")); Serial.println(tmp,HEX);
#endif
writeRegister(0x12,tmp);

	
}


unsigned int readRegister(byte address) {
    Wire.beginTransmission(PMIC_ADDRESS);
    Wire.write(address);
    if (Wire.endTransmission(true) != 0) {
#ifdef DEBUG_LiPo_CHG		
		//Serial.println(F("error Read CHG"));
#endif		
      return -1;
    }
	
	byte low  ;
	byte high ;
    if (Wire.requestFrom(PMIC_ADDRESS, 2, true) == 2) {
		low  =  Wire.read();
//		Serial.print(F("LOW : "));Serial.println(low,HEX);
		high  = Wire.read();
//		Serial.print(F("HIG : "));Serial.println(high,HEX);		
    }
	
	unsigned int tmp = high;
	tmp = tmp <<8;
	tmp = tmp + low;	
//	Serial.print(F("LOW + HIG : "));Serial.println(tmp,HEX);	
    return tmp;
}

byte writeRegister(byte address, unsigned int val) {
    Wire.beginTransmission(PMIC_ADDRESS); // 0x09
    Wire.write(address);                  //0x15
	
	byte LOWer   = (byte)  (val & 0xFF) ;
	byte HIGHer  = (byte)  ((val >> 8) & 0xFF); 
//	Serial.print(F("Write Registar: ")); Serial.print(val); Serial.print(F(", H")); Serial.print(HIGHer); Serial.print(F(", L")); Serial.println(LOWer);
	
	Wire.write(LOWer);  // LOW Data
    Wire.write(HIGHer); // HIGH Data 

    if (Wire.endTransmission(true) != 0) {
#ifdef DEBUG_LiPo_CHG		
//		Serial.println(F("Error Write to CHG"));
#endif		
        return 0;
    }
  return 1;
}

unsigned int getChgCurrent(void){
	unsigned int tmpChCurr = 0;
	unsigned int tmpReg  = 	getLiPoChargeOption();
	tmpReg = tmpReg | 0x20 ; 
	writeRegister(0x12,tmpReg);
	byte loop = 5;
	while(loop--){
		delay(10);
		tmpChCurr =tmpChCurr + analogRead(A10);		
	}
	return 	tmpChCurr; 	
}


unsigned int getSolarCurrent(void){
	unsigned int tmpSolarCurr = 0;
	unsigned int tmpReg  = 	getLiPoChargeOption();
	tmpReg = tmpReg & 0xFFDF ; 
	writeRegister(0x12,tmpReg);
	byte loop = 5;
	while(loop--){
		delay(10);
		tmpSolarCurr = tmpSolarCurr  + analogRead(A10);		
	}
	return 	tmpSolarCurr;	
}

byte doesACOK(void){
	byte loop = 4;	
	byte ACOK = 1;
	while(loop--){
		if ( digitalRead(28) == 0 ) ACOK = 0;
		delay(50);	
	}		
	return ACOK;	
}

byte doesItBatteryPowered(void){
	byte loopTmp = 4;
	byte BatterySupply = 1;
	while(loopTmp--){	
	if(getVoltageSolar()>18.5)
		BatterySupply = 0;
		delay(50);
	}
	return BatterySupply;
}
#endif