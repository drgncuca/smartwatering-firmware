#include <Arduino.h>
#include <SoftwareSerial.h>
#include <EEPROM.h>
#include "EEPROMAnything.h"
#include "Telemetry.h"

#define gprs_restart_pin  7
#define lte_restart_pin  7

#ifdef GSM_LTE

#define TINY_GSM_MODEM_SIM7600

//#define _SS_MAX_RX_BUFF 128
#include <SoftwareSerial.h>
SoftwareSerial SerialAT(10, 11);  // RX, TX

#if !defined(TINY_GSM_RX_BUFFER)
#define TINY_GSM_RX_BUFFER 650
#endif

//#define TINY_GSM_DEBUG Serial

#define GSM_AUTOBAUD_MIN 9600
#define GSM_AUTOBAUD_MAX 115200

#define TINY_GSM_USE_GPRS true

// set GSM PIN, if any
#define GSM_PIN ""

#include <TinyGsmClient.h>
#include <ArduinoHttpClient.h>

TinyGsm        modem(SerialAT);

TinyGsmClient client(modem);

// Your GPRS credentials, if any
const char apn[]      = "internet";
const char gprsUser[] = "internet";
const char gprsPass[] = "internet";

char server[] = "app.smartwatering.rs";

#define BUFF_SMS 50
#define BUFF_MOB_NUM 20
#define MAX_SMS_ATTEMP 2
#define MAX_SMS_FORMAT 2

char sms_text[BUFF_SMS];  //50
char phone_num[BUFF_MOB_NUM]; //20

HttpClient    http(client, server, 80);

byte initGSM_LTE(void){

//raskaci/gasi GSM_GPRS
  PORTE = PORTG & 0xFB; // GPRS_PWR_EN <LOW>
  DDRE  = DDRE  | 0x04; // GPRS_PWR_EN <OUT>  
  PORTE = PORTE & 0xFB; // GPRS_PWR_EN <LOW> OFF	
	
	pinMode(8, OUTPUT);  //GSM_PWR KEY
	pinMode(7, OUTPUT);  //GSM Reset	
	digitalWrite(7, LOW); //GSM Reset  
	digitalWrite(8, LOW); //off GPRS_PWR KEY
//=======================================
	pinMode(5, OUTPUT);  //LTE_Power_BTN	
	digitalWrite(5,LOW); //LTE_Power_BTN LOW->OFF
	delay(100);
	pinMode(22, OUTPUT);  //5V0_LTE_PWR_EN		
	digitalWrite(22,LOW); //GSM 4G Power ON		
	delay(500);
	//togle LTE Power Button for 1s to Turn ON LTE module 
	digitalWrite(5,HIGH); //LTE_Power_BTN HIGH->ON
	delay(1100);
	digitalWrite(5,LOW); //LTE_Power_BTN off
	
    delay(25000);
    SerialAT.begin(115200);
	delay(1000);
	togleWD();
//    Serial.println("LTE set 9600");
    SerialAT.println("AT+IPR=9600");
    delay(1000);	
    //SerialAT.end();
	//delay(1000);
	SerialAT.begin(9600);
	delay(1000);
#ifdef DEBUG_Operater 
	Serial.println(F("Initializing GSM modem..."));
#endif
	modem.init();
  //modem.restart();

#ifdef DEBUG_Operater   
  SerialAT.println("AT");
  delay(100);
  String response = "AT test:";
  while (SerialAT.available()) {
    response += SerialAT.readString();
  }
  Serial.println(response);
#endif

checkAntennaConnection();

Serial.print(F("SIM = "));	
byte SIMpresentChecking = checkSIMPresence();
if (SIMpresentChecking == 0){
		Serial.println(F("Not Inserted Corectly !!!"));	
		while((++SIMpresentChecking)<200){
			digitalWrite(29,LOW);
			delay(150);
			digitalWrite(29,HIGH);
			delay(150);
		}	
}
if (SIMpresentChecking == 1){
	Serial.println(F("Inserted Corectly"));
}

setSMSMemory();

Serial.print(F("GSM = "));
  // Provera mrezne registracije
  if (!waitForNetworkRegistration(2)) {  //2 minutes
    Serial.println(F("Failed To Connect"));
  } else {
    Serial.println(F("Connected"));
	setSMSMode();
  }

}

//timeout in min
bool waitForNetworkRegistration(byte timeout) {
  unsigned long start = millis();
  unsigned long timeOUT = (unsigned long int)timeout * 60 * 1000;
  byte counterOfFailureConnecting = 0; 
  while ( (millis() - start) < timeOUT) {
  
    if (isNetworkConnected()) {
      return true;
    }
    Serial.println(F("Waiting for network registration..."));
	digitalWrite(29,LOW); //Power LED RED
	delay(1000);
	digitalWrite(29,HIGH);
    delay(1000); 
	digitalWrite(29,LOW); //Power LED RED
	delay(1000);
	digitalWrite(29,HIGH);
    delay(1000); 
#ifdef WhatchDog
	togleWD(); //togle WD
#endif
	counterOfFailureConnecting++;
	if (counterOfFailureConnecting > 60) { // ako posle 5min ne uzubi, izadji iz petlje...
		return false;
	}
  }
  return false;
}

bool isNetworkConnected() {
  SerialAT.println("AT+CGREG?");
  delay(200);
  String response = "";
  while (SerialAT.available()) {
    response += SerialAT.readString();
  }

  byte statusIndex = response.indexOf("+CGREG: ");
  if (statusIndex != -1) {
 int commaIndex = response.indexOf(',', statusIndex + 6);
    String statusString = response.substring(commaIndex + 1, commaIndex + 2);
    //Serial.println("Extracted status string: " + statusString); // Debug ispis
    int status = statusString.toInt();
    //Serial.println("Converted status to int: " + String(status)); // Debug ispis
	
    switch (status) {
      case 0:
        Serial.println(F("0,0: Not registered, not searching for new operator."));
        break;
      case 1:
        Serial.print(F("Registered on home network. "));
        return true;
      case 2:
        Serial.println(F("0,2: Not registered, but searching for new operator."));
        break;
      case 3:
        Serial.println(F("0,3: Registration denied."));
        break;
      case 4:
        Serial.println(F("0,4: Unknown status."));
        break;
      case 5:
        Serial.println(F("0,5: Registered, roaming."));
        return true;
      default:
        Serial.println(F("Unknown status."));
        break;
    }
  }

  SerialAT.println("AT+CREG?");
  delay(100);
  response = "";
  while (SerialAT.available()) {
    response += SerialAT.readString();
  }
  //Serial.println(response);

  statusIndex = response.indexOf("+CREG: ");
  if (statusIndex != -1) {
    int status = response.substring(statusIndex + 7, statusIndex + 8).toInt();
    switch (status) {
      case 0:
        Serial.println(F("0,0: Not registered, not searching for new operator."));
        break;
      case 1:
        Serial.println(F("0,1: Registered on home network."));
        return true;
      case 2:
        Serial.println(F("0,2: Not registered, but searching for new operator."));
        break;
      case 3:
        Serial.println(F("0,3: Registration denied."));
        break;
      case 4:
        Serial.println(F("0,4: Unknown status."));
        break;
      case 5:
        Serial.println(F("0,5: Registered, roaming."));
        return true;
      default:
        Serial.println(F("Unknown status."));
        break;
    }
  }


return false;
}

byte setAPN(char* apnname, char* apnuser, char* apnpass){

  // GPRS connection parameters are usually set after network registration
  Serial.print(F("GPRS = "));
//  Serial.print(apnname);
  if (!modem.gprsConnect(apnname, apnuser, apnpass)) {
      Serial.println(F("FAILD....restarting uC"));
	  delay(1000); 
	  digitalWrite(12, LOW); // Reseting Arduino
  }
  Serial.println(F(" Connected"));

#ifdef DEBUG_Operater 
  if (modem.isGprsConnected()) { 
	Serial.println(F("GPRS connected")); 
  }
#endif
// provera IP adrese
String ipAddress = modem.getLocalIP();
//Serial.print(F("Network IP:"); 
Serial.println(ipAddress);

#ifdef DEBUG_Operater 
// Provera mrežnog režima
  SerialAT.println("AT+CPSI?");
  delay(1000);
  while (SerialAT.available()) {
    String response = SerialAT.readString();
    Serial.println(response);
  }
delay(1000);
//informacije o statusu mrežne registracije.
SerialAT.println("AT+CGREG?");
while (SerialAT.available()) {
    String response = SerialAT.readString();
    Serial.println(response);
}
delay(1000);
// trenutnom operatoru i mreži.
SerialAT.println("AT+COPS?");
while (SerialAT.available()) {
    String response = SerialAT.readString();
    Serial.println(response);
}
#endif	

	
}

byte apiCall(const char *path, String& postData,  char* serverResponse)
{
  byte completed = 0;

	int contentLength = postData.length();
  
  http.beginRequest();
  http.post(path);
  http.sendHeader("Content-Type", "application/x-www-form-urlencoded");
  http.sendHeader("Content-Length", contentLength);
  http.beginBody();
  http.print(postData);
  http.endRequest();
  
  // Provera odgovora
  int statusCode = http.responseStatusCode();
  String srvResponse = http.responseBody();

  // Kopiranje odgovora u serverResponse
  srvResponse.toCharArray(serverResponse, MSG_BUF);

if (statusCode == 200) {
    //Serial.println("Podatak je uspešno poslat.");
    completed = 1;
  } else {
    Serial.print(F("Data sending Erorr: "));
    Serial.println(statusCode);
    completed = 0;
  }
if(completed != 1){
	Serial.println(F("ReInit. LTE"));
	initGSM_LTE();	
	setAPN(carrierdata.apnname, carrierdata.apnuser, carrierdata.apnpass); 
}			   
  return completed;
}

byte isLTEconnectionEstablis(void){	
}

//=======================================================================
#define BUFF_SMS 50				   
#define BUFF_MOB_NUM 20

void doesOperaterChanging(void){ // ceka se 5S pri ukljucenju uredjaja, i do 3 pogresno pristigla SMS-a
	
 int smsposition = 0;
byte smsConfigured = 0;	

	  	
  byte atempt =0;	
  byte  wrongSMSformat = 0;
  char tmp[2]="69"; // bilo koji sadrzaj razlicit od "SW".
  bool SMSsent = false;
  String xString;
  byte operaterID =9; // neki br da je razlicit od 1,2,3 
  

    smsposition = checkForSMS();
#ifdef DEBUG_Operater  
		 Serial.print(F("MessageCount")); Serial.println(smsposition);		      
#endif

while (   ((atempt++) < MAX_SMS_ATTEMP )  &&  (wrongSMSformat < MAX_SMS_FORMAT)  ) {//pokusava  60 seukndi da dobije kofiguracione parametre. Cim ih dobije, obrati i izlazi iz petlje.
#ifdef DEBUG_Operater  
		 Serial.print(F("SMS atempt :")); Serial.println(atempt);		      
#endif
		 if(smsposition){     
#ifdef DEBUG_Operater  
		 Serial.print(F("SMS position in loop: ")); Serial.println(smsposition);		      
#endif

		SMSprocessing();
#ifdef DEBUG_Operater  
		Serial.print(F("SMS config: Phone Number:"));Serial.print(phone_num); Serial.print(F(",SMS:"));Serial.println(sms_text); 
		 //delay(1000);	     
#endif	 
			if (startsWith("SW ", sms_text))// ako je dobar sadrzaj poruke, pocinje as "SW", obradi je....
			{
#ifdef DEBUG_Operater
			  Serial.print(F("Promena Operatera SMS-om......."));
#endif
			// probaj dal moze i bez ovoga brisanja EEPROM-a ;-)
			  for (byte i = 0; i < 109; i++) // nije bilo potrebe brisati ceo EEPROM 4096 byte-a
			{
			  EEPROM.write(i, 0);
			}
			strcpy(tmp, "SW");     //mem loc = 64
			EEPROM_writeAnything(EERPOM_mem_loc_SW, tmp); // na  definisanu adresu  upisuje tmp = "SW";
			
			char *ptr =NULL;
			
			ptr = strtok(sms_text, " "); // Definise sta je delimiter -> " " (SPACE)			
			byte position = 1; // preskace se deo SMS poruke "SW".			
			ptr = strtok(NULL, " ");
			strcpy(carrierdata.apnname, ptr);
			position =2;
			ptr = strtok(NULL, " ");
			strcpy(carrierdata.apnuser, ptr); 
			ptr = strtok(NULL, " ");
			strcpy(carrierdata.apnpass, ptr);    //mem loc = 0			
			EEPROM_writeAnything(EERPOM_mem_loc_CarrierData , carrierdata); // upis parametara u EEPROM.			

			SMSsent = sendSMS(phone_num,"Operator APN successfully set");

#ifdef DEBUG_Operater	
			if (SMSsent == false)	Serial.println(F("Korisnik nije obavesten SMS-om o promeni operatera"));
			if (SMSsent == true)	Serial.println(F("Korisnik obavesten o uspesnom podesavanju"));			
#endif					  
			deleteAllSMS();
		
		 Serial.println(F("Operater konfigurisan SMS-om"));
		
			EEPROM.write(EERPOM_mem_loc_sms_Confugured, 0xAA); // indikacija da je operater SMSom podesen.																					 
			return; // sve podeseno -> kraj, izadji iz funkcije!
			}
			 else // ako nije dobar format SMS -> obavesti Korisnika, dati mu fore jos 2 SMS-a max. 
			 {
#ifdef GSM_SMS_Replay				 
			sendSMS(phone_num,"Wrong Configuration Format");        //        POTROSI KREDIT ! Otkomentarisi kad zavrsis testiranje !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#endif
			wrongSMSformat ++; // 3 pogresnig SMS-ova i pod mac batoooo.
			atempt = 0;
#ifdef DEBUG_Operater		
		 Serial.println(F("SMS Wrong Configuration Format"));
#endif		 
			 }   		  

	  }//if(smsposition)
	  delay(1000);
	  //delay(4000); //Hendlanje GSM modula je malo kilavo, treba mu dati vremena............ 				
	  //smsposition = sms.IsSMSPresent(SMS_UNREAD);
	  smsposition = checkForSMS();
}// while (atempt) 

if (EEPROM.read(EERPOM_mem_loc_sms_Confugured) == 0xAA){
#ifdef DEBUG_Operater
Serial.print(F("EEPROM cfg_sms "));	Serial.println(EEPROM.read(EERPOM_mem_loc_sms_Confugured));	
#endif
return;	
} 														
// Ako nema SMS za kofiguraciju onda automatski detektuje mrezu i konketuje se ......

if (1){//IF stavljen zbog debagovanja, izbirsati polse..... Prepoznaje sam mrezu i konfigurisi se..... 
 //delay(20000);
   //SerialAT.println("AT+COPS?");
   SerialAT.println("AT+CPSI?");
  delay(1000);
  // Čitanje odgovora
  String xString = "";
  while (SerialAT.available()) {
    xString += SerialAT.readString();
  }
#ifdef DEBUG_Operater  
  Serial.println(xString);
#endif 
  
 int index = xString.indexOf("220-01"); //Telenor
 
 if (index != -1){
	 
	 Serial.println(F("Rekognize Telenor"));	 
	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != Telenor){
		 strcpy(carrierdata.apnname, operaterTelenor.apnname);   
		 strcpy(carrierdata.apnuser, operaterTelenor.apnuser);     
		 strcpy(carrierdata.apnpass, operaterTelenor.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, Telenor);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje Telenor ")); 
#endif
			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 } 
 
  index = xString.indexOf("220-03");//MTS
  
 if (index != -1){
	 
	 Serial.println(F("Rekognize MTS"));	 

 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != Telekom){
	 strcpy(carrierdata.apnname, operaterMTS.apnname);  
     strcpy(carrierdata.apnuser, operaterMTS.apnuser);    
     strcpy(carrierdata.apnpass, operaterMTS.apnpass); 
	 EEPROM_writeAnything(EEPROM_mem_loc_Operater, Telekom);
	 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
	 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
	Serial.println(F("Operater postaje Telekom ")); 
#endif		 
			
   }	
	else{		
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	
	return;
 } 
 
  index = xString.indexOf("297-03");// mtel CG
  
 if (index != -1){
 	 
	 Serial.println(F("Rekognize mtel CG"));	 
	
 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != mtelCG){
	 strcpy(carrierdata.apnname, operaterMTEL_CG.apnname);  
     strcpy(carrierdata.apnuser, operaterMTEL_CG.apnuser);    
     strcpy(carrierdata.apnpass, operaterMTEL_CG.apnpass); 
	 EEPROM_writeAnything(EEPROM_mem_loc_Operater, mtelCG);
	 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
	 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
	Serial.println(F("Operater postaje mtel CG ")); 
#endif		 
			
   }	
	else{		
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	
	return;
 } 
 
   //index = xString.indexOf("A1 SRB");//vip operater
   index = xString.indexOf("220-05");//vip operater
   
 if (index != -1){
 	 
	 Serial.println(F("Rekognize VIP"));	 
	
 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
  if(operaterID != Vip){
	 strcpy(carrierdata.apnname, operaterVIP.apnname);   
     strcpy(carrierdata.apnuser, operaterVIP.apnuser);    
     strcpy(carrierdata.apnpass, operaterVIP.apnpass); 	
	 EEPROM_writeAnything(EEPROM_mem_loc_Operater, Vip);
	 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
	 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
	Serial.println(F("Operater postaje VIP ")); 
#endif		 
	 	
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 } 
 
    index = xString.indexOf("218-90");
   
 if (index != -1){
 	 
	 Serial.println(F("Rekognize BHmobile"));	 

 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
  if(operaterID != BHmob){
	 strcpy(carrierdata.apnname, operaterBHmob.apnname);   
     strcpy(carrierdata.apnuser, operaterBHmob.apnuser);    
     strcpy(carrierdata.apnpass, operaterBHmob.apnpass); 	
	 EEPROM_writeAnything(EEPROM_mem_loc_Operater, BHmob);
	 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
	 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
	Serial.println(F("Operater postaje BHmobile ")); 
#endif		 
	 	
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 } 
 
  index = xString.indexOf("218-05"); //M:TEL
 
 if (index != -1){
	 
	 Serial.println(F("Rekognize M:TEL"));	 
	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != MTEL){
		 strcpy(carrierdata.apnname, operaterMTEL.apnname);   
		 strcpy(carrierdata.apnuser, operaterMTEL.apnuser);     
		 strcpy(carrierdata.apnpass, operaterMTEL.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, MTEL);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje M:TEL")); 
#endif
			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 } 
 
   index = xString.indexOf("218-299"); //Haloo, PROVERI !!!
 
 if (index != -1){
	 
	 Serial.println(F("Rekognize Haloo"));	 
	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != Haloo){
		 strcpy(carrierdata.apnname, operaterHaloo.apnname);   
		 strcpy(carrierdata.apnuser, operaterHaloo.apnuser);     
		 strcpy(carrierdata.apnpass, operaterHaloo.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, Haloo);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje Haloo")); 
#endif
			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 }
 
 
  index = xString.indexOf("218-03"); //BHeronet
 
 if (index != -1){
	 
	 Serial.println(F("rekognize BHeronet"));	 

	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != BHeronet){
		 strcpy(carrierdata.apnname, operaterBHeronet.apnname);   
		 strcpy(carrierdata.apnuser, operaterBHeronet.apnuser);     
		 strcpy(carrierdata.apnpass, operaterBHeronet.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, BHeronet);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje BHeronet")); 
#endif
			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 }
 
 
index = xString.indexOf("420-03"); //Mobily Arapski Emirati 
 if (index != -1){	 
	 Serial.println(F("Rekognize Mobily"));	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != Mobily){
		 strcpy(carrierdata.apnname, operaterMobily.apnname);   
		 strcpy(carrierdata.apnuser, operaterMobily.apnuser);     
		 strcpy(carrierdata.apnpass, operaterMobily.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, Mobily);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje Mobily")); 
#endif			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 } 
 
 
 index = xString.indexOf("420-04"); //ZAIN Arapski Emirati 
 if (index != -1){	 
	 Serial.println(F("Rekognize ZAIN"));	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != Zain){
		 strcpy(carrierdata.apnname, operaterZain.apnname);   
		 strcpy(carrierdata.apnuser, operaterZain.apnuser);     
		 strcpy(carrierdata.apnpass, operaterZain.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, Zain);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje Zain")); 
#endif			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 } 


 index = xString.indexOf("420-01"); //STC Arapski Emirati 
 if (index != -1){	 
	 Serial.println(F("Rekognize STC"));	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != STC){
		 strcpy(carrierdata.apnname, operaterSTC.apnname);   
		 strcpy(carrierdata.apnuser, operaterSTC.apnuser);     
		 strcpy(carrierdata.apnpass, operaterSTC.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, STC);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje STC")); 
#endif			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 } 
 
 index = xString.indexOf("268-299"); //Portual NOWO
 if (index != -1){	 
	 Serial.println(F("Rekognize NOWO"));	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != NOWO){
		 strcpy(carrierdata.apnname, operaterNOWO.apnname);   
		 strcpy(carrierdata.apnuser, operaterNOWO.apnuser);     
		 strcpy(carrierdata.apnpass, operaterNOWO.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, NOWO);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje NOWO")); 
#endif			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 } 
 
 
  index = xString.indexOf("268-06"); //Portugal MEO
 if (index != -1){	 
	 Serial.println(F("Rekognize MEO"));	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != MEO){
		 strcpy(carrierdata.apnname, operaterMEO.apnname);   
		 strcpy(carrierdata.apnuser, operaterMEO.apnuser);     
		 strcpy(carrierdata.apnpass, operaterMEO.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, MEO);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje MEO")); 
#endif			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 } 
 
   index = xString.indexOf("268-80"); //Portugal MEO
 if (index != -1){	 
	 Serial.println(F("Rekognize MEO"));	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != MEO){
		 strcpy(carrierdata.apnname, operaterMEO.apnname);   
		 strcpy(carrierdata.apnuser, operaterMEO.apnuser);     
		 strcpy(carrierdata.apnpass, operaterMEO.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, MEO);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje MEO")); 
#endif			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 } 
 
    index = xString.indexOf("268-08"); //Portugal MEO
 if (index != -1){	 
	 Serial.println(F("Rekognize MEO"));	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != MEO){
		 strcpy(carrierdata.apnname, operaterMEO.apnname);   
		 strcpy(carrierdata.apnuser, operaterMEO.apnuser);     
		 strcpy(carrierdata.apnpass, operaterMEO.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, MEO);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje MEO")); 
#endif			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 } 
 
   index = xString.indexOf("268-91"); //Portugal Vodafone
 if (index != -1){	 
	 Serial.println(F("Rekognize Vodafone"));	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != MEO){
		 strcpy(carrierdata.apnname, operaterVodafone.apnname);   
		 strcpy(carrierdata.apnuser, operaterVodafone.apnuser);     
		 strcpy(carrierdata.apnpass, operaterVodafone.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, Vodafone);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje Vodafone")); 
#endif			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 }  

   index = xString.indexOf("268-01"); //Portugal Vodafone
 if (index != -1){	 
	 Serial.println(F("Rekognize Vodafone"));	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != MEO){
		 strcpy(carrierdata.apnname, operaterVodafone.apnname);   
		 strcpy(carrierdata.apnuser, operaterVodafone.apnuser);     
		 strcpy(carrierdata.apnpass, operaterVodafone.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, Vodafone);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje Vodafone")); 
#endif			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 }  

   index = xString.indexOf("268-03"); //Portugal NOS
 if (index != -1){	 
	 Serial.println(F("Rekognize NOS"));	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != MEO){
		 strcpy(carrierdata.apnname, operaterNOS.apnname);   
		 strcpy(carrierdata.apnuser, operaterNOS.apnuser);     
		 strcpy(carrierdata.apnpass, operaterNOS.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, NOS);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje NOS")); 
#endif			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 }  

   index = xString.indexOf("268-93"); //Portugal NOS
 if (index != -1){	 
	 Serial.println(F("Rekognize NOS"));	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != MEO){
		 strcpy(carrierdata.apnname, operaterNOS.apnname);   
		 strcpy(carrierdata.apnuser, operaterNOS.apnuser);     
		 strcpy(carrierdata.apnpass, operaterNOS.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, NOS);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje NOS")); 
#endif			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 }

   index = xString.indexOf("268-04"); //Portugal Lucamobile
 if (index != -1){	 
	 Serial.println(F("Rekognize LucaMobile"));	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != MEO){
		 strcpy(carrierdata.apnname, operaterLucaMobile.apnname);   
		 strcpy(carrierdata.apnuser, operaterLucaMobile.apnuser);     
		 strcpy(carrierdata.apnpass, operaterLucaMobile.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, LucaMobile);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje LucaMobile")); 
#endif			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 }
 
    index = xString.indexOf("268-299"); //Portugal ONI
 if (index != -1){	 
	 Serial.println(F("Rekognize ONI"));	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != MEO){
		 strcpy(carrierdata.apnname, operaterONI.apnname);   
		 strcpy(carrierdata.apnuser, operaterONI.apnuser);     
		 strcpy(carrierdata.apnpass, operaterONI.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, ONI);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje ONI")); 
#endif			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 }
 
    index = xString.indexOf("221-07"); //Kosovo D3 Mobile
 if (index != -1){	 
	 Serial.println(F("Rekognize D3 Mobile"));	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != MEO){
		 strcpy(carrierdata.apnname, operaterKosovoD3.apnname);   
		 strcpy(carrierdata.apnuser, operaterKosovoD3.apnuser);     
		 strcpy(carrierdata.apnpass, operaterKosovoD3.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, operaterKosovoD3);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje D3")); 
#endif			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 } 
 
    index = xString.indexOf("221-02"); //Kosovo IPKO 
 if (index != -1){	 
	 Serial.println(F("Rekognize IPKO "));	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != MEO){
		 strcpy(carrierdata.apnname, operaterKosovoIPKO.apnname);   
		 strcpy(carrierdata.apnuser, operaterKosovoIPKO.apnuser);     
		 strcpy(carrierdata.apnpass, operaterKosovoIPKO.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, operaterKosovoIPKO);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje IPKO")); 
#endif			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 } 

    index = xString.indexOf("221-299"); //Kosovo MTS
 if (index != -1){	 
	 Serial.println(F("Rekognize MTS"));	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != MEO){
		 strcpy(carrierdata.apnname, operaterKosovoMTS.apnname);   
		 strcpy(carrierdata.apnuser, operaterKosovoMTS.apnuser);     
		 strcpy(carrierdata.apnpass, operaterKosovoMTS.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, operaterKosovoMTS);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje MTS")); 
#endif			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 } 
 
    index = xString.indexOf("221-01"); //Kosovo Vala
 if (index != -1){	 
	 Serial.println(F("Rekognize Vala"));	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != MEO){
		 strcpy(carrierdata.apnname, operaterKosovoVala.apnname);   
		 strcpy(carrierdata.apnuser, operaterKosovoVala.apnuser);     
		 strcpy(carrierdata.apnpass, operaterKosovoVala.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, operaterKosovoVala);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje Vala")); 
#endif			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 } 
 
}// if ( 1 )


}

void setSMSMemory() {
  //SerialAT.println("AT+CPMS=\"SM\",\"SM\",\"SM\"");// SIM memorija
  SerialAT.println("AT+CPMS=\"ME\",\"ME\",\"ME\""); //FLASH memorija
  delay(1000);
  
  String response = "";
  while (SerialAT.available()) {
    response += SerialAT.readString();
  }
  //Serial.println(response);
#ifdef DEBUG_Operater 	
  if (response.indexOf("OK") == -1) {
    Serial.println(F("Failed to set SMS memory to SIM card"));
  } else {
    Serial.println(F("SMS memory set to SIM card"));
  }
#endif
}

int checkForSMS(void) {
//  Serial.println("Checking for SMS messages...");
  SerialAT.println("AT+CPMS?"); // Check SMS storage status
  delay(1000);

  // Čitanje odgovora
  String response = "";
  while (SerialAT.available()) {
    response += SerialAT.readString();
  }
//  Serial.println(response);

  // Provera da li odgovor sadrži broj poruka veći od 0
  int startIndex = response.indexOf(",");
  if (startIndex != -1) {
    int endIndex = response.indexOf(",", startIndex + 1);
    if (endIndex != -1) {
      String messageCountStr = response.substring(startIndex + 1, endIndex);
      int messageCount = messageCountStr.toInt();
//	  Serial.print("messageCount");Serial.println(messageCount);
      return messageCount;
    }
  }
#ifdef DEBUG_Operater 
 Serial.println(F("No SMS in Inbox"));	
#endif
  
  return 0; // Nema SMS poruka
}

bool startsWith(const char *pre, const char *str)
{
  return strncmp(pre, str, strlen(pre)) == 0;
}

//=============================================================
void setSMSMode() {
  SerialAT.println("AT+CMGF=1"); // Set SMS mode to text
  delay(3000);

  String response = "";
  while (SerialAT.available()) {
    response += SerialAT.readString();
  }
  //Serial.println(response);
#ifdef DEBUG_Operater 
  if (response.indexOf("OK") == -1) {
    Serial.println(F("Failed to set SMS mode"));
  } else {
    Serial.println(F("SMS mode set to text \n\n"));
  }
#endif  

}

//read And Delete All SMS WithPrefix
void SMSprocessing(void) {
  int index = 0; // Start from index 1
  bool found = false;
  byte counter = 50;

  while ((counter--)) {
    // Čitanje i brisanje SMS poruke
    //Serial.print("Reading and deleting SMS at index ");
    //Serial.println(index);
    SerialAT.print("AT+CMGRD=");
    SerialAT.println(index);
    delay(1000);

    String response = "";
    while (SerialAT.available()) {
      response += SerialAT.readString();
    }
    //Serial.println("Read and delete response:");
//    Serial.println(response);

    if (response.indexOf("ERROR") != -1) {
#ifdef DEBUG_Operater 		
      Serial.println(F("No more SMS messages."));
#endif	  
      break; // No more messages to read
    }

    // Parsiranje broja telefona i sadržaja poruke
    int startIndex = response.indexOf("\",\"") + 3;
    int endIndex = response.indexOf("\",\"", startIndex);
    String phoneNumber = response.substring(startIndex, endIndex);
	
	//Serial.print(F("Phone Number: "));
    //Serial.println(phone_num);
	
    startIndex = response.indexOf("\r\n", endIndex) + 2;
    endIndex = response.indexOf("\r\n", startIndex);
    if (endIndex == -1) {
      endIndex = response.length();
    }

    if (startIndex > 0 && startIndex < response.length()) {
      String message = response.substring(startIndex, endIndex);
      message.trim(); // Trim to remove any leading/trailing whitespace
	  phoneNumber.trim();	  
	  phoneNumber.toCharArray(phone_num, BUFF_MOB_NUM);
#ifdef DEBUG_Operater 	  
      Serial.print(F("Phone Number: "));
      Serial.println(phone_num);
      Serial.print(F("SMS Text:"));
      Serial.println(message);
#endif
      if (message.startsWith("SW ")) {
        message.toCharArray(sms_text, BUFF_SMS);		
        found = true;
        //break;
      }
    } else {
#ifdef DEBUG_Operater 		
      Serial.println(F("No valid SMS message Config found."));
#endif
    }
    index++; // Move to the next message
  }

  if (!found) {
    sms_text[0] = '\0'; // No SMS found with the specified prefix
  }
  
}

bool sendSMS(const char* phoneNumber, const char* message) {
  //Serial.println("Slanje SMS poruke...");

  // Slanje komande za slanje SMS poruke
  SerialAT.print("AT+CMGS=\"");
  SerialAT.print(phoneNumber);
  SerialAT.println("\"");
  delay(100);

  // Slanje teksta poruke
  SerialAT.print(message);
  delay(100);

  // Slanje Ctrl+Z za završavanje SMS poruke
  SerialAT.write(26); // ASCII kod za Ctrl+Z
  delay(5000);

  // Čitanje odgovora
  String response = "";
  while (SerialAT.available()) {
    response += SerialAT.readString();
  }
//  Serial.println(response);

#ifdef DEBUG_Operater 
  // Provera odgovora za uspešnost slanja poruke
  if (response.indexOf("OK") != -1) {
    Serial.println(F("SMS poruka uspesno poslata."));
    return true; // Uspešno poslata poruka
  } else {
    Serial.println(F("Greska pri slanju SMS poruke."));
    return false; // Greška pri slanju poruke
  }
#endif
}

void deleteAllSMS() {
//  Serial.println("Brisanje svih SMS poruka...");
  SerialAT.println("AT+CMGD=1,4"); // Brisanje svih SMS poruka
  delay(5000);

  // Čitanje odgovora
  String response = "";
  while (SerialAT.available()) {
    response += SerialAT.readString();
  }
//  Serial.println(response);
#ifdef DEBUG_Operater 
  // Provera odgovora za uspešnost brisanja
  if (response.indexOf("OK") != -1) {
    Serial.println(F("Sve SMS poruke su uspešno obrisane."));
  } else {
    Serial.println(F("Greška pri brisanju SMS poruka."));
  }
#endif
}

#endif

#ifdef GSM_GPRS
byte initGSM_GPRS(void){
  pinMode(gprs_restart_pin, OUTPUT);
  digitalWrite(gprs_restart_pin, HIGH);  
  
  delay(100);  
  digitalWrite(8, HIGH); //on 
  delay(1050);  
  digitalWrite(8, LOW); //off
  delay(500);
  digitalWrite(gprs_restart_pin, LOW);
  delay(150);
  digitalWrite(gprs_restart_pin, HIGH);  
 
  delay(3200);


}
#endif

int getSignalStrength(void) {
  SerialAT.println("AT+CSQ"); // Šalje AT komandu za merenje signala
  delay(1000);

  // Čitanje odgovora
  String response = "";
  while (SerialAT.available()) {
    response += SerialAT.readString();
  }
  //Serial.println(response); // Opcionalno: Ispisuje sirovi odgovor

  // Parsiranje rezultata
  int startIndex = response.indexOf("+CSQ: ");
  if (startIndex != -1) {
    int commaIndex = response.indexOf(",", startIndex);
    String rssiStr = response.substring(startIndex + 6, commaIndex);
    int rssi = rssiStr.toInt();

    if (rssi == 99) {
      return -1; // 99 označava nepoznat nivo signala
    } else {
      // Konvertovanje iz AT+CSQ formata u dBm
      return convertRSSIToDBm(rssi);
    }
  }
  return -1; // Neuspešno čitanje signala
}

int convertRSSIToDBm(int rssi) {
  // RSSI vrednosti od 0 do 31 su mapirane na dBm nivo signala
  if (rssi >= 0 && rssi <= 31) {
    return -113 + (rssi * 2); // Formula za konverziju RSSI u dBm
  }
  return -1; // Ako je vrednost van opsega
}

bool checkAntennaConnection() {

  if (getSignalStrength () == -1) {
    Serial.println(F("Antenna not connected properly"));
	return false;
  }

  return true;
}							 

byte checkSIMPresence() {
  SerialAT.println("AT+CPIN?");  // Šalje AT komandu za proveru SIM statusa
  delay(1000);

  // Čitanje odgovora
  String response = "";
  while (SerialAT.available()) {
    response += SerialAT.readString();
  }
  
  //Serial.print("SIM status ");Serial.println(response);  // Opcionalno: Ispisuje sirovi odgovor
  // Provera statusa
  if (response.indexOf("OK") != -1) {
    return 1;
  } else if (response.indexOf("ERROR") != -1) {
    return 0;
  } else {
    return 5;
  }
}