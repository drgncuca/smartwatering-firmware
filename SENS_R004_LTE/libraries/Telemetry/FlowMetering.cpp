#include <Arduino.h>
#include "Telemetry.h"

#define maxDeltaTimeCNT 1

#ifdef FLOW_10L_imp

byte  flowImplseTick10L = 0;
byte  flowImplseTick10L_old = 0;
unsigned int deltaTime = 0; 
unsigned int deltaTime_old = 0;
byte deltaTimeCounter = 0;
unsigned long   oldTime = 0L; 
extern byte   loopsCounter     = 0;


void flowImplseTick10L_Procesing(void)
{
	
	flowImplseTick10L = flowImplseTick10L + 1; 
	deltaTime = (float)(millis() - oldTime)  ; // u  miliSekundama
	oldTime   = millis();
}


float getCurrentVOLtick10L(void){
byte temp = (flowImplseTick10L - flowImplseTick10L_old);	
if (  temp  > 0  ){	
		flowImplseTick10L_old = flowImplseTick10L;		
		return 10 * temp;		
}
else{
	return 0;
}

}


// protok L/min za protokomere od impuls/100L
float getFlow10L(void){
	//Serial.print(F("Delta Time "));Serial.println(deltaTime);
	if (deltaTime >0 ) {		
		unsigned int tempror = deltaTime;
		if (deltaTime_old == deltaTime) deltaTimeCounter++; 
		if (deltaTimeCounter >maxDeltaTimeCNT){
			deltaTime = 0.0;
			deltaTime_old = 0.0;
			deltaTimeCounter = 0;
		}
		deltaTime_old = deltaTime;
		return 1000*(600.0/tempror) ; // L/min		
	}
	else return 0.0;
}

void resetFlowImpulseTick10L(void){
	flowImplseTick10L = 0;
	deltaTime = 0.0; 
	oldTime = 0L; 	
}
#endif

#ifdef FLOW_100L_imp

byte  flowImplseTick100L = 0;
byte  flowImplseTick100L_old = 0;
unsigned int deltaTime = 0; 
unsigned int deltaTime_old = 0;
byte deltaTimeCounter = 0;
unsigned long   oldTime = 0L; 
extern byte   loopsCounter     = 0;


void flowImplseTick100L_Procesing(void)
{
	
	flowImplseTick100L = flowImplseTick100L + 1; 
	deltaTime = (float)(millis() - oldTime)  ; // u  miliSekundama
	oldTime   = millis();
}


float getCurrentVOLtick100L(void){
byte temp = (flowImplseTick100L - flowImplseTick100L_old);	
if (  temp  > 0  ){	
		flowImplseTick100L_old = flowImplseTick100L;		
		return 100 * temp;		
}
else{
	return 0;
}

}


// protok L/min za protokomere od impuls/100L
float getFlow100L(void){
	//Serial.print(F("Delta Time "));Serial.println(deltaTime);
	if (deltaTime >0 ) {		
		unsigned int tempror = deltaTime;
		if (deltaTime_old == deltaTime) deltaTimeCounter++; 
		if (deltaTimeCounter >maxDeltaTimeCNT){
			deltaTime = 0.0;
			deltaTime_old = 0.0;
			deltaTimeCounter = 0;
		}
		deltaTime_old = deltaTime;
		return 1000*(6000.0/tempror) ; // L/min		
	}
	else return 0.0;
}

void resetFlowImpulseTick100L(void){
	flowImplseTick100L = 0;
	deltaTime = 0.0; 
	oldTime = 0L; 	
}
#endif


#ifdef FLOW_1000L_imp

byte  flowImplseTick1000L = 0;
byte  flowImplseTick1000L_old = 0;
unsigned int deltaTime = 0.0; 
unsigned int deltaTime_old = 0.0;
byte deltaTimeCounter = 0;
unsigned long   oldTime = 0L; 
extern byte   loopsCounter     = 0;


void flowImplseTick1000L_Procesing(void)
{
	
	flowImplseTick1000L = flowImplseTick1000L + 1; 
	deltaTime = (millis() - oldTime) ; // u miliSekundama
	oldTime   = millis();
}


float getCurrentVOLtick1000L(void){
byte temp = (flowImplseTick1000L - flowImplseTick1000L_old);	
if (  temp  > 0  ){	
		flowImplseTick1000L_old = flowImplseTick1000L;		
		return 1000.0 * temp;		
}
else{
	return 0;
}

}


// protok L/min za protokomere od impuls/1000L
float getFlow1000L(void){
	//Serial.print(F("Delta Time "));Serial.println(deltaTime);
	if (deltaTime > 0 ) {		
		unsigned int tempr = deltaTime;
		if (deltaTime_old == deltaTime) deltaTimeCounter++; 
		if (deltaTimeCounter >maxDeltaTimeCNT){
			deltaTime = 0.0;
			deltaTime_old = 0.0;
			deltaTimeCounter = 0;
		}
		deltaTime_old = deltaTime;
		return 1000*(60000.0/(float)tempr); // L/min, Vreme izmedju dva impulsa je u mS. jedan inpuls je 1000m3. L/s = 1000L/s = 1L/mS => a za 1min L/min = 60* 1/mS			
	}
	else return 0.0;
}

void resetFlowImpulseTick1000L(void){
	flowImplseTick1000L = 0;
	deltaTime = 0.0; 
	oldTime = 0L; 	
}
#endif


void initialiseIrrFlowMet(void){
		irrigationFlowMetering.irrigationFlowVolume = 0.00;
		irrigationFlowMetering.ElapsedTime=0.00;
		irrigationFlowMetering.flowMeterCalibKons =1.0;	
	#ifdef DEBUG_FLOW 	
		irrigationFlowMetering[i].flow = 0.00;
	#endif		
}
//-----------------------------------------------------------------------
float getCurrentVOL( ){

unsigned long currentTime = millis();

unsigned long samplElapsedTime = currentTime  - irrigationFlowMetering.ElapsedTime; // proteklo vreme od zadnjeg semplovanja

if (samplElapsedTime < 0) {samplElapsedTime = 0;} //kad miles() funkcija dodje do kraja i pocne ispoceta da broji, prethodna jednacina bi imala ogromanu negativnu vrednost		
irrigationFlowMetering.ElapsedTime = currentTime;

//integralenje protoka po vremenu: Protok * Vreme = ZAPREMINA.	
float tmp = getCurrentFlow();

float  deltaVolume = ( tmp *  (float)(samplElapsedTime)  ) /1000.0; //    protok * vreme = zapremina. millis() vraca u mS vreme.
	
#ifdef DEBUG_FLOW 	
	irrigationFlowMetering.flow = getCurrentFlow();
#endif																				  
//Kad ce resetovati ova promenljiva ? ? ?  
irrigationFlowMetering.irrigationFlowVolume = irrigationFlowMetering.irrigationFlowVolume + deltaVolume;
  
return deltaVolume;//irrigationFlowMetering[].irrigationFlowVolume;		
}
// vraca ukupnu proteklu kolicinu vode [L]
float getTotalVolume(void){

return irrigationFlowMetering.irrigationFlowVolume;


}										  
//-------------------------------------------------------	
								// od 0 do  4
float getCurrentFlow(void){  //vraca vrednost protoka Litara / Sekundi 

float tempFlow=0;  
  byte i = 0;
  int sum = 0;
  float flowSensValue = 0;
  
  //filtriranje signala sa ADC-a
  while (i < 4)
  {
    sum = sum + analogRead(A5);
    delay(5);
    i++;
  }
  if(sum<9) sum = 0;


  flowSensValue = (float)sum * 0.250;//averaging
  // vraca vrednost Litara/Sekundi, za sistem od 33imp/L 
  tempFlow = irrigationFlowMetering.flowMeterCalibKons * flowSensValue / flowConstantImpuls33PerSecond;  
//  if (tempFlow<0.01) {tempFlow = 0.0;} //  pojavljuje se gresa/sum pri nekoj matematici, Istraziti gde je....
  if (tempFlow > 8) {tempFlow = 8.0;} //  Ako je protok veci od 8L/S (480L/min) -> ogromna vrednost, nemoguce da je toliki.																																							


  return tempFlow;
 
}
 //-------------------------------------------------------------------------------------
#ifdef DEBUG_FLOW    //ChanelS = [0..4]
void FlowDebug(){ // funkcija za debagovanje 			 
	  Serial.println(F("------------------------------------"));				   
						 

//		Serial.print(F(", dV: "));           Serial.print(getCurrentVOL( flowmeter_sensor));
		Serial.print(F(", FLOW: ")); Serial.print(irrigationFlowMetering.flow);Serial.print(F("L/s"));	 	
		Serial.print(F(", V: "));    Serial.print(irrigationFlowMetering.irrigationFlowVolume);
		Serial.print(F("L,Time: "));Serial.print(((irrigationFlowMetering.ElapsedTime)/1000.0) );Serial.println(F("S."));
	  
}
#endif


