#include <String.h>

#include <TimeLib.h>
#include <SHT1x.h>

#ifndef Telemetry_h                                                   
  #define Telemetry_h 
  
//================================  B O A R D   HW    V E R S I O N   =================================//

#define BOARD_SENS_R001
//================================   P O D E S I T I    P R E    I S P O R U K E  ====================//

#define secondsWaitingSMS 5	  // Podesiti na 15 - 20,  4s * 15 = 60s cekanje na SMS
#define GSM_SMS_Replay       // Ako korisik hoce povratni SMS -> Otkomentarisati

//#define SKIP_Operater_Detecting   // zakomentarisati ovo !!!! ( samo za debagovanje)

//================================   Electrical  Conductivityu  =================================//

#define NMBRsampling  250  // broj semplova SINusnog signala EC drajvera
#define NMBRaveraging   4  //usrednjavanje min i max vrednosti amplitude

//---------------------------------------------------------------------------------------------
#define BUFF_RTC_TIME 1 // Za debagovanje stavi 256
#define MSG_BUF  400

#define BUFF_SMS 40
#define BUFF_MOB_NUM 20

//============================= V A L V E S   &&    R E L A Y S  ==========================//

  #define DC_Valves 
//#define DC_Valves_Permanent

  #define NUM_SLOTS 4  // each SLOT consume 100B of memory	

//============================= D E B U G   D I R E C T I V E ============================//

//#define DEBUG_SENTEK
//#define DEBUG_SENTEK_Write_to_Excel								   
//#define DEBUG_Server_Comun
#define DEBUG_BASIC
#define DEBUG                //210B
//#define DEBUG_Extend
//#define DEBUG_Telemetry
//#define DEBUG_Fertilisation
//#define DEBUG_FLOW
//#define DEBUG_EC
//#define DEBUG_Temperature
//#define DEBUG_ON          //od GSM modula
//#define DEBUG_Operater
//#define TIME_F_Measure
//#define DEBUG_PT1000
//#define Operater_Setings_Skip
//#define DEBUG_Power_Saving
#define DEBUG_LiPo_CHG
//#define DEBUG_RainFallSensor
//=================================   M O D U L E S  ========================================//
#define SENTEC_Include   //400B  Temperature + Humidity
//#define SENTEK_Salinity   
//#define SENTEK_Include_Extend_Sens  //170B

#define GSM_Carrier_Change 

#define DC_Power_SENS    // Battery and Solar Panel

//#define Power_Saving

#define FLOW
//#define INTERRUPTFLOW   //200B
//#define FLOW_Toggling

//#define Sensor_EC        //memory use: 1.2kB  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


#define Sensor_pH

//#define Sensor_BGT_WSD2  //senzor Vlage i Temperature analogni.

//#define Sensor_10HS_1  //Senzor Electrical Conductivity (u obliku Viljuske ) )

//#define Sensor_10HS_2  //

//#define SHT1sensor   //I2C senzor temp i vlage

#define LiPo_Charger  

//#define WindDurection

//#define WindSpeed

//#define RainFallSensor
																		 
//=================================== P O W E R ==========================================//
#define AC_power_IN 7	

void setupWatchDogTimer(void);
void enterSleep(void);
void PowerStandBy(byte PowerStandByTimeInMin);
void PowerWakeUP(void);		
int getPower(void) ;
float getVoltageSolar(void);
float getVoltageBatt(void);

//=================================== Li-Po Charger Calibration Constants ==========================================//

//Board SN:        1     2     3      4      5       6       7      8      9     10      11      12      13       14      15      16
//Const Batt       
//Const Solar      

//Board SN:        17         18        19     20     21      22      23       24       25      26       27
//Const Batt      
//Const Solar     

#define battConst  334.0
#define solarConst 177.2

void getLiPoDeviceID(void);
unsigned int getLiPoChargeOption(void);

unsigned int readLiPoChargeReg(void);
unsigned int getLiPoChargeCurrent(void);
unsigned int getLiPoChargeVoltage(void);
unsigned int getLiPoInputCurrent(void);
void setLiPoChargeVoltage( unsigned int Voltage);
void setLiPoChargeCurrent( unsigned int Current);
void setLiPoINcurrent( unsigned int Current);
void stopLiPoCharging();
void startLiPoCharging(unsigned int Current);
void setWdogLiPoCharger(byte timeRange);
unsigned int readRegister(byte address) ;	
byte writeRegister(byte address, unsigned int val);
void LiPoChargeInit(void);
void LiPoChargeManager(void);
void LiPoChargeConfigure(void);
//==============================   Temperature PT1000  =================================//
#define RREF      4300.0
#define RNOMINAL  1000.0

// Pins for SPI comm with the AD9833 IC
#define DATA  50  ///< SPI Data pin number
#define CLK   52  ///< SPI Clock pin number
#define FSYNC 53  ///< SPI Load pin number (FSYNC in AD9833 usage)

void setupTemperaturePT1000(void);
void testTemperaturePT1000(void);
float getTemepraturePT1000(void);

//=================================  RS485  =========================================//
void RS485setup(void);
void RS485readEnableCH(void);
void RS485writeStringLN(String string);
void RS485readCharArray(char *CharArray);
String RS485readString(void);
#define SENTEK_SENS_R001

//================================   SENTEK  =======================================//																					  

#ifdef SENTEC_Include
    void setSANTEKsensors(void);
	float getSENTEKtemperature(byte debpth);
	float getSENTEKmoisture(byte debpth);
	float getSENTEKsalinity(byte debpth);
	void setSANTEKsensors(void);	
	float hexStringToFloat(char temp[], byte sensor);	
	void reqSENTEKpresetHoldReg_Temperature(void);
	void reqSENTEKpresetHoldReg_Moisture(void);
	void reqSENTEKpresetHoldReg_Salinity(void);	
	float filtering(float s,byte );	
	byte LRC (char * char_temp);
	void getEcho(void);
	void getIlegalCommand(void);
	void measureStatus(void);
	void ivenCounter(void);
	void slaveID(void);
	void sensorDepth(void);
	void sensorSampledMask(void);
	void scanSelectionMask(void);
	void detectedSelectionMask(void);
	void numberOfSensors(void);
	byte compareLRC(char * char_temp);
	void resetString(void);
	void writeToExcel( unsigned int row, unsigned int column, unsigned int value);
#endif	
//================================= BGT_WSD2 ====================================================//
int getBGTtemperature(void);
int getBGThumidity(void);
//--------------------------------------------------------------------------------------
#define CLS          "\033[2J" 

#define relayFertPump_1 35
#define relayFertPump_2 34
#define relayFertPump_3 36
#define relayFertPump_4 37

#define relay_stop   LOW
#define relay_start  HIGH


#define ph_sensor_pin A2  // pH senzor analog IN
//-------------------------Carier Operater Changing----------------------------------------------//
#define EERPOM_mem_loc_SW  64
#define EEPROM_mem_loc_Operater 62
#define EERPOM_mem_loc_CarrierData 0

#define Telenor 1
#define Vip 2
#define Telekom 3

 struct carrier
{
  char apnname[12];
  char apnuser[12];
  char apnpass[12];
  char operater[12];
} ;

extern carrier carrierdata; 

 struct struct_operater_vip
{
  char apnname[12]="internet";
  char apnuser[12]="internet";
  char apnpass[12]="internet";  
} ;
extern struct_operater_vip operaterVIP;

 struct struct_operater_MTS
{
  char apnname[12]="gprswap"; 
  char apnuser[12]="mts";
  char apnpass[12]="064";  
} ;
extern struct_operater_MTS operaterMTS;

 struct struct_operater_Telenor
{
  char apnname[12]="internet";
  char apnuser[12]="telenor";
  char apnpass[12]="gprs";  
} ;
extern struct_operater_Telenor operaterTelenor;

void doesOperaterChanging(void);
bool startsWith(const char *pre, const char *str);
void initOperators(void);
void deleteALLsms(void);
float getRSSI(void);

//------------------------Temperature and Humidity SHTx sesnor--------------------------------------------//
#define dataPin  20
#define clockPin 21

extern float temperature;
extern float humidity;
extern SHT1x SHT1x_sensor;

//----------------------------Electrical Conductivityu-------------------------------------------//
extern float EC_calibr_Coeff;
extern float EC_calibr_M_Coeff;
void setConductivityDriver(int frequency, int signalShape);
float getELconductance(void);

//--------------------------------FLOW parameters---------------------------------------------//
#define FlowSensorCH1 0
#define FlowSensorCH2 1
#define FlowSensorCH3 2
#define FlowSensorCH4 3
#define FlowSensorCH5 4

#define flowConstantImpuls33PerSecond 117.0

struct irrigation_struct_FlowMetering
{
	float irrigationFlowVolume;	
	unsigned long ElapsedTime;
	float flowMeterCalibKons;

#ifdef DEBUG_FLOW 	
	float flow ;
#endif
};

extern irrigation_struct_FlowMetering  irrigationFlowMetering;


struct pH_struct {
  int rawPh;
  float pH;
  float desiredPh;
  float ferphK;   // Y = k*X +M za korekciju pri merenju pH
  float ferphM;	
  float ferKp;
  float ferKi;
  float ferKd;
};
extern pH_struct pHmeasuring;

extern int valves_fert[] ;
//-----------------------------------------------------------------
float getPh(void);
//-----------------------------------------------------------------
float getCurrentFlow(void);
void Flow(void);
void initialiseIrrFlowMet(void);
float getCurrentVOL(void);
void FlowDebug(void);
//time_t currentTime(void);
				
#endif