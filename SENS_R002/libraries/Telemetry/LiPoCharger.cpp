#include <Arduino.h>
#include <SoftwareSerial.h>
#include "Telemetry.h"
#include "wire.h"

#ifdef LiPo_Charger

#define PMIC_ADDRESS 0x09

#define MAX_Batt_Voltage 12544  //12.544V

byte BattCharging = 0; // Status  baterije
byte BattFullCounter = 0;
byte BattLowCounter = 0;

byte tricklChargeRefresh = 1;
byte tricklChargeStatur  = 0;

#ifdef Power_Saving
extern	byte PowerStandByTimeInMin;
#endif
extern byte BatterySupplying ;


void LiPoChargeInit(void){
 BattCharging = 0;

 // Ne Otkacuj Bateriju sa sistem. 
 pinMode(37, OUTPUT); //BATTERY_RL_R
 digitalWrite(37, LOW); // 
 
 // Zakaci Bateriju na sistem. 
 pinMode(35, OUTPUT); //BATTERY_RL_S
 digitalWrite(35, HIGH); // 
 delay(100);
 digitalWrite(35, LOW); // 
 pinMode(35, INPUT); //Oslobodi pin da moze da hednla taster na ploci
 
 pinMode(36, OUTPUT); //Battery_CH_EN
 digitalWrite(36, HIGH); // Enejbluje napajanje za Charger
 
 pinMode(28, INPUT);//Battery_CH_ACOK

 delay(800);
#ifdef DEBUG_LiPo_CHG 
 getLiPoDeviceID();
#endif 
writeRegister(0x12,0x81B3); // Disable Charging 

//	0x81B3 = 1 0 0 0   0 0 0 1   1 0 1 1   0 0 1 1
//          [15] ACOK rising edge deglitch time 1.3s
//          [14:13] Disable Watchdog Timer
//          [6] LEARN Disable
//          [0] Charge Disabled 

// setWdogLiPoCharger(0);

// Orginalno 12570 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
setLiPoChargeVoltage(MAX_Batt_Voltage); // puni bateriju do 12.57V, 	12.6V je max, i sto je blize toj vrednosti skracuje vek baterije !!!
//setLiPoChargeCurrent(1664); //max struja punjenja
setLiPoChargeCurrent(300); //max struja punjenja
setLiPoINcurrent(1024);	// max ulazna struja.
/*
#ifdef DEBUG_LiPo_CHG 
Serial.print("Set Voltage max"); Serial.println(getLiPoChargeVoltage());
Serial.print("Set Current max"); Serial.println(getLiPoChargeCurrent());
#endif
*/
}

	


//================================ 20%    ===== 40%   ================ 80%    ========== 100%   =======================
//================================ 11.20V ===== 11.40 ================ 12.00V ========== 12.60V =======================
//=====================================================================================================================	
void LiPoChargeManager(void){
	
float batt   = 0;
float solar  = getVoltageSolar();
byte cnt=0;

// Kad se tek nakaci baterija na sistem, treba vremena da se podigne napon na razdelniku koji ide na ADC od uC-a. Veliki kondezator u filteru.
while (cnt<20){		
	batt   = getVoltageBatt();
	if (batt > 11.15) { break;}
	cnt++;
	delay(400);
}

#ifdef DEBUG_LiPo_CHG
	Serial.println("------------------------------------------");
	Serial.print("SOLAR ");  Serial.print(solar); Serial.println("V");
	Serial.print("BATT  ");  Serial.print(batt);  Serial.println("V"); 
//	Serial.print(F("Current OUT:"));Serial.print(getLiPoChargeCurrent());Serial.println(F(" mA"));
#endif


// POKRENI Punjenje ako ima Napajanja sa Solara i ako je baterija prazna na 80%
if ( (solar > 14.0) && ( batt < 12.15 ) && (BattCharging == 0)) {
	BattFullCounter = 0;
	BattCharging = 1;
	tricklChargeStatur = 0;
	
    pinMode(35, OUTPUT); 
	digitalWrite(35, HIGH); // Zakaci Bateriju na sistem. 
	delay(100);
	digitalWrite(35, LOW); // 
	pinMode(35, INPUT); //Oslobodi pin da moze da hednla taster na ploci

	startLiPoCharging(896); // nekih 300mA  // Vrednos struje koju panel moze i po losem suncu da provajduje.........	
	delay(200);
	float tmpSolarVoltage = getVoltageSolar();
	if (tmpSolarVoltage > 14.0 && tmpSolarVoltage < 17.0){ // ako je napon solara veci od 18.5V povecaj struju punjenja na nekih 600mA
		startLiPoCharging(896);	
	}
	if (  (tmpSolarVoltage >= 17.0) && (tmpSolarVoltage < 19.5)  ){ // ako je napon solara veci od 18.5V povecaj struju punjenja na nekih 600mA
		startLiPoCharging(1700);	
	}
	if (  (tmpSolarVoltage >= 19.5) ){ // ako je napon solara veci od 18.5V povecaj struju punjenja na nekih 600mA
		startLiPoCharging(3840); //1200mA	
	}
	
	
#ifdef DEBUG_LiPo_CHG		
	Serial.println(F("START Charging"));
#endif	
BattLowCounter = 0; // resetuj Battery Low Counter

}

//DOPUNJAVANJE, Svakih ( 100*12s = 20min )pokreni dopunjavanje baterije.............              // stavi 100
if ( (solar > 14) && ( batt > 12.25 ) && ( batt < 12.55 ) && (BattCharging == 0) && (tricklChargeRefresh > 100) ) {

	BattCharging = 1;	
	tricklChargeRefresh = 0;
	tricklChargeStatur  = 1;
	startLiPoCharging(432); // nekih 150mA 
	
#ifdef DEBUG_LiPo_CHG		
	Serial.println(F("START Trickl Charging"));
#endif	
BattLowCounter = 0; // resetuj Battery Low Counter
}

if ( BattCharging == 0)
	tricklChargeRefresh ++;
else 
	tricklChargeRefresh = 0;

// Brute force STOP charging
if (batt > 12.61) {
	stopLiPoCharging(); 
}

// stavi 12.54 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
if (batt > 12.55) {
BattFullCounter++;
#ifdef DEBUG_LiPo_CHG	
    Serial.print(F("Batt FULL Count = "));    Serial.println(BattFullCounter); 
#endif	
}
// ZAUSTAVI punjenje 
if ( (BattCharging == 1) && (BattFullCounter > 5)) {
	BattFullCounter = 0;	
	BattCharging = 0;
	tricklChargeStatur = 0;
	
	stopLiPoCharging(); 

#ifdef DEBUG_LiPo_CHG		
	Serial.println(F("FINISH Charging"));
#endif	
}

// Low Battery treshold 11.15V
if((solar < 14) && (batt < 11.15)){
BattLowCounter++;
#ifdef DEBUG_LiPo_CHG	
    Serial.print(F("Batt LOW Count = "));    Serial.println(BattLowCounter); 
#endif
// PowerStandByTimeInMin =0, 1, 15, 60 minuta
#ifdef Power_Saving
if ( PowerStandByTimeInMin >  14)  BattLowCounter += 6;// ako je Power Save modu, i ako se koristi SLEEP onda je rizicno ako cekati da jos koji put se detektuje nizak napon baterije 
if ( PowerStandByTimeInMin == 1 )  BattLowCounter += 2; // 6 ako se nekad uvede i SLEEP od 5min
//if ( PowerStandByTimeInMin == 0 )  BattLowCounter += 1;
#endif
}
if(solar > 14.5) BattLowCounter = 0;

// RASKACI bateriju ako je napon solara prenizak i baterija previse ispraznjenja. Zastita baterije !!!!!!
if ( (BattLowCounter > 6 ) ){//Baterija na 10% 
	BattLowCounter = 0;
	BattCharging   = 0;
#ifdef DEBUG_LiPo_CHG	
    Serial.println(F("Battery Low & NO Source => Batt CUTT OFF"));
#endif
    delay(100);
    stopLiPoCharging();
    delay(100);
	
// Raskaci bateriju sa sistema, GASI  KONTROLER !!!!!!! 
    digitalWrite(37, HIGH); 
	delay(100);//ovde ce se ugasiti uC posle par mS....
	digitalWrite(37, LOW);
	pinMode(37, INPUT); 
}

if ( solar < 14){//Nema Solara da puni bateriju 

#ifdef DEBUG_LiPo_CHG	
	  Serial.println(F("NO SOLAR Source"));
#endif

	if (BattCharging == 1) {
		stopLiPoCharging();
	}
	digitalWrite(25, LOW); // Battery Boost Disable	
	BattCharging = 0; 
	tricklChargeStatur = 0;  
}

if(  ( solar >= 16) && ( solar < 18.5)  )
{	
	digitalWrite(25, HIGH);	// Battery Boost Enable
#ifdef DEBUG_LiPo_CHG	
	  Serial.println(F("SOLAR Booster ON"));
#endif	
}

if ( solar > 18.5)
{	
	digitalWrite(25, LOW);	// Battery Boost Enable
#ifdef DEBUG_LiPo_CHG	
	  Serial.println(F("SOLAR Booster OFF"));
#endif	
}

#ifdef DEBUG_LiPo_CHG	
	if(BattCharging == 1){
		float battChgCurr = analogRead(A10);
		battChgCurr =  24.4 * battChgCurr ; // [mA]  1000 * (5 * battChgCurr /1024) / 0.2;
		Serial.print(F("CHARGING: "));Serial.print(battChgCurr / 3.0); Serial.print(F(" mA, "));
		Serial.print(F("Set Charging: "));Serial.print(getLiPoChargeCurrent()); Serial.print(F(" mA"));
		Serial.print(F(", to ")); Serial.print(getLiPoChargeVoltage());Serial.println(F(" V"));
		BatterySupplying = 0;
	}
	else{
		if (digitalRead(28)){
			Serial.println(F("SOLAR Suppling")); // pin 28- Battery_CH_ACOK
			BatterySupplying = 0;
		}
		else {
			Serial.println(F("BATTERY  Suppling"));
			BatterySupplying = 1;
			BattFullCounter  = 0;
		}
		
	} 
	if(tricklChargeStatur == 1){

		Serial.println(F("[ Trickl Charging ]"));
	}	
#endif


unsigned  tmp =	readLiPoChargeReg(); // keep alive charging proces, just ping the charger 
if (tmp & 0x01 == 1) {

#ifdef DEBUG_LiPo_CHG	

  if (BattCharging == 1)Serial.println(F("Charger STOPED Charging"));// Charger zaustavio punjenje.
#endif	
	BattCharging = 0; 

}
#ifdef DEBUG_LiPo_CHG	
	Serial.print(F("Register: "));Serial.println(tmp,HEX);
#endif

if ( (tmp !=0x81B2) && (BattCharging ==1) ){
#ifdef DEBUG_LiPo_CHG	
	Serial.print(F("Re-Start charging"));
#endif	
writeRegister(0x12,0x81B2);	
}

}
//================================================================================================	
void getLiPoDeviceID(void){
//Wire.begin();
Serial.print(F("Read Device ID: "));Serial.println(readRegister(0xFE),HEX);
Serial.print(F("Read Manufa ID: "));Serial.println(readRegister(0xFF),HEX);
//Wire.end();

}
unsigned int getLiPoChargeOption(void){
	return readRegister(0x12);
}

unsigned int readLiPoChargeReg(void){
unsigned int tmp = readRegister(0x12);	
return tmp;
}

unsigned int getLiPoInputCurrent(void){
unsigned int tmp = readRegister(0x3F);	
//Wire.begin();
//Serial.print(F("InputCurrent: "));Serial.print(F(" , "));Serial.print(tmp);Serial.println(F(" mA"));//Serial.print(tmp,HEX);
//Wire.end();
//tmp = tmp>>7;
//tmp = 128 * tmp; // 128mA
//Serial.print(F(" , "));Serial.print(tmp);Serial.println(F(" mA"));
return tmp;
}

unsigned int getLiPoChargeCurrent(void){
unsigned int tmp = readRegister(0x14);	
//Wire.begin();
//Serial.print(F("ChargeCurrent: "));Serial.print(F(" , "));Serial.print(tmp);Serial.println(F(" mA"));//Serial.print(tmp,HEX);
//Wire.end();
//tmp = tmp >> 6;
//tmp = 64 * tmp; // 64mA
//Serial.print(F(" , "));Serial.print(tmp);Serial.println(F(" mA"));
return tmp;
}

unsigned int getLiPoChargeVoltage(void){
unsigned int tmp = readRegister(0x15);	
//Wire.begin();
//Serial.print(F("ChargeVoltage: "));Serial.print(F(" , "));Serial.print(tmp);Serial.println(F(" mV"));//Serial.print(tmp,HEX);
//Wire.end();
//tmp = tmp>>4;
//tmp = 16 * tmp; // 16mV
//Serial.print(F(" , "));Serial.print(tmp);Serial.println(F(" mV"));
return tmp;
}

void setLiPoChargeVoltage( unsigned int Voltage){

Voltage = Voltage & 0x7FF0;
writeRegister(0x15,Voltage);	
}

void setLiPoChargeCurrent( unsigned int Current){

Current = Current & 0x1FC0;
writeRegister(0x14,Current);	
}

void setLiPoINcurrent( unsigned int Current){

Current = Current & 0x1F80;
writeRegister(0x3F,Current);	
}

void stopLiPoCharging(){
unsigned int tmp  = 	getLiPoChargeOption();
tmp = tmp | 0x01;

writeRegister(0x12,tmp);//DIsejbluj punjenje	
}

void startLiPoCharging(unsigned int Current){

//Bilo Orginalno  12560 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
setLiPoChargeVoltage(MAX_Batt_Voltage); // puni bateriju do 12.56V, 	12.6V je max, i sto je blize toj vrednosti skracuje vek baterije !!!
delay(10);
setLiPoChargeCurrent(Current); //max struja punjenja
delay(10);
setLiPoINcurrent(1024);	// max ulazna struja.	
delay(10);	
writeRegister(0x12,0x81B2);// start charging


}

void setWdogLiPoCharger(byte timeRange){ // 0 - disable WDOG
										 // 1 - 44s
										 // 2 - 88s		
										 // 3 - 175s
unsigned int tmp  = 	getLiPoChargeOption(); // Charge Options Register			 					
switch(timeRange){
	case 0: tmp = tmp & 0x9FFF; break; 
	case 1: tmp = tmp & 0xBFFF; tmp = tmp | 0x2000; break;
	case 2: tmp = tmp & 0xDFFF; tmp = tmp | 0x4000; break;
	case 3: tmp = tmp | 0x6000; break;		
}
#ifdef DEBUG_LiPo_CHG		
//	Serial.print(F("Write S_REG: ")); Serial.println(tmp,HEX);
#endif
writeRegister(0x12,tmp);

	
}


unsigned int readRegister(byte address) {
    Wire.beginTransmission(PMIC_ADDRESS);
    Wire.write(address);
    if (Wire.endTransmission(true) != 0) {
#ifdef DEBUG_LiPo_CHG		
		//Serial.println(F("error Read CHG"));
#endif		
      return -1;
    }
	
	byte low  ;
	byte high ;
    if (Wire.requestFrom(PMIC_ADDRESS, 2, true) == 2) {
		low  =  Wire.read();
//		Serial.print(F("LOW : "));Serial.println(low,HEX);
		high  = Wire.read();
//		Serial.print(F("HIG : "));Serial.println(high,HEX);		
    }
	
	unsigned int tmp = high;
	tmp = tmp <<8;
	tmp = tmp + low;	
//	Serial.print(F("LOW + HIG : "));Serial.println(tmp,HEX);	
    return tmp;
}

byte writeRegister(byte address, unsigned int val) {
    Wire.beginTransmission(PMIC_ADDRESS); // 0x09
    Wire.write(address);                  //0x15
	
	byte LOWer   = (byte)  (val & 0xFF) ;
	byte HIGHer  = (byte)  ((val >> 8) & 0xFF); 
//	Serial.print(F("Write Registar: ")); Serial.print(val); Serial.print(F(", H")); Serial.print(HIGHer); Serial.print(F(", L")); Serial.println(LOWer);
	
	Wire.write(LOWer);  // LOW Data
    Wire.write(HIGHer); // HIGH Data 

    if (Wire.endTransmission(true) != 0) {
#ifdef DEBUG_LiPo_CHG		
//		Serial.println(F("Error Write to CHG"));
#endif		
        return 0;
    }
//Wire.end(); // pitanje je da li ovo treba......

 //   Wire.beginTransmission(PMIC_ADDRESS); // 0x09
 //   Wire.write(address);                  //0x15
	
	
//	Serial.print(F("Write Registar: ")); Serial.print(val); Serial.print(F(", H")); Serial.print(HIGHer); Serial.print(F(", L")); Serial.println(LOWer);
	
	//Wire.write(LOWer);  // LOW Data
 //   Wire.write(HIGHer); // HIGH Data 

 //   if (Wire.endTransmission(true) != 0) {
//		Serial.print(F("Error III"));
 //       return 0;
 //   }




  return 1;
}



#endif