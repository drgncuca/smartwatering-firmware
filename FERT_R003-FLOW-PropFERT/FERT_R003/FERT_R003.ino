#include <EEPROM.h>
#include "EEPROMAnything.h"
#include <SoftwareSerial.h>
#include <Wire.h>
#include <TimeLib.h>
#include <ArduinoJson.h>
#include "SIM900.h"
#include "ds3231.h"
#include "inetGSM.h"
#include "sms.h"
#include <String.h>
#include "Telemetry.h"					
#include <SPI.h>
#include <Adafruit_MAX31865.h>

#ifdef Sensor_EC
  Adafruit_MAX31865 TemperaturePT1000 = Adafruit_MAX31865(49);
  EC_struct ECmeasuring;
#endif

#ifdef INTERRUPTFLOW
  #include <FlowMeter.h>
#endif  

InetGSM inet;
//TODO Check max size of buffer
StaticJsonBuffer<650> jsonBuffer;
int numdata;
boolean gsm_started = false;

bool isperm3 = false;

bool isautomode = false;

byte fertMode = 1;

char server[] = "app.smartwatering.rs";

//char parcel_id[] = "rjTXqd"; // Testing Single Device FERT+Zone, Kruska NS !!
  
//char parcel_id[] = "MGTnV5"; //FERT DID for TESTING  (DID) // Testing multi device // Tresnja Irig Developer test
//char parcel_id[] = "NmL73F"; //Zone DID for TESTING  (DID) // Testing multi device

//char parcel_id[] = "y6ljUx";  // Lesnik Novi Sad - Lazar
//char parcel_id[] = "A4dgm";  // Lesnik Zvornik  (PID)

//char parcel_id[] = "yuIFb";  //seskejabuke
//char parcel_id[] = "HxK8jk"; //Aleksandar Rancic, Borovnice, FERT 
//char parcel_id[] = "TRZyr";  //Darko Ristic, Borovnica Ub, PID
//char parcel_id[] = "HIsWGI"; //Ivica Todorovic Borovnica. DID
//char parcel_id[] = "IS7Zj"; //Delta Agrar, Jabuka, Tornjas. PID , DC-ventili
//char parcel_id[] = "qWJdQZ"; //Borovnica Trlić FERT 10 Zones
//char parcel_id[] = "LgomjY"; //Borovnica Trlić Meteo 1 : 
//char parcel_id[] = "tCHkuC"; //Borovnica Trlić Meteo 2 :  
//char parcel_id[] = "JXUnNw"; //Zemlja i Sunce - Parcela Ivan
//char parcel_id[] = "gHduMG";//Borovnica Aleksandar Uzice, DC Relays permanent ON/OFF [DEL-13]
//char parcel_id[] = "sfWSqk";//Kupina Andrija Jelinac 4 zone AC [DEL-5]
//char parcel_id[] = "LgomjY";//Trlic FERT [DEL-1] pH, EC, SENTEK, STH20.
//char parcel_id[] = "tCHkuC";//Trlic FERT [DEL-1] SENTEK, STH20.
//char parcel_id[] = "aQjkKo";// Tornjos II
//char parcel_id[] = "ffCIYQ";//Darko Smiljkovski - Lesnik
//char parcel_id[] = "eD1BdU";//Sava Coop testna stanica
//char parcel_id[] = "8taxAn";//Damir Bulj
//char parcel_id[] = "8YOyxi";//Vladan Redzic Ecoland JAbuka  (Vlaški do)
//char parcel_id[] = "7aCids";//Radoica Radomirovic - Borovnica (Leposavic, Kosovo)
//char parcel_id[] = "B7FmJb";//Marko Smederevo : Zone 1-4
//char parcel_id[] = "M9xswP";//Marko Smederevo : Zone 5-8 
//char parcel_id[] = "9YyhWN";//Milos Aleksic Lesnik 
//char parcel_id[] = "7m4wBq";//Aleksandar Rancic, Borovnica, Zones 
//char parcel_id[] = "uPt2he";//Moshe Lifhitz, Borovnica, SENS
//char parcel_id[] = "tIzUOI";//Ivica Donji Racnik
//char parcel_id[] = "OqLzTt";//Ivica Donji Racnik
//char parcel_id[] = "qTY5yN";//DEL-28
//char parcel_id[] = "s4IUok";//Strahinja Malin START Airplant Cortanovci 
//char parcel_id[] = "qWJdQZ";//Trlic [DEL-36]
//char parcel_id[] = "8Ch2xq";//Nikola MSederevo  [DEL-29]
//char parcel_id[] = "7aCids";//Radoica Radomirovic [DEL-47]
//char parcel_id[] = "erI09D";//AQM Mileta [DEL-45]  
//char parcel_id[] = "Rp8Gt";// Zeljko Balac Borovnica [DEL-33]
//char parcel_id[] = "HxK8jk";//Aleksandar Rancic
//char parcel_id[] = "8XP4YK";//AQM Gorevnica
//char parcel_id[] = "9qErR";//Darko Calic
//char parcel_id[] = "AwQ3dJ";//Armin Terzic
//char parcel_id[] = "4FTeqk";//Sasa Tirnanic Krnjevo
//char parcel_id[] = "Knv5hB";// Ceramilac-Royal Stroj Group- Lesnik Kosjeric
//char parcel_id[] = "Rp8Gt";// Zeljko Balac Fiksni FERT fertMode = 2
  char parcel_id[] = "EwMfBR";// Armin Fruit Prom, Plastenik BiH, proporcionalni FERT fertMode = 1

byte number_of_zones = 14;

struct irrigation_struct
{
  unsigned int duration = 0;
  unsigned long int amount = 0L; //Zeljena kolicina vode
  byte status[14] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; // 0 = off, 1 = on
  //unsigned long started_time[14] = {0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L};
  unsigned long int started_time = 0L;
  unsigned int max_duration = 0;
  String irrigation_id = "";
  //unsigned long int current[14] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; //Trenutno protekla kolicina vode // probati optimizovati
  unsigned long int current = 0L;  
} zonal_irrigation_struct;

//pH global variabla
pH_struct pHmeasuring;

fert_struct fertilizations[4];
float fertCurrentDriverCoeff;

irrigation_struct  zonal_irrigations[NUM_SLOTS];

irrigation_struct_FlowMetering  irrigationFlowMetering[5]; // Globalna promenljiva koja sadrzi informacije o protekloj kolicini VODE na 5 kanalala flowMeter-a i proteklo vreme.

//   FERT R002 // 1   2   3   4   5   6   7   8   9   10  11  12  13  14     15  16  17  18  19, 20
//byte valves[] = {24, 23, 22, 25, 26, 27, 28, 29, 34, 35, 37, 36, 33, 32};//, 31, 30, 41, 40, 39, 48};      


 // FERT R003  // 1   2   3   4   5   6   7   8   9   10  11  12  13  14     15  16  17  18  19, 20      
 byte valves[] = {24, 23, 22, 25, 26, 27, 28, 29, 37, 36, 35, 34, 33, 32};//, 31, 30, 41, 40, 39, 48};   

             //Valves  16  17  18  19
byte valves_fert[4] = {30, 41, 40, 39};

byte mainpump_pin = 31; // Main Pump Relay AC15.


#ifdef FLOW_Toggling
  byte flow_pin_Toggling = 15;// za FERT -> UART3_RX, na FERT1 PCB-u je oznacen sa: UART3_TX. 
#endif

//----------Temperature and Humidity-----
#ifdef SHT1sensor
  float temperature;
  float humidity;
  SHT1x SHT1x_sensor(   20  ,   21);
#endif

float ph_value = 0;

#ifdef MIXER
	// mixer for fertilization
	int mixer_pin = 48; // Relay - 20.
	int mixer_status = 0; // 0 = off, 1 = on
	String mixer_id = "";
	String mixer_irrigation_id = "";
	int mixer_duration = 0;
	unsigned long mixer_started_time = 0L;
#endif

byte sim_restart_pin = 7;
byte arduino_restart_pin = 12;

String params = "";
//TODO: Check max size of the message and reduce it's size accordingly
char msg[MSG_BUF]; 

carrier carrierdata;
struct_operater_vip operaterVIP;
struct_operater_MTS operaterMTS;
struct_operater_Telenor operaterTelenor;

// last time successfully contacted server
unsigned long last_seen = 0L;
#ifdef INTERRUPTFLOW
	// fmeter sensor
	double flowmeter_capacity = 30;
	double flowmeter_kfactor = 4.25;
	double flowmeter_mfactor = 1;

	FlowSensorProperties flowmeter_sensor = {flowmeter_capacity, flowmeter_kfactor, {flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor}};
	FlowMeter Meter = FlowMeter(2, flowmeter_sensor);
	long flowmeter_period = 1000; // one second (in milliseconds)
	long flowmeter_lasttime = 0;

void MeterISR()
{  
  Meter.count();// let our flow meter count the pulses
}
#endif

#if defined FLOW || defined INTERRUPTFLOW					   
struct flowmeter_struct
{
  float currentflow;
  float currentvolume;
};
#endif

bool carrierChange = false;

#ifdef TIME_F_Measure
unsigned long currentTimeMilles;
#endif
//============================================= S E T U P =========================================================
void setup()
{  
  digitalWrite(arduino_restart_pin, HIGH);
  delay(200);
  pinMode(arduino_restart_pin, OUTPUT);
  Serial.begin(115200); // Serial connection.
  
  RS485setup(); 
#ifdef SENTEC_Include  
  setSANTEKsensors(); 
#endif
  Serial.println(F("(RE)start uC"));
#ifdef Fertilisation
  initialiseFertilisationParameters();
#endif
  valve_stop_fertilization(1);
  valve_stop_fertilization(2);
  valve_stop_fertilization(3);
  valve_stop_fertilization(4);

  initialiseIrrFlowMet(); //REsetovanje Zapremine i globalnog proteklog vremena na 0 vrednosti.
  
  Wire.begin();
  
#ifdef Sensor_EC  
  setConductivityDriver(70, 1); // setovanje SINusnog generatora: 70Hz, 1-> sinus.  
  setupTemperaturePT1000();// Setovanje temperaturnog senzora za PT1000.
#endif
  
  struct ts t;

  DS3231_get(&t);

#ifdef INTERRUPTFLOW
  // enable a call to the 'interrupt service handler' (ISR) on every rising edge at the interrupt pin
  attachInterrupt(INT7, MeterISR, RISING);
  // sometimes initializing the gear generates some pulses that we should ignore
  Meter.reset();
#endif
						   
  pinMode(sim_restart_pin, OUTPUT);
  digitalWrite(sim_restart_pin, HIGH);
  digitalWrite(sim_restart_pin, LOW);
  delay(500);
  digitalWrite(sim_restart_pin, HIGH);
  delay(50);

#ifdef DEBUG
  Serial.print(F("GSM="));
#endif 
  
  if (gsm.begin(9600))
  {
    gsm_started = true;    
#ifdef DEBUG
    Serial.println(F("READY"));
#endif 

//Pri Debagovanju, preskoci jedan deo koda, da ubrza startovanje uC-a
#ifdef SKIP_Operater_Detecting
    goto skip_operater_detecting;
#endif

  }
  else
  {
    gsm_started = false;
#ifdef DEBUG
    Serial.println(F("IDLE"));
#endif
  
  }

doesOperaterChanging();// Promena operatera

EEPROM_readAnything(EERPOM_mem_loc_CarrierData, carrierdata);

#ifdef DEBUG_Operater 
    char tmp[2]="00"; 
    Serial.println("\n---------------------------");    
    Serial.println("Sadrzaj EEPROM-a : ");   
    EEPROM_readAnything(64, tmp);
    Serial.println(String(tmp).c_str());     
    Serial.println("Data in the Carrier Data Structure");+
    Serial.println(carrierdata.apnname);
    Serial.println(carrierdata.apnuser);
    Serial.println(carrierdata.apnpass);        
#endif; 

if (gsm_started)
  {
    Serial.print(F("GPRS="));    
//Pri Debagovanju, preskoci jedan deo koda, da ubrza startovanje uC-a    
#ifdef SKIP_Operater_Detecting  
    skip_operater_detecting:
    Serial.print(F("GPRS="));  
  //if (inet.attachGPRS("3Gnet", "mts", "064"))
  if (inet.attachGPRS("internet", "telenor", "gprs"))
    {
#else
    if (inet.attachGPRS(String(carrierdata.apnname).c_str(), String(carrierdata.apnuser).c_str(), String(carrierdata.apnpass).c_str()))
    {
#endif    
    
#ifdef DEBUG_Extend
     Serial.println(F("ATTACHED"));     
     gsm.SimpleWriteln("AT+CIFSR");//Read IP address.
     delay(5000);     
     gsm.WhileSimpleRead();//Read until serial buffer is empty
#endif
    }
    else
    {
#ifdef DEBUG
      Serial.println(F("ERROR"));
#endif
    }
    delay(1000); 
  }
else
  {
#ifdef DEBUG    
   Serial.println(F("GSM&GPRS=NOTstarted"));
#endif  
  }  
  // Initialize  PINs of Valeves.
#ifdef AC_Valves  
  for (int x=0; x < number_of_zones;  x++)
  {
    pinMode(valves[x], OUTPUT);
    digitalWrite(valves[x], relay_stop);
  }
#endif  



#ifdef FLOW_Toggling
   pinMode(flow_pin_Toggling, INPUT);
//   DDRE  = DDRE & 0xEF;  // Za SENS - FLOW input 
#endif

  pinMode(mainpump_pin, OUTPUT);
  digitalWrite(mainpump_pin, relay_stop);

#ifdef MIXER
  pinMode(mixer_pin, OUTPUT);
  digitalWrite(mixer_pin, relay_stop);
#endif

  pinMode(sim_restart_pin, OUTPUT);
  digitalWrite(sim_restart_pin, HIGH);   

};
//============================================ L O O P ====================================================
void loop()
{
#ifdef DEBUG
  Serial.println(F("Usao u loop"));
#endif
/*
char x = 'x';
x = Serial.read();
if (x == '0') fertMode = 0;
if (x == '1') fertMode = 1;
if (x == '2') fertMode = 2;
x = 'x';
Serial.print(F("fertmode:"));Serial.println(fertMode);
*/
#if defined FLOW || defined INTERRUPTFLOW
  struct flowmeter_struct flow = checkFlowMeter(); //azurira vrednost trenutnog protoka[L/min] i zapremine u [L] u toku jednog LOOP-a
  checkValves(flow.currentvolume);
#else
  checkValves(0);
#endif
 
#ifdef INTERRUPTFLOW  
   Meter.reset();				 
#endif				   
#ifdef FLOW_Toggling 
   bool currentFlowStatus = checkFlow() ? 1 : 0;  // ako ima bilo kakvog protoka vraca '1'
#endif
 
  if(isautomode) {
    checkLastSeen();
  }

#ifdef Sensor_pH  
  ph_value = getPh();
#endif

#ifdef SHT1sensor
   Wire.end();  // u konfliktu dva I2C-a RTC i Temp-Humid senzor
   temperature = SHT1x_sensor.readTemperatureC();  
   humidity = SHT1x_sensor.readHumidity();
   Wire.begin();    
#endif

#ifdef Sensor_EC
//  el_conductance = getELconductance(); 
  ECmeasuring.measured = getELconductance();
#endif  

  params = "";  
  params.concat("did=" + String(parcel_id));
//params.concat("pid=" + String(parcel_id));
 
//----------------------- SENTEK sensors 1, 2, 3 -------------------------
#ifdef SENTEC_Include   

        reqSENTEKpresetHoldReg_Temperature();
        delay(200);  
        params.concat("&t1=" + String(getSENTEKtemperature(1))); //temperature
        delay(200);  
        params.concat("&t2=" + String(getSENTEKtemperature(2)));
        delay(200);  
        params.concat("&t3=" + String(getSENTEKtemperature(3)));  
        delay(200);        
        reqSENTEKpresetHoldReg_Moisture();
        delay(800);
        params.concat("&m1=" + String(getSENTEKmoisture(1))); //moisture
        delay(200);        
        params.concat("&m2=" + String(getSENTEKmoisture(2)));
        delay(200);
        params.concat("&m3=" + String(getSENTEKmoisture(3)));
        
   #ifdef SENTEK_Salinity      
        delay(50);
        reqSENTEKpresetHoldReg_Salinity();
        delay(50);
        params.concat("&s1=" + String(getSENTEKsalinity(1)));//salinity
        delay(50);
        params.concat("&s2=" + String(getSENTEKsalinity(2)));
        delay(50);
        params.concat("&s3=" + String(getSENTEKsalinity(3)));
        
   #endif   
#endif

//----------------------- SENTEK sensors 4, 5, 6 -------------------------
#ifdef SENTEK_Include_Extend_Sens

        reqSENTEKpresetHoldReg_Temperature();
        delay(200);  
        params.concat("&t4=" + String(getSENTEKtemperature(4))); //temperature
        delay(200);  
        params.concat("&t5=" + String(getSENTEKtemperature(5)));
        delay(200);  
        params.concat("&t6=" + String(getSENTEKtemperature(6)));  
        delay(200);
        reqSENTEKpresetHoldReg_Moisture();
        delay(800);
        params.concat("&m4=" + String(getSENTEKmoisture(4))); //moisture
        delay(200);
        params.concat("&m5=" + String(getSENTEKmoisture(5)));
        delay(200);
        params.concat("&m6=" + String(getSENTEKmoisture(6)));
    #ifdef SENTEK_Salinity        
        delay(50);
        reqSENTEKpresetHoldReg_Salinity();
        delay(50);
        params.concat("&s4=" + String(getSENTEKsalinity(4)));//salinity
        delay(50);
        params.concat("&s5=" + String(getSENTEKsalinity(5)));
        delay(50);
        params.concat("&s6=" + String(getSENTEKsalinity(6)));
    #endif
#endif

#ifdef Sensor_10HS
  params.concat("&h1=" + String(analogRead(A8)/2)); // podeljeno sa 2, jer je analogni ulaz pojacan sa 2x. A8 - Chanl 4 (na PCB je labela AN_IN3)
#endif  
//params.concat("&h2=" + String(0));

#ifdef SHT1sensor
  params.concat("&ah=" + String(humidity));
  params.concat("&at=" + String(temperature));
#endif  
//  params.concat("&temp=" + String(0));

#ifdef AC_Power_FERT
int power = getPower();
  params.concat("&pow=" + String(power));                           // za FERT plocu
#endif

#ifdef MIXER
  params.concat("&mid=" + String(mixer_id));
#endif
  
#ifdef Fertilisation  
  #ifdef FERT_0
     params.concat("&fid="  + String(fertilizations[0].fertilization_id));
  #endif
  #ifdef FERT_2
     params.concat("&fid2=" + String(fertilizations[1].fertilization_id));
  #endif
  #ifdef FERT_3
     params.concat("&fid3=" + String(fertilizations[2].fertilization_id));
  #endif
  #ifdef FERT_4 
     params.concat("&fid4=" + String(fertilizations[3].fertilization_id));
  #endif
#endif

#ifdef Sensor_pH
  params.concat("&ferph=" + String(pHmeasuring.rawPh));//salje se RAW pH.
#endif

#if defined FLOW || defined INTERRUPTFLOW 
  params.concat("&cfl=" + String(flow.currentflow));    // trenutni protok
  params.concat("&tfv=" + String(flow.currentvolume)); // delat volume 
#endif  

#ifdef FLOW_Toggling  
  params.concat("&cfl=" + String(currentFlowStatus));
#endif    

#ifdef Sensor_EC
  params.concat("&ecv=" + String(ECmeasuring.measured));
#endif
  
  String activeirrigations;
  for (int i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id != "")
    {
      activeirrigations.concat("," + String(zonal_irrigations[i].irrigation_id));
    }
  }
  activeirrigations.remove(0, 1);
  params.concat("&iids=" + String(activeirrigations));

#ifdef DEBUG  
  Serial.println(params.c_str());
#endif

  memset(msg, 0, sizeof(msg)); 
 // apiCall(server, 80, "/api/post/sync.php", params.c_str(), msg, MSG_BUF);
    apiCall(server, 80, "/api/post/sync_device.php", params.c_str(), msg, MSG_BUF);


  char *jsonResponse = strstr(msg, "{"); 
#ifdef DEBUG			  
  Serial.println(F("json:")); Serial.println(jsonResponse);
#endif
//--------------------------------------------------------------------------------
  JsonObject &root = jsonBuffer.parseObject(jsonResponse);
//--------------------------------------------------------------------------------
 
  JsonVariant numberofzones_json = root["zones"];
  if (numberofzones_json.success())
  {
    if (number_of_zones != root["zones"])
    {
      number_of_zones = root["zones"];
    }
  }

//---------------------------------------------------------------------
  JsonObject &flowmeter_config = root["flowconf"];
  if (flowmeter_config.success())
  {
#ifdef FLOW
    irrigationFlowMetering[FlowSensorCH5].flowMeterCalibKons = flowmeter_config["kf"];
#endif

#ifdef INTERRUPTFLOW
bool flow_config_changed = false;
    if (flowmeter_config["cap"] != flowmeter_capacity)
    {
      flowmeter_capacity = flowmeter_config["cap"];
      flow_config_changed = true;
    }
    if (flowmeter_config["kf"] != flowmeter_kfactor)
    {
      flowmeter_kfactor = flowmeter_config["kf"];
      flow_config_changed = true;
    }
    if (flowmeter_config["mf"] != flowmeter_mfactor)
    {
      flowmeter_mfactor = flowmeter_config["mf"];
      flow_config_changed = true;
    }

    if (flow_config_changed)
    {
      FlowSensorProperties flowmeter_sensor = {flowmeter_capacity, flowmeter_kfactor, {flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor}};
      Meter = FlowMeter(2, flowmeter_sensor);
    }
#endif								 
  }

#ifdef MIXER  
  JsonObject &start_mixer = root["mix"];
  if (start_mixer.success())
  {
    if (strcmp(start_mixer["mid"].as<String>().c_str(), mixer_id.c_str()) != 0)
    {
      mixer_start(start_mixer["mid"].as<String>(), start_mixer["iid"].as<String>(), start_mixer["duration"]);
    }
  }
#endif

  JsonObject &start_irrigation = root["irr"];
  if (start_irrigation.success())
  {  
    if (strcmp(start_irrigation["irrigationid"].as<String>().c_str(), root["irr"]["iid"].as<String>().c_str()) != 0)
    {
      isautomode = start_irrigation["automode"];
      if (start_irrigation["zonal"])
      {
        if (start_irrigation["duration"])
        {
          //per time
          isperm3 = false;
          zonal_irrigation_start(start_irrigation["iid"].as<String>(), start_irrigation["duration"], start_irrigation["zones"]);
        }
        else
        {
          //per m3
          isperm3 = true;
          zonal_irrigation_start(start_irrigation["iid"].as<String>(), start_irrigation["amount"], start_irrigation["zones"], start_irrigation["maxduration"]);
        }
      }
    }
  }

//-----------------------koeficijent za kalibraciju pH senzora-- Y = k*X + M -----------------------//
   JsonVariant ferphK_json = root["ferphk"];
    if (ferphK_json.success())
    {
      if (pHmeasuring.ferphK != root["ferphk"])
      {
          pHmeasuring.ferphK = root["ferphk"];
      }
    }

   JsonVariant ferphM_json = root["ferphm"];
    if (ferphK_json.success())
    {
      if (pHmeasuring.ferphM != root["ferphm"])
      {
          pHmeasuring.ferphM  = root["ferphm"];
      }
    }
//-----------------Koeficijenti P I D regulatora za pH ----------------------------------
 JsonVariant ferPID_Kp_json = root["ferkp"];
    if (ferPID_Kp_json.success())
    {
      if (pHmeasuring.ferKp != root["ferkp"])
      {
          pHmeasuring.ferKp = root["ferkp"];
      }
    }
 JsonVariant ferPID_Ki_json = root["ferki"];
    if (ferPID_Ki_json.success())
    {
      if (pHmeasuring.ferKi != root["ferki"])
      {
          pHmeasuring.ferKi = root["ferki"];
      }
    }
 JsonVariant ferPID_Kd_json = root["ferkd"];
    if (ferPID_Kd_json.success())
    {
      if (pHmeasuring.ferKd != root["ferkd"])
      {
          pHmeasuring.ferKd = root["ferkd"];
      }
    }

//---------EC calbracioni koeficijent EC = k*X + m --------------------------
#ifdef Sensor_EC
 JsonVariant EC_CalibrCoeff_json = root["eck"];
    if (EC_CalibrCoeff_json.success())
    {
      if ( ECmeasuring.ecK != root["eck"])
      {
           ECmeasuring.ecK = root["eck"];
      }
    }
    
 JsonVariant EC_CalibrCoeff_M_json = root["ecm"];
    if (EC_CalibrCoeff_M_json.success())
    {
      if ( ECmeasuring.ecM != root["ecm"])
      {
           ECmeasuring.ecM = root["ecm"];
      }
    }
    
#ifdef Sensor_EC_PID  

//-------------  EC desire  ---------------------------------
JsonObject &EC2_PID_desireEC = root["fer2"];
    if (EC2_PID_desireEC.success())
    {

      if ( ECmeasuring.desired != EC2_PID_desireEC["dec"])
      {
           ECmeasuring.desired = EC2_PID_desireEC["dec"];
      }
    }
//-----------------Koeficijenti P I D regulatora za EC ----------------------
JsonVariant EC_PID_Kp_json = root["ecp"];
    if (EC_PID_Kp_json.success())
    {
      if (ECmeasuring.ecKp != root["ecp"])
      {
          ECmeasuring.ecKp = root["ecp"];
      }
    }
JsonVariant EC_PID_Ki_json = root["eci"];
    if (EC_PID_Ki_json.success())
    {
      if (ECmeasuring.ecKi != root["eci"])
      {
          ECmeasuring.ecKi = root["eci"];
      }
    }
JsonVariant EC_PID_Kd_json = root["ecd"];
    if (EC_PID_Kd_json.success())
    {
      if (ECmeasuring.ecKd != root["ecd"])
      {
          ECmeasuring.ecKd = root["ecd"];
      }
    }
	
//---------------- Koeficijenti strujnog drajvera za EC PID upravljanje 
  JsonVariant EC_PID_CH2 = root["ech2"];
    if (EC_PID_CH2.success())
    {
      if (ECmeasuring.CH2_Curr_Strenght != root["ech2"])
      {
          ECmeasuring.CH2_Curr_Strenght = root["ech2"];
      }
    }

  JsonVariant EC_PID_CH3 = root["ech3"];
    if (EC_PID_CH3.success())
    {
      if (ECmeasuring.CH3_Curr_Strenght != root["ech3"])
      {
          ECmeasuring.CH3_Curr_Strenght = root["ech3"];
      }
    }  

#endif  //Sensor_EC_PID 
    
#endif  //Sensor_EC

    
//---------------Fertilisation Flow Calibration Koeficient------------------
#ifdef Fertilisation
//    JsonVariant ferFlowKoef_json = root["ferkf"];
//    if (ferFlowKoef_json.success())
//    {
//      if (fertFlowMeterCallCoeff != root["ferkf"])
//      {
//          fertFlowMeterCallCoeff = root["ferkf"];
//      }
//    }

//--------------Fertilisation Current Driver Calibration Koeficient----------------------------
    JsonVariant ferCurrentKoef_json = root["ferdrv"];
    if (ferCurrentKoef_json.success())
    {
      if (fertCurrentDriverCoeff != root["ferdrv"])
      {
          fertCurrentDriverCoeff = root["ferdrv"];
      }
    }

#endif

//-------------- Fertilisation  M O D E  -------------------------------------
    JsonVariant isfertperm3_json = root["fertmode"];
    if (isfertperm3_json.success())
    {
      if (fertMode != root["fertmode"])
      {
          fertMode = root["fertmode"];
      }
    }
	if (fertMode > 2) {fertMode = 0;} // zastita
	
//---------------------------------------------------------------------------
#ifdef Fertilisation

JsonObject &start_fertilization = root["fer"];

#ifdef FERT_0  
  if (start_fertilization.success())
{	
    fertilization_start(start_fertilization["fid"].as<String>() , start_fertilization["iid"].as<String>() , start_fertilization["duration"], 1, start_fertilization["dph"],start_fertilization["dpw"]);
}
#endif

#ifdef FERT_2
  JsonObject &start_fertilization2 = root["fer2"];
  if (start_fertilization2.success())
  {		  
    fertilization_start(start_fertilization2["fid2"].as<String>(), start_fertilization2["iid"].as<String>(), start_fertilization2["duration"], 2, start_fertilization2["dph"],start_fertilization2["dpw"]);
  }
#endif

#ifdef FERT_3
  JsonObject &start_fertilization3 = root["fer3"];
  if (start_fertilization3.success())
  {		
    fertilization_start(start_fertilization3["fid3"].as<String>(), start_fertilization3["iid"].as<String>(), start_fertilization3["duration"], 3, start_fertilization3["dph"],start_fertilization3["dpw"]);
  }

#endif

#ifdef FERT_4 
  JsonObject &start_fertilization4 = root["fer4"];
  if (start_fertilization4.success())
  {	
    fertilization_start(start_fertilization4["fid4"].as<String>(), start_fertilization4["iid"].as<String>(), start_fertilization4["duration"] ,4, start_fertilization4["dph"] ,start_fertilization4["dpw"]);
  }
#endif
  
#endif  
//-----------------------------------------------------------------------------------------------------//

JsonVariant stop_irrigation_id_json = root["stopirr"];
  if (stop_irrigation_id_json.success())
  {    
    char irrid[5];
    strcpy(irrid, root["stopirr"].as<const char *>());

#ifdef MIXER    
    if (mixer_id != "")  stop_mixer(irrid);
#endif
      
#ifdef Fertilisation  
    if (fertilizations[0].fertilization_id != "") stop_fertilization(irrid, 1);
      
    if (fertilizations[1].fertilization_id != "") stop_fertilization(irrid, 2);
      
    if (fertilizations[2].fertilization_id != "") stop_fertilization(irrid, 3);
      
    if (fertilizations[3].fertilization_id != "") stop_fertilization(irrid, 4);
#endif      
    delay(4000); // saceka da se strujni drajver smanji na min pa tek onda da gasi reley-pumpu  
    stop_irrigation(irrid);
    
  }
		
  
jsonBuffer.clear();

}
//================================ END  OF   L O O P ( )  ===========================================


//per duration
void zonal_irrigation_start(String irrigationid, int duration, JsonArray &zones)
{
//  fertMode = 0;

  for (byte i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id == "")
    {
	  zonal_irrigations[i].started_time = currentTime(); /// - ubaceno eksperimenta radi !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      zonal_irrigations[i].irrigation_id = irrigationid;
      for (int zone : zones)
      {
        delay(200);
        zonal_irrigations[i].duration = duration * 60L;
#ifdef AC_Valves        
        digitalWrite(valves[zone - 1], relay_start);
#endif
        
		delay(200);
        digitalWrite(mainpump_pin, relay_start);
        //zonal_irrigations[i].started_time[zone - 1] = currentTime();
        zonal_irrigations[i].status[zone - 1] = 1;
#ifdef DEBUG
        String logger = "Palim zonu " + String(zone);
        Serial.println(logger);
#endif
      }
#ifdef DEBUG
      String logger = "Started zonal irrigation ID " + String(zonal_irrigations[i].irrigation_id);
      Serial.println(logger);
#endif
      params = "";
      memset(msg, 0, sizeof(msg));
      params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));
      apiCall(server, 80, "/api/post/irrigation_started.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif
      return;
    }
  } //for (int i = 0; i < NUM_SLOTS; i++)
}

//per m3
void zonal_irrigation_start(String irrigationid, long amount, JsonArray &zones, int max_duration)
{
//  fertMode = 1;
  for (byte i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id == "")
    {
	  zonal_irrigations[i].started_time = currentTime(); /// - ubaceno eksperimenta radi !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!	
	  zonal_irrigations[i].current = 0;                  /// - ubaceno eksperimenta radi !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	  
      zonal_irrigations[i].irrigation_id = irrigationid;
	  zonal_irrigations[i].amount = amount; // vrednost u litrima.
	  zonal_irrigations[i].max_duration = max_duration * 60L; // vreme u sekundama
   
      for (int zone : zones) //ukljucuje zadate zone
      {
        delay(150); 

#ifdef AC_Valves        
        digitalWrite(valves[zone - 1], relay_start);
#endif   
		//delay(200);     
        //digitalWrite(mainpump_pin, relay_start);
        //zonal_irrigations[i].current[zone - 1] = 0;	/// - probati opt. eksperimenta radi !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		
        zonal_irrigations[i].status[zone - 1] = 1;
        //zonal_irrigations[i].started_time[zone - 1] = currentTime(); /// - probati opt. eksperimenta radi !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#ifdef DEBUG_Extend
        Serial.print(F("Starting zone ")); Serial.println(zone);
#endif
      }
	  
	  delay(150);     
      digitalWrite(mainpump_pin, relay_start); // ukljucuje pumpu
	  
#ifdef DEBUG_Extend
      Serial.print(F("Started zonal [per m3] irr ID ")); Serial.println(zonal_irrigations[i].irrigation_id);
#endif
      params = "";
      memset(msg, 0, sizeof(msg));
      params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));
      apiCall(server, 80, "/api/post/irrigation_started.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG_Extend
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif
      return; // prvi slobodan slot, popuni s parametrima i izlazi iz funkcije
    }
  }
}

#ifdef MIXER
void mixer_start(String mixerid, String irrigationid, int duration)
{
  mixer_id = mixerid;
  mixer_irrigation_id = irrigationid;
  mixer_duration = duration * 60; // u sekundama
  mixer_status = 1;
  digitalWrite(mixer_pin, relay_start);
  mixer_started_time = currentTime();
#ifdef DEBUG
  String logger = "Started mixer ID " + String(mixer_id);
  Serial.println(logger);
#endif
  params = "";
  params.concat("mid=" + String(mixer_id));
  memset(msg, 0, sizeof(msg));
  apiCall(server, 80, "/api/post/mixer_started.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
  char *jsonResponse = strstr(msg, "{");
  Serial.println(jsonResponse);
#endif
}
#endif

//=================================================   F E R T I L I S A T I O N    =========================================================================
#ifdef Fertilisation

void fertilization_start(String fertilizationid, String irrigationid, byte duration, byte fertnum , float fert_pHtarget, byte pumpPercentFert )
{
	fertilizations[fertnum-1].fertilization_id            =  fertilizationid;	
	fertilizations[fertnum-1].fertilization_irrigation_id =  irrigationid;
	fertilizations[fertnum-1].fertilization_status        =  1;
	fertilizations[fertnum-1].fertilization_started_time  =  currentTime();		

if ( fertMode == 0 ){

    fertilizations[fertnum-1].fertilization_duration = duration * 60L;    
    valve_start_fertilization(fertnum); //ukljucuje relej-pumpu za fertilizaciju.
    
    if(fertnum == 1){
        pHmeasuring.desiredPh = fert_pHtarget;
        fertilizationCheck( CurrDrvCH1, fert_pHtarget);// pocni sa merenjem pH i doziranjem djubriva
    }        
}
// Proporcionalni FERT - jos je u fazi razvoja..........................
if ( fertMode == 1 ){

    fertilizations[fertnum-1].fertilization_duration = duration; // kolicina prihrane u L
	
for (int y = 0;y<14; y++){

	if( irrigationid == zonal_irrigations[y].irrigation_id ) {
		if(duration !=0) {         
			fertilizations[fertnum-1].ratioWaterFert = (zonal_irrigations[y].amount) / duration ; // Odnos vode i prihrane. 1000L = 1m3 
			Serial.print("Y "); Serial.print(y); Serial.print(", zonal_irr_Amount "); Serial.print(zonal_irrigations[y].amount); Serial.print(", fert per m3 : ");  Serial.print("fertnum "); Serial.print(fertnum);Serial.print(", duartion ");Serial.print(duration); Serial.print(", ratioWaterFert "); Serial.println(fertilizations[fertnum-1].ratioWaterFert);
		}
	break;	
	}		
}
    valve_start_fertilization(fertnum); //ukljucuje  relej-pumpu za fertilizaciju.   
}
// Fiksni FERT - u fazi testiranja ..........................
if ( fertMode == 2 ){

    fertilizations[fertnum-1].fertilization_duration =  duration * 60L;  // trajanje u sekundama

	float tmp =  (float)pumpPercentFert * 0.7  +  16.30 ; // Konvertovanje iz procenata 0 - 100%  U  faktor ispune od 17% - 85% koje odgovara strujnom drajveru 4..20mA
	if (pumpPercentFert == 0) tmp = 0 ; // ako korisnik zada 0% znaci da hoce ugasenu pumpu.
	
	if (tmp > maxDutyCycle) {tmp = maxDutyCycle;}
	if (tmp < minDutyCycle) {tmp = 0;}
	
	fertilizations[fertnum-1].pumpPrcntCurrFert  = tmp; 
	
    valve_start_fertilization(fertnum); //ukljucuje  relej-pumpu za fertilizaciju.
	
	byte temCurrentDriver;
	switch(fertnum){
		case 1: temCurrentDriver = CurrDrvCH1; break;
		case 2: temCurrentDriver = CurrDrvCH2; break;
		case 3: temCurrentDriver = CurrDrvCH3; break;
		case 4: temCurrentDriver = CurrDrvCH4; break;
	//  case 5: temCurrentDriver = CurrDrvCH5; break;
	};
	setCurrentDriverPumpFert(fertilizations[fertnum-1].pumpPrcntCurrFert , temCurrentDriver ); // Podesava pumpu, slanjem vrednosti struje, podesava kojim intezitetom ce da radi pumpa, tj doziranje prihrane.
	
}

#ifdef DEBUG_Extend  
      Serial.print(F("Start FERT ")); Serial.print(fertnum); Serial.print(F(", ID:")); Serial.print(fertilizationid); Serial.print(F(", FertMode "));Serial.println(fertMode);
#endif
      params = "";
      params.concat("fid=" + String(fertilizationid));
      memset(msg, 0, sizeof(msg));
      apiCall(server, 80, "/api/post/fertilization_started.php", params.c_str(), msg, MSG_BUF);
	  
#ifdef DEBUG_Extend
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif
}
#endif

void stop_irrigation(String irrigationid)
{
  // stop zonal irrigation
  for (byte i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id == irrigationid)
    {
	   
      // turn of all active zones
      for (byte checking_zone = 0; checking_zone < number_of_zones; checking_zone++)
      {
        if (zonal_irrigations[i].status[checking_zone ] == 1)
        {
		delay(150);
#ifdef AC_Valves          
     digitalWrite(valves[checking_zone ], relay_stop);
#endif

#ifdef DEBUG_Extend
     Serial.print(F("Gasim zonu[force stop] "));Serial.println(checking_zone+1);
#endif          
          zonal_irrigations[i].status[checking_zone ] = 0;
        }
        //zonal_irrigations[i].started_time[checking_zone ] = 0L; // ubaceno radi eksperimenta
      }
      zonal_irrigations[i].irrigation_id = "";
      zonal_irrigations[i].duration = 0;
	  
	  zonal_irrigations[i].started_time = 0L; // ubaceno radi eksperimenta !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	  zonal_irrigations[i].current = 0;       // ubaceno radi eksperimenta !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	  zonal_irrigations[i].amount  = 0;       // ubaceno radi eksperimenta !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	  
      // check to turn off pump if there is no active irrrigation
      bool is_some_irrigation_on = false;
      for (byte checking_slot = 0; checking_slot < NUM_SLOTS; checking_slot++)
      {
        if (zonal_irrigations[checking_slot].irrigation_id != "")
        {
          for (byte checking_zone = 0; checking_zone < number_of_zones; checking_zone++)
          {
            if (zonal_irrigations[checking_slot].status[checking_zone ] == 1)
            {
              is_some_irrigation_on = true;
              break;
            }
          }
        }
      }
	  // AKO nepostoji ni jedna aktivna zona, gasi pumpu
      if (!is_some_irrigation_on)
      {
        digitalWrite(mainpump_pin, relay_stop);      
      }
#ifdef DEBUG_Extend
      Serial.print(F("Stopped irr ID ")); Serial.println(irrigationid);
#endif
      params = "";
      params.concat("iid=" + String(irrigationid));
      memset(msg, 0, sizeof(msg));
      apiCall(server, 80, "/api/post/irrigation_stopped.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG_Extend
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif
    }
  }
}

#ifdef MIXER
void valve_mixer_stop() {
   digitalWrite(mixer_pin, relay_stop);
    mixer_status = 0;
    mixer_duration = 0;
    mixer_started_time = 0L;
    String stopped_mixer_id = String(mixer_id);
    mixer_id = "";
    mixer_irrigation_id = "";

#ifdef DEBUG_Extend
    Serial.print(F("Stopped mixer ID ")); Serial.println(stopped_mixer_id);
#endif
}

void stop_mixer(String irrigationid)
{
  if (mixer_irrigation_id == irrigationid)
  {
    valve_mixer_stop();
    params = "";
    params.concat("mid=" + String(mixer_id));
    memset(msg, 0, sizeof(msg));
    apiCall(server, 80, "/api/post/mixer_stopped.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG_Extend
    char *jsonResponse = strstr(msg, "{");
    Serial.println(msg);
#endif
  }
}
#endif

#ifdef Fertilisation
void stop_fertilization(String irrigationid, byte fertnum)
{
   if (fertilizations[fertnum-1].fertilization_irrigation_id == irrigationid)
    {
      String tmp = fertilizations[fertnum-1].fertilization_id ; // funkcija ispod resetuje ovaja parametar
      valve_stop_fertilization(fertnum); // Resetuje parametre ferta-a i gasi releje-pumpu.
  
#ifdef DEBUG_Extend
      Serial.print(F("Stopped FERT ")); Serial.println(fertnum); Serial.print(F("  ID:")); Serial.println(tmp);
#endif
      params = "";
      params.concat("fid=" + tmp);
      memset(msg, 0, sizeof(msg));
      apiCall(server, 80, "/api/post/fertilization_stopped.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
      char *jsonResponse = strstr(msg, "{");
      Serial.println(msg);
#endif    
    }
}
#endif
//=========================================================================================================== C H E C H I N G    V A L V E S  ==============================================================
void checkValves(float deltaVolumePerOneLoop) //deltaVolumePerOneLoop - sa glavnog protokomera 
{
#ifdef MIXER
  // mixer check
  if ( (mixer_status == 1) && ( (currentTime() - mixer_started_time) > mixer_duration ))
  {
    digitalWrite(mixer_pin, relay_stop);
    mixer_status = 0;
    mixer_duration = 0;
    mixer_started_time = 0L;
    String completed_mixer_id = String(mixer_id);
    mixer_id = "";
    mixer_irrigation_id = "";

#ifdef DEBUG_Extend
    String logger = "Completed mixer ID " + String(completed_mixer_id);
    Serial.println(logger);
#endif
    params = "";
    params.concat("mid=" + String(completed_mixer_id));
    memset(msg, 0, sizeof(msg));
    apiCall(server, 80, "/api/post/mixer_finished.php", params.c_str(), msg, MSG_BUF);
    
#ifdef DEBUG_Extend
    char *jsonResponse = strstr(msg, "{");
    Serial.println(msg);
#endif
  }
#endif


#ifdef Fertilisation

if ( fertMode == 0){  
	
#ifdef FERT_0
  // fertilization 1 check
  if ( (fertilizations[0].fertilization_status == 1) && ( (currentTime() - fertilizations[0].fertilization_started_time) > fertilizations[0].fertilization_duration))
  {    
#ifdef DEBUG_Extend
    Serial.println(F("Gasim FERT 1")); 
#endif    
valve_stop_fertilization(1); //fertnum = 1;
  }
  else if (fertilizations[0].fertilization_status == 1) {
   fertilizationCheck( CurrDrvCH1 , pHmeasuring.desiredPh);
  }  
#endif

#ifdef FERT_2 
  if ( (fertilizations[1].fertilization_status == 1) && ( (currentTime() - fertilizations[1].fertilization_started_time) > fertilizations[1].fertilization_duration))
  { 
#ifdef DEBUG_Extend
    Serial.println(F("Gasim FERT 2")); 
#endif 
   valve_stop_fertilization(2); //fertnum = 1; setuje sve vrednosti na 0, gasi relej koji napaja pumpu 
  }
#ifdef Sensor_EC_PID   
    else if (fertilizations[1].fertilization_status == 1){
        EC_PID_Control_Checking(CurrDrvCH2, ECmeasuring.desired  );
  }
#endif 
#endif

#ifdef FERT_3
  if ( (fertilizations[2].fertilization_status == 1) && ( (currentTime() - fertilizations[2].fertilization_started_time) > fertilizations[2].fertilization_duration))
  {
#ifdef DEBUG_Extend
    Serial.println(F("Gasim FERT 3")); 
#endif
    valve_stop_fertilization(3); 
  }
#ifdef Sensor_EC_PID   
  else if (fertilizations[2].fertilization_status == 1){
        EC_PID_Control_Checking(CurrDrvCH3, ECmeasuring.desired  );
  }
#endif
#endif

//============================================= CUSTOM resenje za jednog klijenta == Kontrolise fert na konto protekle kolicine na kanalu 2 (FlowSensorCH2)
#ifdef FERT_4

  // fertilization 4 check               "fertilization_duration" -  je zeljena protekla kolicina djubriva
  if ( (fertilizations[3].fertilization_status == 1)  && (fertilizations[3].fertilization_duration < irrigationFlowMetering[FlowSensorCH2].irrigationFlowVolume)   )
  {
#ifdef DEBUG_Extend
    Serial.println(F("Gasim FERT 4")); 
    Serial.print("Proteklo Djubriva ");  Serial.print(irrigationFlowMetering[FlowSensorCH2].irrigationFlowVolume); Serial.print("L");   Serial.print(", od trazenih "); Serial.println(fertilizations[3].fertilization_duration);
#endif
    valve_stop_fertilization(4);    
    initialiseIrrFlowMet();

  }
#endif  
//================================================================================================================

}

// proprocionalna fertilizacija. Podesava sistem tako da protice odgovarajuce kolicina prihrane po jednom metru kubnom protekle kolicine vode.
if ( fertMode == 1){ // SETOVATI na fertMode == 1 - > ovo je samo zbog debagovanja FERTmod2
	
#ifdef FERT_0
if (fertilizations[0].fertilization_status == 1) { 
	setRatioWaterFert(FlowSensorCH1,CurrDrvCH1);
}  
#endif

#ifdef FERT_2  
 if (fertilizations[1].fertilization_status == 1){
		setRatioWaterFert(FlowSensorCH2,CurrDrvCH2);
	}
#endif

#ifdef FERT_3
 if (fertilizations[2].fertilization_status == 1){
	setRatioWaterFert(FlowSensorCH3,CurrDrvCH3);
  }
#endif

#ifdef FERT_4
 if (fertilizations[3].fertilization_status == 1){
	setRatioWaterFert(FlowSensorCH3,CurrDrvCH4);
  }
#endif

}
	
// fiksna fertilizacija, pumpa podesena na fiksnu vrednost, koju korisnik zadaje.
if ( fertMode == 2){

#ifdef FERT_0
  if ( (fertilizations[0].fertilization_status == 1) && ( (currentTime() - fertilizations[0].fertilization_started_time) > fertilizations[0].fertilization_duration))
  {    
#ifdef DEBUG_Extend
    //Serial.print("cur time "); Serial.print(currentTime());Serial.print(", fert_1 time "); Serial.print(fertilizations[0].fertilization_started_time);Serial.print(", fert_1 duration "); Serial.println(fertilizations[0].fertilization_duration);
    Serial.println(F("Gasim FERT 1")); 	
#endif    
	stopCurrentDriverPumpFert(CurrDrvCH1);
	valve_stop_fertilization(1);
  }  
  else if (fertilizations[0].fertilization_status == 1) {
		setCurrentDriverPumpFert(fertilizations[0].pumpPrcntCurrFert, CurrDrvCH1);
  }
#endif

#ifdef FERT_2
  // fertilization 2 check
  if ( (fertilizations[1].fertilization_status == 1) && ( (currentTime() - fertilizations[1].fertilization_started_time) > fertilizations[1].fertilization_duration))
  {    
#ifdef DEBUG_Extend
    Serial.println(F("Gasim FERT 2")); 
#endif    
	stopCurrentDriverPumpFert(CurrDrvCH2);
	valve_stop_fertilization(2);
  }
	else if (fertilizations[0].fertilization_status == 1) {
		setCurrentDriverPumpFert(fertilizations[1].pumpPrcntCurrFert, CurrDrvCH2);  
	}
#endif
	
#ifdef FERT_3
  if ( (fertilizations[2].fertilization_status == 1) && ( (currentTime() - fertilizations[2].fertilization_started_time) > fertilizations[2].fertilization_duration))
  {    
#ifdef DEBUG_Extend
    Serial.println(F("Gasim FERT 3")); 
#endif    
	stopCurrentDriverPumpFert(CurrDrvCH3);
	valve_stop_fertilization(3);
  } 
	else if (fertilizations[0].fertilization_status == 1) {
		setCurrentDriverPumpFert(fertilizations[2].pumpPrcntCurrFert, CurrDrvCH3);  
	}  
#endif

#ifdef FERT_4
  // fertilization 4 check
  if ( (fertilizations[3].fertilization_status == 1) && ( (currentTime() - fertilizations[3].fertilization_started_time) > fertilizations[3].fertilization_duration))
  {    
#ifdef DEBUG_Extend
    Serial.println(F("Gasim FERT 4")); 
#endif    
	stopCurrentDriverPumpFert(CurrDrvCH4);
	valve_stop_fertilization(4);
  }
	else if (fertilizations[0].fertilization_status == 1) {
		setCurrentDriverPumpFert(fertilizations[3].pumpPrcntCurrFert, CurrDrvCH4);  
	}  
#endif

}  // if ( fertMode == 2){	

#endif  // A L L  Fertilisations and ALL mode

//============================================= proveravanje sistema na konto zadatog VREMENA navodnjavanja =========================================
  if (!isperm3)
  {
    for (byte i = 0; i < NUM_SLOTS; i++)
    {
      if (zonal_irrigations[i].irrigation_id != "")
      {
        for (byte processing_zone = 0; processing_zone < number_of_zones; processing_zone++)
        {
          //if ((zonal_irrigations[i].status[processing_zone] == 1) && !(currentTime() - zonal_irrigations[i].started_time[processing_zone ] < zonal_irrigations[i].duration))
		  if ((zonal_irrigations[i].status[processing_zone] == 1) && ( (currentTime() - zonal_irrigations[i].started_time) > zonal_irrigations[i].duration)) //ubaceno radi eksperimenta !!!!!!!
          {
delay(150);            
#ifdef AC_Valves            
            digitalWrite(valves[processing_zone ], relay_stop);
#endif            
 
#ifdef DEBUG_Extend
    Serial.print(F("Gasim zonu[per time] "));Serial.println(processing_zone+1);
#endif          
            zonal_irrigations[i].status[processing_zone ] = 0;
			
            bool is_some_zone_on = false;
            for (byte checking_zone = 0; checking_zone < number_of_zones; checking_zone++)
            {
              if (zonal_irrigations[i].status[checking_zone ] == 1)
              {
                is_some_zone_on = true;
                break;
              }
            }
			
            if (is_some_zone_on == false)
            {
              bool is_some_irrigation_on = false;
              for (byte checking_slot = 0; checking_slot < NUM_SLOTS; checking_slot++)
              {
                if (zonal_irrigations[checking_slot].irrigation_id != "")
                {
                  for (byte checking_zone = 0; checking_zone < number_of_zones; checking_zone++)
                  {
                    if (zonal_irrigations[checking_slot].status[checking_zone ] == 1)
                    {
                      is_some_irrigation_on = true;
                      break;
                    }
                  }
                }
              }

              if (!is_some_irrigation_on)
              {
				valve_stop_fertilization(1);// dodato radi eksperimenta
				valve_stop_fertilization(2);// dodato radi eksperimenta
				valve_stop_fertilization(3);// dodato radi eksperimenta
				valve_stop_fertilization(4);// dodato radi eksperimenta	
				delay (2000);
				digitalWrite(mainpump_pin, relay_stop);              
              }
#ifdef DEBUG_Extend
    Serial.print(F("Completed zonal [by time] irr ID ")); Serial.println(zonal_irrigations[i].irrigation_id);
           
#endif
              params = "";
              params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));
              memset(msg, 0, sizeof(msg));
              apiCall(server, 80, "/api/post/irrigation_finished.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG_Extend
              char *jsonResponse = strstr(msg, "{");
              Serial.println(jsonResponse);
#endif

              // clear slot
              zonal_irrigations[i].irrigation_id = "";
              zonal_irrigations[i].duration = 0;
			  zonal_irrigations[i].started_time = 0L; // ubaceno radi eksperimenta !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
              for (byte checking_zone = 0; checking_zone < number_of_zones; checking_zone++)
              {
                zonal_irrigations[i].status[checking_zone ] = 0;
                //zonal_irrigations[i].started_time[checking_zone - 1] = 0L;  // izbaceno radi eksperimenta !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
              }			  
			  
              initialiseIrrFlowMet();// resetuje parametre o protekloj kolicini vode i vreme start-stop. Da li ovo ovde ima potrebe da stoji ????????????????????????????????????

            }
          }//currentTime()
        }//<= number_of_zones;
      }//irrigation_id != ""
    }//i < NUM_SLOTS
  }//  if (!isperm3)
  
//per m3 ======================================== Proveravanje sistema na konto protekle KOLICINE vode / prihrane  ========================================================
  else
  {
	bool fertRun = true;
   for (byte i = 0; i < NUM_SLOTS; i++)
    {
Serial.print(F("[SLOT]")); Serial.print(i);  Serial.print(", Volume ");Serial.println(zonal_irrigations[i].current);         
      if (zonal_irrigations[i].irrigation_id != "")
      {
		   zonal_irrigations[i].current += deltaVolumePerOneLoop; /// - ubaceno eksperimenta radi !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        for (byte processing_zone = 0; processing_zone < number_of_zones; processing_zone++)
        {
          if (zonal_irrigations[i].status[processing_zone] == 1)
          {
          //zonal_irrigations[i].current[processing_zone] += deltaVolumePerOneLoop;         // kolicina vode u [L] Glavniog protokomera u jednom LOOP-u. /// - probati eksperimenta radi !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!            
          //if (zonal_irrigations[i].current[processing_zone] > zonal_irrigations[i].amount) /// - izbaceno radi eksperimenta !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			if ( zonal_irrigations[i].current > zonal_irrigations[i].amount ) /// - ubaceno eksperimenta radi !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            {
				if (fertRun == true)
				{ // samo jednom gasi fertilizaciju.
					valve_stop_fertilization(1);
					valve_stop_fertilization(2);
					valve_stop_fertilization(3);
					valve_stop_fertilization(4);
					fertRun =false;
					delay(3000); // zadrska od 3s da bi se mogao isprati sistem od prihrane.
				}
				
				delay(150);  
				digitalWrite(valves[processing_zone], relay_stop); // gasi jedan po jedan relej zone.
				
#ifdef DEBUG_Extend         
    Serial.print(F("[SLOT][per m3] ")); Serial.print(i); Serial.print(F(", Gasim zonu"));Serial.print(processing_zone+1); Serial.print(F("Istekla kolicina : ")); Serial.println(zonal_irrigations[i].current);
#endif          
              zonal_irrigations[i].status[processing_zone] = 0;
			  
              bool is_some_zone_on = false;
              for (byte checking_zone = 0; checking_zone < number_of_zones; checking_zone++)
              {
                if (zonal_irrigations[i].status[checking_zone ] == 1)
                {
                  is_some_zone_on = true;
                  break;
                }
              }
			  
			  //kad su pogasene sve zone od SLOT[i]-> gasi i glavnu pumpu, i obavesti server.
              if (is_some_zone_on == false)
              {
#ifdef DEBUG
    Serial.print(F("Completed zonal irr ID ")); Serial.println(zonal_irrigations[i].irrigation_id);                  
#endif				
				digitalWrite(mainpump_pin, relay_stop);
				
                params = "";
                params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));// i - SLOT[i];
                memset(msg, 0, sizeof(msg));
                apiCall(server, 80, "/api/post/irrigation_finished.php", params.c_str(), msg, MSG_BUF);         

                // clear slot
                zonal_irrigations[i].irrigation_id = "";
                zonal_irrigations[i].amount = 0L;
                zonal_irrigations[i].max_duration = 0;
				zonal_irrigations[i].current = 0; /// - ubaceno eksperimenta radi !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                for (byte checking_zone = 0; checking_zone < number_of_zones; checking_zone++)
                {
                  zonal_irrigations[i].status[checking_zone] = 0;
                  //zonal_irrigations[i].current[checking_zone] = 0;/// - Izbaceno eksperimenta radi !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!				  
                }
                initialiseIrrFlowMet();
              }
            }
// Zastita : Ako iz nekog razloga se nedetektuje protok u odredjenom vremenu "max_duration" gasi se sve....			
//if ((zonal_irrigations[i].status[processing_zone] == 1) && (zonal_irrigations[i].max_duration > 0) && ( (currentTime() - zonal_irrigations[i].started_time[processing_zone]) > zonal_irrigations[i].max_duration))
if ((zonal_irrigations[i].status[processing_zone] == 1) && /*(zonal_irrigations[i].max_duration > 0) && */( (currentTime() - zonal_irrigations[i].started_time) > zonal_irrigations[i].max_duration))	
			{
				digitalWrite(valves[processing_zone], relay_stop); // gasi jedan po jedan relej zone.
#ifdef DEBUG_Extend         
 Serial.print(F("Gasim zonu [max_dur expired]"));Serial.println(processing_zone+1);
#endif				
				
				zonal_irrigations[i].status[processing_zone] = 0;
				bool is_some_zone_on = false;
				for (byte checking_zone = 0; checking_zone < number_of_zones; checking_zone++)
				{
					if (zonal_irrigations[i].status[checking_zone] == 1)
					{
						is_some_zone_on = true;
						break;
					}
				}
			  
                if (is_some_zone_on == false)
                {
				  
#ifdef DEBUG_Extend
    Serial.print(F("Stopped zonal irr [max duration] ID "));  Serial.print(zonal_irrigations[i].irrigation_id); Serial.print(F(", Elapsed time ")); Serial.print(zonal_irrigations[i].max_duration/60); Serial.println(F("S"));
#endif
				digitalWrite(mainpump_pin, relay_stop);
                params = "";
                params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));
                params.concat("&maxduration=1");
                memset(msg, 0, sizeof(msg));
                apiCall(server, 80, "/api/post/irrigation_stopped.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG_Extend
            char *jsonResponse = strstr(msg, "{");
            Serial.println(jsonResponse);
 #endif
                // clear slot
                zonal_irrigations[i].irrigation_id = "";
                zonal_irrigations[i].amount = 0L;
                zonal_irrigations[i].max_duration = 0;				
				zonal_irrigations[i].current = 0;/// - ubaceno eksperimenta radi !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				
                for (byte checking_zone = 0; checking_zone < number_of_zones; checking_zone++)
                {
                  zonal_irrigations[i].status[checking_zone] = 0;
                  //zonal_irrigations[i].current[checking_zone] = 0;/// - izbaceno eksperimenta radi !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				  
                }
                initialiseIrrFlowMet();
              }
            }
          }
        }		
      }
    }
  }
}

time_t currentTime()
{
  struct ts t;
  tmElements_t te;
  DS3231_get(&t);
  te.Second = t.sec;
  te.Minute = t.min;
  te.Hour = t.hour;
  te.Day = t.mday;
  te.Month = t.mon;
  te.Year = t.year - 1970;

  time_t timeInSeconds = makeTime(te);
  return timeInSeconds;
}
//==================================================================================================================================
int apiCall(const char *server, int port, const char *path, const char *parameters, char *result, int resultlength)
{
  int completed = 0;
  delay(200);
  completed = inet.httpPOST(server, port, path, parameters, result, resultlength);

  if (completed == 0)
  {
#ifdef DEBUG
    Serial.println(F("Dettach GPRS connection..."));
#endif
    inet.dettachGPRS();
    delay(2000);
#ifdef DEBUG
    Serial.println(F("Restarting SIM module..."));
#endif
    digitalWrite(sim_restart_pin, LOW);
    delay(5000);
    digitalWrite(sim_restart_pin, HIGH);
    delay(5000);
#ifdef DEBUG
    Serial.println(F("GSM Shield testing."));
#endif
    // Start configuration of shield with baudrate.
    // For http uses is recommended to use 4800 or slower.
    if (gsm.begin(9600))
    {
#ifdef DEBUG
      Serial.println(F("\nstatus=READY"));
#endif
      gsm_started = true;
    }
    else
    {
#ifdef DEBUG
      Serial.println(F("\nstatus=IDLE"));
#endif
    }
    if (gsm_started)
    {
#ifdef DEBUG
      Serial.println(F("Attach GPRS connection..."));
#endif
      gsm.SimpleWriteln("AT+CGATT=1");
      delay(3000);

      if (inet.attachGPRS(String(carrierdata.apnname).c_str(), String(carrierdata.apnuser).c_str(), String(carrierdata.apnpass).c_str()))
      {
#ifdef DEBUG
        Serial.println(F("status=ATTACHED"));         
        gsm.SimpleWriteln("AT+CIFSR");
        delay(5000);
      //Read until serial buffer is empty.s
        gsm.WhileSimpleRead();
#endif
      }
      else
      {
#ifdef DEBUG
        Serial.println(F("status=ERROR"));
#endif
      }
    
    }
    delay(1000);
  }

  
  if (completed == 0)
  {
    // check to turn off pump if there is no active irrrigation
    bool is_some_irrigation_on = false;
    for (int checking_slot = 0; checking_slot < NUM_SLOTS; checking_slot++)
    {
      if (zonal_irrigations[checking_slot].irrigation_id != "")
      {
        for (int checking_zone = 0; checking_zone < number_of_zones; checking_zone++)
        {
          if (zonal_irrigations[checking_slot].status[checking_zone ] == 1)
          {
            is_some_irrigation_on = true;
            break;
          }
        }
      }
    }
    if (!is_some_irrigation_on)
    {
#ifdef DEBUG
      Serial.println(F("Reseting Arduino..."));
#endif
      digitalWrite(arduino_restart_pin, LOW); // Reset Arduino
//      delay(5000);
    }
    else
    {
#ifdef DEBUG
      Serial.println(F("Not reseting Arduino beacuse of active irrigation."));
#endif
    }
  }
  return completed;
}

#ifdef FLOW
// VRACA trenutni protok [L/min], i proteklu kolicinu vode u[L] u jednom loop-u za glavnog cevovoda (FlowSensorCH5). I takodje radi obracun protoka za ostale protokomere
struct flowmeter_struct checkFlowMeter()
{
  struct flowmeter_struct flow;
  flow.currentflow   = getCurrentFlow(FlowSensorCH5)*60; //getCurrentFlow vraca protok L/s, sa *60 dobija se u L/min protok
  flow.currentvolume = getCurrentVOL( FlowSensorCH5); // kolicina vode u litrima za jedan loop 
	
#ifdef FertProportional	
	getCurrentVOL( FlowSensorCH1);
	getCurrentVOL( FlowSensorCH2);
	getCurrentVOL( FlowSensorCH3);
    getCurrentVOL( FlowSensorCH4);
#endif

 
#ifdef DEBUG_FLOW
    FlowDebug( FlowSensorCH1);
    FlowDebug( FlowSensorCH2);
    FlowDebug( FlowSensorCH3);
    FlowDebug( FlowSensorCH4);
    FlowDebug( FlowSensorCH5);
#endif
  
return flow;
}
#endif


#ifdef INTERRUPTFLOW
struct flowmeter_struct checkFlowMeter()
{
struct flowmeter_struct flow_tmp;
    // process the (possibly) counted ticks
  Meter.tick(flowmeter_period); 
  flow_tmp.currentflow = Meter.getCurrentFlowrate();
  flow_tmp.currentvolume = Meter.getCurrentVolume();
  Meter.reset();
  return flow_tmp;
}
#endif
			
void checkLastSeen() {
  if (last_seen > 0 && !(currentTime() - last_seen < 20 * 60L)) {
#ifdef DEBUG_Extend      
     Serial.println(F("Stop ALL"));
#endif 	  
    stopAll();
  }
}

void stopAll() {

#ifdef Fertilisation
  // stop fertilization
  valve_stop_fertilization(1);
  valve_stop_fertilization(2);
  valve_stop_fertilization(3);
  valve_stop_fertilization(4);
  delay(2000);// saceka da se zaustavi isticanje prihrane u sistem.......
#endif

#ifdef MIXER    
  valve_mixer_stop();//stop mixer
#endif
  
  // stop ALL zonal irrigations
  for (int checking_slot = 0; checking_slot < NUM_SLOTS; checking_slot++) {
    for (int checking_zone=0; checking_zone < number_of_zones; checking_zone++) {
      
#ifdef AC_Valves            
    digitalWrite(valves[checking_zone ], relay_stop);
#endif            

#ifdef DEBUG_Extend      
     Serial.print(F("Gasim ZONU [Stop ALL]: "));Serial.println(checking_zone+1);
#endif      
      zonal_irrigations[checking_slot].status[checking_zone] = 0;
      //zonal_irrigations[checking_slot].started_time[checking_zone] = 0L;/// - izbaceno eksperimenta radi !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      //zonal_irrigations[checking_slot].current[checking_zone] = 0; /// - izbaceno eksperimenta radi !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	  
    }
    zonal_irrigations[checking_slot].irrigation_id = "";
    zonal_irrigations[checking_slot].amount = 0L;
    zonal_irrigations[checking_slot].duration = 0;
    zonal_irrigations[checking_slot].max_duration = 0;
	
	zonal_irrigations[checking_slot].current = 0;   /// - ubaceno eksperimenta radi !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	zonal_irrigations[checking_slot].started_time = 0L; /// - ubaceno eksperimenta radi !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
  }
  digitalWrite(mainpump_pin, relay_stop);
  
  initialiseIrrFlowMet();
}



#ifdef FLOW_Toggling
bool checkFlow() {
     byte flowreadcount = 0;  
     byte firstflowstate = digitalRead(flow_pin_Toggling);
//     byte firstflowstate = ( PINE & 0x80);
     while (flowreadcount < 30) {
        byte flowstate = digitalRead(flow_pin_Toggling); // Za FERT
//        byte flowstate = ( PINE & 0x80);               // Za SENS
        if (flowstate != firstflowstate) {
          return true;
        }
        delay(50);
        flowreadcount++;
     }  
     return false;
  }
#endif

#ifdef TIME_F_Measure
void funcTimeMeasure(String funcBlock){
    Serial.print("Exe Time of "); Serial.print(funcBlock);Serial.print(" = "); Serial.print((float)(millis()-currentTimeMilles),0); Serial.println(" mS");
    currentTimeMilles = millis();
}
#endif
