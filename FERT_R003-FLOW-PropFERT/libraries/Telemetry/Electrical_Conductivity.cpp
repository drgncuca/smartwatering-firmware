#include <Arduino.h>
#include <SPI.h>
#include <MD_AD9833.h>
#include "DS3231M.h"

#include "Telemetry.h"

#define ampUperLimit  485

#ifdef Sensor_EC

#ifdef Sensor_EC_PID
 extern float EC_PID_dutyCycle     ; //[%]
 extern float EC_PID_dutyCycle_OLD ;
#endif

float tempPT1000;

extern EC_struct ECmeasuring;

//initialise data for Low Pass Filter function. (SIN signal 70Hz-a)
int          temp[NMBRsampling];

void sampling(void){
	
	int numberSampling = NMBRsampling-1;	
unsigned long tmp =  millis(); 
    while (numberSampling){//max time sampling 0.42mS !!!     
		temp[numberSampling] = analogRead(A6);		
		delayMicroseconds(200);		
		numberSampling--;
	}	
//Serial.print("Loop Time: "); Serial.println(millis() - tmp); // za debagovanje, iscrtavanje na Ploter	
	// za debagovanje, iscrtavanje na Ploter
/* 	numberSampling = NMBRsampling-1;	
	while (numberSampling){   
		Serial.println(temp[numberSampling]); // za debagovanje, iscrtavanje na Ploter
		numberSampling--;
	}
 */
}

int getSinAmplitude(){
	int sineMin=513;
	int sineMax=511;	
	int numberSampling = NMBRsampling-1;	
 	
	while(numberSampling)//trazi min i max SINusuide
	{	
		if (temp[numberSampling] > sineMax ) {sineMax = temp[numberSampling] ;}//searching for max of SINE signal
        if (temp[numberSampling] < sineMin ) {sineMin = temp[numberSampling] ;}//searching for min of SINE signal
		numberSampling--;
	}  	
	return sineMax - sineMin;
}

int getSineFilteredAmplitude(void){
	
int numberAveraging = NMBRaveraging; //usrednjavanje min i max vrednosti amplitude
int tmp_ampl = 0;	

	digitalWrite(53, HIGH);
	
	while (numberAveraging){	
		sampling(); 
		tmp_ampl =  tmp_ampl + getSinAmplitude(); // vraca digitalnu vrednost amplitude SINusnog signala 
		numberAveraging--;
	}

return tmp_ampl / NMBRaveraging;
  
}
//--------------------------------------------------------------------------------------------------------
float getELconductance(void){

int ADC_Amplitude;
int MuxSwitch = 0;
//float Resistance  = 0;
float Conductance  = 0;
float TempCoefficient=1;

	ADC_Amplitude = getSineFilteredAmplitude(); 

	//Conductance = ADC_Amplitude * 0.0048 - 0.34;
	Conductance = ADC_Amplitude * 0.0047 - 0.236;
	
	tempPT1000 = getTemepraturePT1000();


if ( (tempPT1000 <1) || (tempPT1000>55) ) 
	{
		tempPT1000 = 12;
	}
	TempCoefficient = 1 - (tempPT1000-25) /50; //skaliranje EC na 25°C

                   //     temp. kompezacija------- sa servera kalibr koeficijenti.
 float EC_Temp_Compesated = TempCoefficient * ( ECmeasuring.ecK * Conductance + ECmeasuring.ecM ) ;// vraca vrednost u mS !
 if (EC_Temp_Compesated < 0) EC_Temp_Compesated = 0; 

#ifdef DEBUG_EC
		float amp_Temp_Compesated;
		amp_Temp_Compesated =  (1 - (tempPT1000-25) /50) * ADC_Amplitude; 
		Serial.println(F("------------------------------------"));
		Serial.print(F("EC = ")); Serial.print(EC_Temp_Compesated);  Serial.println(F("mS/cm @ 25°C"));//Serial.print(F(", Ampl:")); Serial.print(ADC_Amplitude); Serial.print(F(", Ampl.Compesated:")); Serial.print(amp_Temp_Compesated); Serial.print(F", Conductance = "));Serial.print(Conductance); Serial.println(F("mS/cm,")); 
		Serial.print(F("Temp:")); Serial.print(tempPT1000); Serial.println(F(" °C"));		
		Serial.print(F("K = "));  Serial.print(ECmeasuring.ecK);  Serial.print(F(", M = ")); Serial.println(ECmeasuring.ecM);

#endif

 ECmeasuring.measured = EC_Temp_Compesated;
 return EC_Temp_Compesated;
}

								// 40Hz         1- Sinus 
void setConductivityDriver(int frequency, int signalShape){
	        			 //Rmux   0.51k  1k5    3K     5K1
	pinMode(44, OUTPUT);  //A0      0     1      0      1
	pinMode(45, OUTPUT);  //A1      0     0      1      1
	digitalWrite(44, LOW); //select 510R   
	digitalWrite(45, LOW);
	
	pinMode(FSYNC, OUTPUT); 
	
	MD_AD9833 AD(FSYNC); // Hardware SPI
	AD.begin();
	AD.setFrequency(0, frequency); //CHAN_0 40Hz
	AD.setMode(signalShape);  									  
	   
	digitalWrite(53, HIGH); //Multiplexer and SIN generator Enabled
	delay(300); //saceka da se ustabili sinusni signal na izlaz/ulaz SIN generatora i operacionog buffer-a	
	
	ECmeasuring.measured = 0.2;
	ECmeasuring.ecK = 1;
	ECmeasuring.ecM = 0;
#ifdef Sensor_EC_PID	
	ECmeasuring.desired  = 0.81; //neka vrednost /=0 ....
	
	ECmeasuring.ecKp = 4.5;
	ECmeasuring.ecKd = 3.5;
	ECmeasuring.ecKi = 2.5;	
	ECmeasuring.CH2_Curr_Strenght =0.75;
	ECmeasuring.CH3_Curr_Strenght =0.25;
#endif	
}
           
#endif