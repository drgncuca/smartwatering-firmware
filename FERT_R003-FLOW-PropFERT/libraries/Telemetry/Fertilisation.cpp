#include <Arduino.h>
#include "Telemetry.h"

 
#define dutyKoeficient 5  // definise koliki je duty PWMa u funkciji razlike zadatog pH i izmerenog.
#define dutyCycle_for_ZeroErrorPh 22 //



#define pHmultiplKoeficijent 7.5

extern pH_struct pHmeasuring;
extern float fertCurrentDriverCoeff ;


int dutyCycle=15;
#ifdef fertPIDregulation
	extern float PID_dutyCycle;
	extern float PID_dutyCycle_OLD; 
#endif

#ifdef Sensor_EC_PID
	 extern float EC_PID_dutyCycle;
	 extern float EC_PID_dutyCycle_OLD;
#endif

float getPh()
{
  int i = 1;
  long sum = 0;
  while (i <= 10)
  {
    sum = sum + analogRead(ph_sensor_pin);
    delay(20);
    i++;
  }
 	//RAW pH
	pHmeasuring.rawPh =  sum * 0.1;
	//Serial.print(" RAW pH =");Serial.println(pHmeasuring.rawPh);						
  return  (pHmeasuring.rawPh) *  pHmeasuring.ferphK   / 204.6  + pHmeasuring.ferphM;	  //(ADDC * 5/1023) = 1/204.6 konverzija  iz digitalne vrednosti u realnu vrednost napona.
}

void valve_stop_fertilization(byte fertnum){
  byte fertnumIDEchanel = 0;
   switch (fertnum){
    case 1:fertnumIDEchanel = 5;break;  //CH1 - Strujni drajver fertnume  (IDE pinout = 5)
    case 2:fertnumIDEchanel = 3;break;
    case 3:fertnumIDEchanel = 6;break;
    case 4:fertnumIDEchanel = 9;break;
    case 5:fertnumIDEchanel = 8;break;  //CH5    
    default : break;
  }
  pinMode(fertnumIDEchanel, OUTPUT);
  digitalWrite(fertnumIDEchanel, LOW); //gasi  strujni drajver(pumpu) za fertilization.   	
	  
     digitalWrite(fertilizations[fertnum-1].fertilization_pin, relay_stop); 
				  fertilizations[fertnum-1].fertilization_status = 0;
				  fertilizations[fertnum-1].fertilization_duration = 0;
				  fertilizations[fertnum-1].fertilization_started_time = 0L;  
				  fertilizations[fertnum-1].fertilization_id = "";
				  fertilizations[fertnum-1].fertilization_irrigation_id = ""; 
				  
				  fertilizations[fertnum-1].ratioWaterFert    = 0;
				  fertilizations[fertnum-1].pumpPrcntCurrFert = 0;
#ifdef fertPIDregulation
	 if (fertnum == 1){
		PID_dutyCycle = 12; // vraca vrednost strujnog drajvera FER1 na 4mA -> pumpa iskljucena
		PID_dutyCycle_OLD = 2;
	 }
#endif  

#ifdef Sensor_EC_PID
	 if (fertnum == 2){
		EC_PID_dutyCycle     = 0; 
		EC_PID_dutyCycle_OLD = 0;
		StopCurrentDriver(CurrDrvCH2);	
		StopCurrentDriver(CurrDrvCH3); // zakomentarisati ovo kad se implementira EC PID i za CH3	
	 }
	 //------- za buducu implementaciju doziranje fertilizacije na strujnom kanalu CH3
	 // if (fertnum == 3){
		// EC_PID_dutyCycle     = 0; 
		// EC_PID_dutyCycle_OLD = 0;
		// StopCurrentDriver(CurrDrvCH3);		
	 // }
#endif 
}


#ifdef Fertilisation

void valve_start_fertilization(byte fertnum){  
    pinMode(fertilizations[fertnum-1].fertilization_pin, OUTPUT);   
	digitalWrite(fertilizations[fertnum-1].fertilization_pin, HIGH  );
}


void initialiseFertilisationParameters(void){
	for (int i=0;  i<4 ; i++){
	  
	fertilizations[i].fertilization_status = 0; // 0 = OFF, 1 = ON
	fertilizations[i].fertilization_id="";
	fertilizations[i].fertilization_irrigation_id="";
	fertilizations[i].fertilization_duration = 0;
	fertilizations[i].fertilization_started_time = 0;	  
		 
	fertilizations[i].fertilization_pin = valves_fert[i];
	pinMode(fertilizations[i].fertilization_pin, OUTPUT); 
	digitalWrite(fertilizations[i].fertilization_pin, LOW);

    fertilizations[i].ratioWaterFert    = 0;
	fertilizations[i].pumpPrcntCurrFert	        = 0;  
	  
	}
	fertCurrentDriverCoeff = 1;  // "ferdrv "
	//fertFlowMeterCallCoeff = 1;  // "ferkf  "
	
	pHmeasuring.rawPh     = 400; // neka RAW vrednost pH=7
	pHmeasuring.desiredPh = 5.5;//
	pHmeasuring.ferphK    = 8.81;  // Y = k*X + M
	pHmeasuring.ferphM    =-9.87;
	pHmeasuring.ferKp = 4.0;
	pHmeasuring.ferKd = 2.5;
	pHmeasuring.ferKi = 1.5;
	
}
//----------------------------------------------------------------------------------------
						// strujni drajver    zeljeni pH
void fertilizationCheck(byte CurrDrvChanel,         float pHtarget){ 

float errorPh;


pHmeasuring.pH = getPh(); 

errorPh =  pHmeasuring.pH - pHtarget; // errorPh je u opsegu od -1 do 14.

#ifdef fert_Iout_Fix20mA   // Nema PID regulacije, Io = 20mA FIX. 
	dutyCycle = 83;
#else
	#ifndef  fertPIDregulation
		 if (errorPh < -1.4  ){   //ako je izmereni pH veci od zadatog za vise od 1.4 ( 10%) odrzi drajver na min upravljanja, 4mA     
		  pinMode(CurrDrvChanel, OUTPUT);
		  digitalWrite(CurrDrvChanel, LOW);	//iskljuci drajver Fertn. i stavi drajver u Hi-Z i
												//izadji iz funkcije
		  dutyCycle = 16; //drzi drajver na 4mA
		  return; // ako je dosegnut nivo pHmeasuring.pH koji je zadat zaustavlja se drajver pumpe i izlazi iz fukncije u nekom narednom ciklusu kad i ako se promeni pHmeasuring.pH nastavice nadjubravanjem.
		}
	#endif

	// Samo se za jedan strujni drajver racuna dutyCycle (jedno je pH merenje). 
	    
#ifdef  fertPIDregulation //Ako je ukljucena PID regulacija 
	dutyCycle = gerDutyCycle_fertPh_PID( pHtarget );  
#else //Ako nije ukljucen  PID regulacija																											// 5 
	dutyCycle = fertCurrentDriverCoeff * dutyKoeficient * errorPh + dutyCycle_for_ZeroErrorPh ; //value in percente.	
if (dutyCycle > maxDutyCycle) dutyCycle = maxDutyCycle;  //ograniciti struju Io = 25mA  max
if (dutyCycle < minDutyCycle) dutyCycle = minDutyCycle;    // podesiti pumpu za doziranje tako da ne bude premala vrednost, jer bi predugo trajalo djubrenje.
#endif
	
#endif	
																																					  
CurrentDriverSet( dutyCycle, CurrDrvChanel) ;

#ifdef DEBUG_Fertilisation  
  Serial.println(F("------------------------------------"));
  
  Serial.print(F("pH Measured = ")); Serial.print(pHmeasuring.pH); Serial.print(F(", pH Desire  = ")); Serial.print(pHtarget);  Serial.print(F(", Error = ")); Serial.println(errorPh);  
  
  Serial.print("Kp = ");Serial.print(pHmeasuring.ferKp);Serial.print(F(" Ki = "));Serial.print(pHmeasuring.ferKi);Serial.print(" Kd = ");Serial.println(pHmeasuring.ferKd); 
  
  Serial.print(F("Current CH1 = ")); Serial.print(0.24 * dutyCycle + 0.13 );Serial.print("mA  (");   Serial.print(dutyCycle);Serial.println("%)");
  
 
#endif 
}


#endif
