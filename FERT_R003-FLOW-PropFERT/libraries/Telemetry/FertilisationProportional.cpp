#include <Arduino.h>
#include "Telemetry.h"

int ratioWaterFertDutyCycle_OLD = 55; // eksperimentisati
int ratioWaterFertDutyCycle     = 55;	

#define ratioFLOW_error 5

// FLOW sensor parametri & istorija (dinamika ) FLOW greske.
 float errorFLOW0 = 0;
 float errorFLOW1 = 0;
 float errorFLOW2 = 0;


void setRatioWaterFert(byte flowmeter_sensor, byte CurrDrvChanPin){
byte Kp = 15;
byte Ki = 8;
byte Kd = 2;

float FlowSensorCH5temp;
float FlowSensorTemp;																					             
float ratioWaterFertTemp =0;

byte loop = 5; // eksperimantisati ! ! ! Vrti X krugova (merenja, upravljanja), a greska zanemarljiva zavrsi s upravljanjem instant.

while (loop){

// kalulacija odnosa protoka sa glavnog protokomera i FERT[x] protokomera
 FlowSensorCH5temp = getCurrentFlow(FlowSensorCH5);
 FlowSensorTemp = getCurrentFlow(flowmeter_sensor);
 
 ratioWaterFertTemp = 1 + FlowSensorCH5temp / ( FlowSensorTemp + 0.04);// protok na glavnom meracu je zbir protoka vode i prihrane ! (A+B)/B = 1+ A/B . LSB = 0.04 - zastita od delenja s 0

errorFLOW0 =  fertilizations[flowmeter_sensor].ratioWaterFert - ratioWaterFertTemp; // racunanje kolika je greska 

// ovo otkomentarisati kad se utvrde vrednosti i dinamika sistema
	if(  (-ratioFLOW_error < errorFLOW0)  && (errorFLOW0 < ratioFLOW_error)    ){ // ako je greska zanemarljiva, onda zavrsi sa korigovanjem protoka fert pumpe
		return;		
	}
	
	ratioWaterFertDutyCycle_OLD = ratioWaterFertDutyCycle;																						
	ratioWaterFertDutyCycle = (ratioWaterFertDutyCycle + Kp * ( errorFLOW0 - errorFLOW1) + Ki * errorFLOW0 + Kd * ( errorFLOW0 - 2*errorFLOW1 + errorFLOW2));	//	
	errorFLOW2 = errorFLOW1;
	errorFLOW1 = errorFLOW0;

	if ( (ratioWaterFertDutyCycle  - ratioWaterFertDutyCycle_OLD) > 15 ){ //soft start, da pumpa ne dobije slucajno preveliku vrednos upravljackog signala.
		ratioWaterFertDutyCycle = ratioWaterFertDutyCycle_OLD + 15;
	}

	if ( ( ratioWaterFertDutyCycle_OLD - ratioWaterFertDutyCycle ) > 15 ){
		ratioWaterFertDutyCycle = ratioWaterFertDutyCycle_OLD - 15;
	}

	if (ratioWaterFertDutyCycle > maxDutyCycle) { ratioWaterFertDutyCycle = maxDutyCycle;} // Ograniciti struju Io = 20mA  max
	if (ratioWaterFertDutyCycle < minDutyCycle) { ratioWaterFertDutyCycle = minDutyCycle;} // 4mA, podesiti pumpu za doziranje tako da ne bude premala vrednost, jer bi predugo trajalo djubrenje.
	

#ifdef FERT_0
	if(CurrDrvChanPin == CurrDrvCH1)	{ CurrentDriverSet(ratioWaterFertDutyCycle * ECmeasuring.CH1_Curr_Strenght, CurrDrvCH1); return;} 
#endif//  upravljacki strujni drajver za pumpu ( FERT 1) 

#ifdef FERT_2
	if(CurrDrvChanPin == CurrDrvCH2)	{CurrentDriverSet(ratioWaterFertDutyCycle * ECmeasuring.CH2_Curr_Strenght, CurrDrvCH2);return;}
#endif//  upravljacki strujni drajver za pumpu ( FERT 2)

#ifdef FERT_3
	if(CurrDrvChanPin == CurrDrvCH3)	{CurrentDriverSet(ratioWaterFertDutyCycle * ECmeasuring.CH3_Curr_Strenght, CurrDrvCH3);return;}
#endif//  upravljacki strujni drajver za pumpu ( FERT 3)	

#ifdef FERT_4
	if(CurrDrvChanPin == CurrDrvCH4)	{CurrentDriverSet(ratioWaterFertDutyCycle * ECmeasuring.CH4_Curr_Strenght, CurrDrvCH4); return;}
#endif//  upravljacki strujni drajver za pumpu ( FERT 4)	
	
loop--;			
}	
#ifdef DEBUG_FertProportional 
  Serial.println(F("------------------------------------"));
  
  Serial.print(F("FERT CH ")); Serial.println(flowmeter_sensor);  
  
  Serial.print(F("Water Flow")); Serial.print(FlowSensorCH5temp); Serial.print(F(", FERT Flow")); Serial.println(FlowSensorTemp);

  Serial.print(F("Ratio Set : ")); Serial.print(fertilizations[flowmeter_sensor].ratioWaterFert); Serial.print(F(", Ratio Achieve : ")); Serial.print(ratioWaterFertTemp);  Serial.print(F(", Ratio Error : ")); Serial.println(errorFLOW0);   
  
//  Serial.print(F("Kp = "));Serial.print(Kp);Serial.print(F(",Ki = "));Serial.print(Ki);Serial.print(F(",Kd = "));Serial.println(Kd); 
  
  Serial.print(F("Current CH")); Serial.print(CurrDrvChanPin); Serial.print(F(" = ")); Serial.print(0.24 * ratioWaterFertDutyCycle + 0.13 );Serial.print(F("mA  ("));   Serial.print(ratioWaterFertDutyCycle);Serial.println(F("%)"));
  
 
#endif 	
}

// FertMode = 2
// ZA fiksno podesavanje rada FERT pumpe
// convPercentToDutyCycle: [17 - 85] = [0 - 100%] = [4 - 20mA]

void setCurrentDriverPumpFert(byte convPercentToDutyCycle, byte CurrDrvChanPin){	

CurrentDriverSet(convPercentToDutyCycle, CurrDrvChanPin);	

Serial.print(F("Set Current Driver Pump Fert Current ")); Serial.print(CurrDrvChanPin); Serial.print(" ,"); Serial.print((0.24 * (float)convPercentToDutyCycle + 0.13) );Serial.println(F("mA"));  
	
}
void stopCurrentDriverPumpFert(byte CurrDrvChanPin){
	
StopCurrentDriver(CurrDrvChanPin);
	
}
