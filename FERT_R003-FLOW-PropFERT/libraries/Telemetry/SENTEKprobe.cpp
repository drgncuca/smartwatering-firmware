#include <Arduino.h>
#include "Telemetry.h"

#ifdef SENTEC_Include
/*                   //   0    1     2     3     4     5
float oldmeasure[18]={25.00,25.00,25.00,25.00,25.00,25.00 //  0-5 temperature,
                     ,00.00,00.00,00.00,00.00,00.00,00.00 //  6-11 moisture
					 ,00.00,00.00,00.00,00.00,00.00,00.00};//12-17 Salinity */

#if defined (SENTEK_FERT_R001) || defined (SENTEK_FERT_R002) 
void setSANTEKsensors(void){	
	Serial2.begin(9600,SERIAL_7N2); 	
}
#endif

#ifdef SENTEK_SENS_R001
void setSANTEKsensors(void){	
	Serial1.begin(9600,SERIAL_7N2); 	
}
#endif

char char_temp[20];


float getSENTEKtemperature(byte debpthSesnor){	
int atempt =0;
float t = 0;

do{
	switch(debpthSesnor){
	/*10cm*/	case 1:RS485writeStringLN(":01040180000278");break;// Reading temperature from reg 0x30385
	/*20cm*/	case 2:RS485writeStringLN(":01040182000276");break;// Reading temperature from reg 0x30387
	/*30cm*/	case 3:RS485writeStringLN(":01040184000274");break;// Reading temperature from reg 0x30389
#ifdef SENTEK_Include_Extend_Sens	
	/*40cm*/	case 4:RS485writeStringLN(":01040186000272");break;
	/*50cm*/	case 5:RS485writeStringLN(":01040188000270");break;
	/*60cm*/	case 6:RS485writeStringLN(":0104018A00026E");break;
#endif	
			   default:break;	
	}					
	RS485readCharArray(ReceiveChanellRS485, char_temp);	
	t = hexStringToFloat(char_temp, debpthSesnor);
	delay(100);	
}while((t < 0.001)  &&  ((atempt++ <3) ));		
//if(atempt>1){Serial.print("atempt: ");Serial.print(atempt);	Serial.print(" ");}
return 	t;

}

float getSENTEKmoisture(byte debpthSesnor){	
int atempt =0;
float m = 0;
do{
	switch(debpthSesnor){
	/*10cm*/	case 1:RS485writeStringLN(":010401000002F8");break;// Reading moisture from reg 0x30257
	/*20cm*/	case 2:RS485writeStringLN(":010401020002F6");break;// Reading moisture from reg 0x30259
	/*30cm*/	case 3:RS485writeStringLN(":010401040002F4");break;// Reading moisture from reg 0x30261
#ifdef SENTEK_Include_Extend_Sens	
	/*40cm*/	case 4:RS485writeStringLN(":010401060002F2");break;
	/*50cm*/	case 5:RS485writeStringLN(":010401070002F0");break;
	/*60cm*/	case 6:RS485writeStringLN(":0104010A0002EE");break;
#endif	
			   default:break;
	}		
	RS485readCharArray(ReceiveChanellRS485, char_temp);	
	m = hexStringToFloat(char_temp, 6 + debpthSesnor);
	delay(100);	
}while((m < 0.001)  &&  ((atempt++ <5) ));
return m;	
}

float getSENTEKsalinity(byte debpthSesnor){	
int atempt =0;
float s = 0;
do{
	switch(debpthSesnor){
	/*10cm*/	case 1:RS485writeStringLN(":010401400002B8");break;// Reading Salinity from reg 0x30231
	/*20cm*/	case 2:RS485writeStringLN(":010401420002B6");break;// Reading Salinity from reg 0x30233
	/*30cm*/	case 3:RS485writeStringLN(":010401440002B4");break;// Reading Salinity from reg 0x30235
#ifdef SENTEK_Include_Extend_Sens	
	/*40cm*/	case 4:RS485writeStringLN(":010401460002B2");break;
	/*50cm*/	case 5:RS485writeStringLN(":010401480002B0");break;
	/*60cm*/	case 6:RS485writeStringLN(":0104014A0002AF");break;
#endif	
			   default:break;
	}	    
	RS485readCharArray(ReceiveChanellRS485, char_temp);	
	s = hexStringToFloat(char_temp, 12 + debpthSesnor);
	delay(200);
}while((s < 0.001)  &&  ((atempt++ <5)));

return s;	
}

void reqSENTEKpresetHoldReg_Temperature(void){
	RS485writeStringLN(":010600000004F5");// preset Holding Register 40001 to the value 4, which will start a scan of all humidity sensors

}
void reqSENTEKpresetHoldReg_Moisture(void){
	RS485writeStringLN(":010600000002F7");// preset Holding Register 40001 to the value 2, which will start a scan of all moisture sensors

}
void reqSENTEKpresetHoldReg_Salinity(void){
	RS485writeStringLN(":010600000003F6");// preset Holding Register 40001 to the value 3, which will start a scan of all humidity sensors

}

//----------------------------------------------------------------------------
//sensorID: [1-6] temperature, [7-12] moisture, [13- 18] Salinity
float hexStringToFloat(char char_temp[],int sensorID){ 
	//Serial.print("hex convert:");Serial.println(char_temp);
	char temp[9] = "01234567"; 
	int i = 0;
	for (i=0;i<4;i++){        //BigEndian format of 32bit
		temp[i] = char_temp[i+11] ;
	}	
	for (int i=4;i<8;i++){
		temp[i] = char_temp[i+7] ;
	}	
	long l = strtol(temp, NULL, 16);
	float sens_value = *(float *)&l;	
	
	if (isnan(sens_value)) {// if (NAN = 1) - Not A Number
		sens_value = 0;
#ifdef DEBUG_SENTEK		
		Serial.print("NAN-error");	
#endif		
	} 
	
//	Temperature i moisture limiting value
if ( sensorID < 13) { // senzori od 1-12 (temp, mois)
	if ( (sens_value < 0.001) || (sens_value > 100) ){// limiting value			
//		Serial.print("SENTEK ERROR = ");Serial.print(sens_value,2);
		sens_value = 0;//oldmeasure[sensorID-1];	
	}

}
#ifdef SENTEK_Salinity 
else{  // senzori od 13-18 Saliniti
	if ( (sens_value < 0.001) || (sens_value > 10000) ){// limiting value			
//		Serial.print("SENTEK ERROR = ");Serial.print(sens_value,2);
		sens_value = 0;//oldmeasure[sensorID-1];	
	}		
}	
#endif	


return sens_value;	
}
/*
float hexStringToFloat(String temp){ //BigEndian format of 32bit
	temp = temp.substring(7,15);
	String tempT_L = temp.substring(0,4);
	String tempT_H = temp.substring(4,8);
	temp = tempT_H + tempT_L;
	//Serial.println(temp);
	char ch[] = "00000000";
	temp.toCharArray(ch, 9);
	long l = strtol(ch, NULL, 16);
	float f = *(float *)&l;
	//Serial.println(f,4);
	return f;	
}
*/

/*
float getSENTEKhumidity(byte debpthSesnor){	
	switch(debpthSesnor){
		case 1:RS485writeStringLN(":010401C0000238");// Reading temperature from reg 0x30449 
		case 2:RS485writeStringLN(":010401C2000236");// Reading temperature from reg 0x30451
		case 3:RS485writeStringLN(":010401C4000234");// Reading temperature from reg 0x30453
			   default:RS485writeStringLN(":010401C4000234");// Reading temperature from reg 0x30453	
	}	
		String temp = RS485readString(4);	
		delay(500);
//		Serial.print(temp);	
	return hexStringToFloat(temp);	
	
}
*/


/*
byte getSENTEKmeasureStatus(void){
	RS485writeStringLN(":010400000001FA");// Status of Measurement
	String temp = (RS485readString(4));
	Serial.print(temp);
	delay(400);	
	
	return 	1;
	
}
*/
//extract string from "temp" and reverse low and high 16bits and convert to float.
 
/*
void echoSENTEKtoMaster(void){
	RS485writeStringLN(":01080000A5371B");  //simply echoed back to the Master
	Serial.print(RS485readString(4));
	delay(500);		
}
void SENTEKslaveID(void){
	RS485writeStringLN(":0111EE");// Report Slave ID
	Serial.print(RS485readString(4));
	delay(500);
}
*/
/*
void presetSENTEKholdingReg(String temp){
	RS485writeStringLN(temp);//preset Holding Registers .
	Serial.print(RS485readString(4));
	delay(500);	
	
}
*/
/*
void reqSENTEKreadingHoldReg(void){

	RS485writeStringLN(":010300000001F8");//preset Holding Registers 40001 to the value 1. Read sensors specified in mask (40002 through 40009)
	//Serial.print(RS485readString(4));
	delay(500);
}
*/


/*
void readSENTEKinputReg(String temp){
	RS485writeStringLN(temp);
	Serial.print(RS485readString(4));
	delay(500);
}
*/
/*
int calculateLRC(byte* bytes) {
    unsigned int LRC = 0;
    for (int i = 0; i < 14; i++) {
      LRC ^= bytes[i];
      Serial.write(bytes[i]);
    }
    return LRC;
}
*/

#endif