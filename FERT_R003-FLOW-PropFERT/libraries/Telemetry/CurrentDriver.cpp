#include <Arduino.h>
#include "Telemetry.h"


void CurrentDriverSet(byte dutyCycle, byte CurrDrvChanPin){
//PWM--parametri
int pulse = 5;  // vreme trajanja pulsa u mS.
int pause = 15; 

if(dutyCycle < 0 )            dutyCycle = 0;
if(dutyCycle > maxDutyCycle)  dutyCycle = maxDutyCycle;

pulse = ((float)dutyCycle/100) * PWMperiodPulse; 
pause = PWMperiodPulse - pulse;

byte PWMpulses = PWMpulseTogle; // broj PWM impulsa da se dosegne zeljeni DC nivo na strujnom drajveru.
//byte K = 3; // Soft start
pinMode(CurrDrvChanPin, OUTPUT);  // drajver aktiviran

while(PWMpulses){   // generise PWM signal, sa odredjenim faktorom ispune.
  digitalWrite(CurrDrvChanPin, HIGH);  
  delay( pulse );  //Pocetni ciklus PWM signala da bude uzi nego proracunati, da bi finije startovao sa uspotvaljanjem DC vrednosti na strujnom drajveru
  digitalWrite(CurrDrvChanPin, LOW);  
  delay( pause ); 
  PWMpulses--;
}
  // Kad se zavrsi PWM ciklus, drajver PWMa treba staviti Hi-Z da ne bi praznio kondezator na R-C filteru drajvera.
  pinMode(CurrDrvChanPin, INPUT); 
  
#ifdef DEBUG_CurrentDriver  
	Serial.print(F("Current CH-"));Serial.print(CurrDrvChanPin);Serial.print(F(" ,Curent : ")); Serial.print((0.24 * dutyCycle + 0.13) );Serial.println(F("mA"));  
#endif  
}

void StopCurrentDriver( byte CurrDrvChanPin){
	
pinMode(CurrDrvChanPin, OUTPUT);  
digitalWrite(CurrDrvChanPin, LOW); 
	
}