#include <Arduino.h>
#include "Telemetry.h"

int ratioWaterFertDutyCycle_OLD = 55; // eksperimentisati
int ratioWaterFertDutyCycle     = 55;	

#define ratioFLOW_error 0.05

// FLOW sensor parametri & istorija (dinamika ) FLOW greske.
 float errorFLOW0 = 0;
 float errorFLOW1 = 0;
 float errorFLOW2 = 0;

// za FERT Mode 1
#ifdef FertProportional
void setRatioWaterFert(byte flowmeter_sensor, byte CurrDrvChanPin){
byte Kp = 5;
byte Ki = 4;
byte Kd = 2;

float FlowSensorCH5temp;
float FlowSensorTemp;																					             
float ratioWaterFertTemp =0;

byte loop = 4; // eksperimantisati ! ! ! Vrti X krugova (merenja, upravljanja), a greska zanemarljiva zavrsi s upravljanjem instant.

while (loop){

// kalulacija odnosa protoka sa glavnog protokomera i FERT[x] protokomera
 FlowSensorCH5temp = getCurrentFlow(FlowSensorCH5);
 FlowSensorTemp    = getCurrentFlow(flowmeter_sensor);
 
 //ratioWaterFertTemp = 1 + FlowSensorCH5temp / ( FlowSensorTemp + 0.02);// protok na glavnom meracu je zbir protoka vode i prihrane ! (A+B)/B = 1+ A/B . LSB = 0.02 - zastita od delenja s 0
 ratioWaterFertTemp =  FlowSensorCH5temp / ( FlowSensorTemp + 0.01);// 0.01 - zastita od delenja s 0
         errorFLOW0 =  ratioWaterFertTemp - fertilizations[flowmeter_sensor].ratioWaterFert ; // racunanje kolika je greska zadatog odnosa protoka vode i fert-a i dostignutog odnosa protoka... 


if(  ( errorFLOW0 < -ratioFLOW_error )  || (errorFLOW0 > ratioFLOW_error)    ){ // ako greska NIJE zanemarljiva -> procesuiraj

	ratioWaterFertDutyCycle_OLD = ratioWaterFertDutyCycle;																						
	ratioWaterFertDutyCycle = (ratioWaterFertDutyCycle + Kp * ( errorFLOW0 - errorFLOW1) + Ki * errorFLOW0 + Kd * ( errorFLOW0 - 2*errorFLOW1 + errorFLOW2));	//	
	errorFLOW2 = errorFLOW1;
	errorFLOW1 = errorFLOW0;

	if ( (ratioWaterFertDutyCycle  - ratioWaterFertDutyCycle_OLD) > 9 ){ //soft start, da pumpa ne dobije slucajno preveliku vrednos upravljackog signala.
		ratioWaterFertDutyCycle = ratioWaterFertDutyCycle_OLD + 9;
	}

	if ( ( ratioWaterFertDutyCycle_OLD - ratioWaterFertDutyCycle ) > 9 ){
		ratioWaterFertDutyCycle = ratioWaterFertDutyCycle_OLD - 9;
	}

	if (ratioWaterFertDutyCycle > maxDutyCycle) { ratioWaterFertDutyCycle = maxDutyCycle;} // Ograniciti struju Io = 20mA  max
	if (ratioWaterFertDutyCycle < minDutyCycle) { ratioWaterFertDutyCycle = minDutyCycle;} // 4mA, podesiti pumpu za doziranje tako da ne bude premala vrednost, jer bi predugo trajalo djubrenje.
	}
	else{
#ifdef DEBUG_FertProportional
	Serial.println(F("LOOP Locked "));
#endif	
	}

#ifdef FERT_0
	if(CurrDrvChanPin == CurrDrvCH1)	{ CurrentDriverSet(ratioWaterFertDutyCycle , CurrDrvCH1); } 
#endif//  upravljacki strujni drajver za pumpu ( FERT 1) 

#ifdef FERT_2
	if(CurrDrvChanPin == CurrDrvCH2)	{CurrentDriverSet(ratioWaterFertDutyCycle , CurrDrvCH2);}
#endif//  upravljacki strujni drajver za pumpu ( FERT 2)

#ifdef FERT_3
	if(CurrDrvChanPin == CurrDrvCH3)	{CurrentDriverSet(ratioWaterFertDutyCycle , CurrDrvCH3);}
#endif//  upravljacki strujni drajver za pumpu ( FERT 3)	

#ifdef FERT_4
	if(CurrDrvChanPin == CurrDrvCH4)	{CurrentDriverSet(ratioWaterFertDutyCycle , CurrDrvCH4); }
#endif//  upravljacki strujni drajver za pumpu ( FERT 4)	
	
loop--;		
delay(800);	
}	
#ifdef DEBUG_FertProportional
  Serial.println(F("------------------------------------"));
  
  Serial.print(F("FERT_")); Serial.print(flowmeter_sensor+1);  
  
  Serial.print(F(" Flow "));Serial.print(FlowSensorTemp); Serial.print(F(" L/s"));Serial.print(F(", Water Flow"));Serial.print(FlowSensorCH5temp);Serial.println(F(" L/s"));  

  Serial.print(F("Water/FERT Ratio Set : ")); Serial.print(fertilizations[flowmeter_sensor].ratioWaterFert); Serial.print(F(", Achieved : ")); Serial.print(ratioWaterFertTemp);  Serial.print(F(", Error : ")); Serial.println(errorFLOW0);   
  
//  Serial.print(F("Kp = "));Serial.print(Kp);Serial.print(F(",Ki = "));Serial.print(Ki);Serial.print(F(",Kd = "));Serial.println(Kd); 
  
  Serial.print(F("Current CH")); Serial.print(flowmeter_sensor+1); Serial.print(F(" = ")); Serial.print(0.24 * ratioWaterFertDutyCycle + 0.13 );Serial.print(F("mA  ("));   Serial.print(ratioWaterFertDutyCycle);Serial.println(F("%)"));
  
 
#endif 	
}
#endif

// FertMode = 2
// ZA fiksno podesavanje rada FERT pumpe
// convPercentToDutyCycle: [17% - 84%] = [0% - 100%] = [4mA - 20mA]

void setCurrentDriverPumpFert(byte convPercentToDutyCycle, byte CurrDrvChanPin){	

CurrentDriverSet(convPercentToDutyCycle, CurrDrvChanPin);	
#ifdef DEBUG_FertMode_2 
Serial.print(F("Set Current Fert: ")); Serial.print(CurrDrvChanPin); Serial.print(F("   ")); Serial.print((0.24 * (float)convPercentToDutyCycle + 0.13) );Serial.print(F("mA , ")); Serial.print(convPercentToDutyCycle); Serial.println(F(" %")); 
#endif 		
}
void stopCurrentDriverPumpFert(byte CurrDrvChanPin){
	
StopCurrentDriver(CurrDrvChanPin);
	
}
