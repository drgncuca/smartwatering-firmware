#include <Arduino.h>
#include "Telemetry.h"

#ifdef IsolatCurrDrv

void initCurrentDAC(){

PORTJ = PORTJ & 0xDF; // DAC_D <LOW>
DDRJ  = DDRJ  | 0x20; // DAC_D <OUT>   
PORTJ = PORTJ & 0xDF; // DAC_D <LOW>

PORTJ = PORTJ & 0xBF; // DAC_CLK <LOW>
DDRJ  = DDRJ  | 0x40; // DAC_CLK <OUT> 	
PORTJ = PORTJ & 0xBF; // DAC_CLK <LOW>

PORTJ = PORTJ | 0x80; // DAC_CS <HIGH>
DDRJ  = DDRJ  | 0x80; // DAC_CS <OUT> 	
PORTJ = PORTJ | 0x80; // DAC_CS <HIGH>	
	
}



void sendDataToDAC(byte data, byte chanel){
	
//Serial.print("DAC current: ");Serial.println(data);	
byte x = 0;
	
PORTJ = PORTJ & 0x7F; // DAC_CS <LOW>
delay(1);//delayMicroseconds(90);


PORTJ = PORTJ | 0x40; //CLK = 1
delay(1);//delayMicroseconds(90);
PORTJ = PORTJ & 0xBF; //CLK = 0

for(x=0; x<4; x++){
	
	if( (chanel & 0x01) == 0x01 ) // MSB
	{
		PORTJ = PORTJ | 0x20; //data set 1
		delay(1);//delayMicroseconds(90);
		PORTJ = PORTJ | 0x40; //CLK = 1
		delay(1);//delayMicroseconds(90);
		PORTJ = PORTJ & 0xBF; //CLK = 0
	}	
	else
	{
		PORTJ = PORTJ & 0xDF;//data set 0
		delay(1);//delayMicroseconds(90);	
		PORTJ = PORTJ | 0x40; //CLK = 1
		delay(1);//delayMicroseconds(90);
		PORTJ = PORTJ & 0xBF; //CLK = 0	
	}

	chanel = chanel>>1;	
}

// salje se podatak koji se zeli podesiti na odg izlaz DACa.	
for(x=0; x<8; x++){
	
	if( (data & 0x80) == 0x80 ) // MSB
	{
		PORTJ = PORTJ | 0x20; //data set 1
		delay(1);//delayMicroseconds(90);
		PORTJ = PORTJ | 0x40; //CLK = 1
		delay(1);//delayMicroseconds(90);
		PORTJ = PORTJ & 0xBF; //CLK = 0
	}	
	else
	{
		PORTJ = PORTJ & 0xDF;//data set 0
		delay(1);//delayMicroseconds(90);	
		PORTJ = PORTJ | 0x40; //CLK = 1
		delay(1);//delayMicroseconds(90);
		PORTJ = PORTJ & 0xBF; //CLK = 0	
	}

	data = data<<1;	
}	

delay(1);//delayMicroseconds(90);
PORTJ = PORTJ | 0x80; // DAC_CS <HIGH>	

}


#endif



