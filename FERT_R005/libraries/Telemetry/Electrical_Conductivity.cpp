#include <Arduino.h>
#include <SPI.h>
#include <MD_AD9833.h>
#include "DS3231M.h"

#include "Telemetry.h"

#ifdef Sensor_EC

extern EC_struct ECmeasuring;

//initialise data for Low Pass Filter function. (SIN signal 70Hz-a)
int          temp[NMBRsampling];

void sampling(void){
	
	byte numberSampling = NMBRsampling-1;	
unsigned long tmp =  millis(); 
    while (numberSampling){//min time sampling 0.42mS !!!     
		temp[numberSampling] = analogRead(A6);		
		delayMicroseconds(200);		
		numberSampling--;
	}	

}

int getSinAmplitude(){
	int sineMin=513;
	int sineMax=511;	
	byte numberSampling = NMBRsampling-1;	
 	
	while(numberSampling)//trazi min i max SINusuide
	{	
		if (temp[numberSampling] > sineMax ) {sineMax = temp[numberSampling] ;}//searching for max of SINE signal
        if (temp[numberSampling] < sineMin ) {sineMin = temp[numberSampling] ;}//searching for min of SINE signal
		numberSampling--;
	}  	
	return sineMax - sineMin;
}

int getSineFilteredAmplitude(void){
	
byte numberAveraging = NMBRaveraging; //usrednjavanje min i max vrednosti amplitude
int tmp_ampl = 0;	

	digitalWrite(53, HIGH);
	
	while (numberAveraging){	
		sampling(); 
		tmp_ampl =  tmp_ampl + getSinAmplitude(); // vraca digitalnu vrednost amplitude SINusnog signala 
		numberAveraging--;
	}

return tmp_ampl / NMBRaveraging;
  
}
//--------------------------------------------------------------------------------------------------------
float getELconductance(void){

int ADC_Amplitude;

//float Resistance  = 0;
float Conductance  = 0;
float TempCoefficient=1;
float tempPT1000;

	ADC_Amplitude = getSineFilteredAmplitude(); 

	//Conductance = ADC_Amplitude * 0.0048 - 0.34;
	Conductance = (float)ADC_Amplitude * 0.0047 - 0.236;
	
	tempPT1000 = getTemepraturePT1000();


if ( (tempPT1000 <1) || (tempPT1000>55) ) 
	{
		tempPT1000 = 12;
	}
	TempCoefficient = 1.0 - (tempPT1000-25.0) /50.0; //skaliranje EC na 25°C

                   //     temp. kompezacija------- sa servera kalibr koeficijenti.
 float EC_Temp_Compesated = TempCoefficient * ( ECmeasuring.ecK * Conductance + ECmeasuring.ecM ) ;// vraca vrednost u mS !
 if (EC_Temp_Compesated < 0) EC_Temp_Compesated = 0; 

#ifdef DEBUG_EC
		float amp_Temp_Compesated;
		amp_Temp_Compesated =  (1.0 - (tempPT1000-25.0) /50.0) * ADC_Amplitude; 
		Serial.println(F("------------------------------------"));
		Serial.print(F("EC = ")); Serial.print(EC_Temp_Compesated,4);  Serial.print(F("mS/cm @ 25°C"));Serial.print(F(", Ampl:")); Serial.print(ADC_Amplitude); Serial.print(F(", Conductance = "));Serial.println(Conductance,3); //Serial.print(F(", Ampl.Compesated:")); Serial.print(amp_Temp_Compesated);  Serial.println(F("mS/cm,")); 
		Serial.print(F("Temp:")); Serial.print(tempPT1000); Serial.println(F(" °C"));		
//		Serial.print(F("K = "));  Serial.print(ECmeasuring.ecK);  Serial.print(F(", M = ")); Serial.println(ECmeasuring.ecM);

#endif

 ECmeasuring.measured = EC_Temp_Compesated ;
 return EC_Temp_Compesated ;
}
								
void setConductivityDriver(void){

	pinMode(FSYNC, OUTPUT); 
	
	MD_AD9833 AD(FSYNC); // Hardware SPI
	AD.begin();
	AD.setFrequency(0, 70); //CHAN_0 70Hz
	AD.setMode(1);  									  
	   
	digitalWrite(53, HIGH); //Multiplexer and SIN generator Enabled
	delay(300); //saceka da se ustabili sinusni signal na izlaz/ulaz SIN generatora i operacionog buffer-a	
	
	ECmeasuring.measured = 0.2;
	ECmeasuring.ecK = 1;
	ECmeasuring.ecM = 0;
#ifdef Sensor_EC_PID	
	ECmeasuring.desired  = 0.01; //neka vrednost /=0 ....
	
	ECmeasuring.ecKp = 5;
	ECmeasuring.ecKd = 3;
	ECmeasuring.ecKi = 2;	
	ECmeasuring.CH2_Curr_Strenght =0;
	ECmeasuring.CH3_Curr_Strenght =0;
	ECmeasuring.CH4_Curr_Strenght =0;
//	ECmeasuring.CH5_Curr_Strenght =1;
#endif	
}
           
#endif