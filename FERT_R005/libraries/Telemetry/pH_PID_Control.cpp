#include <Arduino.h>
#include "Telemetry.h"

#if defined(fertPIDregulation) && !defined(Sensor_pH_Isolated)

#define dutyKoeficient 5  // definise koliki je duty PWMa u funkciji razlike zadatog pH i izmerenog.
#define dutyCycle_for_ZeroErrorPh 22 //

#define pH_error 0.18
int pH_PIDloopTimeDelay = 100; 

byte dutyCycle = 17;


extern pH_struct pHmeasuring; 
	 // pH sensor parametri & istorija (dinamika ) pH greske.
		 float errorPh0 = 0; // greska pH vrednosti u trenutoj iteraciji (LOOP-u)
		 float errorPh1 = 0; // greska pH vrednosti u prethodnoj iteraciji (LOOP-u)
		 float errorPh2 = 0;// greska pH vrednosti pre dve iteracije (LOOP-u) .
  extern float PID_dutyCycle=40; //[%]
  extern float PID_dutyCycle_OLD = 40;

float gerDutyCycle_fertPh_PID(float errorPh0){

float Ki = pHmeasuring.ferKi;
float Kd = pHmeasuring.ferKd;
float Kp = pHmeasuring.ferKp;  

	if(  ( - pH_error < errorPh0)  && (errorPh0<pH_error)    ){ //kad je greska zanemarljiva, ne babraj nista.
#ifdef DEBUG_Fertilisation_PID	
		 Serial.println(F("pH PID LOCKED"));
#endif
		return PID_dutyCycle;		
	}
	
	PID_dutyCycle_OLD = PID_dutyCycle;																						
	PID_dutyCycle = (PID_dutyCycle + Kp * ( errorPh0 - errorPh1) + Ki * errorPh0 + Kd * ( errorPh0 - 2*errorPh1 + errorPh2));	//	
	errorPh2 = errorPh1;
	errorPh1 = errorPh0;

	if ( (PID_dutyCycle  - PID_dutyCycle_OLD) > 8 ){ //soft start, da pumpa ne dobije slucajno preveliku vrednos upravljackog signala.
		PID_dutyCycle = PID_dutyCycle_OLD + 8;
	}

	if ( ( PID_dutyCycle_OLD - PID_dutyCycle ) > 8 ){
		PID_dutyCycle = PID_dutyCycle_OLD - 8;
	}

	if (PID_dutyCycle > maxDutyCycle) { PID_dutyCycle = maxDutyCycle;} //ograniciti struju Io = 25mA  max
	if (PID_dutyCycle < minDutyCycle) { PID_dutyCycle = minDutyCycle;} // 4mA, podesiti pumpu za doziranje tako da ne bude premala vrednost, jer bi predugo trajalo djubrenje.
#ifdef DEBUG_Fertilisation_PID	
		 Serial.print(F("pH PID_dutyCycle "));Serial.print(PID_dutyCycle);Serial.print(F(", pH error "));Serial.println(errorPh0);
#endif
	return PID_dutyCycle;
}

//----------------------------------------------------------------------------------------
						//    zeljeni pH
void fertilizationCheck( float pHtarget){ 

float errorPh;

byte loop =1;
while(loop--){

pHmeasuring.pH = getPh(); 

errorPh =  pHmeasuring.pH - pHtarget; // errorPh je u opsegu od -1 do 14.

#ifndef  fertPIDregulation
		 if (errorPh < -1.4  ){   //ako je izmereni pH veci od zadatog za vise od 1.4 ( 10%) odrzi drajver na min upravljanja, 4mA     
		  pinMode(CurrDrvCH1, OUTPUT);
		  digitalWrite(CurrDrvCH1, LOW);	//iskljuci drajver Fertn. i stavi drajver u Hi-Z i
												//izadji iz funkcije
		  dutyCycle = 17; //drzi drajver na 4mA
		  return; // ako je dosegnut nivo pHmeasuring.pH koji je zadat zaustavlja se drajver pumpe i izlazi iz fukncije u nekom narednom ciklusu kad i ako se promeni pHmeasuring.pH nastavice nadjubravanjem.
		}
#endif


	// Samo se za jedan strujni drajver racuna dutyCycle (jedno je pH merenje). 
	    
#ifdef  fertPIDregulation //Ako je ukljucena PID regulacija, izracunava novi faktor ispune za strujni drajver 
	if( ! ((-pH_error < errorPh)  && (errorPh < pH_error))    ){
		dutyCycle = gerDutyCycle_fertPh_PID( errorPh );  
	}

#else //Ako nije ukljucen  PID regulacija																											// 5 
	dutyCycle =  dutyKoeficient * errorPh + dutyCycle_for_ZeroErrorPh ; //value in percente.	
if (dutyCycle > maxDutyCycle) dutyCycle = maxDutyCycle;  //ograniciti struju Io = 20mA  max
if (dutyCycle < minDutyCycle) dutyCycle = minDutyCycle;    // podesiti pumpu za doziranje tako da ne bude premala vrednost, jer bi predugo trajalo djubrenje.
#endif	
	
CurrentDriverSet( dutyCycle, CurrDrvCH1) ;

	if( ! ((-pH_error < errorPh)  && (errorPh < pH_error))    ){	
		delay( pH_PIDloopTimeDelay );
	}
	else{
	pH_PIDloopTimeDelay = 100 ;	
	}

}

#ifdef DEBUG_Fertilisation  
  Serial.println(F("------------------------------------"));  
  Serial.print(F("pH Measured = ")); Serial.print(pHmeasuring.pH); Serial.print(F(", pH Desire  = ")); Serial.print(pHtarget);  Serial.print(F(", Error = ")); Serial.println(errorPh);  
//  Serial.print("Kp = ");Serial.print(pHmeasuring.ferKp);Serial.print(F(" Ki = "));Serial.print(pHmeasuring.ferKi);Serial.print(" Kd = ");Serial.println(pHmeasuring.ferKd);   
  if(  ((-pH_error < errorPh)  && (errorPh < pH_error))    )  Serial.println("pH PID Loop LOCKED");
  Serial.print(F("Current CH1 = ")); Serial.print(0.24 * dutyCycle +0.13 );Serial.print("mA, DutyCycle ");   Serial.print(dutyCycle);Serial.println("%"); 
#endif 
}

#endif



#if defined(fertPIDregulation) && defined(Sensor_pH_Isolated)
 

#define pH_Error 0.2
#define maxCurrent 20  //21mA
#define minCurrent 5   //5mA
 
extern pH_struct pHmeasuring;
//extern float fertCurrentDriverCoeff ;

extern pH_struct pHmeasuring; 
// pH sensor parametri & istorija (dinamika ) pH greske.
float errorPh0 = 0; // greska pH vrednosti u trenutoj iteraciji (LOOP-u)
float errorPh1 = 0; // greska pH vrednosti u prethodnoj iteraciji (LOOP-u)
float errorPh2 = 0;// greska pH vrednosti pre dve iteracije (LOOP-u) .
float errorPh = 0;
byte PID_Current     =12; //12mA
byte PID_Current_OLD = 12;


float gerCurrent_fertPh_PID(float errorPh0){

float Ki = pHmeasuring.ferKi;
float Kd = pHmeasuring.ferKd;
float Kp = pHmeasuring.ferKp;  

	if(  ( - pH_Error < errorPh0)  && (errorPh0<pH_Error)    ){ //kad je greska zanemarljiva, ne babraj nista.
#ifdef DEBUG_Fertilisation_PID	
		 Serial.println(F("pH PID LOCKED"));
#endif
		return PID_Current;		
	}
	
	PID_Current_OLD = PID_Current;																						
	PID_Current = (PID_Current + Kp * ( errorPh0 - errorPh1) + Ki * errorPh0 + Kd * ( errorPh0 - 2*errorPh1 + errorPh2));	//	
	errorPh2 = errorPh1;
	errorPh1 = errorPh0;

	if ( (PID_Current  - PID_Current_OLD) > 4 ){ //soft start, da pumpa ne dobije slucajno preveliku vrednos upravljackog signala.
		PID_Current = PID_Current_OLD + 4;
	}

	if ( ( PID_Current_OLD - PID_Current ) > 4 ){
		PID_Current = PID_Current_OLD - 4;
	}

	if (PID_Current > maxCurrent) { PID_Current = maxCurrent;} //ograniciti struju Io = 20mA  max
	if (PID_Current < minCurrent) { PID_Current = minCurrent;} // 5mA, podesiti pumpu za doziranje tako da ne bude premala vrednost, jer bi predugo trajalo djubrenje.

	return PID_Current;
}

//----------------------------------------------------------------------------------------
						//    zeljeni pH
void fertilizationCheck( float pHtarget){ 

byte current;

pHmeasuring.pH = getPh(); 

errorPh =  pHmeasuring.pH - pHtarget; // errorPh je u opsegu od 1 do 14.
	
	if( ! ((-pH_Error < errorPh)  && (errorPh < pH_Error))    ){
		current = gerCurrent_fertPh_PID( errorPh );  

	}

CurrentDriverSet( current, CurrDrvCH1) ;

#ifdef DEBUG_Fertilisation_PID	
Serial.println(F("------------------------------------"));  
Serial.print(F("pH = ")); Serial.print(pHmeasuring.pH); Serial.print(F(", pH Set = ")); Serial.print(pHtarget);  Serial.print(F(", pH Error = ")); Serial.println(errorPh);  
Serial.print(F("Current CH1 = "));  if (fertilizations[0].fertilization_status == 1){	Serial.print(PID_Current);Serial.println(F("mA"));	} else {	Serial.println(F("OFF")); }
if(  ((-pH_Error < errorPh)  && (errorPh < pH_Error))    ){Serial.println("pH PID Loop LOCKED");}
Serial.println(); 
#endif

}

byte pH_PID_Locked(void){
if(  ((-pH_Error < errorPh)  && (errorPh < pH_Error))    ){return 1;}
else {return 0;}	
	
	
}

void ADCsetup(void){

PORTJ = PORTJ | 0x10; // ADC_CS <HIGH>
DDRJ  = DDRJ  | 0x10; //  <OUT> 	
PORTJ = PORTJ | 0x10; //  <HIGH>

PORTJ = PORTJ | 0x08; // ADC_CLK <HIGH>
DDRJ  = DDRJ  | 0x08; //  <OUT> 	
PORTJ = PORTJ | 0x08; //  <CLK HIGH>

PORTJ = PORTJ | 0x04; // ADC_DATA <HIGH>
DDRJ  = DDRJ  & 0xFB; //  <IN> 	


	
}



float getPhFromADC(void)
{  

  // Pull CS low to select the MAX1075
	PORTJ = PORTJ & 0xEF; //  <LOW>
	delay(1); // Small delay
	PORTJ = PORTJ & 0xF7;//CLK LOW
	
	unsigned int result = 0;
	
int i = 0;
  for ( i = 0; i<3; i++) {
    
	PORTJ = PORTJ | 0x08; //  <CLK HIGH>
	delayMicroseconds(100); // Small delay
	PORTJ = PORTJ & 0xF7;//CLK LOW
	//Serial.print("i ");Serial.println(i);
	delayMicroseconds(100); // Small delay
  }

  for (i = 9; i >= 0; i--) {
    
	PORTJ = PORTJ | 0x08; //  <CLK HIGH>
	delayMicroseconds(100); // Small delay
	PORTJ = PORTJ & 0xF7;//CLK LOW
	delayMicroseconds(100); // Small delay
	byte n = (PINJ & 0x04) >> 2;	
	//Serial.print("i ");Serial.println(i);
    result |= n << i;
    delayMicroseconds(100); // Small delay
  }
  
  
    for (i = 0; i<3; i++) {
    
	PORTJ = PORTJ | 0x08; //  <CLK HIGH>
	delayMicroseconds(100); // Small delay
	PORTJ = PORTJ & 0xF7;//CLK LOW
	//Serial.print("i ");Serial.println(i);
	delayMicroseconds(50); // Small delay
  }
    
	delayMicroseconds(100); // Small delay
	PORTJ = PORTJ | 0x10; // ADC_CS <HIGH>
	delayMicroseconds(100); // Small delay
	PORTJ = PORTJ | 0x08; //  <CLK HIGH>

 	//RAW pH
	pHmeasuring.rawPh =  result * 2;
	//Serial.print("RAW pH =");Serial.println(result);						
  return  (pHmeasuring.rawPh) *  pHmeasuring.ferphK   / 102.3  + pHmeasuring.ferphM;	  //(ADDC/2 * 5/1023) = 1/204.6 konverzija  iz digitalne vrednosti u realnu vrednost napona.
}


#endif