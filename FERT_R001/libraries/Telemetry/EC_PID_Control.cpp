#include <Arduino.h>
#include "Telemetry.h"

#define EC_error 0.02

#ifdef Sensor_EC_PID
// EC sensor parametri & istorija (dinamika ) EC greske.
 float errorEC0 = 0;
 float errorEC1 = 0;
 float errorEC2 = 0;
 extern float EC_PID_dutyCycle     = 10; //[%]
 extern float EC_PID_dutyCycle_OLD = 0;
 
 extern EC_struct ECmeasuring;

//void EC_PID_Control_Checking( void){
void EC_PID_Control_Checking(byte CurrDrvChanPin, float  ECdesired){

	
float Ki = ECmeasuring.ecKi;
float Kd = ECmeasuring.ecKd;
float Kp = ECmeasuring.ecKp;

	errorEC0 = ECdesired - ECmeasuring.measured;  
//Ako je greska merenja u nekim razumljivim niskim granicama, prekosci obradu........
	if(  (-EC_error < errorEC0)  && (errorEC0 < EC_error)    ){ 
		return;		
	}
	
	EC_PID_dutyCycle_OLD = EC_PID_dutyCycle;																						
	EC_PID_dutyCycle = (EC_PID_dutyCycle + 3 * Kp * ( errorEC0 - errorEC1) + 2 * Ki * errorEC0 + Kd * ( errorEC0 - 2*errorEC1 + errorEC2));	//	
	errorEC2 = errorEC1;
	errorEC1 = errorEC0;

	if ( (EC_PID_dutyCycle  - EC_PID_dutyCycle_OLD) > 10 ){ //soft start, da pumpa ne dobije slucajno preveliku vrednos upravljackog signala.
		EC_PID_dutyCycle = EC_PID_dutyCycle_OLD + 10;
	}

	if ( ( EC_PID_dutyCycle_OLD - EC_PID_dutyCycle ) > 10 ){
		EC_PID_dutyCycle = EC_PID_dutyCycle_OLD - 10;
	}

	if (EC_PID_dutyCycle > maxDutyCycle) { EC_PID_dutyCycle = maxDutyCycle;} //ograniciti struju Io = 25mA  max
	if (EC_PID_dutyCycle < minDutyCycle) { EC_PID_dutyCycle = minDutyCycle;} // 4mA, podesiti pumpu za doziranje tako da ne bude premala vrednost, jer bi predugo trajalo djubrenje.
	
	//CH2
	if(CurrDrvChanPin == 3)	CurrentDriverSet(EC_PID_dutyCycle * ECmeasuring.CH2_Curr_Strenght, CurrDrvCH2); // glavni upravljacki strujni drajver
	//CH3
	if(CurrDrvChanPin == 6)CurrentDriverSet(EC_PID_dutyCycle * ECmeasuring.CH3_Curr_Strenght, CurrDrvCH3); // dodatni drajver, za buducu funkcionalnost

#ifdef DEBUG_EC
		Serial.println(F("-----------------------------------------"));
//		Serial.print(F("Temp: "));  Serial.print(tempPT1000); Serial.print(" °C");
//		Serial.print(F(", Conduct: ")); Serial.print(Conductance); Serial.print(F("mS/cm,  ")); Serial.print(Conuctance_temp);  Serial.println(F("mS/cm @ 25°C"));	
		Serial.print(F("Measured ")); Serial.print(ECmeasuring.measured); Serial.print(F(", Desire ")); Serial.print(ECmeasuring.desired);Serial.print(F(", Error ")); Serial.println( ECmeasuring.desired - ECmeasuring.measured );
		Serial.print(F("K = ")); Serial.print (ECmeasuring.ecK);Serial.print(F(", M = ")); Serial.print(ECmeasuring.ecM);
		Serial.print (". Kp ");Serial.print (ECmeasuring.ecKp);Serial.print (", Ki ");Serial.print (ECmeasuring.ecKi);Serial.print (", Kd ");Serial.println (ECmeasuring.ecKd);
		Serial.print(F("Current OUT =")); Serial.print(0.24 * EC_PID_dutyCycle + 0.13 );Serial.print("mA  (");   Serial.print(EC_PID_dutyCycle);Serial.print("%)");
		Serial.print(F(" = CH2 [")); Serial.print(ECmeasuring.CH2_Curr_Strenght * (0.24 * EC_PID_dutyCycle + 0.13) );Serial.print("mA] + CH3 [");Serial.print(ECmeasuring.CH3_Curr_Strenght * (0.24 * EC_PID_dutyCycle + 0.13) );Serial.println("mA]");
		Serial.println(F("-----------------------------------------"));
#endif

	
}

#endif