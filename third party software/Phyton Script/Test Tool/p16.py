import tkinter as tk
from tkinter import ttk
import serial
import serial.tools.list_ports
from threading import Thread
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.dates as mdates
import urllib.parse
import os
from PIL import ImageGrab

class SerialReader:
    def __init__(self):
        self.ser = None
        self.running = False

    def connect(self, port, baudrate):
        if self.ser:
            self.ser.close()
        self.ser = serial.Serial(port, baudrate)
        self.running = True

    def disconnect(self):
        if self.ser:
            self.running = False
            self.ser.close()
            self.ser = None

    def read_data(self, callback):
        while self.running:
            if self.ser and self.ser.in_waiting:
                try:
                    data = self.ser.readline().decode().strip()
                    callback(data)
                except Exception as e:
                    print(f"Error reading data: {e}")

class Application(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("SmartWatering Data Logger")

        self.serial_reader = SerialReader()
        self.battery_voltage_data = []
        self.time_data = []
        self.start_time = datetime.now()

        self.create_widgets()
        self.update_ports()

    def create_widgets(self):
        font_style_sensors = ("TkDefaultFont", 11, "bold")  # 30% smaller font for sensor data
        font_style_device_info = ("TkDefaultFont", 13, "bold")  # Standard font for device information

        # Connection settings
        self.port_label = tk.Label(self, text="COM Port:", font=font_style_device_info)
        self.port_label.grid(row=0, column=0, padx=5, pady=5, sticky=tk.W)

        self.port_var = tk.StringVar()
        self.port_menu = ttk.Combobox(self, textvariable=self.port_var)
        self.port_menu.grid(row=0, column=1, padx=5, pady=5, sticky=tk.W)

        self.connect_button = tk.Button(self, text="Connect", command=self.toggle_connect, font=font_style_device_info)
        self.connect_button.grid(row=0, column=2, padx=5, pady=5, sticky=tk.W)

        # Device Information (Device, Revision, Serial No.)
        self.device_info_frame = tk.Frame(self)
        self.device_info_frame.grid(row=1, column=0, columnspan=6, padx=5, pady=5, sticky=tk.W)

        self.device_label = tk.Label(self.device_info_frame, text="Device:", font=font_style_device_info)
        self.device_label.grid(row=0, column=0, padx=5, pady=5, sticky=tk.W)
        self.device_var = tk.StringVar(value="N/A")
        self.device_menu = ttk.Combobox(self.device_info_frame, textvariable=self.device_var, values=["FERT", "SENS", "START"], font=font_style_device_info, width=10)
        self.device_menu.grid(row=0, column=1, padx=5, pady=5, sticky=tk.W)

        self.revision_label = tk.Label(self.device_info_frame, text="Revision:", font=font_style_device_info)
        self.revision_label.grid(row=0, column=2, padx=5, pady=5, sticky=tk.W)
        self.revision_var = tk.StringVar(value="R001")
        self.revision_menu = ttk.Combobox(self.device_info_frame, textvariable=self.revision_var, values=["R001", "R002", "R003", "R004", "R005", "R006", "R007"], font=font_style_device_info, width=10)
        self.revision_menu.grid(row=0, column=3, padx=5, pady=5, sticky=tk.W)

        self.serial_no_label = tk.Label(self.device_info_frame, text="Serial No.:", font=font_style_device_info)
        self.serial_no_label.grid(row=0, column=4, padx=5, pady=5, sticky=tk.W)
        self.serial_no_var = tk.StringVar(value="N/A")
        self.serial_no_entry = tk.Entry(self.device_info_frame, textvariable=self.serial_no_var, font=font_style_device_info, width=10)
        self.serial_no_entry.grid(row=0, column=5, padx=5, pady=5, sticky=tk.W)

        # Sensor data organized in the specified order
        sensors = [
            ('t1', '°C'), ('t2', '°C'), ('t3', '°C'), 
            ('m1', '%'), ('m2', '%'), ('m3', '%'), 
            ('s1', ''), ('s2', ''), ('s3', ''), 
            ('t4', '°C'), ('t5', '°C'), ('t6', '°C'), 
            ('m4', '%'), ('m5', '%'), ('m6', '%'), 
            ('s4', ''), ('s5', ''), ('s6', ''), 
            ('Battery', 'V'), ('Solar', 'V'), ('Air Humidity', '%'), 
            ('Air Temperature', '°C'), ('FLOW', 'L/min'), ('Wind Direction', '°'), 
            ('Wind Speed', 'm/s'), ('Sun Radiation', 'W/m²'), ('Rain Fall', 'mm'), 
            ('ROW pH', ''), ('RSSI', 'dBm'), 
            ('h1', ''), ('h2', '')
        ]

        self.data_vars = {key: tk.StringVar(value="N/A") for key, _ in sensors}
        self.pass_fail_vars = {key: tk.StringVar(value="N/A") for key, _ in sensors}

        # Display sensors on the left side
        row = 2
        for idx, (label, unit) in enumerate(sensors):
            tk.Label(self, text=label, font=font_style_sensors).grid(row=row, column=0, padx=5, pady=2, sticky=tk.W)
            value_label = tk.Label(self, textvariable=self.data_vars[label], font=font_style_sensors)
            value_label.grid(row=row, column=1, padx=5, pady=2, sticky=tk.W)
            tk.Label(self, text=unit, font=font_style_sensors).grid(row=row, column=2, padx=5, pady=2, sticky=tk.W)
            pass_fail_label = tk.Label(self, textvariable=self.pass_fail_vars[label], font=font_style_sensors)
            pass_fail_label.grid(row=row, column=3, padx=5, pady=2, sticky=tk.W)
            self.pass_fail_vars[label + '_label'] = pass_fail_label
            self.data_vars[label + '_label'] = value_label
            row += 1

        # Battery voltage graph aligned to the right
        self.fig, self.ax = plt.subplots(figsize=(5, 4))
        self.canvas = FigureCanvasTkAgg(self.fig, master=self)
        self.canvas.get_tk_widget().grid(row=2, column=4, rowspan=20, padx=5, pady=5, sticky=tk.N + tk.S + tk.E + tk.W)
        self.ax.set_ylim(10.2, 12.8)
        self.ax.set_xlim(0, 60)  # Set X-axis from 0 to 60 minutes
        self.ax.set_xlabel('Time (minutes)')
        self.ax.set_ylabel('Voltage (V)')
        self.ax.set_title('Battery Voltage Graph')
        self.ax.xaxis.set_major_locator(plt.MultipleLocator(10))  # Major ticks every 10 minutes
        self.ax.xaxis.set_minor_locator(plt.MultipleLocator(1))  # Minor ticks every 1 minute
        self.ax.grid(True, which='both')

        # Generate Report button
        self.generate_report_button = tk.Button(self, text="Generate Report", command=self.generate_report, font=font_style_device_info)
        self.generate_report_button.grid(row=0, column=5, padx=5, pady=5)

    def update_ports(self):
        ports = [port.device for port in serial.tools.list_ports.comports()]
        self.port_menu['values'] = ports

    def toggle_connect(self):
        if self.connect_button.cget('text') == 'Connect':
            self.connect()
        else:
            self.disconnect()

    def connect(self):
        port = self.port_var.get()
        if port:
            self.serial_reader.connect(port, 115200)
            self.connect_button.config(text='Disconnect', bg='red')
            self.start_reading()

    def disconnect(self):
        self.serial_reader.disconnect()
        self.connect_button.config(text='Connect', bg='green')

    def start_reading(self):
        Thread(target=self.serial_reader.read_data, args=(self.update_data,)).start()

    def update_data(self, data):
        # Parse data and update sensor values
        parsed_data = self.parse_data(data)
        current_time = datetime.now()

        # Update sensor values
        for key, value in parsed_data.items():
            if key in self.data_vars:
                self.data_vars[key].set(value)
                if key == 'RSSI':
                    if float(value) < -90:
                        self.pass_fail_vars[key].set('FAIL')
                        self.pass_fail_vars[key + '_label'].config(fg='red')
                    else:
                        self.pass_fail_vars[key].set('PASS')
                        self.pass_fail_vars[key + '_label'].config(fg='green')
                elif key == 'FLOW':
                    if 57 <= float(value) <= 63:
                        self.pass_fail_vars[key].set('PASS')
                        self.pass_fail_vars[key + '_label'].config(fg='green')
                    else:
                        self.pass_fail_vars[key].set('FAIL')
                        self.pass_fail_vars[key + '_label'].config(fg='red')
                elif key == 'ROW pH':
                    if 300 <= float(value) <= 400:
                        self.pass_fail_vars[key].set('PASS')
                        self.pass_fail_vars[key + '_label'].config(fg='green')
                    else:
                        self.pass_fail_vars[key].set('FAIL')
                        self.pass_fail_vars[key + '_label'].config(fg='red')
                elif key == 'Air Temperature':
                    if 1 <= float(value) <= 5:
                        self.pass_fail_vars[key].set('PASS')
                        self.pass_fail_vars[key + '_label'].config(fg='green')
                    else:
                        self.pass_fail_vars[key].set('FAIL')
                        self.pass_fail_vars[key + '_label'].config(fg='red')
                elif key == 'Air Humidity':
                    if 30 <= float(value) <= 36:
                        self.pass_fail_vars[key].set('PASS')
                        self.pass_fail_vars[key + '_label'].config(fg='green')
                    else:
                        self.pass_fail_vars[key].set('FAIL')
                        self.pass_fail_vars[key + '_label'].config(fg='red')
                elif key == 'h1':
                    if 650 <= float(value) <= 700:
                        self.pass_fail_vars[key].set('PASS')
                        self.pass_fail_vars[key + '_label'].config(fg='green')
                    else:
                        self.pass_fail_vars[key].set('FAIL')
                        self.pass_fail_vars[key + '_label'].config(fg='red')
                elif key == 'h2':
                    if 650 <= float(value) <= 700:
                        self.pass_fail_vars[key].set('PASS')
                        self.pass_fail_vars[key + '_label'].config(fg='green')
                    else:
                        self.pass_fail_vars[key].set('FAIL')
                        self.pass_fail_vars[key + '_label'].config(fg='red')
                elif key == 'Wind Direction':
                    if 300 <= float(value) <= 360:
                        self.pass_fail_vars[key].set('PASS')
                        self.pass_fail_vars[key + '_label'].config(fg='green')
                    else:
                        self.pass_fail_vars[key].set('FAIL')
                        self.pass_fail_vars[key + '_label'].config(fg='red')
                elif key == 'Wind Speed':
                    if 300 <= float(value) <= 360:
                        self.pass_fail_vars[key].set('PASS')
                        self.pass_fail_vars[key + '_label'].config(fg='green')
                    else:
                        self.pass_fail_vars[key].set('FAIL')
                        self.pass_fail_vars[key + '_label'].config(fg='red')
                elif key == 'Sun Radiation':
                    if 300 <= float(value) <= 360:
                        self.pass_fail_vars[key].set('PASS')
                        self.pass_fail_vars[key + '_label'].config(fg='green')
                    else:
                        self.pass_fail_vars[key].set('FAIL')
                        self.pass_fail_vars[key + '_label'].config(fg='red')
                elif key == 'Battery':
                    if 10.2 <= float(value) <= 12.65:
                        self.pass_fail_vars[key].set('PASS')
                        self.pass_fail_vars[key + '_label'].config(fg='green')
                    else:
                        self.pass_fail_vars[key].set('FAIL')
                        self.pass_fail_vars[key + '_label'].config(fg='red')
                elif key == 'Solar':
                    if 20 <= float(value) <= 24:
                        self.pass_fail_vars[key].set('PASS')
                        self.pass_fail_vars[key + '_label'].config(fg='green')
                    else:
                        self.pass_fail_vars[key].set('FAIL')
                        self.pass_fail_vars[key + '_label'].config(fg='red')
                elif key == 't1':
                    if 20 <= float(value) <= 35:
                        self.pass_fail_vars[key].set('PASS')
                        self.pass_fail_vars[key + '_label'].config(fg='green')
                    else:
                        self.pass_fail_vars[key].set('FAIL')
                        self.pass_fail_vars[key + '_label'].config(fg='red')
                elif key == 't2':
                    if 20 <= float(value) <= 35:
                        self.pass_fail_vars[key].set('PASS')
                        self.pass_fail_vars[key + '_label'].config(fg='green')
                    else:
                        self.pass_fail_vars[key].set('FAIL')
                        self.pass_fail_vars[key + '_label'].config(fg='red')
                elif key == 't3':
                    if 20 <= float(value) <= 35:
                        self.pass_fail_vars[key].set('PASS')
                        self.pass_fail_vars[key + '_label'].config(fg='green')
                    else:
                        self.pass_fail_vars[key].set('FAIL')
                        self.pass_fail_vars[key + '_label'].config(fg='red')
                else:
                    if float(value) > 5:
                        self.pass_fail_vars[key].set('PASS')
                        self.pass_fail_vars[key + '_label'].config(fg='green')
                    else:
                        self.pass_fail_vars[key].set('FAIL')
                        self.pass_fail_vars[key + '_label'].config(fg='red')

        # Update battery voltage graph
        if 'Battery' in parsed_data:
            battery_voltage = float(parsed_data['Battery'])
            self.battery_voltage_data.append(battery_voltage)
            elapsed_time = (current_time - self.start_time).total_seconds() / 60.0
            self.time_data.append(elapsed_time)
            self.update_graph()

    def parse_data(self, data):
        parsed_result = {}
        try:
            # Parsing format "pow=12.37&sol=22.50"
            if "pow=" in data and "sol=" in data:
                pairs = data.split('&')
                for pair in pairs:
                    if '=' in pair:
                        key, value = pair.split('=')
                        if key == 'pow':
                            parsed_result['Battery'] = value
                        elif key == 'sol':
                            parsed_result['Solar'] = value

            # Generic sensor parsing logic
            params = urllib.parse.parse_qs(data)
            fields = {
                't1': 't1', 't2': 't2', 't3': 't3', 't4': 't4', 't5': 't5', 't6': 't6',
                'm1': 'm1', 'm2': 'm2', 'm3': 'm3', 'm4': 'm4', 'm5': 'm5', 'm6': 'm6',
                's1': 's1', 's2': 's2', 's3': 's3', 's4': 's4', 's5': 's5', 's6': 's6',
                'pow': 'Battery', 'sol': 'Solar', 'ah': 'Air Humidity', 'at': 'Air Temperature',
                'cfl': 'FLOW', 'wd': 'Wind Direction', 'ws': 'Wind Speed',
                'sr': 'Sun Radiation', 'rf': 'Rain Fall', 'ferph': 'ROW pH', 'RSSI': 'RSSI',
                'h1': 'h1', 'h2': 'h2'
            }
            for key, label in fields.items():
                if key in params:
                    parsed_result[label] = params[key][0]
        except Exception as e:
            print(f"Error parsing data: {e}")
        return parsed_result

    def update_graph(self):
        self.ax.clear()
        self.ax.set_ylim(10.2, 12.8)
        self.ax.set_xlim(0, 60)
        self.ax.set_xlabel('Time (minutes)')
        self.ax.set_ylabel('Voltage (V)')
        self.ax.set_title('Battery Voltage Graph')
        self.ax.grid(True, which='both')
        self.ax.plot(self.time_data, self.battery_voltage_data, color='blue')
        self.canvas.draw()

    def generate_report(self):
        # Generate a screenshot of the entire application window
        x = self.winfo_rootx()
        y = self.winfo_rooty()
        width = self.winfo_width()
        height = self.winfo_height()

        # Take the screenshot using ImageGrab from PIL
        screenshot = ImageGrab.grab(bbox=(x, y, x + width, y + height))

        # Build the filename using the format Device_Revision_Serial No
        device = self.device_var.get()
        revision = self.revision_var.get()
        serial_no = self.serial_no_var.get()

        # Remove spaces or invalid characters for filenames
        device_clean = device.replace(" ", "_")
        revision_clean = revision.replace(" ", "_")
        serial_no_clean = serial_no.replace(" ", "_")

        # Base filename
        base_filename = f"{device_clean}_{revision_clean}_{serial_no_clean}.jpg"
        filename = base_filename

        # Check if a file with the same name exists and increment the suffix if needed
        file_counter = 1
        while os.path.exists(filename):
            filename = f"{device_clean}_{revision_clean}_{serial_no_clean}_{file_counter}.jpg"
            file_counter += 1

        # Save the screenshot
        screenshot.save(os.path.join(os.getcwd(), filename))

        print(f"Screenshot saved as {filename}")

if __name__ == "__main__":
    app = Application()
    app.mainloop()
