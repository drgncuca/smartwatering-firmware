#include <Arduino.h>
#include "Telemetry.h"

extern pH_struct pHmeasuring;

int dutyCycle=15;

float getPh()
{
  int i = 1;
  long sum = 0;
  while (i <= 10)
  {
    sum = sum + analogRead(ph_sensor_pin);
    delay(20);
    i++;
  }
 	//RAW pH
	pHmeasuring.rawPh =  sum * 0.1;
	//Serial.print(" RAW pH =");Serial.println(pHmeasuring.rawPh);						
  return  (pHmeasuring.rawPh) *  pHmeasuring.ferphK   / 204.6  + pHmeasuring.ferphM;	  //(ADDC * 5/1023) = 1/204.6 konverzija  iz digitalne vrednosti u realnu vrednost napona.
}



