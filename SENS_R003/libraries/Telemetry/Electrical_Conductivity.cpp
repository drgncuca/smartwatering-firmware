#include <Arduino.h>
#include <SPI.h>
#include <MD_AD9833.h>
#include "DS3231M.h"

#include "Telemetry.h"

#define ampUperLimit  485

#ifdef Sensor_EC

extern float EC_calibr_Coeff   = 1; // inicijalna vrednos 1, sve dok se sa servera ne promeni
extern float EC_calibr_M_Coeff = 0;

float tempPT1000;

//initialise data for Low Pass Filter function. (SIN signal 70Hz-a)
int          temp[NMBRsampling];
int data_filtered[NMBRsampling];

#define Sin_Buff_Leng 5
int Sin_Amplitud_Filt[Sin_Buff_Leng];
int Sin_Amp_Fil_counter=0; 
bool empty_Sin_Amplitud_Filt = true;


void sampling(void){
	
	int numberSampling = NMBRsampling-1;
     while (numberSampling--){//max time sampling 0.42mS !!!     
     temp[numberSampling] = analogRead(A6);
	 //temp[numberSampling] = analogRead(A7); // za debagovanje
	 //Serial.println(temp[numberSampling]);
	 //Serial.println(analogRead(A6));
    } 	
}

void lowPassFiltar()
{	
const float alpha = 0.15 ;
int sampls=1; 
data_filtered[0] = temp[0];

while(sampls < NMBRsampling){  
   data_filtered[sampls] = (1-alpha) * data_filtered[sampls-1] + alpha*(temp[sampls]  + temp[sampls-1])/2.0 ; 
	//data_filtered[sampls] = 0.8 * data_filtered[sampls-1] + 0.1*(temp[sampls]  + temp[sampls-1]) ;	
	//temp[sampls] = (1-alpha) * (float)temp[sampls-1] + alpha*((float)temp[sampls]  + (float)temp[sampls-1])/2.0 ; 		
    data_filtered[sampls-1] = data_filtered[sampls];	
	//temp[sampls-1] = temp[sampls];	
	//Serial.println((int)data_filtered[sampls]);
	//Serial.print(" ");
	//Serial.println(temp[sampls]);		
	sampls++;
}
}

int getSinAmplitude(){
	int sineMin=1023,sineMax=0;
	
	int numberSampling = NMBRsampling-1;	
 	
	while(numberSampling--)//trazi min i max SINusuide
	{
		if (temp[numberSampling] > sineMax ) {sineMax = temp[numberSampling] ;}//searching for max of SINE signal
        if (temp[numberSampling] < sineMin ) {sineMin = temp[numberSampling] ;}//searching for min of SINE signal
	}  	
 
return (sineMax - sineMin);
}
								// 70Hz         1- Sinus 
void setConductivityDriver(int frequency, int signalShape){
 
	MD_AD9833 AD(FSYNC); // Hardware SPI
	AD.begin();
	AD.setFrequency(0, frequency); //CHAN_0 100Hz
	AD.setMode(signalShape);                  
                                         // (10M) 
                       //Rmux   0.51k  1k5    3K     5K1
 pinMode(44, OUTPUT);  //A0      0     1      0      1
 pinMode(45, OUTPUT);  //A1      0     0      1      1
 
 pinMode(53, OUTPUT);  //Multiplexer EN  
 digitalWrite(53, HIGH); 

}

int getSineFilteredAmplitude(void){
	
int numberAveraging = NMBRaveraging; //usrednjavanje min i max vrednosti amplitude	 
int tmp_ampl = 0;	
 while (numberAveraging--){	
	
 digitalWrite(53, LOW); // signal SINUS startuje sa NULA vrednoscu
 delay(3);
 digitalWrite(53, HIGH);
 delay(3); 

 sampling(); 

 lowPassFiltar();
 
 tmp_ampl =  tmp_ampl + getSinAmplitude(); // vraca digitalnu vrednost amplitude SINusnog signala
 
}
return tmp_ampl / NMBRaveraging;
  
}
//--------------------------------------------------------------------------------------------------------
float getELconductance(void){

int amp;
int MuxSwitch = 0;
//float Resistance  = 0;
float Conductance  = 0;
float TempCoefficient=1;

digitalWrite(53, HIGH); //Enable Multiplexer and SIN generator

//----------trazi max opseg merenja-----------// amp[]={510, 1.5k, 3k, 5k1}

digitalWrite(44, LOW);  
digitalWrite(45, LOW);
amp = getSineFilteredAmplitude(); 

 if(empty_Sin_Amplitud_Filt == true) { // napuni buffer nekim frednostima da ubrza smirivanje conuctivity signala
	 for(int i=0;i<Sin_Buff_Leng;i++){
		Sin_Amplitud_Filt[i] = amp;	 
	 }
	 empty_Sin_Amplitud_Filt = false;
 }
 
 Sin_Amplitud_Filt[Sin_Amp_Fil_counter] = amp;
 Sin_Amp_Fil_counter++;
 if (Sin_Amp_Fil_counter == Sin_Buff_Leng) Sin_Amp_Fil_counter = 0;
 
 int Ampl_Temp = 0; 
 for(int x=0;x<Sin_Buff_Leng;x++){
		Ampl_Temp += Sin_Amplitud_Filt[x] ;	 
	 }
	 
 Ampl_Temp = Ampl_Temp / Sin_Buff_Leng;
 
	Conductance = Ampl_Temp * 0.007 - 0.269; 

	tempPT1000 = getTemepraturePT1000();

#ifdef BOARD_FERT_R001	     // sa I2C senzora
	if ( (temperature <0) || (temperature>45) ) 
	{
		temperature = 25.01; // .01 je zbog debagovanja - minorna greska u kalkulaciji EC-a
	}
	TempCoefficient = 1 - (temperature-25) /50; //skaliranje EC na 25°C	
#endif

#if defined(BOARD_FERT_R002) || defined(BOARD_SENS_R001)
if ( (tempPT1000 <0) || (tempPT1000>45) ) 
	{
		tempPT1000 = 25.02;
	}
	TempCoefficient = 1 - (tempPT1000-25) /50; //skaliranje EC na 25°C
#endif	
	
#ifdef DEBUG_EC
	Serial.println("-------------------------------------------------------------");
  //Serial.print(F("Mux SW :"));Serial.println(MuxSwitch);  Serial.print(F("ampUperLimit - "));Serial.println(ampUperLimit);
	Serial.print(F("SIN Amp: ")); Serial.print(amp); Serial.print(F(" Ampl: "));Serial.print((5.00 * amp )/1023); Serial.print(" V, "); Serial.print("Average SIN Ampl: "); Serial.println( Ampl_Temp);
//	Serial.print(F("Resist : ")); Serial.print(Resistance );  Serial.println(F(" ohm"));
	Serial.print(F("Conduct: ")); Serial.print(Conductance );  Serial.println(F(" mS/cm"));
	#ifdef BOARD_FERT_R001	     // sa I2C senzora
		Serial.print(F("Temper I2C : "));  Serial.print(temperature); Serial.println(" °C");
	#endif
	#if defined(BOARD_FERT_R002) || defined(BOARD_SENS_R001)
		Serial.print(F("Temper PT100: "));  Serial.print(tempPT1000); Serial.println(" °C");
	#endif	
	Serial.print(F("Conduct: ")); Serial.print(Conductance * TempCoefficient );  Serial.println(F(" mS/cm @ 25°C"));
	Serial.print(F("k = ")); Serial.print (EC_calibr_Coeff);Serial.print(F(", M = ")); Serial.println(EC_calibr_M_Coeff);
#endif

//     temp. kompezacija------- sa servera kalibr koef.---u [mS]------otpornost
 float temp_rez = TempCoefficient * EC_calibr_Coeff * Conductance + EC_calibr_M_Coeff;// vraca vrednost u mS !
 if (temp_rez < 0) return 0;
 
 return temp_rez;
}

           
#endif