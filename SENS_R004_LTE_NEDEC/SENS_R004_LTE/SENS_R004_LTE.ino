#include <EEPROM.h>
#include "EEPROMAnything.h"
#include <SoftwareSerial.h>
#include <Wire.h>
#include <TimeLib.h>
#include <ArduinoJson.h>
//#include "SIM900.h"
#include "ds3231.h"
//#include "inetGSM.h"
//#include "sms.h"
#include <String.h>
#include "Telemetry.h"					
#include <SPI.h>
#include <Adafruit_MAX31865.h>

#define FirwareVersion "FV405N"

#ifdef Sensor_EC
  Adafruit_MAX31865 TemperaturePT1000 = Adafruit_MAX31865(49);
#endif

#ifdef INTERRUPTFLOW
  #include <FlowMeter.h>
#endif  

//InetGSM inet;
//TODO Check max size of buffer
StaticJsonBuffer<650> jsonBuffer;
int numdata;
boolean gsm_started = false;


bool isperm3 = false;
bool isautomode = false;
bool is_some_irrigation_on = false;

//char server[] = "app.smartwatering.rs";


char parcel_id[] = "rjTXqd"; // Testing Single Device FERT+Zone, Kruska NS Dragan Cuca!!
//char parcel_id[] = "JxRKeA";//Test Veternik SENS1
//char parcel_id[] = "1Ua2n1";//Test Veternik SENS2 
	
//char parcel_id[] = "vZn2YY";//AgriCraftersp Rumunija

//char parcel_id[] = "VWarVU";// Saudijska Arabija A001
//char parcel_id[] = "9GZniN";// Saudijska Arabija A002 [Ventili PILOT, CONTROL]
//char parcel_id[] = "gIiavq";// Saudijska Arabija A003
//char parcel_id[] = "6PMrLt";// Saudijska Arabija A004
//char parcel_id[] = "yYXys3";// Saudijska Arabija A005
//char parcel_id[] = "WhqOrk";// Saudijska Arabija A006
//char parcel_id[] = "UR2n3f";// Saudijska Arabija A007	
//char parcel_id[] = "iMn9nR";// Saudijska Arabija A008
//char parcel_id[] = "4Y9Tdp";// Saudijska Arabija A009
//char parcel_id[] = "rFk2n1";// Saudijska Arabija A010
//char parcel_id[] = "4yOkEn";// Saudijska Arabija A011
//char parcel_id[] = "jbOvin";// Saudijska Arabija A012

//char parcel_id[] = "NEVYbw";//"Jorge Duarte Portugal"




#ifdef Power_Saving
byte PowerStandByTimeInMin = 0;
byte BatterySupplying = 0;
#endif

#ifdef LiPo_Charger

float BatteryVoltage = 0;
byte BattCharging = 0; // Status  baterije
int BatteryCChargingCurrent = 0;
unsigned int setChargingCurrent = 128 ;								
#endif

struct irrigation_struct
{
  unsigned int duration = 0; // vreme trajanja navodnjavanja u sekundama
  unsigned long amount = 0L; // Kolicina vode u litrima
  byte status[4] = {0, 0, 0, 0}; // 0 = off, 1 = on
  unsigned long started_time =  0L;// u sekundama
//  unsigned int max_duration = 0; // max vreme navodnjavanja po zapremini, zastitni parametar.
  String irrigation_id = "";
  float current = 0;// kolicinu vode u litrima.
  //float current = 0L; 
} zonal_irrigation_struct;


//pH global variabla
pH_struct pHmeasuring;

irrigation_struct  zonal_irrigations[NUM_SLOTS];

irrigation_struct_FlowMetering  irrigationFlowMetering ; // Globalna promenljiva koja sadrzi informacije o protekloj kolicini VODE na 5 kanalala flowMeter-a i proteklo vreme.

byte number_of_zones = 4;//

#ifdef DC_Valves  //DC1_P  2   3   4
  byte valves_on[]  = {33, 31, 41, 39};
                   //DC1_N  2   3   4
  byte valves_off[] = {32, 30, 40, 75};
  int valvedelay = 100; // neka vrednost po difoultu u slucaju da zakaze komunikacija sa serverom 
#endif


	#define TransitionTime 60 //Transition time izmedju preklapanja CONTROL i PILOT zone, u sekundama

	unsigned long control_pilot_transition_timer =  0L;// u sekundama

	byte control_pilot_status = 0; 
	/*
	0- ALL OFF
	1- control ON,         pilot OFF
	2- pilot OFF->ON    -> control ON->OFF
	3- control OFF,        pilot ON
	4- control OFF->ON  -> pilot OFF

	*/

 
 
#ifdef FLOW_Toggling
 byte flow_pin_Toggling = 72;// za SENS -> FLOW.
#endif

float el_conductance = 0;

//----------Temperature and Humidity-----
#ifdef SHT1sensor
  float temperature;
  float humidity;
  SHT1x SHT1x_sensor(   20  ,   21);
#endif

float ph_value = 0;

#define arduino_restart_pin  12

String params = "";
//TODO: Check max size of the message and reduce it's size accordingly
char msg[MSG_BUF]; 

carrier carrierdata;
struct_operater_vip operaterVIP;
struct_operater_MTS operaterMTS;
struct_operater_Telenor operaterTelenor;
struct_operater_BHmob operaterBHmob;	
struct_operater_MTEL operaterMTEL;	
struct_operater_Haloo operaterHaloo;	
struct_operater_BHeronet operaterBHeronet;						
struct_operater_mtelCG operaterMTEL_CG;	

struct_operater_Mobily operaterMobily;
struct_operater_Zain operaterZain;
struct_operater_STC operaterSTC;

struct_operater_MEO operaterMEO;
struct_operater_Vodafone operaterVodafone;
struct_operater_NOS operaterNOS;
struct_operater_LucaMobile operaterLucaMobile;
struct_operater_ONI operaterONI;

// last time successfully contacted server
unsigned long last_seen = 0L;
#ifdef INTERRUPTFLOW
	// fmeter sensor
	double flowmeter_capacity = 30;
	double flowmeter_kfactor = 4.25;
	double flowmeter_mfactor = 1;

	FlowSensorProperties flowmeter_sensor = {flowmeter_capacity, flowmeter_kfactor, {flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor}};
	FlowMeter Meter = FlowMeter(2, flowmeter_sensor);
	long flowmeter_period = 1000; // one second (in milliseconds)
	long flowmeter_lasttime = 0;

void MeterISR()
{  
  Meter.count();// let our flow meter count the pulses
}
#endif

#if defined FLOW || defined INTERRUPTFLOW					   
struct flowmeter_struct
{
  float currentflow;
  float currentvolume;
};
#endif

bool carrierChange = false;

#ifdef TIME_F_Measure
unsigned long currentTimeMilles;
#endif

#ifdef RainFallSensor
volatile unsigned int RainFallCaunterTich = 0;
#endif

//--------------------------------------------------------------------------------------
void setup()
{ 
Serial.begin(115200); // Serial connection. 

Serial.print(F("\n\n(RE)start uC - FW Version : ")); Serial.print(FirwareVersion); Serial.println(F(" SENS R004 + LTE\n\n"));

initiWD(); //disabled by default

#ifdef WhatchDog
	enableWD(1); //E N A B L E    WatchDog
#endif
 
#ifdef LiPo_Charger 
  pinMode(36, OUTPUT); //Battery_Charger Supply EN
  digitalWrite(36, HIGH);
#endif

  pinMode(26, OUTPUT); //Battery Measure ON/OFF  //  R002:5V0_Relay_PWR_EN
  pinMode(29, OUTPUT); //PWR_LED RED
  pinMode(24, OUTPUT); //5V0_SYS_PWR_EN 
  pinMode(23, OUTPUT); //5V0_I2C_PWR_EN  

  pinMode(34, OUTPUT);  //12V_SYS_PWR_EN
  pinMode(27, OUTPUT);  //5Vneg_PWR_EN

  pinMode(13, OUTPUT);  //OTA WC
  delay(100); 

  digitalWrite(23,LOW); //ON  
  digitalWrite(24,LOW); //ON

  digitalWrite(29,HIGH); //ON
  digitalWrite(34,HIGH);//ON
  digitalWrite(27,LOW);//ON

  digitalWrite(26,HIGH); //Battery Measure ON
  digitalWrite(13,LOW);  //OTA WC

  pinMode(48, OUTPUT);  //LED1
  pinMode(47, OUTPUT);   
  pinMode(42, OUTPUT);  
  pinMode(17, OUTPUT); //LED4   
   
  digitalWrite(arduino_restart_pin, HIGH);
  pinMode(arduino_restart_pin, OUTPUT);  

 #ifdef DC_Valves
   for (byte i=0; i<3;i++ )
  {
    pinMode(valves_on[i], OUTPUT);
    digitalWrite(valves_on[i],LOW);
    
    pinMode(valves_off[i], OUTPUT);
    digitalWrite(valves_off[i],LOW);
    
  }
  pinMode(39, OUTPUT);  // Relay DC4_P <OUT>
  digitalWrite(39,LOW); // Relay DC4_P <LOW>
	
  DDRG  = DDRG  | 0x08; // Relay DC4_N <OUT>  
  PORTG = PORTG & 0xF7; // Relay DC4_N <LOW>

#endif

byte active_zona = EEPROM.read(127);

Serial.print(F("EEPROM addr 127 status:")); Serial.println(active_zona); 

//Ako je pri navodnjavanju WD resetovao uC-e, u EERPOMu na lokaciji 127 bice vrednost 1 ili 2. 3-sko su obe zone aktivne
if (  active_zona == 255)
{
	startValve(1); //Osigurati da je CONTROL ON !!	
	delay(2500);
	//startValve(1); //Osigurati da je CONTROL ON - duplo potvrdjena komanda !!	
	//delay(1500);
	stopValve(0); //Osigurati da je PILOT OFF !!	
	//delay(1500);
	//stopValve(0); //Osigurati da je PILOT OFF - duplo potvrdjena komanda !!	
	
}

//Inicijalizacija modema  
initGSM_LTE();

#ifdef SENTEC_Include   
  RS485setup();  
  setSANTEKsensors(); 
  sensorSampledMask(); 
#endif
	

  initialiseIrrFlowMet(); //REsetovanje Zapremine i globalnog proteklog vremena na 0 vrednosti.
  
   Wire.begin();
   delay(100);
#ifdef LiPo_Charger   
   LiPoChargeInit();  
   LiPoChargeManager();
#endif
       
#ifdef Sensor_EC  
  setConductivityDriver(70, 1); // setovanje SINusnog generatora: 70Hz, 1-> sinus.  
  setupTemperaturePT1000();// Setovanje temperaturnog senzora za PT1000.
#endif
//Parametri za merenje pH zemljista
pHmeasuring.ferphK = 1.75;
pHmeasuring.ferphM = 3.91;								   

  struct ts t;

  DS3231_get(&t);
	
doesOperaterChanging();// Promena operatera

EEPROM_readAnything(EERPOM_mem_loc_CarrierData, carrierdata);

#ifdef DEBUG_Operater 
    char tmp[2]="00"; 
    Serial.println("\n---------------------------");    
    Serial.println("Sadrzaj EEPROM-a : ");   
    EEPROM_readAnything(64, tmp);
    Serial.println(String(tmp).c_str());     

    Serial.println(carrierdata.apnname);
    Serial.println(carrierdata.apnuser);
    Serial.println(carrierdata.apnpass);        
#endif

setAPN(carrierdata.apnname, carrierdata.apnuser, carrierdata.apnpass);
  


active_zona = EEPROM.read(127);

Serial.print(F("EEPROM zona(e) active : ")); Serial.println(active_zona); 

//Ako je pri navodnjavanju WD resetovao uC-e, u EERPOMu na lokaciji 127 bice vrednost 1 ili 2. 3-sko su obe zone aktivne
if (  (active_zona == 1) || (active_zona == 2) ||  (active_zona == 3)){
			//Start_Control_Pilot_irrigation("RESET",1); /// Polse reseta pokrece CONTROL  zonu, sa nekim dami parametrima, ID=RESET, 5 minuta, i zona 2.
			
			String irrigID ;
									
			control_pilot_status = EEPROM.read(128);
			//Serial.print(F("EEPROM Status")); Serial.println(control_pilot_status); 
			
			if(active_zona == 1)
			{				
				//control_pilot_status = 3;
				//digitalWrite(47, HIGH);
				
				irrigID = readStringFromEEPROM(228);
				Serial.print(F("EEPROM irrig ID ")); Serial.println(irrigID); 
				Start_Control_Pilot_irrigation(irrigID,1);
			}
			if(active_zona == 2)
			{				
				//control_pilot_status = 1;
				//digitalWrite(48, HIGH);
				
				irrigID = readStringFromEEPROM(328);
				Serial.print(F("EEPROM irrig ID ")); Serial.println(irrigID); 
				Start_Control_Pilot_irrigation(irrigID,2);
			}
			if(active_zona == 3)
			{				
				irrigID = readStringFromEEPROM(328);
				Serial.print(F("EEPROM irrig ID ")); Serial.println(irrigID); 
				Start_Control_Pilot_irrigation(irrigID,2);
				
				irrigID = readStringFromEEPROM(228);
				Serial.print(F("EEPROM irrig ID ")); Serial.println(irrigID); 
				Start_Control_Pilot_irrigation(irrigID,1);
				
			}	
			
		}
		else{
			Start_Control_Pilot_irrigation("StarT",2); /// u startu pokrece CONTROL zonu, sa nekim dami parametrima, ID=START, 5 minuta, i zona 1.
			
			stopValve(0); //Osigurati da je PILOT OFF !!	
			delay(1500);
			//stopValve(0); //Osigurati da je PILOT OFF - duplo potvrdjena komanda !!
		} 



 
#ifdef FLOW_Toggling
   pinMode(flow_pin_Toggling, INPUT);
//   DDRE  = DDRE & 0xEF;  // Za SENS - FLOW input 
#endif
											  
					
#ifdef SENTEC_Include         
  reqSENTEKpresetHoldReg_Temperature();delay(400); 
  reqSENTEKpresetHoldReg_Moisture();delay(400); 
#endif
#ifdef SENTEK_Salinity   
  reqSENTEKpresetHoldReg_Salinity();delay(400); 
#endif	

#ifdef RainFallSensor
attachInterrupt(digitalPinToInterrupt(2), RainFallCaunter, RISING);
RainFallCaunterTich = 0;
#endif

#ifdef INTERRUPTFLOW
  // enable a call to the 'interrupt service handler' (ISR) on every rising edge at the interrupt pin
  attachInterrupt(INT7, MeterISR, RISING);
  // sometimes initializing the gear generates some pulses that we should ignore
  Meter.reset();
#endif

#ifdef FLOW_10L_imp
  // enable a call to the 'interrupt service handler' (ISR) on every rising edge at the interrupt pin
  attachInterrupt(INT7, flowImplseTick100L_Procesing, RISING);
#endif

#ifdef FLOW_100L_imp
  // enable a call to the 'interrupt service handler' (ISR) on every rising edge at the interrupt pin
  attachInterrupt(INT7, flowImplseTick100L_Procesing, RISING);
#endif

#ifdef FLOW_1000L_imp
DDRE &= ~(1 << DDE7);  // Podesi PE7 kao ulazni pin
  PORTE |= (1 << PORTE7); // Uključi pull-up otpornik na PE7
  // enable a call to the 'interrupt service handler' (ISR) on every rising edge at the interrupt pin
  attachInterrupt(INT7, flowImplseTick1000L_Procesing, RISING);
#endif

setRTCtime();

};
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void loop()
{
/*
Serial.println(F("SLOT Irigation Status:"));
for (byte Irigation = 0; Irigation < NUM_SLOTS; Irigation++)
{
	Serial.print(F("Irigation : ")); Serial.print(Irigation);Serial.print(F(" "));Serial.println(zonal_irrigations[Irigation].irrigation_id );
	for (byte processing_zone = 0; processing_zone < number_of_zones; processing_zone++)
	{
		Serial.print(F("Zone : ")); Serial.print(processing_zone);Serial.print(F("  "));Serial.println(zonal_irrigations[Irigation].status[processing_zone] );
	}
}
*/

  Serial.println(F("Usao u loop"));
  //Serial.print(F("RSSI = ")); Serial.print(getSignalStrength());Serial.println(F("dB"));
  

	togleWD(); //togle WD

#ifdef LiPo_Charger 
  LiPoChargeManager(); 
#endif

  params = "";  
  params.concat("did=" + String(parcel_id));
//params.concat("pid=" + String(parcel_id));
  params.concat("&" + String(FirwareVersion));

 
#ifdef Power_Saving 

if(getVoltageSolar() < 19.0){
BatterySupplying = 1;
Serial.println(F("Battery Supplaing"));
}
delay(1500);
#endif  

//----------------------- SENTEK sensors 1, 2, 3 -------------------------
#ifdef SENTEC_Include

        reqSENTEKpresetHoldReg_Temperature();
        //delay(200);  
        params.concat("&t1=" + String(getSENTEKtemperature(1))); //temperature
        //delay(200);  
        params.concat("&t2=" + String(getSENTEKtemperature(2)));
        //delay(200);  
        params.concat("&t3=" + String(getSENTEKtemperature(3)));  
        //delay(200);        
        reqSENTEKpresetHoldReg_Moisture();
        //delay(200);
        params.concat("&m1=" + String(getSENTEKmoisture(1))); //moisture
        //delay(200);        
        params.concat("&m2=" + String(getSENTEKmoisture(2)));
        //delay(200);
        params.concat("&m3=" + String(getSENTEKmoisture(3)));
       
#ifdef SENTEK_Salinity   			   
        //delay(200);
        reqSENTEKpresetHoldReg_Salinity();
        delay(600);
        params.concat("&s1=" + String(getSENTEKsalinity(1)));//salinity
        //delay(200);
        params.concat("&s2=" + String(getSENTEKsalinity(2)));
        //delay(200);
        params.concat("&s3=" + String(getSENTEKsalinity(3)));
        
   #endif  
 
#endif
//----------------------- SENTEK sensors 4, 5, 6 -------------------------
#ifdef SENTEK_Include_Extend_Sens
        reqSENTEKpresetHoldReg_Temperature();
        //delay(200);  
        params.concat("&t4=" + String(getSENTEKtemperature(4))); //temperature
        //delay(200);  
        params.concat("&t5=" + String(getSENTEKtemperature(5)));
        //delay(200);  
        params.concat("&t6=" + String(getSENTEKtemperature(6)));  
        //delay(200);
        reqSENTEKpresetHoldReg_Moisture();
        //delay(800);
        params.concat("&m4=" + String(getSENTEKmoisture(4))); //moisture
        //delay(200);
        params.concat("&m5=" + String(getSENTEKmoisture(5)));
        //delay(200);
        params.concat("&m6=" + String(getSENTEKmoisture(6)));
    #ifdef SENTEK_Salinity        
        delay(600);
        reqSENTEKpresetHoldReg_Salinity();
        //delay(50);
        params.concat("&s4=" + String(getSENTEKsalinity(4)));//salinity
        //delay(50);
        params.concat("&s5=" + String(getSENTEKsalinity(5)));
        //delay(50);
        params.concat("&s6=" + String(getSENTEKsalinity(6)));
    #endif
	
#endif

#if defined FLOW || defined INTERRUPTFLOW
  struct flowmeter_struct flow = checkFlowMeter(); //azurira vrednost protoka i zapremine
  checkValves(flow.currentvolume);
#else
  checkValves(0);
#endif
 
#ifdef INTERRUPTFLOW  
   Meter.reset();				 
#endif

#ifdef FLOW_Toggling 
   bool currentFlowStatus = checkFlow() ? 1 : 0;  // ako ima bilo kakvog protoka vraca '1'
#endif

#ifdef Sensor_pH  
  ph_value = getPh();
#endif

#ifdef SHT1sensor
   Wire.end();  // u konfliktu dva I2C-a RTC i Temp-Humid senzor
   temperature = SHT1x_sensor.readTemperatureC();  
   humidity = SHT1x_sensor.readHumidity();
   Wire.begin();    
#endif

#ifdef Sensor_EC
  el_conductance = getELconductance(); 
#endif  

#ifdef Sensor_10HS_1
  params.concat("&h1=" + String(analogRead(A4))); 
#endif  
#ifdef Sensor_10HS_2
  params.concat("&h2=" + String(analogRead(A3)));
#endif  

#ifdef SHT1sensor
  params.concat("&ah=" + String(humidity));
  params.concat("&at=" + String(temperature));
#endif  
//  params.concat("&temp=" + String(0));

	  
#ifdef LiPo_Charger
  params.concat("&pow=" + String( BatteryVoltage ));// Battery Voltage for SENS board
  params.concat("&sol=" + String(getVoltageSolar() ));// Solar Voltage for SENS board 

//	  params.concat("&cc=" + String(BatteryCChargingCurrent)); // Struja punjenja
//	  params.concat("&Scc=" + String(setChargingCurrent));

#endif


#ifdef Sensor_BGT_WSD2
#ifdef Power_Saving  
  //delay(5500);
#endif  
    params.concat("&ah=" + String(getBGThumidity()));
    params.concat("&at=" + String(getBGTtemperature()));
#endif  

#ifdef Sensor_pH
  params.concat("&ferph=" + String(pHmeasuring.rawPh));//salje se RAW pH.
#endif
 
#if defined FLOW || defined INTERRUPTFLOW 
  params.concat("&cfl=" + String(flow.currentflow));
  params.concat("&tfv=" + String(flow.currentvolume));  
#endif  

#ifdef FLOW_Toggling  
  params.concat("&cfl=" + String(currentFlowStatus));
#endif    

#ifdef FLOW_10L_imp 

  params.concat("&cfl=" + String(getFlow10L()));
  unsigned int tmpFlowVolume = getCurrentVOLtick10L();
  params.concat("&tfv=" + String(tmpFlowVolume)); 
  checkValves(tmpFlowVolume);

#endif 

#ifdef FLOW_100L_imp 

  params.concat("&cfl=" + String(getFlow100L()));
  unsigned int tmpFlowVolume = getCurrentVOLtick100L();
  params.concat("&tfv=" + String(tmpFlowVolume)); 
  checkValves(tmpFlowVolume);

#endif 

#ifdef FLOW_1000L_imp 

  params.concat("&cfl=" + String(getFlow1000L()));
  unsigned int tmpFlowVolume = getCurrentVOLtick1000L();
  params.concat("&tfv=" + String(tmpFlowVolume)); 
  checkValves(tmpFlowVolume);

#endif


#ifdef Sensor_EC
  params.concat("&ecv=" + String(el_conductance));
#endif

#ifdef WhatchDog
	togleWD(); //togle WD
#endif
/*
#if defined(Power_Saving) && ( defined(WindDurection) || defined(WindSpeed) )
if (BatterySupplying == 1)  delay(1500);
#endif
*/
#ifdef WindDurection
  params.concat("&wd=" + String(analogRead(A0)));
#endif

#ifdef WindSpeed 
int tmp =  analogRead(A1);
if (tmp<4) tmp = 0;
  params.concat("&ws=" + String(tmp));
#endif

#ifdef SunRadiation 
int tmp1 =  analogRead(A11);
if (tmp1<4) tmp1 = 0;
  params.concat("&sr=" + String(tmp1));
#endif


#ifdef RainFallSensor
  params.concat("&rf=" + String(RainFallCaunterTich));
  //RainFallCaunterTich = 0;
#endif
//========================================  E N D  SENSORS Reading  ============================== 
#ifdef Power_Saving    
  if (BatterySupplying == 1){
  	//digitalWrite(34, LOW); //12V_SYS_PWR power OFF
    digitalWrite(29, LOW); // Power LED OFF
	//digitalWrite(27, HIGH);// -5V power OFF
	
//	digitalWrite(17, LOW);// LED Relay-4 OFF
//	digitalWrite(42, LOW);// LED Relay-3 OFF
//	digitalWrite(47, LOW);// LED Relay-2 OFF
//	digitalWrite(48, LOW);// LED Relay-1 OFF
	
  }
  else {
    digitalWrite(29, HIGH); //Power LED ON
	//digitalWrite(34, HIGH); //12V_SYS_PWR power ON 
	//digitalWrite(27, LOW);// -5V power ON
  }
#endif

params.concat("&RSSI=" + String(getSignalStrength()));

  String activeirrigations;
  for (byte i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id != "")
    {
      activeirrigations.concat("," + String(zonal_irrigations[i].irrigation_id));
    }
  }
  activeirrigations.remove(0, 1);
  params.concat("&iids=" + String(activeirrigations));

#ifdef DEBUG_BASIC  
  Serial.println(params.c_str());
#endif

#ifdef WhatchDog
	togleWD(); //togle WD
#endif

  memset(msg, 0, sizeof(msg)); 

  apiCall("/api/post/sync_device.php", params, msg);

char *jsonResponse = strstr(msg, "{");    
#ifdef DEBUG_BASIC   
    Serial.println(jsonResponse);													   
#endif
//--------------------------------------------------------------------------------
  JsonObject &root = jsonBuffer.parseObject(jsonResponse);
//--------------------------------------------------------------------------------

  JsonVariant numberofzones_json = root["zones"];
  if (numberofzones_json.success())
  {
    if (number_of_zones != root["zones"])
    {
      number_of_zones = root["zones"];
    }
  }

if ( (number_of_zones<1) || (number_of_zones > 4)  ) number_of_zones = 4;																		 

#ifdef DC_Valves
  JsonVariant valvedelay_json = root["valvedelay"];
  if (valvedelay_json.success())
  {
    if (valvedelay != root["valvedelay"])
    {
      valvedelay = root["valvedelay"];
    }
  }
  if ( (valvedelay < 10) || (valvedelay > 1000) ) valvedelay =1000;																  
#endif
//---------------------------------------------------------------------
  JsonObject &start_irrigation = root["irr"];
  if (start_irrigation.success())
  {
    // TODO: check if this irrigationid is started
    if (strcmp(start_irrigation["irrigationid"].as<String>().c_str(), root["irr"]["iid"].as<String>().c_str()) != 0)
    {
      isautomode = start_irrigation["automode"];
      if (start_irrigation["zonal"])
      {
        if (start_irrigation["duration"])
        {
          //per time
          isperm3 = false;
          start_zonal_irrigation(start_irrigation["iid"].as<String>(),  start_irrigation["zones"]);
          //Serial.println("Per Time");
          //start_zonal_irrigation_DBG(start_irrigation["iid"].as<String>(), start_irrigation["duration"], start_irrigation["zones"]);
        }

      }
    }
  }


//-----------------------------------------------------------------------------------------------------//

  JsonVariant stop_irrigation_id_json = root["stopirr"];
  if (stop_irrigation_id_json.success())
  {    
    char irrid[5];
    strcpy(irrid, root["stopirr"].as<const char *>()); 
    stop_irrigation(irrid);    
  }
//--------------------------------------------------------------------------------	
																	  
#ifdef Power_Saving

 JsonVariant PowerStandByTime_json = root["slp"];
    if (PowerStandByTime_json.success())
    {
      if (PowerStandByTimeInMin != root["slp"])
      {
          PowerStandByTimeInMin = root["slp"];
      }
      if ((PowerStandByTimeInMin <0) || (PowerStandByTimeInMin > 90 )) PowerStandByTimeInMin = 0;
   }
 
   
if (   (isSomeIrigationON() == 0) && ( BatterySupplying == 1) ){  

	if (PowerStandByTimeInMin != 0){
#ifdef WhatchDog
		enableWD(1);
#endif 
		Serial.print(F("Stand BY for "));Serial.print(PowerStandByTimeInMin);Serial.print(F("min"));delay(50);
		PowerStandBy(PowerStandByTimeInMin); 
#ifdef DEBUG
	Serial.println(F(" => Wake UP...."));
#endif 	

		PowerWakeUP();
		goto pwrLabel;
	}
	if(BatteryVoltage <9.3){
#ifdef WhatchDog
		enableWD(1);
#endif 
		Serial.println(F("Go to Sleep 15min"));
		PowerStandBy(15); 
		PowerWakeUP();
		
		goto pwrLabel;
	}
	if(BatteryVoltage <9.7){
#ifdef WhatchDog
		enableWD(1);
#endif 
		Serial.println(F("Go to Sleep 10min"));
		PowerStandBy(10); 
		PowerWakeUP();
		
		goto pwrLabel;
	}
	if(BatteryVoltage <10.2){
#ifdef WhatchDog
		enableWD(1);
#endif 
		Serial.println(F("Go to Sleep 8min"));
		PowerStandBy(8); 
		PowerWakeUP();
		
		goto pwrLabel;
	}
	if(BatteryVoltage <10.5){
#ifdef WhatchDog
		enableWD(1);
#endif 
		Serial.println(F("Go to Sleep 6min"));
		PowerStandBy(6); 
		PowerWakeUP();
		
		goto pwrLabel;
	}
	if(BatteryVoltage <10.8){
#ifdef WhatchDog
		enableWD(1);
#endif 
		Serial.println(F("Go to Sleep 4min"));
		PowerStandBy(4); 
		PowerWakeUP();
		
		goto pwrLabel;
	}
		if(BatteryVoltage <11.0){
#ifdef WhatchDog
		enableWD(1);
#endif 	
		Serial.println(F("Go to Sleep 2min"));
		PowerStandBy(2); 
		PowerWakeUP();
		
		goto pwrLabel;
	}

	
}
pwrLabel:	
 
#endif		


jsonBuffer.clear();

delay(LOOP_Delay);

}

//============================= END ====== OF ================  L O O P ( ) =================================================================

void Start_Control_Pilot_irrigation(String irrigationid, int ControlPilotZona){

 //kreiranje JSON objekta
    DynamicJsonBuffer jsonBuffer;
    JsonArray& zones = jsonBuffer.createArray();
    zones.add(ControlPilotZona);
    start_zonal_irrigation(irrigationid,zones);
}

/*EEPROM Content:
Address |  Sadrzaj
---------------------
127		| 1 : zona 1, 2: zona 2, 3: Obe zone aktivne
128     | status: 0 : obe zone off, 1: Pilot off, control ON, 2:prelaz pilot OFF->ON control ON->OFF, 3:Pilot ON, Control OFF, 4:prelaz control OFF->ON pilot ON->OFF  
228     | irrID zone 1
328     | irrID zone 2
*/
void control_pilot_transition_proc(void){	
	
unsigned long deltaTime;
	
if (	(control_pilot_status == 2) || (control_pilot_status == 4) ) // Proverava da li je switcovanje izmedju zona u toku
	{	// obrada procesa tranzicije izmedju prebacivanja sa control na pilot zonu	
		deltaTime = millis() - control_pilot_transition_timer  ; // 		
		deltaTime = deltaTime / 1000; // u sekundama;
		
		if (deltaTime > TransitionTime ){ //Kad istekne prelazni period, [sekundama]
			if (control_pilot_status == 2) //Pali se Pilot, a gasi Control
			{	
				if(EEPROM.read(128) != 3) 
				{	
					EEPROM.write(128, 3);//status;
				}
				
				//Serial.println(F("Gasim CONTROL"));
				
				control_pilot_status = 3;
				
				stop_control_pilot_irrigation(1);
				
			}
			if (control_pilot_status == 4)
			{	
				if(EEPROM.read(128) != 1) 
				{	
					EEPROM.write(128, 1);
				}	
				
				//Serial.println(F("Gasim PILOT"));
				
				control_pilot_status = 1;
				
				stop_control_pilot_irrigation(0);
				
			}
			
		control_pilot_transition_timer = millis(); // resetovanje tajmera
		}
		
	}
else{
	
control_pilot_transition_timer = millis(); // resetovanje tajmera	
	
}	

	//Serial.print(F("Timer : ")); Serial.println(deltaTime);
	if (isSomeIrigationON() == 0) 
	{
		Serial.println(F("No Irigation"));
		control_pilot_status = 0;	
		if(EEPROM.read(127) != 255 ) EEPROM.write(127, 255);//memLoc 127, value 255 - nema navodnjavanja
	}
}


void start_zonal_irrigation(String irrigationid, JsonArray &zones)
{		
	//for (byte i = 0; i < NUM_SLOTS; i++)
	//{
		//if (zonal_irrigations[i].irrigation_id == "")
		//{																														  
			
			for (byte zone : zones)
			{
				if(  ( zone !=1 )  && ( zone !=2 )) {
					
					Serial.print(F("Zona: "));Serial.print(zone);Serial.println(F(" nije od interesa !"));
					return;				
				}
				
				

				if(zone == 1)
				{ //Pilot    
					Serial.print(F("Palim Zonu PILOT, irrigID: "));	Serial.println(irrigationid);	
					//Serial.print(F("WAS status: "));Serial.println(control_pilot_status);
					
					zonal_irrigations[0].status[0] = 1;				
					zonal_irrigations[0].irrigation_id = irrigationid;
				
					if (control_pilot_status == 0)
					{
						control_pilot_status = 3;
						if(EEPROM.read(127) != 1) {	EEPROM.write(127, 1);	} // aktivna samo PILOT zona
						
					}		
					if (control_pilot_status == 1)
					{
						control_pilot_status = 2;
						if(EEPROM.read(127) != 3) {	EEPROM.write(127, 3);	} // aktivne OBE zone
					} 
					control_pilot_transition_timer = millis();
					//Serial.print(F("STATUS : "));Serial.println(control_pilot_status);	
					
					EEPROM.write(128, control_pilot_status);//
					
					writeStringToEEPROM(228, irrigationid);						
					//Serial.print(F("Provera irrID u EEPROM : "));Serial.println(readStringFromEEPROM(228));
					
					startValve(0);
					delay(1500);
					//startValve(0); // dupla komanda, osigurana da je PILOT ON
					
					params = "";
					memset(msg, 0, sizeof(msg));
					params.concat("iid=" + String(zonal_irrigations[0].irrigation_id));
					apiCall("/api/post/irrigation_started.php", params, msg);
				
					char *jsonResponse = strstr(msg, "{");
					Serial.println(jsonResponse);
				}
								
				if(zone == 2)
				{ // Control  
					Serial.print(F("Palim Zonu CONTROL, irrigID: "));	Serial.println(irrigationid);	
					//Serial.print(F("WAS STATUS : "));Serial.println(control_pilot_status);
					zonal_irrigations[1].status[1] = 1;				
					zonal_irrigations[1].irrigation_id = irrigationid;
					
					if (control_pilot_status == 0)
					{
						control_pilot_status = 1;
						if(EEPROM.read(127) != 2) {	EEPROM.write(127, 2);	} // aktivne CONTROL zona
					}  
					if (control_pilot_status == 3)
					{
						control_pilot_status = 4;
						if(EEPROM.read(127) != 3) {	EEPROM.write(127, 3);	} // aktivne OBE zone
					} 
					control_pilot_transition_timer = millis();
					//Serial.print(F("status: "));Serial.println(control_pilot_status);
					
					EEPROM.write(128, control_pilot_status);//
					
					writeStringToEEPROM(328, irrigationid);						
					//Serial.print(F("Provera irrID u EEPROM : "));Serial.println(readStringFromEEPROM(328));
					
					startValve(1);
					delay(1500);
					//startValve(1); // dupla komanda, osigurava da je CONTROL ON
					
					params = "";
					memset(msg, 0, sizeof(msg));
					params.concat("iid=" + String(zonal_irrigations[1].irrigation_id));
					apiCall("/api/post/irrigation_started.php", params, msg);
				
					char *jsonResponse = strstr(msg, "{");
					Serial.println(jsonResponse);
		
					return;
				
				}	
			}
			
		

		//}
	//} //for (int i = 0; i < NUM_SLOTS; i++)
}

void stop_control_pilot_irrigation(byte turn_off_zone)  // turn_off_zone = 0 PILOT 
{														// turn_off_zone = 1 CONTROL 
  // stop zonal irrigation
	for (byte i = 0; i < NUM_SLOTS; i++)
	{
		if(zonal_irrigations[i].irrigation_id !="")
		{
			
      // turn of all active zones
			for (byte checking_zone = 0; checking_zone < number_of_zones; checking_zone++)
			{
				if (zonal_irrigations[i].status[turn_off_zone] == 1 )
				{
					
					
					if(turn_off_zone == 0)
					{
						Serial.print(F("Gasim zonu : PILOT, "));
						if(EEPROM.read(127) != 2) {	EEPROM.write(127, 2);	} // aktivna CONTROL zona
						
					}
					if(turn_off_zone == 1)
					{
						Serial.print(F("Gasim zonu : CONTROL, "));
						if(EEPROM.read(127) != 1) {	EEPROM.write(127, 1);	} // aktivna PILOT zona
					}

					zonal_irrigations[i].status[turn_off_zone ] = 0;	
					String tmpstr = zonal_irrigations[i].irrigation_id;  
					Serial.print(F("irrigation  ID: ")); Serial.println(tmpstr);
					
					zonal_irrigations[i].irrigation_id = "";
					zonal_irrigations[i].duration = 0;
					zonal_irrigations[i].started_time = 0L;

					if( isSomeIrigationON() == 0) {
						if(EEPROM.read(127) != 255 ) EEPROM.write(127, 255);//memLoc 127, value 255 - nema navodnjavanja  		
					} 
					
					stopValve(turn_off_zone);
					delay(1500);
					//stopValve(turn_off_zone);
					
					params = "";
					params.concat("iid=" + String(tmpstr));
					memset(msg, 0, sizeof(msg));
					apiCall("/api/post/irrigation_stopped.php", params, msg);		
					char *jsonResponse = strstr(msg, "{");
					Serial.println(jsonResponse);					
					
					return;			
				}      
			}
		}
	}

}

void stop_irrigation(String irrigationid) // gasi zonu komandom Sa SERVERA
{
  // stop zonal irrigation
	for (byte processingSlot = 0; processingSlot < 2; processingSlot++)
	{
		if (zonal_irrigations[processingSlot].irrigation_id == irrigationid)
		{
		// turn of all active zones
			//for (byte checking_zone = 0; checking_zone < 2; checking_zone++)
			//{
				//if (zonal_irrigations[i].status[checking_zone ] == 1)
				//{
					if (processingSlot == 0){ // PILOT
						control_pilot_status = 4; // Pilot treba da se ugasi
						Start_Control_Pilot_irrigation("CN_ON",  2); //Control treba upaliti
					}
					if (processingSlot == 1){ //CONTROL
						control_pilot_status = 2;// control treba da se ugasi
						Start_Control_Pilot_irrigation("PL_ON",  1); // Pilot treba upaliti
					}			
				//}
				//zonal_irrigations[i].started_time[checking_zone ] = 0L;
			//}
			
			params = "";
			params.concat("iid=" + String(irrigationid));
			memset(msg, 0, sizeof(msg));
			apiCall("/api/post/irrigation_stopped.php", params, msg);
		
			char *jsonResponse = strstr(msg, "{");
			Serial.println(jsonResponse);
	  
			return;
		}
	}
}

//per m3 
void start_zonal_irrigation(String irrigationid, long amount, JsonArray &zones, int max_duration){
//nije od znacaja
}

void checkValves(float deltaVolumePerOneLoop)
{	

control_pilot_transition_proc(); // henlda tranziciju izmedju control i pilot zone
 
    if( isSomeIrigationON() == 0)  {
		if(EEPROM.read(127) != 255 ) EEPROM.write(127, 255);//memLoc 127, value 0 - nema navodnjavanja	
	}

}
//------------------------------------------------------------
void IrigationProcesingBYtime(void){
//nije od znacaja
	
}

void IrigationProcesingBYvolumetric(float deltaVolumePerOneLoop){
//nije od znacaja
}

time_t currentTime()
{
  struct ts t;
  tmElements_t te;
  DS3231_get(&t);
  //char buff[BUFF_RTC_TIME];
  //snprintf(buff, BUFF_RTC_TIME, "Trenutno vreme %d.%02d.%02d %02d:%02d:%02d", t.year, t.mon, t.mday, t.hour, t.min, t.sec);
  //Serial.println(buff);

  te.Second = t.sec;
  te.Minute = t.min;
  te.Hour = t.hour;
  te.Day = t.mday;
  te.Month = t.mon;
  te.Year = t.year - 1970;

  time_t timeInSeconds = makeTime(te);
  return timeInSeconds;
}


void stopAll(void) {

  // stop zonal irrigations
  for (byte checking_slot = 0; checking_slot < NUM_SLOTS; checking_slot++) {
    for (byte checking_zone=0; checking_zone < number_of_zones; checking_zone++) { 
      zonal_irrigations[checking_slot].status[checking_zone] = 0;
      
    }
	zonal_irrigations[checking_slot].current= 0;
    zonal_irrigations[checking_slot].started_time = 0L;
    zonal_irrigations[checking_slot].irrigation_id = "";
    zonal_irrigations[checking_slot].amount = 0L;
    zonal_irrigations[checking_slot].duration = 0;

  }
    for (byte checking_zone=0; checking_zone < number_of_zones; checking_zone++){
		
	stopValve(checking_zone); 
	delay(500);
	}
  
  initialiseIrrFlowMet();
}

//------------------------------------------------------------------------

void startValve(byte zone) {	
   
    if (zone == 3) { 
		PORTG = PORTG & 0xFB; //Reley za P na '0'
		PORTG = PORTG | 0x08; //Relay za N na '1' 	
		delay(valvedelay);		
		PORTG = PORTG & 0xF7; //Relay za N na '0'
	}
	else{		
		digitalWrite(valves_on[zone], LOW);  // Releji za P (pozitivan) prikljucak vential 
		digitalWrite(valves_off[zone], HIGH);// Releji za N (negativan) prikljucak ventila
						  
		delay(valvedelay);
		digitalWrite(valves_off[zone], LOW);			
		
	}


    switch(zone)  {
    case 0: { digitalWrite(48, HIGH); break; }  
    case 1: { digitalWrite(47, HIGH); break; }  
    case 2: { digitalWrite(42, HIGH); break; }  
    case 3: { digitalWrite(17, HIGH); break; }   
    }
}

void stopValve(byte zone) {	

    if (zone == 3) {
		PORTG = PORTG & 0xF7;  //Relay za N na '0'
		PORTG = PORTG | 0x04;  //Reley za P na '1'
		delay(valvedelay);
		PORTG = PORTG & 0xFB;  //Reley za P na '0'
	}
	else{
		digitalWrite(valves_off[zone], LOW);   //Releji za N (negativan) prikljucak ventila	
 
		digitalWrite(valves_on[zone], HIGH);  // Releji za P (pozitivan) prikljucak vential   
		delay(valvedelay);
		digitalWrite(valves_on[zone], LOW); 		
	}

  switch(zone)  { //LEDs
    case 0: {  digitalWrite(48, LOW); break; }  
    case 1: {  digitalWrite(47, LOW); break; }  
    case 2: {  digitalWrite(42, LOW); break; }  
    case 3: {  digitalWrite(17, LOW); break; }      
   }

}

//bool is_some_irrigation_on = false;
byte isSomeIrigationON(void){
		
	    byte slot_i  = 0;
	    byte count_active_slots  = 0;	
        for (byte slot_i = 0; slot_i < NUM_SLOTS; slot_i++){
		    if (zonal_irrigations[slot_i].irrigation_id != "")	count_active_slots++;		
        }
	    if (count_active_slots == 0)return 0;
		else return 1;		

}
void setRTCtime(void){
  /*
String 	xString;
char x[60];
gsm.SimpleWriteln("AT+CLTS=?"); 	
 delay(2000);//  Add Max Response Time
 gsm.SimpleRead1(x);
 xString = String(x).c_str(); 	
 Serial.print("CCLK: ");Serial.println(xString);
 Serial.print("CCLK: ");Serial.println(x);
	*/
}

// Funkcija za upisivanje String-a u EEPROM
void writeStringToEEPROM(int eepromAddress, String str) {
  char charArray[6];   // Kreiramo char niz iste dužine kao String + 1 za '\0'
  str.toCharArray(charArray, str.length() + 1); // Konvertujemo String u char niz
 EEPROM_writeAnything(eepromAddress, charArray); // Upisujemo char niz u EEPROM


}

String readStringFromEEPROM(int eepromAddress) {
  char charArray[6]; // Kreiramo char niz dovoljne veličine za očekivane podatke
  EEPROM_readAnything(eepromAddress, charArray); // Čitamo char niz iz EEPROM-a
  String str = String(charArray); // Konvertujemo char niz nazad u String
  return str;
}