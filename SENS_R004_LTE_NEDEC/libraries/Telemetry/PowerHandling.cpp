#include <Arduino.h>
#include <SoftwareSerial.h>
//#include "SIM900.h"
//#include "inetGSM.h"
#include "Telemetry.h"

#ifdef Power_Saving
	#include <avr/sleep.h>
	#include <avr/power.h>
	#include <avr/wdt.h>

	int InterruptWDcauntIvent = 0;
	

ISR(WDT_vect) {
 InterruptWDcauntIvent++;
}

void setupWatchDogTimer(void) {
MCUSR &= ~(1<<WDRF); 
WDTCSR |= (1<<WDCE) | (1<<WDE); 
WDTCSR  = (1<<WDP3) | (0<<WDP2) | (0<<WDP1) | (1<<WDP0);  //8s 
//WDTCSR  = (0<<WDP3) | (1<<WDP2) | (1<<WDP1) | (0<<WDP0);  //1s 
WDTCSR |= _BV(WDIE);
}

void enterSleep(void)
{
     set_sleep_mode(SLEEP_MODE_PWR_DOWN);	 
     sleep_enable(); 	 
     sleep_mode();       
  // The program will continue from here after the WDT timeout
  // First thing to do is disable sleep.
     sleep_disable(); 
     
  // Re-enable the peripherals.
	 power_all_enable();
}
	
void PowerStandBy(byte PowerStandByTimeInMin){
int SleepTime_8s = PowerStandByTimeInMin * 6;//bilo je   /10; // Watch Dog Interrupt is 8 seconds !
if (PowerStandByTimeInMin == 1) SleepTime_8s = 3;
#ifdef DEBUG_Power_Saving
	Serial.print(F("SENS Sleep..."));
#endif 

//Power OFF >
digitalWrite(29, LOW);//PWR LED
digitalWrite(34,LOW);//12V_SYS_PWR_EN (SENTEK)
digitalWrite(8, HIGH);//GPRS_PWR KEY
digitalWrite(24,HIGH);//5V0_SYS_PWR_EN
digitalWrite(23,HIGH);//5V0_I2C_PWR_EN
digitalWrite(27,HIGH); //-5V0  
//digitalWrite(26,LOW); //Battery Measuring OFF
 digitalWrite(22,HIGH); //GSM 4G OFF

digitalWrite(7, LOW);//GSM RST

//GSM Power down
//DDRH  = DDRH  | 0x20; PORTH = PORTH & 0xDF;
//delay(10);
//DDRE  = DDRE  | 0x04; PORTE = PORTE & 0xFB;
#ifdef DEBUG_Power_Saving
//	SleepTime = currentTime();
#endif
	InterruptWDcauntIvent = 0 ;
	setupWatchDogTimer(); 
    while (InterruptWDcauntIvent < SleepTime_8s) { //ONE while lopp is 8s
        
		enterSleep();
		
#ifdef WhatchDog
		togleWD(); //togle WD
#endif
		
    } // InterruptWDcauntIvent * 8 sekundi

	wdt_disable();
}

void PowerWakeUP(void){
	

// Power ON>
digitalWrite(29, HIGH);//PWR LED

//digitalWrite(8, LOW);//GPRS_PWR KEY
digitalWrite(24,LOW);//5V0_SYS_PWR_EN
digitalWrite(23,LOW);//5V0_I2C_PWR_EN
digitalWrite(27,LOW); //-5V0

//digitalWrite(26,HIGH); //Battery Measuring ON

//GSM Power UP
//DDRE  = DDRE  | 0x04; PORTE = PORTE | 0x04;
//delay(10);
//DDRH  = DDRH  | 0x20; PORTH = PORTH | 0x20;
//delay(500);
initGSM_LTE();

setAPN(carrierdata.apnname, carrierdata.apnuser, carrierdata.apnpass);

digitalWrite(34,HIGH);//12V_SYS_PWR_EN (SENTEK)	
}
#endif

// byte - nije bitan je precizan napon solarnog panela
float getVoltageSolar(void){

float solar = 0;
byte cnt = 0;
while (cnt<5){
	solar = solar + analogRead(A9);
	delay(10);
	cnt++;
}
solar = solar / solarConst ;  //163 = 1024 /  (10kohm + 47kohm)/10kohm
return solar;
}

// float - bitan je precizan napon baterije
float getVoltageBatt(void){

float battery = 0;
byte cnt = 0;

while (cnt<5){
	battery = battery + analogRead(A8);
	delay(10);
	cnt++;
}

battery = battery / battConst;  //335 = 1024 /  (402kohm+200kohm)/200kohm;
return battery;
}

#ifdef AC_Power_FERT
// Vraca vrednost mrenzog napona ~230Vac
int getPower(void){
  int i = 0;
  int sensmax = 0;
  int sensmin = 1023;
while (i++<150){  
int  sensorValue = analogRead(A7);
if (sensmax < sensorValue ) sensmax = sensorValue;
if (sensmin > sensorValue ) sensmin = sensorValue;
}
return (sensmax - sensmin)/4.27;
//Serial.println(sensorValue); 
}
#endif