#include <Arduino.h>
#include "Telemetry.h"

#ifdef SENTEC_Include

byte atempt =0;
#ifdef DEBUG_SENTEK_Write_to_Excel
unsigned int row = 9; // prvo polje u koje ce upisati u excel
#endif

#define const_alfa 0.8
#define const_beta 0.1  // = (1-const_alfa) / 2

float yn1s1 , xn1s1 ;
float yn1s2 , xn1s2 ;
float yn1s3  , xn1s3;
byte firstCapture = 0;								  


void setSANTEKsensors(void){	
	Serial1.begin(9600,SERIAL_7N2); 	
}


char char_temp[20];

/*
//30002
//Measurement Status - reading is in progress (30002==1)
//0 - No measurement
//1 - Measurement in progress
//2 - Measurement completed, with no errors
//3 - Measurement completed, but errors occurred
void measureStatus(void){
	
	Serial.print("[S]");RS485writeStringLN(":010400010001F9");	RS485readCharArray( char_temp);	
	Serial.print(char_temp);
}
//30003 - 30022
//Sensors Scan Selection Masks
//These registers specify which sensors will be scanned when 40001 is set to 1
void scanSelectionMask(void){
	
Serial.println("Scan Selection Mask:");
Serial.print("M");RS485writeStringLN(":010400020001F8");RS485readCharArray( char_temp);		
Serial.print("M");RS485writeStringLN(":010400030001F7");RS485readCharArray( char_temp);	
Serial.print("S");RS485writeStringLN(":010400040001F6");RS485readCharArray( char_temp);			
Serial.print("S");RS485writeStringLN(":010400050001F5");RS485readCharArray( char_temp);		
Serial.print("T");RS485writeStringLN(":010400060001F4");RS485readCharArray( char_temp);			
Serial.print("T");RS485writeStringLN(":010400070001F3");RS485readCharArray( char_temp);		
Serial.println("");	
}
//30009 - 30014
//Detected (and configured) Sensors Mask
void detectedSelectionMask(void){
	
Serial.println("Detected Selection Mask:");
Serial.print("M");RS485writeStringLN(":010400080001F2");RS485readCharArray( char_temp);	
Serial.print("M");RS485writeStringLN(":010400090001F1");RS485readCharArray( char_temp);	
Serial.print("S");RS485writeStringLN(":0104000A0001F0");RS485readCharArray( char_temp);	
Serial.print("S");RS485writeStringLN(":0104000B0001EF");RS485readCharArray( char_temp);	
Serial.print("T");RS485writeStringLN(":0104000C0001EE");RS485readCharArray( char_temp);	
Serial.print("T");RS485writeStringLN(":0104000D0001ED");RS485readCharArray( char_temp);	
Serial.println("");	
}



//30101 - 30196
//Sensor Depth Information
void sensorDepth(void){

	Serial.println("Sesnors Depth ");
	//Temperature
	Serial.print("T");RS485writeStringLN(":010400A4000156");	RS485readCharArray( char_temp);		
	Serial.print("T");RS485writeStringLN(":010400A5000155");	RS485readCharArray( char_temp);	
	Serial.print("T");RS485writeStringLN(":010400A6000154");	RS485readCharArray( char_temp);	

	//Moisture
	Serial.print("M");RS485writeStringLN(":01040064000196");	RS485readCharArray( char_temp);	
	Serial.print("M");RS485writeStringLN(":01040065000195");	RS485readCharArray( char_temp);	
	Serial.print("M");RS485writeStringLN(":01040066000194");	RS485readCharArray( char_temp);	
	
	
	//salinity
	Serial.print("S");RS485writeStringLN(":01040084000176");	RS485readCharArray( char_temp);		
	Serial.print("S");RS485writeStringLN(":01040085000175");	RS485readCharArray( char_temp);		
	Serial.print("S");RS485writeStringLN(":01040086000174");	RS485readCharArray( char_temp);	
	Serial.println("");	
	
	
}

void slaveID(void){
	Serial.print("ID : ");	RS485writeStringLN(":0111EE");	RS485readCharArray( char_temp);	
	Serial.println("");	
	
}

//the response should be identical to the command
void getEcho(void){
	
	Serial.print("ECHO : ");RS485writeStringLN(":01080000A5371B");	RS485readCharArray( char_temp);	
	Serial.println("");	
}
//Exception Responses - Illegal commands result in an exception response
void getIlegalCommand(void){
	
	Serial.print("Ilegal Command : ");RS485writeStringLN(":0181017D");	RS485readCharArray( char_temp);	
	Serial.println("");	
}

void ivenCounter(void){
	
	Serial.print("Ivent Counter : ");RS485writeStringLN(":010BF4");	RS485readCharArray( char_temp);	
	Serial.println("");		
}	

void numberOfSensors(void){
Serial.print("Number Of Sensors:");	
RS485writeStringLN(":01040063000197");RS485readCharArray( char_temp);			
Serial.println("");	
	
}
*/

//40002 - 40009
//Selection mask for the sensors to be sampled -> ovo radi po logici kako treba. ECHO komada je......
void sensorSampledMask(void){

//Serial.println("Sampled Mask:");	
RS485writeStringLN(":010600010007F1");
//RS485readCharArray( char_temp);		
RS485writeStringLN(":010600020007F0");
//RS485readCharArray( char_temp);		
RS485writeStringLN(":010600030007EF");
//RS485readCharArray( char_temp);		
RS485writeStringLN(":010600040007EE");
//RS485readCharArray( char_temp);		
RS485writeStringLN(":010600050007ED");
//RS485readCharArray( char_temp);		
RS485writeStringLN(":010600060007EC");
//RS485readCharArray( char_temp);			
	
}

float getSENTEKtemperature(byte debpthSesnor){	

float t = 0;
atempt =0;
do{
	delay(100*atempt);
	switch(debpthSesnor){
	/*10cm*/	case 1:RS485writeStringLN(":01040180000278");break;// Reading temperature from reg 0x30385
	/*20cm*/	case 2:RS485writeStringLN(":01040182000276");break;// Reading temperature from reg 0x30387
	/*30cm*/	case 3:RS485writeStringLN(":01040184000274");break;// Reading temperature from reg 0x30389
#ifdef SENTEK_Include_Extend_Sens	
	/*40cm*/	case 4:RS485writeStringLN(":01040186000272");break;
	/*50cm*/	case 5:RS485writeStringLN(":01040188000270");break;
	/*60cm*/	case 6:RS485writeStringLN(":0104018A00026E");break;
#endif	
			   default:break;	
	}
//	for (int i = 0;i<20;i++) char_temp[i] = '0';
delay(150); 					
	RS485readCharArray( char_temp);	
	t = hexStringToFloat(char_temp, debpthSesnor);
//	measureStatus();
//	Serial.print("atempt ");Serial.print(atempt);
//	Serial.print("T :");Serial.println(t,3);
		
	if( (t > 5) && (t < 55) && (compareLRC(char_temp) == 1) ) break;
}while(atempt++ <4);	
//}while(  (((t < 1) || (t>55)) && (atempt++ <15))  || (compareLRC(char_temp) == 0)  );	
//if(atempt>1){	Serial.print(" ");}
//Serial.print("|");Serial.print(atempt);Serial.print("|");
resetString();
return 	t;

}

float getSENTEKmoisture(byte debpthSesnor){	

float m = 0;
atempt =0;
do{
	delay( 150*atempt);
	switch(debpthSesnor){
	/*10cm*/	case 1:RS485writeStringLN(":010401000002F8");break;// Reading moisture from reg 0x30257
	/*20cm*/	case 2:RS485writeStringLN(":010401020002F6");break;// Reading moisture from reg 0x30259
	/*30cm*/	case 3:RS485writeStringLN(":010401040002F4");break;// Reading moisture from reg 0x30261
#ifdef SENTEK_Include_Extend_Sens	
	/*40cm*/	case 4:RS485writeStringLN(":010401060002F2");break;
	/*50cm*/	case 5:RS485writeStringLN(":010401080002F0");break;
	/*60cm*/	case 6:RS485writeStringLN(":0104010A0002EE");break;
#endif	
			   default:break;
	}
//	for (int i = 0;i<20;i++) char_temp[i] = '0';
delay(100); // Da li treba ovaj delay? Cim uC posalje komandu ka SENTKu, on odgovara cim pre....		
	RS485readCharArray( char_temp);	
	m = hexStringToFloat(char_temp, 6 + debpthSesnor);
//	Serial.print("atempt ");Serial.print(atempt);
//	Serial.print("M: ");Serial.println(m,3);
//	measureStatus();

	if( (m > 0.01) && (m < 100) &&(compareLRC(char_temp) == 1) ) break;
}while(atempt++ <4);
//}while((m < 0.1)  &&  (compareLRC(char_temp) == 0) && ((atempt++ <15)));
//Serial.print("|");Serial.print(atempt);Serial.print("|");
resetString();
return m;	
}

float getSENTEKsalinity(byte debpthSesnor){	

float s = 0;
atempt =0;
do{
	delay(250 * atempt);
	switch(debpthSesnor){
	/*10cm*/	case 1:RS485writeStringLN(":010401400002B8");break;// Reading Salinity from reg 0x30231
	/*20cm*/	case 2:RS485writeStringLN(":010401420002B6");break;// Reading Salinity from reg 0x30233
	/*30cm*/	case 3:RS485writeStringLN(":010401440002B4");break;// Reading Salinity from reg 0x30235
#ifdef SENTEK_Include_Extend_Sens	
	/*40cm*/	case 4:RS485writeStringLN(":010401460002B2");break;
	/*50cm*/	case 5:RS485writeStringLN(":010401480002B0");break;
	/*60cm*/	case 6:RS485writeStringLN(":0104014A0002AF");break;
#endif	
			   default:break;
	}
//	for (int i = 0;i<20;i++) char_temp[i] = '0';	
delay(100);    
	RS485readCharArray( char_temp);	
	s = hexStringToFloat(char_temp, 12 + debpthSesnor);
	//Serial.print("atempt ");Serial.print(atempt);
//	Serial.print("S: ");Serial.println(s,3);	

	
	if( (s > 0) && (s < 19000) && (compareLRC(char_temp) == 1) ) {
	s = filtering(s,debpthSesnor);	

#ifdef DEBUG_SENTEK_Write_to_Excel
	
		switch (debpthSesnor){
		case 1:writeToExcel( row,   2, s);break;
		case 2:writeToExcel( row,   3, s);break;
		case 3:writeToExcel( row++, 4, s); break;
		}
#endif
		
		break;
    }
	
}while(atempt++ <2);
//}while((s < 0.1)  &&  (compareLRC(char_temp) == 0) && ((atempt++ <15)));
//Serial.print("|A ");Serial.print(atempt);Serial.print(" |");
//Serial.print("|");Serial.print(s);Serial.print("|");
//Serial.print("|");Serial.print(atempt);Serial.print("|");
resetString();
return s;	


}
float filtering(float s,byte debpthSesnor){
//	return s;
float yn;

if (firstCapture == 1){ // da ubrza startovanje rada filtera
		switch (debpthSesnor){
		case 2:yn = const_alfa * yn1s2 + const_beta * s + const_beta * xn1s2; xn1s2 = s; yn1s2 = yn;break;
		case 3:yn = const_alfa * yn1s3 + const_beta * s + const_beta * xn1s3; xn1s3 = s; yn1s3 = yn;break;
		case 1:yn = const_alfa * yn1s1 + const_beta * s + const_beta * xn1s1; xn1s1 = s; yn1s1 = yn;break;
		}
}
else{
		switch (debpthSesnor){
		case 1:xn1s1 = s; yn1s1 = s;break;
		case 2:xn1s2 = s; yn1s2 = s;break;
		case 3:xn1s3 = s; yn1s3 = s; firstCapture = 1; break;
		}
		
}
return yn;	
}										   

void reqSENTEKpresetHoldReg_Temperature(void){
	RS485writeStringLN(":010600000004F5");// preset Holding Register 40001 to the value 4, which will start a scan of all humidity sensors
//delay(550);	// vreme koje treba sondi za izmeri i iskalkulise podatke
}
void reqSENTEKpresetHoldReg_Moisture(void){
	RS485writeStringLN(":010600000002F7");// preset Holding Register 40001 to the value 2, which will start a scan of all moisture sensors
//delay(1050);
}
void reqSENTEKpresetHoldReg_Salinity(void){
	RS485writeStringLN(":010600000003F6");// preset Holding Register 40001 to the value 3, which will start a scan of all humidity sensors
//delay(1050);
}

//----------------------------------------------------------------------------
//sensorID: [1-6] temperature, [7-12] moisture, [13- 18] Salinity
float hexStringToFloat(char char_temp[],byte sensorID){ 
	//Serial.print("hex convert:");Serial.println(char_temp);
	char temp[9] = "01234567"; 
	int i = 0;
	for (i=0;i<4;i++){        //BigEndian format of 32bit
		temp[i] = char_temp[i+11] ;
	}	
	for (int i=4;i<8;i++){
		temp[i] = char_temp[i+7] ;
	}	
	long l = strtol(temp, NULL, 16);
	float sens_value = *(float *)&l;	
	
	if (isnan(sens_value)) {// if (NAN = 1) - Not A Number
		sens_value = 0;
#ifdef DEBUG_SENTEK		
		Serial.print("NAN-error");	
#endif		
	} 
/*	
//	Temperature i moisture limiting value
if ( sensorID < 13) { // senzori od 1-12 (temp, mois)
	if ( (sens_value < -20) || (sens_value > 100) ){// limiting value			
//		Serial.print("SENTEK ERROR = ");Serial.print(sens_value,2);
		sens_value = 0;//oldmeasure[sensorID-1];	
	}

}
#ifdef SENTEK_Salinity 
else{  // senzori od 13-18 Saliniti
	if ( (sens_value < 0) || (sens_value > 10000) ){// limiting value			
//		Serial.print("SENTEK ERROR = ");Serial.print(sens_value,2);
		sens_value = 0;//oldmeasure[sensorID-1];	
	}		
}	
#endif	
*/
//resetString();

return sens_value;	
}

// Longitudinal Redundancy Check - CRC
// radi za niz koji vraca sadrzaj 2 registra, 4 bajta 
byte LRC (char * char_temp){
	
	int lrc=0;
    byte i=1;
	char char_tmp[3];
	char_tmp[0] = 0;
	char_tmp[1] = 0;
	char_tmp[2] = 0;

    for (i=1;i<15;i++){
		char_tmp[0]  = char_temp[i];
        char_tmp[1]  = char_temp[i+1];
        lrc = lrc + strtol(char_tmp, NULL, 16);
//		Serial.print("|");Serial.print(char_tmp);/*Serial.print("|-|");Serial.print(lrc);*/Serial.print("|");
		i++;
    }
	lrc = (~lrc + 1 ) & 0xFF;

//	Serial.print("LRC:[");Serial.print(lrc,HEX);Serial.print("]");
	return (byte)lrc ;
}

//poredi valjanost pristiglog sadrzaja.
// radi za niz koji vraca sadrzaj 2 registra, 4 bajta 
byte compareLRC(char * char_temp){
	
char char_tmp[3];
char_tmp[0] = char_temp[15];
char_tmp[1] = char_temp[16];
char_tmp[2] = 0;
int lrc=0; 

	lrc = 	strtol(char_tmp, NULL, 16);

	if(lrc == LRC(char_temp)) {
		//Serial.print("{OK}");
		return 1;
	}
	else {
		//Serial.print("{FL}");
		return 0;
	}
	     
}

void resetString(void){
	
	for(byte i = 0; i<19; i++){char_temp[i] = '0';}
	
	
}
#ifdef DEBUG_SENTEK_Write_to_Excel
void writeToExcel( unsigned int row, unsigned int column, unsigned int value) {
	if( 1 ) {
		// send writeCells message
		Serial.print("XLS,writeCells,");
		Serial.print("EC");
		Serial.print(",");
		Serial.print(row);
		Serial.print(",");
		Serial.print(column);
		Serial.print(",");
		Serial.print(value);
		Serial.print("\n");	
		
	}
}
#endif								  

#endif

