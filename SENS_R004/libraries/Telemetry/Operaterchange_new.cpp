#include <EEPROM.h>

#include "EEPROMAnything.h"
#include <SoftwareSerial.h>
#include <Wire.h>
#include <TimeLib.h>
#include <ArduinoJson.h>
#include "SIM900.h"
#include "ds3231.h"
#include "inetGSM.h"

#include "sms.h"
#include <String.h>
#include <FlowMeter.h>
#include "Telemetry.h"


extern SMSGSM sms;
byte smsposition = 0;
byte smsConfigured = 0;					   

void doesOperaterChanging(void){ // ceka se 5S pri ukljucenju uredjaja, i do 3 pogresno pristigla SMS-a
	
 
  char sms_text[BUFF_SMS];  //50
  char phone_num[BUFF_MOB_NUM]; //20	  	
  byte atempt =0;	
  byte  wrongSMSformat = 0;
  char tmp[2]="69"; // bilo koji sadrzaj razlicit od "SW".
  char SMSsent;
  String xString;
  byte operaterID =9; // neki br da je razlicit od 1,2,3 
  char x[60];    

    smsposition = sms.IsSMSPresent(SMS_UNREAD);
#ifdef DEBUG_Operater  
		 Serial.print(F("SMS position :")); Serial.print(smsposition);		      
#endif

while (   ((atempt++) < secondsWaitingSMS )  &&  (wrongSMSformat <3)  ) {//pokusava  60 seukndi da dobije kofiguracione parametre. Cim ih dobije, obrati i izlazi iz petlje.
#ifdef DEBUG_Operater  
		 Serial.print(F("SMS atempt :")); Serial.println(atempt);		      
#endif
		 if(smsposition){     
#ifdef DEBUG_Operater  
		 Serial.print(F("SMS smsposition : ")); Serial.println(smsposition);		      
#endif
		 sms.GetSMS(smsposition, phone_num, BUFF_MOB_NUM, sms_text, BUFF_SMS);
#ifdef DEBUG_Operater  
		 Serial.println(F("SMS config:"));
		 Serial.print(F("Br. :"));Serial.print(String(phone_num).c_str()); Serial.print(F(", SMS : "));Serial.println(String(sms_text).c_str()); 
		 //delay(1000);	     
#endif	 
			if (startsWith("SW ", sms_text))// ako je dobar sadrzaj poruke, pocinje as "SW", obradi je....
			{
#ifdef DEBUG_Operater
			  Serial.print(F("Promena Operatera SMS-om......."));
#endif
			// probaj dal moze i bez ovoga brisanja EEPROM-a ;-)
			  for (byte i = 0; i < 109; i++) // nije bilo potrebe brisati ceo EEPROM 4096 byte-a
			{
			  EEPROM.write(i, 0);
			}
			strcpy(tmp, "SW");     //mem loc = 64
			EEPROM_writeAnything(EERPOM_mem_loc_SW, tmp); // na  definisanu adresu  upisuje tmp = "SW";
			
			char *ptr =NULL;
			
			ptr = strtok(sms_text, " "); // Definise sta je delimiter -> " " (SPACE)			
			byte position = 1; // preskace se deo SMS poruke "SW".			
			ptr = strtok(NULL, " ");
			strcpy(carrierdata.apnname, ptr);
			position =2;
			ptr = strtok(NULL, " ");
			strcpy(carrierdata.apnuser, ptr); 
			ptr = strtok(NULL, " ");
			strcpy(carrierdata.apnpass, ptr);    //mem loc = 0			
			EEPROM_writeAnything(EERPOM_mem_loc_CarrierData , carrierdata); // upis parametara u EEPROM.	
			
#ifdef GSM_SMS_Replay			
	SMSsent  = sms.SendSMS(phone_num,"Operater uspesno Podesen/Promenjen");  //        POTROSI KREDIT ! Otkomentarisi kad zavrsis testiranje !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#endif

#ifdef DEBUG_Operater	
			if (SMSsent == 0)	Serial.println("Korisnik nije obavesten SMS-om o promeni operatera");
			if (SMSsent == 1)	Serial.println("Korisnik obavesten o uspesnom podesavanju");
			if (SMSsent == 3)	Serial.println("ERROR -GSM module has answered error.");
#endif		
			  
			deleteALLsms();	
#ifdef DEBUG_Operater		
		 Serial.println(F("Operater konfigurisan SMS-om"));
#endif			
			EEPROM.write(EERPOM_mem_loc_sms_Confugured, 0xAA); // indikacija da je operater SMSom podesen.																					 
			return; // sve podeseno -> kraj, izadji iz funkcije!
			}
			 else // ako nije dobar format SMS -> obavesti Korisnika, dati mu fore jos 2 SMS-a max. 
			 {
#ifdef GSM_SMS_Replay				 
			sms.SendSMS(phone_num, "Pogresan Format SMS poruke");        //        POTROSI KREDIT ! Otkomentarisi kad zavrsis testiranje !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#endif
			wrongSMSformat ++; // 3 pogresnig SMS-ova i pod mac batoooo.
			atempt = 0;
#ifdef DEBUG_Operater		
		 Serial.println(F("SMS Config = ERROR. Try again.."));
#endif		 
			 }   		  
		  sms.DeleteSMS(smsposition); //obrisati OBAVEZNO pristigli i procitani  SMS
	  }//if(smsposition)
	  
	  delay(4000); //Hendlanje GSM modula je malo kilavo, treba mu dati vremena............ 				
	  smsposition = sms.IsSMSPresent(SMS_UNREAD);
}// while (atempt) 

if (EEPROM.read(EERPOM_mem_loc_sms_Confugured) == 0xAA){
Serial.print(F("EEPROM cfg_sms "));	Serial.println(EEPROM.read(EERPOM_mem_loc_sms_Confugured));	
return;	
} 														
// Ako nema SMS za kofiguraciju onda automatski detektuje mrezu i konketuje se ......

if (1){//IF stavljen zbog debagovanja, izbirsati polse..... Prepoznaje sam mrezu i konfigurisi se..... 
 delay(20000);
 gsm.SimpleWriteln("AT+COPS?"); // komanda za upit o operateru
 delay(10000);//  Add Max Response Time
 gsm.SimpleRead1(x);
 xString = String(x).c_str(); 
 
 
#ifdef DEBUG_Operater 
	Serial.println();
	Serial.print(F("AT COPS?:"));
	Serial.println(xString);	
 #endif
  
 int index = xString.indexOf("MOBTEL"); //Telenor
 
 if (index != -1){
#ifdef DEBUG_Operater 	 
	 Serial.println(F("Prepoznao Telenor"));	 
#endif	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != Telenor){
		 strcpy(carrierdata.apnname, operaterTelenor.apnname);   
		 strcpy(carrierdata.apnuser, operaterTelenor.apnuser);     
		 strcpy(carrierdata.apnpass, operaterTelenor.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, Telenor);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje Telenor ")); 
#endif
			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 } 
 
  index = xString.indexOf("Telekom");
  
 if (index != -1){
#ifdef DEBUG_Operater 	 
	 Serial.println(F("Prepoznao Telekom "));	 
#endif	
 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != Telekom){
	 strcpy(carrierdata.apnname, operaterMTS.apnname);  
     strcpy(carrierdata.apnuser, operaterMTS.apnuser);    
     strcpy(carrierdata.apnpass, operaterMTS.apnpass); 
	 EEPROM_writeAnything(EEPROM_mem_loc_Operater, Telekom);
	 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
	 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
	Serial.println(F("Operater postaje Telekom ")); 
#endif		 
			
   }	
	else{		
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	
	return;
 } 
 
  index = xString.indexOf("29703");// mtel CG
  
 if (index != -1){
#ifdef DEBUG_Operater 	 
	 Serial.println(F("Prepoznao mtel CG"));	 
#endif	
 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != mtelCG){
	 strcpy(carrierdata.apnname, operaterMTEL_CG.apnname);  
     strcpy(carrierdata.apnuser, operaterMTEL_CG.apnuser);    
     strcpy(carrierdata.apnpass, operaterMTEL_CG.apnpass); 
	 EEPROM_writeAnything(EEPROM_mem_loc_Operater, mtelCG);
	 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
	 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
	Serial.println(F("Operater postaje mtel CG ")); 
#endif		 
			
   }	
	else{		
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	
	return;
 } 
 
   index = xString.indexOf("22005");//vip operater
   
 if (index != -1){
#ifdef DEBUG_Operater 	 
	 Serial.println(F("Prepoznao VIP "));	 
#endif	
 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
  if(operaterID != Vip){
	 strcpy(carrierdata.apnname, operaterVIP.apnname);   
     strcpy(carrierdata.apnuser, operaterVIP.apnuser);    
     strcpy(carrierdata.apnpass, operaterVIP.apnpass); 	
	 EEPROM_writeAnything(EEPROM_mem_loc_Operater, Vip);
	 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
	 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
	Serial.println(F("Operater postaje VIP ")); 
#endif		 
	 	
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 } 
 
    index = xString.indexOf("GSMBIH");
   
 if (index != -1){
#ifdef DEBUG_Operater 	 
	 Serial.println(F("Prepoznao BHmobile"));	 
#endif	
 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
  if(operaterID != BHmob){
	 strcpy(carrierdata.apnname, operaterBHmob.apnname);   
     strcpy(carrierdata.apnuser, operaterBHmob.apnuser);    
     strcpy(carrierdata.apnpass, operaterBHmob.apnpass); 	
	 EEPROM_writeAnything(EEPROM_mem_loc_Operater, BHmob);
	 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
	 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
	Serial.println(F("Operater postaje BHmobile ")); 
#endif		 
	 	
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 } 
 
  index = xString.indexOf("Mobilna Srpske"); //M:TEL
 
 if (index != -1){
#ifdef DEBUG_Operater 	 
	 Serial.println(F("Prepoznao M:TEL"));	 
#endif	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != MTEL){
		 strcpy(carrierdata.apnname, operaterMTEL.apnname);   
		 strcpy(carrierdata.apnuser, operaterMTEL.apnuser);     
		 strcpy(carrierdata.apnpass, operaterMTEL.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, MTEL);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje M:TEL")); 
#endif
			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 } 
 
 
   index = xString.indexOf("ERONET"); //Haloo
 
 if (index != -1){
#ifdef DEBUG_Operater 	 
	 Serial.println(F("Prepoznao Haloo"));	 
#endif	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != Haloo){
		 strcpy(carrierdata.apnname, operaterHaloo.apnname);   
		 strcpy(carrierdata.apnuser, operaterHaloo.apnuser);     
		 strcpy(carrierdata.apnpass, operaterHaloo.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, Haloo);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje Haloo")); 
#endif
			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 }
 
    index = xString.indexOf("?"); //BHeronet
 
 if (index != -1){
#ifdef DEBUG_Operater 	 
	 Serial.println(F("Prepoznao BHeronet"));	 
#endif	 
	 EEPROM_readAnything(EEPROM_mem_loc_Operater,operaterID);
	 if(operaterID != BHeronet){
		 strcpy(carrierdata.apnname, operaterHaloo.apnname);   
		 strcpy(carrierdata.apnuser, operaterHaloo.apnuser);     
		 strcpy(carrierdata.apnpass, operaterHaloo.apnpass); 
		 EEPROM_writeAnything(EEPROM_mem_loc_Operater, BHeronet);
		 EEPROM_writeAnything(EERPOM_mem_loc_SW, "SW");
		 EEPROM_writeAnything(EERPOM_mem_loc_CarrierData, carrierdata);
#ifdef DEBUG_Operater 
 Serial.println(F("Operater postaje Haloo")); 
#endif
			
   }	
	else{
#ifdef DEBUG_Operater 
	Serial.println(F("Operater se ne menja")); 
#endif		    
	}
	return;
 }
 
}// if ( 1 )


}
//-------------------------------------------------------------------
void deleteALLsms(void){
	
for (int i=1; i<33;i++) sms.DeleteSMS(i);

//gsm.SimpleWriteln("AT+CMGDA=\"DEL INBOX\"");
//gsm.SimpleWriteln("AT+CMGDA=\"DEL ALL\"");

}

bool startsWith(const char *pre, const char *str)
{
  return strncmp(pre, str, strlen(pre)) == 0;
}

int getSignalStrength(void) {
	
  char x[60];
  
  gsm.SimpleWriteln("AT+CSQ"); // Šalje AT komandu za merenje signala
  delay(1000);

  // Čitanje odgovora
  gsm.SimpleRead1(x);
  String response = "";
  response = String(x).c_str();

  //Serial.println(response); // Opcionalno: Ispisuje sirovi odgovor

  // Parsiranje rezultata
  int startIndex = response.indexOf("+CSQ: ");
  if (startIndex != -1) {
    int commaIndex = response.indexOf(",", startIndex);
    String rssiStr = response.substring(startIndex + 6, commaIndex);
    int rssi = rssiStr.toInt();

    if (rssi == 99) {
      return -1; // 99 označava nepoznat nivo signala
    } else {
      // Konvertovanje iz AT+CSQ formata u dBm
      return convertRSSIToDBm(rssi);
    }
  }
  return -1; // Neuspešno čitanje signala
}

int convertRSSIToDBm(int rssi) {
  // RSSI vrednosti od 0 do 31 su mapirane na dBm nivo signala
  if (rssi >= 0 && rssi <= 31) {
    return -113 + (rssi * 2); // Formula za konverziju RSSI u dBm
  }
  return -1; // Ako je vrednost van opsega
}				
