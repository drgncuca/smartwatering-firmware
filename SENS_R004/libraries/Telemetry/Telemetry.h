#include <String.h>

#include <TimeLib.h>
#include <SHT1x.h>

#ifndef Telemetry_h                                                   
  #define Telemetry_h 
  
//================================  B O A R D   HW    V E R S I O N   =================================//

#define BOARD_SENS_R001
//================================   P O D E S I T I    P R E    I S P O R U K E  ====================//

#define secondsWaitingSMS 1  // Podesiti na 15 - 20,  4s * 15 = 60s cekanje na SMS
#define GSM_SMS_Replay       // Ako korisik hoce povratni SMS -> Otkomentarisati
#define LOOP_Delay 0         // Pauza u loop-u, noviji moduli brzo salju/primaju podatke
//#define SKIP_Operater_Detecting   // zakomentarisati ovo !!!! ( samo za debagovanje)

//================================   Electrical  Conductivityu  =================================//

#define NMBRsampling  250  // broj semplova SINusnog signala EC drajvera
#define NMBRaveraging   4  //usrednjavanje min i max vrednosti amplitude

//---------------------------------------------------------------------------------------------
#define BUFF_RTC_TIME 1 // Za debagovanje stavi 256
#define MSG_BUF  400  // bilo 400 - moguce da zeza ....

#define BUFF_SMS 50
#define BUFF_MOB_NUM 20

//============================= V A L V E S   &&    R E L A Y S  ==========================//

  #define DC_Valves 
  //#define DC_Valves_5V // Pecovanje obavezno. Vreme izmedju dva ukljcenja zona = 1S
//#define DC_Valves_Permanent

  #define NUM_SLOTS 4  // each SLOT consume 100B of memory	

//============================= D E B U G   D I R E C T I V E ============================//

//#define DEBUG_SENTEK					   
//#define DEBUG_Server_Comun
#define DEBUG_BASIC
#define DEBUG                //210B
//#define DEBUG_Extend
//#define DEBUG_Telemetry
//#define DEBUG_Fertilisation
//#define DEBUG_FLOW
//#define DEBUG_EC
//#define DEBUG_Temperature
//#define DEBUG_ON          //od GSM modula
//#define DEBUG_Operater
//#define TIME_F_Measure
//#define DEBUG_PT1000
//#define Operater_Setings_Skip
//#define DEBUG_Power_Saving
//#define DEBUG_LiPo_CHG
//#define DEBUG_RainFallSensor
//#define DEBUG_WhatchDog   //ADD Jumper to RUN WatchDog Debug
//#define DEBUG_FLOW_100L_imp
//#define Irigation_Per_m3 
//=================================   M O D U L E S  ========================================//
#define SENTEC_Include     //3-dubine   Temperature + Humidity  400B
//#define SENTEK_Salinity    //VIC-Salinitet 
//#define SENTEK_Include_Extend_Sens  //6 dubina

#define GSM_Carrier_Change 

#define DC_Power_SENS    // Battery and Solar Panel

#define Power_Saving

#define LiPo_Charger

#define WhatchDog

#define FLOW
//#define INTERRUPTFLOW   //200B
//#define FLOW_Toggling
//#define FLOW_10L_imp
//#define FLOW_100L_imp
//#define FLOW_1000L_imp


#define Sensor_pH

#define Sensor_BGT_WSD2  //senzor Vlage i Temperature Vazduha analogni.

#define Sensor_10HS_1  //Senzor Vlage zemljista Dekagon(u obliku Viljuske ) )

#define Sensor_10HS_2  //

#define WindDurection

#define WindSpeed

#define RainFallSensor

#define SunRadiation

//#define SHT1sensor   //I2C senzor temp i vlage
//=================================== Li-Po Charger Calibration Constants ==========================================//
//#define Charging_SENS_R002 // R002
//#define battConst  335.50  // R002

#define Charging_SENS_R003 // R003
#define battConst  378  // R003/R004

#define SOLAR_40W
//#define SOLAR_20W


#define solarConst 177.2

void getLiPoDeviceID(void);
unsigned int getLiPoChargeOption(void);

unsigned int readLiPoChargeReg(void);
unsigned int getLiPoChargeCurrent(void);
unsigned int getLiPoChargeVoltage(void);
unsigned int getLiPoInputCurrent(void);
void setLiPoChargeVoltage( unsigned int Voltage);
void setLiPoChargeCurrent( unsigned int Current);
void setLiPoINcurrent( unsigned int Current);
void stopLiPoCharging();
void startLiPoCharging(unsigned int Current);
void setWdogLiPoCharger(byte timeRange);
unsigned int readRegister(byte address) ;	
byte writeRegister(byte address, unsigned int val);
void LiPoChargeInit(void);
void LiPoChargeManager(void);
void LiPoChargeConfigure(void);
unsigned int getChgCurrent(void);
unsigned int getSolarCurrent(void);
byte doesACOK(void);
byte doesItBatteryPowered(void);
//====================================Whatch Dog ==========================================//
void initiWD(void);
void enableWD(byte enable);
void togleWD(void);	
void stopAll(void);																	 
//=================================== P O W E R ==========================================//
#define AC_power_IN 7	

void setupWatchDogTimer(void);
void enterSleep(void);
void PowerStandBy(byte PowerStandByTimeInMin);
void PowerWakeUP(void);		
int getPower(void) ;
float getVoltageSolar(void);
float getVoltageBatt(void);

//==============================   Temperature PT1000  =================================//
#define RREF      4300.0
#define RNOMINAL  1000.0

// Pins for SPI comm with the AD9833 IC
#define DATA  50  ///< SPI Data pin number
#define CLK   52  ///< SPI Clock pin number
#define FSYNC 53  ///< SPI Load pin number (FSYNC in AD9833 usage)

void setupTemperaturePT1000(void);
void testTemperaturePT1000(void);
float getTemepraturePT1000(void);

//=================================  RS485  =========================================//
void RS485setup(void);
void RS485readEnableCH(void);
void RS485writeStringLN(String string);
void RS485readCharArray(char *CharArray);
String RS485readString(void);
#define SENTEK_SENS_R001

//================================   SENTEK  =======================================//																					  

#ifdef SENTEC_Include
    void setSANTEKsensors(void);
	float getSENTEKtemperature(byte debpth);
	float getSENTEKmoisture(byte debpth);
	float getSENTEKsalinity(byte debpth);
	void setSANTEKsensors(void);	
	float hexStringToFloat(char temp[], byte sensor);	
	void reqSENTEKpresetHoldReg_Temperature(void);
	void reqSENTEKpresetHoldReg_Moisture(void);
	void reqSENTEKpresetHoldReg_Salinity(void);	
	float filtering(float s,byte );	
	byte LRC (char * char_temp);
	void getEcho(void);
	void getIlegalCommand(void);
	void measureStatus(void);
	void ivenCounter(void);
	void slaveID(void);
	void sensorDepth(void);
	void sensorSampledMask(void);
	void scanSelectionMask(void);
	void detectedSelectionMask(void);
	void numberOfSensors(void);
	byte compareLRC(char * char_temp);
	void resetString(void);
	void writeToExcel( unsigned int row, unsigned int column, unsigned int value);
#endif	
//================================= BGT_WSD2 ====================================================//
int getBGTtemperature(void);
int getBGThumidity(void);
//--------------------------------------------------------------------------------------
#define CLS          "\033[2J" 

#define relayFertPump_1 35
#define relayFertPump_2 34
#define relayFertPump_3 36
#define relayFertPump_4 37

#define relay_stop   LOW
#define relay_start  HIGH


#define ph_sensor_pin A2  // pH senzor analog IN
//-------------------------Carier Operater Changing----------------------------------------------//
#define EERPOM_mem_loc_SW  64
#define EEPROM_mem_loc_Operater 62
#define EERPOM_mem_loc_CarrierData 0
#define EERPOM_mem_loc_sms_Confugured 70										

#define Telenor 1
#define Vip 2
#define Telekom 3
#define BHmob 4
#define MTEL 5
#define Haloo 6
#define BHeronet 7
#define mtelCG 8

struct carrier
{
  char apnname[20];
  char apnuser[20];
  char apnpass[20];
  char operater[20];
} ;

extern carrier carrierdata; 

 struct struct_operater_mtelCG
{
  char apnname[20]="mtelinternet";
  char apnuser[20]="internet";
  char apnpass[20]="068";  
} ;
extern struct_operater_mtelCG operaterMTEL_CG;

 struct struct_operater_vip
{
  char apnname[20]="internet";
  char apnuser[20]="internet";
  char apnpass[20]="internet";  
} ;
extern struct_operater_vip operaterVIP;

 struct struct_operater_MTS
{
  char apnname[20]="gprswap"; 
  char apnuser[20]="mts";
  char apnpass[20]="064";  
} ;
extern struct_operater_MTS operaterMTS;

 struct struct_operater_Telenor
{
  char apnname[20]="internet";
  char apnuser[20]="telenor";
  char apnpass[20]="gprs";  
} ;
extern struct_operater_Telenor operaterTelenor;

 struct struct_operater_BHmob
{
  //char apnname[20]="active.bhmobile.ba"; //pripaid
  char apnname[20]="smart.bhmobile.ba";  //postpaid
  char apnuser[20]="U";
  char apnpass[20]="P";  
} ;
extern struct_operater_BHmob operaterBHmob;

 struct struct_operater_MTEL
{
  char apnname[20]="mtelfrend";
  char apnuser[20]="";
  char apnpass[20]="";  
} ;
extern struct_operater_MTEL operaterMTEL;

 struct struct_operater_BHeronet
{
  char apnname[20]="gprs.eronet.ba";
  char apnuser[20]="";
  char apnpass[20]="";  
} ;
extern struct_operater_BHeronet operaterBHeronet;

 struct struct_operater_Haloo
{
  char apnname[20]="web.haloo.ba";
  char apnuser[20]="";
  char apnpass[20]="";  
} ;
extern struct_operater_Haloo operaterHaloo;

void doesOperaterChanging(void);
bool startsWith(const char *pre, const char *str);
void initOperators(void);
void deleteALLsms(void);
float getRSSI(void);
int getSignalStrength(void);
int convertRSSIToDBm(int );	

//------------------------Temperature and Humidity SHTx sesnor--------------------------------------------//
#define dataPin  20
#define clockPin 21

extern float temperature;
extern float humidity;
extern SHT1x SHT1x_sensor;

//----------------------------Electrical Conductivityu-------------------------------------------//
extern float EC_calibr_Coeff;
extern float EC_calibr_M_Coeff;
void setConductivityDriver(int frequency, int signalShape);
float getELconductance(void);

//============================   FLOW ======================================================//
#define FlowSensorCH1 0
#define FlowSensorCH2 1
#define FlowSensorCH3 2
#define FlowSensorCH4 3
#define FlowSensorCH5 4

#define flowConstantImpuls33PerSecond 117.0

struct irrigation_struct_FlowMetering
{
	float irrigationFlowVolume;	
	unsigned long ElapsedTime;
	float flowMeterCalibKons;

#ifdef DEBUG_FLOW 	
	float flow ;
#endif
};

extern irrigation_struct_FlowMetering  irrigationFlowMetering;

#ifdef FLOW_10L_imp
void flowImplseTick10L_Procesing(void);
unsigned int getFlowImplseTick10L(void);
float getFlow10L(void);
void resetFlowImpulseTick10L(void);
float getCurrentVOLtick10L(void);
#endif

#ifdef FLOW_100L_imp
void flowImplseTick100L_Procesing(void);
unsigned int getFlowImplseTick100L(void);
float getFlow100L(void);
void resetFlowImpulseTick100L(void);
float getCurrentVOLtick100L(void);
#endif

#ifdef FLOW_1000L_imp
void flowImplseTick1000L_Procesing(void);
unsigned int getFlowImplseTick1000L(void);
float getFlow1000L(void);
void resetFlowImpulseTick1000L(void);
float getCurrentVOLtick1000L(void);
#endif

//---------------------------------------------------------------------------------------------------
struct pH_struct {
  int rawPh;
  float pH;
  float desiredPh;
  float ferphK;   // Y = k*X +M za korekciju pri merenju pH
  float ferphM;	
  float ferKp;
  float ferKi;
  float ferKd;
};
extern pH_struct pHmeasuring;

extern int valves_fert[] ;
//-----------------------------------------------------------------
float getPh(void);
//-----------------------------------------------------------------
float getCurrentFlow(void);
void Flow(void);
void initialiseIrrFlowMet(void);
float getCurrentVOL(void);
void FlowDebug(void);
//time_t currentTime(void);
				
#endif