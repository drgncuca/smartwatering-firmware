#include <Arduino.h>
#include "Telemetry.h"


//-------Disable  RX  AND  TX ---------------------//    
void RS485setup(void){       
  
    //TX driver
    DDRH  = DDRH  | 0x04; //PH2 is out.
    PORTH = PORTH & 0xFB; //PH2 is LOW. ->Disable TX
    
    // set all RX enable control pins, as OUT and non-active.
    // CH1  RX,
    DDRD  = DDRD  | 0x10; //PD4 is out.
    PORTD = PORTD | 0x10;   //PD4 is HIGH -> Disable receiv data.
    
    // CH2  RX,
    DDRD  = DDRD  | 0x80;  //PD7 is out.
    PORTD = PORTD | 0x80;  //PD7 is HIGH -> Disable receiv data.
    
    // CH3  RX,
    DDRD  = DDRD  | 0x40;  //PD6 is out.
    PORTD = PORTD | 0x40;  //PD6 is HIGH -> Disable receiv data
    
    // CH4  RX,
    DDRD  = DDRD  | 0x20;  //PD5 is out.
    PORTD = PORTD | 0x20;  //PD5 is HIGH -> Disable receiv data
    
}

//------Enable ONE  RX  port, Other MUST be disabled------------------------//
void RS485readEnableCH(int chanell){
 
  PORTH = PORTH & 0xFB; //PH2=LOW->Disable TX
  switch(chanell){
     case 1: { //CH1
         PORTD = PORTD & 0xEF;//PD4 is LOW - > Enable Receive data on CH1
         PORTD = PORTD | 0x80;
         PORTD = PORTD | 0x40;
         PORTD = PORTD | 0x20;
         break;
    }
    case 2: { //CH2
         PORTD = PORTD | 0x10;
         PORTD = PORTD & 0x7F;
         PORTD = PORTD | 0x40;
         PORTD = PORTD | 0x20;
         break;
    }
   case 3:{//CH3
         PORTD = PORTD | 0x10;
         PORTD = PORTD | 0x80;
         PORTD = PORTD & 0xBF;
         PORTD = PORTD | 0x20;
         break;
    }
    case 4:{//CH4
         PORTD = PORTD | 0x10;
         PORTD = PORTD | 0x80;
         PORTD = PORTD | 0x40;
         PORTD = PORTD & 0xDF;
         }
   }//switch()  
}

void RS485writeStringLN(String string){   
    //disable ALL  RX  chanells
     PORTD = PORTD | 0x10;
     PORTD = PORTD | 0x80;
     PORTD = PORTD | 0x40;
     PORTD = PORTD | 0x20;
     
     // enable TX 
     PORTH = PORTH | 0x04; //PH2 is HIGH.
#if defined (SENTEK_FERT_R001) || defined (SENTEK_FERT_R002)	 
     Serial2.println(string);  
#endif	 

#ifdef SENTEK_SENS_R001	 
     Serial1.println(string);  
#endif
     delay(30); // NEED  some Delay !!!!!!
     //Disable TX
     PORTH = PORTH & 0xFB; //PH2 is LOW.
}
String RS485readString(int chanell){
	RS485readEnableCH(chanell); 
#if defined (SENTEK_FERT_R001) || defined (SENTEK_FERT_R002)  
	String str = Serial2.readString();  
#endif

#ifdef SENTEK_SENS_R001  
	String str = Serial1.readString();  
#endif
  return str; 
  
}

#ifdef SENTEK_SENS_R001 
void RS485readCharArray(int chanell, char *CharArray){
  RS485readEnableCH(chanell); 
  delay(300);
  int i = 0;
 
while (  (Serial1.available() > 0)   )
  {
    CharArray[i] = Serial1.read(); 	
	i++;
//	delay(10);
  } 
//   CharArray[17] = '\0';       
}
#endif

#if defined (SENTEK_FERT_R001) || defined (SENTEK_FERT_R002)
void RS485readCharArray(int chanell, char *CharArray){
  RS485readEnableCH(chanell); 
  delay(300);
  int i = 0;

while (  (Serial2.available() > 0)  )
  {
    CharArray[i] = Serial2.read(); 	
	i++;	
  }
//   CharArray[17] = '\0';    
}
#endif

/*
//---------Send ONE character---------------------///
void RS485writeChar(char charToSent){   
    //disable ALL  RX  chanells
     PORTD = PORTD | 0x10;
     PORTD = PORTD | 0x80;
     PORTD = PORTD | 0x40;
     PORTD = PORTD | 0x20;
     
     // enable TX 
     PORTH = PORTH | 0x04; //PH2 is HIGH.
     Serial2.print(charToSent);           
     //Disable TX
     delay(2);//  //100uS whait for send data than disable TX driver. 
     PORTH = PORTH & 0xFB; //PH2 is LOW.
}

char RS485readChar(int chanell){
  RS485readEnableCH(chanell);  
  char x = Serial2.read();  
  return x;  
}


//----Enable TX, Dissable RX----//
void preTransmission(void)
{
  RS485setup();
 
  PORTH = PORTH | 0x04; //PH2 is HIGH. ->Enable TX
}

void postTransmission(int chanell)
{
  RS485readEnableCH(chanell); // Enabled ONE  RX Chanell.
  PORTH = PORTH & 0xFB;       //PH2 is LOW. ->Disable TX
}   

//---------------Send  STRING------------------///
void RS485writeString(String string){   
    //disable ALL  RX  chanells
     PORTD = PORTD | 0x10;
     PORTD = PORTD | 0x80;
     PORTD = PORTD | 0x40;
     PORTD = PORTD | 0x20;
     
     // enable TX 
     PORTH = PORTH | 0x04; //PH2 is HIGH.
     Serial2.print(string);      
     delay(30); // NEED  some Delay !!!!!!
     //Disable TX
     PORTH = PORTH & 0xFB; //PH2 is LOW.
}
    
*/