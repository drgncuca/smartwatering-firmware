#include <Arduino.h>
#include "Telemetry.h"


void initialiseIrrFlowMet(void){
	int i = 0;
	while(i < 5)// 5  kanala za merenje protoka
	{
		irrigationFlowMetering[i].irrigationFlowVolume = 0;
		irrigationFlowMetering[i].ElapsedTime=0;
		irrigationFlowMetering[i].flowMeterConfigChanged = false;
		irrigationFlowMetering[i].flowMeterCalibKons =1.01;
		i++;
	}	
		
}
//-----------------------------------------------------------------------
float getCurrentVOL(int flowmeter_sensor){

unsigned long currentTime = millis();


unsigned long samplElapsedTime = currentTime  - irrigationFlowMetering[flowmeter_sensor].ElapsedTime; // proteklo vreme od zadnjeg semplovanja
if (samplElapsedTime < 0) {samplElapsedTime = 0;} //kad miles() funkcija dodje do kraja i pocne ispoceta da broji, prethodna jednacina bi imala ogromanu negativnu vrednost		
irrigationFlowMetering[flowmeter_sensor].ElapsedTime = currentTime;

//integralenje protoka po vremenu: Protok * Vreme = ZAPREMINA.	
float  deltaVolume = (getCurrentFlow(flowmeter_sensor)  *  (float)(samplElapsedTime)  ) /1000.0; //    protok * vreme = zapremina. millis() vraca u mS vreme.
	
//irrigationFlowMetering[flowmeter_sensor].irrigationFlowVolume = irrigationFlowMetering[flowmeter_sensor].irrigationFlowVolume + deltaVolume;

// vraca SAMO proteklu kolicinu vode u jednom loop-u
return deltaVolume;//irrigationFlowMetering[flowmeter_sensor].irrigationFlowVolume;		
}
//-------------------------------------------------------	
								// od 0 do  4
float getCurrentFlow(int flowmeter_sensor){  //vraca vrednost protoka Litara / Sekundi 

int flowIDchanel ;
float tempFlow=0;
 
switch (flowmeter_sensor){
	 case 0:flowIDchanel = A3; break;
	 case 1:flowIDchanel = A1; break;
	 case 2:flowIDchanel = A0; break;
	 case 3:flowIDchanel = A4; break;
	 case 4:flowIDchanel = A5; break;	 
	 default : break;
}
  
  int i = 0;
  float sum = 0;
  float flowSensValue = 0;
  
  //filtriranje signala sa ADC-a
  while (i < 10)
  {
    sum = sum + (float)analogRead(flowIDchanel);
    delay(1);
    i++;
  }
  flowSensValue = sum*0.10;  // averaging
  // vraca vrednost Litara/Sekundi, za sistem od 33imp/L 
  tempFlow = irrigationFlowMetering[flowmeter_sensor].flowMeterCalibKons * flowSensValue / flowConstantImpuls33PerSecond;  
  if (tempFlow<0.006) {tempFlow = 0;} //  pojavljuje se gresa/sum pri nekoj matematici, Istraziti gde je....
  
  return tempFlow;
 
}
 //-------------------------------------------------------------------------------------
		#ifdef DEBUG_FLOW 
			 void FlowDebug(int flowmeter_sensor){ // funkcija za debagovanje 			 
			 float val;
			 char buff[3];  
			 int sensorID=1;
			 int sensorValue=1;			 
			  sensorValue = analogRead(sensorID); // first read switches the MUX to this input 			  
			  val = ((float)sensorValue * 5) / 1023 ; 
			  dtostrf(val, 1, 2, buff);
			  Serial.println("-----------------------------------------------------------------");
			  Serial.print("FLOW metering CH-"); Serial.print(flowmeter_sensor); Serial.print(" "); val = getCurrentFlow(flowmeter_sensor); Serial.print(val);Serial.println("[L/s]");
			  Serial.println("");
			  Serial.print("Protekla Kolicina: ");Serial.print(getCurrentVOL(flowmeter_sensor));  Serial.print("[L]. ElapsedTime: "); Serial.print( ( (irrigationFlowMetering[flowmeter_sensor].ElapsedTime)/1000.0) );Serial.println("[S].");
			  
			}
		#endif


