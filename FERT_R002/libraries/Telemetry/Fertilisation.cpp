#include <Arduino.h>
#include "Telemetry.h"

/* Io = D * 5V / 180R.   
 *  25mA -> 4.5V -> D = 90% 
 *  4mA  -> 0.7V -> D = 15% 
 */
 #define minDutyCycle 16 // 16% -> 4mA
 #define maxDutyCycle 90 // 92% -> 25mA
 
#define dutyKoeficient 5  // definise koliki je duty PWMa u funkciji razlike zadatog pH i izmerenog.
#define dutyCycle_for_ZeroErrorPh 22 //

#ifdef fert_Iout_Fix20mA
	#define PWMpulseTogle 36
#else
	#define PWMpulseTogle 18
#endif

#define pHmultiplKoeficijent 7.5
#define PWMperiodPulse  20 // 20mS period tj frequencija PWMa: pulse + pause = 20mS -> 50Hz, inicijalno D = 25%.

extern pH_struct pHmeasuring;

int dutyCycle=15;
#ifdef fertPIDregulation
	extern float PID_dutyCycle;
	extern float PID_dutyCycle_OLD; 
#endif

float getPh()
{
  int i = 1;
  long sum = 0;
  while (i <= 10)
  {
    sum = sum + analogRead(ph_sensor_pin);
    delay(20);
    i++;
  }
 	//RAW pH
	pHmeasuring.rawPh =  sum * 0.1;
	//Serial.print(" RAW pH =");Serial.println(pHmeasuring.rawPh);						
  return  (pHmeasuring.rawPh) *  pHmeasuring.ferphK   / 204.6  + pHmeasuring.ferphM;	  //(ADDC * 5/1023) = 1/204.6 konverzija  iz digitalne vrednosti u realnu vrednost napona.
}

void valve_stop_fertilization(int fertnum){
  int fertnumIDEchanel = 0;
   switch (fertnum){
    case 1:fertnumIDEchanel = 5;break;  //CH1 - drajver fertnume1 je IDE pinout = 5
    case 2:fertnumIDEchanel = 3;break;
    case 3:fertnumIDEchanel = 6;break;
    case 4:fertnumIDEchanel = 9;break;
    case 5:fertnumIDEchanel = 8;break;  //CH5    
    default : break;
  }
  pinMode(fertnumIDEchanel, OUTPUT);
  digitalWrite(fertnumIDEchanel, LOW); //gasi  strujni drajver(pumpu) za fertilization.   	
	  
     digitalWrite(fertilizations[fertnum-1].fertilization_pin, relay_stop); 
				  fertilizations[fertnum-1].fertilization_status = 0;
				  fertilizations[fertnum-1].fertilization_duration = 0;
				  fertilizations[fertnum-1].fertilization_started_time = 0L;  
				  fertilizations[fertnum-1].fertilization_id = "";
				  fertilizations[fertnum-1].fertilization_irrigation_id = ""; 
#ifdef fertPIDregulation
	 if (fertnum == 1){
		PID_dutyCycle = 12; // vraca vrednost strujnog drajvera FER1 na 4mA -> pumpa iskljucena
		PID_dutyCycle_OLD = 2;
	 }
#endif  
}


#ifdef Fertilisation

void valve_start_fertilization(int fertnum){  //valves_fert[4] = {34, 35, 37, 36};   
     pinMode(fertilizations[fertnum-1].fertilization_pin, OUTPUT);   
digitalWrite(fertilizations[fertnum-1].fertilization_pin, HIGH  );
}


void initialiseFertilisationParameters(void){
	for (int i=0;  i<4 ; i++){
	  
	  fertilizations[i].fertilization_status = 0; // 0 = OFF, 1 = ON
	  fertilizations[i].fertilization_id="";
	  fertilizations[i].fertilization_irrigation_id="";
	  fertilizations[i].fertilization_duration = 0;
	  fertilizations[i].fertilization_started_time = 0;	  
	  
	  fertilizations[i].fertCurrentDriverCoeff = 1;  // "ferdrv "
	  fertilizations[i].fertFlowMeterCallCoeff = 1;  // "ferkf  "
	  
	  fertilizations[i].fertilization_pin = valves_fert[i];
	  pinMode(fertilizations[i].fertilization_pin, OUTPUT); 
	  digitalWrite(fertilizations[i].fertilization_pin, LOW); 
	  
	}
	pHmeasuring.rawPh     = 400; // neka RAW vrednost pH=7
	pHmeasuring.desiredPh = 5.5;//
	pHmeasuring.ferphK    = 8.81;  // Y = k*X + M
	pHmeasuring.ferphM    =-9.87;
	pHmeasuring.ferKp = 4.0;
	pHmeasuring.ferKd = 2.5;
	pHmeasuring.ferKi = 1.5;
	
}
//----------------------------------------------------------------------------------------

void fertilizationCheck(int fertnum, float pHtarget){ 

//PWM--parametri
int pulse = 5;  // vreme trajanja pulsa u mS.
int pause = 15; 

#ifdef fert_Iout_Fix20mA
	int PWMpulses=200;
	int K = 4; // Soft start
#else
	int PWMpulses=20;
	int K = 3;
#endif

float errorPh;
int fertnumIDEchanel;

//-----ako nije pokrenuta fertilizacija izadji iz funkcije.
if ( fertilizations[fertnum-1].fertilization_status == 0)
{
	return;	
}

//--------podrzano 5 strujnih drajvera, 4 se trenutno koriste
 switch (fertnum){// selektuje (IDE pin) odgovarajuci PWM strijni drajver.
  case 1:fertnumIDEchanel = 5;break;  //CH1 - drajver fertnum-e 1 na IDE pinout-u 5
  case 2:fertnumIDEchanel = 3;break;
  case 3:fertnumIDEchanel = 6;break;
  case 4:fertnumIDEchanel = 9;break;
  case 5:fertnumIDEchanel = 8;break;  //CH5
  default : break;
  }
//--------------------------------------------------------------------------------------
pHmeasuring.pH = getPh(); 

errorPh =  pHmeasuring.pH - pHtarget;                       // errorPh je u opsegu od -1 do 14.

#ifdef fert_Iout_Fix20mA   // Nema PID regulacije, Io = 20mA FIX. 
	dutyCycle = 83;
#else
	#ifndef  fertPIDregulation
		 if (errorPh < -1.4  ){   //ako je izmereni pH veci od zadatog za vise od 1.4 ( 10%) odrzi drajver na min upravljanja, 4mA     
		  pinMode(fertnumIDEchanel, OUTPUT);
		  digitalWrite(fertnumIDEchanel, LOW);	//iskljuci drajver Fertn. i stavi drajver u Hi-Z i
												//izadji iz funkcije
		  dutyCycle = 16; //drzi drajver na 4mA
		  return; // ako je dosegnut nivo pHmeasuring.pH koji je zadat zaustavlja se drajver pumpe i izlazi iz fukncije u nekom narednom ciklusu kad i ako se promeni pHmeasuring.pH nastavice nadjubravanjem.
		}
	#endif

	// Samo za FERT 1 se racuna dutyCycle -> generise struja jer je jedno pH merenje. 
	if(fertnum == 1){     
		#ifdef  fertPIDregulation //Ako je ukljucena PID regulacija 
			dutyCycle = gerDutyCycle_fertPh_PID (pHtarget);  
		#else //Ako nije ukljucen  PID regulacija																											// 5 
			dutyCycle = fertilizations[fertnum-1].fertCurrentDriverCoeff * dutyKoeficient * errorPh + dutyCycle_for_ZeroErrorPh ; //value in percente.	
		if (dutyCycle > maxDutyCycle) dutyCycle = maxDutyCycle;  //ograniciti struju Io = 25mA  max
		if (dutyCycle < minDutyCycle) dutyCycle = minDutyCycle;    // podesiti pumpu za doziranje tako da ne bude premala vrednost, jer bi predugo trajalo djubrenje.
		#endif
	}
#endif	
																																					  
pulse = ((float)dutyCycle/100) * PWMperiodPulse; 
pause = PWMperiodPulse - pulse;

PWMpulses = PWMpulseTogle; // broj PWM impulsa da se dosegne zeljeni DC nivo na strujnom drajveru.
K = 3; // Soft start
pinMode(fertnumIDEchanel, OUTPUT);  // drajver aktiviran
while(PWMpulses--){   // generise PWM signal, sa odredjenim faktorom ispune.
  digitalWrite(fertnumIDEchanel, HIGH);  
  delay(pulse / K);  //Pocetni ciklus PWM signala da bude uzi nego proracunati, da bi finije startovao sa uspotvaljanjem DC vrednosti na strujnom drajveru
  digitalWrite(fertnumIDEchanel, LOW);  
  delay(pause / K); 
  K--;
  if ( K == 0) K = 1; // zastita od delenja s 0. //izbrisati ovaj deo ako se K ne koristi.
}
  // Kad se zavrsi PWM ciklus, drajver PWMa treba staviti Hi-Z da ne bi praznio kondezator na R-C filteru drajvera.
  pinMode(fertnumIDEchanel, INPUT);  

#ifdef DEBUG_Fertilisation  
  Serial.println(F("------------------------------"));
  Serial.print(F("Fertilizacija ")); Serial.println(fertnum);
  Serial.print(F("pH zadati  = ")); Serial.println(pHtarget);
  Serial.print(F("pH izmeren = ")); Serial.println(pHmeasuring.pH); 
  Serial.print(F("pH error   = ")); Serial.println(errorPh);  
  Serial.print(F("Current OUT =")); Serial.print(0.24 * dutyCycle + 0.13 );Serial.print("mA  (");   Serial.print(dutyCycle);Serial.println("%)");
  Serial.print("Kp = ");Serial.print(pHmeasuring.ferKp);Serial.print(F(" Ki = "));Serial.print(pHmeasuring.ferKi);Serial.print(" Kd = ");Serial.println(pHmeasuring.ferKd); 
  Serial.println(F("------------------------------")); 
#endif 
}


#endif
