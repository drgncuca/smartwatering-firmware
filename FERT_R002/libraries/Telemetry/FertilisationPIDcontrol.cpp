#include <Arduino.h>
#include "Telemetry.h"

#define minDutyCycle 15   // 4mA
#define maxDutyCycle 90   //25mA
#define PWMpulseTogle 18
#define PWMperiodPulse  20 // 20mS period tj frequencija PWMa: pulse + pause = 20mS -> 50Hz, inicijalno D = 25%.

#ifdef fertPIDregulation

extern pH_struct pHmeasuring; 
	 // pH sensor parametri & istorija (dinamika ) pH greske.
		 float errorPh0 = 0; // greska pH vrednosti u trenutoj iteraciji (LOOP-u)
		 float errorPh1 = 0; // greska pH vrednosti u prethodnoj iteraciji (LOOP-u)
		 float errorPh2 = 0;// greska pH vrednosti pre dve iteracije (LOOP-u) .
  extern float PID_dutyCycle=10; //[%]
  extern float PID_dutyCycle_OLD = 0;


float gerDutyCycle_fertPh_PID(float pHtarget){

float Ki = pHmeasuring.ferKi;
float Kd = pHmeasuring.ferKd;
float Kp = pHmeasuring.ferKp;

	errorPh0 =  pHmeasuring.pH - pHtarget;  

	if(  (-0.12 < errorPh0)  && (errorPh0<0.12)    ){ //kad je greska zanemarljiva, ne babraj nista.
		return PID_dutyCycle;		
	}
	
	PID_dutyCycle_OLD = PID_dutyCycle;																						
	PID_dutyCycle = (PID_dutyCycle + 3 * Kp * ( errorPh0 - errorPh1) + 2 * Ki * errorPh0 + Kd * ( errorPh0 - 2*errorPh1 + errorPh2));	//	
	errorPh2 = errorPh1;
	errorPh1 = errorPh0;

	if ( (PID_dutyCycle  - PID_dutyCycle_OLD) > 10 ){ //soft start, da pumpa ne dobije slucajno preveliku vrednos upravljackog signala.
		PID_dutyCycle = PID_dutyCycle_OLD + 10;
	}

	if ( ( PID_dutyCycle_OLD - PID_dutyCycle ) > 10 ){
		PID_dutyCycle = PID_dutyCycle_OLD - 10;
	}

	if (PID_dutyCycle > maxDutyCycle) { PID_dutyCycle = maxDutyCycle;} //ograniciti struju Io = 25mA  max
	if (PID_dutyCycle < minDutyCycle) { PID_dutyCycle = minDutyCycle;} // 4mA, podesiti pumpu za doziranje tako da ne bude premala vrednost, jer bi predugo trajalo djubrenje.

	return PID_dutyCycle;
}

#endif