#include <Arduino.h>
#include <SoftwareSerial.h>
#include "SIM900.h"
#include "inetGSM.h"
#include "Telemetry.h"

#ifdef Power_Saving
	#include <avr/sleep.h>
	#include <avr/power.h>
	#include <avr/wdt.h>

	int InterruptWDcauntIvent = 0;
#ifdef DEBUG_Power_Saving
	unsigned long SleepTime;
#endif
extern InetGSM inet;

ISR(WDT_vect) {
 InterruptWDcauntIvent++;
}

void setupWatchDogTimer(void) {
MCUSR &= ~(1<<WDRF); 
WDTCSR |= (1<<WDCE) | (1<<WDE); 
WDTCSR  = (1<<WDP3) | (0<<WDP2) | (0<<WDP1) | (1<<WDP0);  //8s 
//WDTCSR  = (0<<WDP3) | (1<<WDP2) | (1<<WDP1) | (0<<WDP0);  //1s 
WDTCSR |= _BV(WDIE);
}

void enterSleep(void)
{
     set_sleep_mode(SLEEP_MODE_PWR_DOWN);	 
     sleep_enable(); 	 
     sleep_mode();       
  // The program will continue from here after the WDT timeout
  // First thing to do is disable sleep.
     sleep_disable(); 
     
  // Re-enable the peripherals.
	 power_all_enable();
}
	
void PowerStandBy(int StandByINminute){
int SleepTime_8s = StandByINminute * 6;//bilo je   /10; // Watch Dog Interrupt is 8 seconds !

#if defined SENTEK_FERT_R001 ||  defined SENTEK_SENS_R001
//----SENTEK---- StandBy
#endif
//GSM Power down
DDRH  = DDRH  | 0x20; PORTH = PORTH & 0xDF;  //LOW
delay(10);
//GPRS_PWR_EN
DDRE  = DDRE  | 0x04; PORTE = PORTE & 0xFB;  //LOW
//	SleepTime = currentTime();
	InterruptWDcauntIvent = 0 ;
	setupWatchDogTimer(); 
    while (InterruptWDcauntIvent < SleepTime_8s) { //ONE while lopp is 8s
        
		enterSleep();
		
    } // InterruptWDcauntIvent * 8 sekundi

	wdt_disable();
}

void PowerWakeUP(void){

#if defined SENTEK_FERT_R001 ||  defined SENTEK_SENS_R001
//----SENTEK---- Wake UP
#endif
//GSM Power UP
DDRE  = DDRE  | 0x04; PORTE = PORTE | 0x04;
delay(10);
DDRH  = DDRH  | 0x20; PORTH = PORTH | 0x20;
delay(500);

digitalWrite(7, LOW);  //GPRS RESET
delay(50);
digitalWrite(7, HIGH);
delay(50);

#ifdef DEBUG_Power_Saving
	Serial.print(F("GSM  WakeUP="));
#endif 

bool gsm_started = false;
if (gsm.begin(9600))
  {
    gsm_started = true;    
#ifdef DEBUG_Power_Saving
    Serial.println(F("READY"));
#endif 
  }
  else
  {
    gsm_started = false;
#ifdef DEBUG_Power_Saving
    Serial.println(F("IDLE"));
#endif
  
  }

if (gsm_started)
  {
#ifdef DEBUG_Power_Saving	  
    Serial.print(F("GPRS WakeUP="));
#endif	
    //if (inet.attachGPRS("internet", "telenor", "gprs"))
    if (inet.attachGPRS(String(carrierdata.apnname).c_str(), String(carrierdata.apnuser).c_str(), String(carrierdata.apnpass).c_str()))	
    {
    
#ifdef DEBUG_Power_Saving
     Serial.println(F("ATTACHED")); 
#endif
    }
    else
    {
#ifdef DEBUG_Power_Saving
      Serial.println(F("ERROR"));
#endif
    }
    delay(1000); 
  }
else
  {
#ifdef DEBUG_Power_Saving    
   Serial.println(F("GSM&GPRS=NOTstarted"));
#endif  
  }  
#ifdef DEBUG_Power_Saving  
	//Serial.print("StandBY time:"); Serial.println( currentTime() - SleepTime );   
#endif 	
}
#endif

#ifdef AC_Power_FERT
// Vraca vrednost mrenzog napona ~230Vac
int getPower(void){
  int i = 0;
  int sensmax = 0;
  int sensmin = 1023;
while (i++<150){  
int  sensorValue = analogRead(A7);
if (sensmax < sensorValue ) sensmax = sensorValue;
if (sensmin > sensorValue ) sensmin = sensorValue;
}
return (sensmax - sensmin)/4.27;
//Serial.println(sensorValue); 
}
#endif