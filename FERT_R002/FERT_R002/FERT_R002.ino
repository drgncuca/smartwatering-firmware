#include <EEPROM.h>
#include "EEPROMAnything.h"
#include <SoftwareSerial.h>
#include <Wire.h>
#include <TimeLib.h>
#include <ArduinoJson.h>
#include "SIM900.h"
#include "ds3231.h"
#include "inetGSM.h"
#include "sms.h"
#include <String.h>
#include "Telemetry.h"					
#include <SPI.h>
#include <Adafruit_MAX31865.h>

#ifdef Sensor_EC
  Adafruit_MAX31865 TemperaturePT1000 = Adafruit_MAX31865(49);
#endif

#ifdef INTERRUPTFLOW
  #include <FlowMeter.h>
#endif  

InetGSM inet;
//TODO Check max size of buffer
StaticJsonBuffer<530> jsonBuffer;
int numdata;
boolean gsm_started = false;

#ifdef Power_Saving
  int PowerStandByTimeInSeconds   = 80;
#endif

uint8_t time[8]; /// ?  ?  ?  ?  ?  ? ?  ?  ? ?  ?  ? ?  ?  ? ?  ?  ? ?  ?  ? ?  ?  ? ?  ?  ? ?  ?  ?

bool isperm3 = false;
bool isautomode = false;

char server[] = "app.smartwatering.rs";

char parcel_id[] = "rjTXqd"; // Testing Single Device FERT+Zone, Kruska NS !!
  
//char parcel_id[] = "MGTnV5"; //FERT DID for TESTING  (DID) // Testing multi device
//char parcel_id[] = "NmL73F"; //Zone DID for TESTING  (DID) // Testing multi device

//char parcel_id[] = "A4dgm";  // Lesnik Zvornik  (PID)
//char parcel_id[] = "yuIFb";  //seskejabuke
//char parcel_id[] = "HxK8jk"; //Aleksandar Rancic, Borovnice, FERT 
//char parcel_id[] = "TRZyr";  //Darko Ristic, Borovnica Ub, PID
//char parcel_id[] = "HIsWGI"; //Ivica Todorovic Borovnica. DID
//char parcel_id[] = "IS7Zj"; //Delta Agrar, Jabuka, Tornjas. PID , DC-ventili
//char parcel_id[] = "qWJdQZ"; //Borovnica Trlić FERT 10 Zones
//char parcel_id[] = "LgomjY"; //Borovnica Trlić Meteo 1 : 
//char parcel_id[] = "tCHkuC"; //Borovnica Trlić Meteo 2 :  
//char parcel_id[] = "JXUnNw"; //Zemlja i Sunce - Parcela Ivan
//char parcel_id[] = "gHduMG";//Borovnica Aleksandar Uzice, DC Relays permanent ON/OFF [DEL-13]
//char parcel_id[] = "sfWSqk";//Kupina Andrija Jelinac 4 zone AC [DEL-5]
//char parcel_id[] = "LgomjY";//Trlic FERT [DEL-1] pH, EC, SENTEK, STH20.
//char parcel_id[] = "tCHkuC";//Trlic FERT [DEL-1] SENTEK, STH20.
//char parcel_id[] = "aQjkKo";// Tornjos II
//char parcel_id[] = "ffCIYQ";//Darko Smiljkovski - Lesnik
//char parcel_id[] = "eD1BdU";//Sava Coop testna stanica
//char parcel_id[] = "8taxAn";//Damir Bulj
//char parcel_id[] = "8YOyxi";//Vladan Redzic Ecoland JAbuka  (Vlaški do)
//char parcel_id[] = "7aCids";//Radoica Radomirovic - Borovnica (Leposavic, Kosovo)
//char parcel_id[] = "B7FmJb";//Marko Smederevo : Zone 1-4
//char parcel_id[] = "M9xswP";//Marko Smederevo : Zone 5-8 
//char parcel_id[] = "9YyhWN";//Milos Aleksic Lesnik 
//char parcel_id[] = "7m4wBq";//Aleksandar Rancic, Borovnica, Zones 
//char parcel_id[] = "uPt2he";//Moshe Lifhitz, Borovnica, SENS
//char parcel_id[] = "tIzUOI";//Ivica Donji Racnik
//char parcel_id[] = "OqLzTt";//Ivica Donji Racnik
//char parcel_id[] = "qTY5yN";//DEL-28
//char parcel_id[] = "s4IUok";//Strahinja Malin START Airplant Cortanovci
//char parcel_id[] = "TkEoPS";//Poljoprivredni Fax
//char parcel_id[] = "X8pZeK";//Superior doo
  //char parcel_id[] = "U92HYT";//Milan Simovic

int number_of_zones = 10;

struct irrigation_struct
{
  int duration = 0;
  long amount = 0L;
  int status[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; // 0 = off, 1 = on
  unsigned long started_time[10] = {0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L};
  int max_duration = 0;
  String irrigation_id = "";
  float current[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
} zonal_irrigation_struct;

//pH global variabla
pH_struct pHmeasuring;

fert_struct fertilizations[4];

irrigation_struct  zonal_irrigations[NUM_SLOTS];

irrigation_struct_FlowMetering  irrigationFlowMetering[5]; // Globalna promenljiva koja sadrzi informacije o protekloj kolicini VODE na 5 kanalala flowMeter-a i proteklo vreme.

#ifdef AC_Valves // 1   2   3   4   5   6   7   8   11  12  13  14  15  16  17  18  19 - 20ti relej se mora u Asembleru hendlati
    int valves[] = {24, 23, 22, 25, 26, 27, 28, 29, 37, 36, 33, 32, 31, 30, 41, 40, 39}; // 8 AC releja (AC1-AC8) + 2 AC od FERT-a (AC9 i AC10) + 9 AC releja (AC13 - AC19).
#endif
                //AC   9  10  11  12
int valves_fert[4] = {34, 35, 37, 36};

#ifdef DC_Valves  //DC1_P  2   3   4
  byte valves_on[]  = {33, 31, 41, 39};
                   //DC1_N  2   3   4
  byte valves_off[] = {32, 30, 40, 75};
  int valvedelay = 100; // neka vrednost po difoultu u slucaju da zakaze komunikacija sa serverom 
#endif
 
#ifdef FLOW_Toggling
  byte flow_pin_Toggling = 15;// za FERT -> UART3_RX, na FERT1 PCB-u je oznacen sa: UART3_TX. 
//  byte flow_pin_Toggling = 72;// za SENS -> FLOW.
#endif

#ifdef MainPump
  byte mainpump_pin = 29; // Main Pump Relay AS IS AC_Valves for Zone 8.
#else
  byte mainpump_pin = 87;   //Some Dummies Pin
#endif

float el_conductance = 0;

//----------Temperature and Humidity-----
#ifdef SHT1sensor
  float temperature;
  float humidity;
  SHT1x SHT1x_sensor(   20  ,   21);
#endif

float ph_value = 0;

// mixer for fertilization
int mixer_pin = 4; // ne treba ovaj pin.
int mixer_status = 0; // 0 = off, 1 = on
String mixer_id = "";
String mixer_irrigation_id = "";
int mixer_duration = 0;
unsigned long mixer_started_time = 0L;

int sim_restart_pin = 7;
int arduino_restart_pin = 12;

String params = "";
//TODO: Check max size of the message and reduce it's size accordingly
char msg[MSG_BUF]; 

carrier carrierdata;
struct_operater_vip operaterVIP;
struct_operater_MTS operaterMTS;
struct_operater_Telenor operaterTelenor;

// last time successfully contacted server
unsigned long last_seen = 0L;
#ifdef INTERRUPTFLOW
	// fmeter sensor
	double flowmeter_capacity = 30;
	double flowmeter_kfactor = 4.25;
	double flowmeter_mfactor = 1;

	FlowSensorProperties flowmeter_sensor = {flowmeter_capacity, flowmeter_kfactor, {flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor}};
	FlowMeter Meter = FlowMeter(2, flowmeter_sensor);
	long flowmeter_period = 1000; // one second (in milliseconds)
	long flowmeter_lasttime = 0;

void MeterISR()
{  
  Meter.count();// let our flow meter count the pulses
}
#endif

#if defined FLOW || defined INTERRUPTFLOW					   
struct flowmeter_struct
{
  float currentflow;
  float currentvolume;
};
#endif

bool carrierChange = false;

#ifdef TIME_F_Measure
unsigned long currentTimeMilles;
#endif
//--------------------------------------------------------------------------------------
void setup()
{  
  digitalWrite(arduino_restart_pin, HIGH);
  delay(200);
  pinMode(arduino_restart_pin, OUTPUT);
  Serial.begin(115200); // Serial connection.
  
  RS485setup(); 
#ifdef SENTEC_Include  
  setSANTEKsensors(); 
#endif
  Serial.println(F("(RE)start uC"));
#ifdef Fertilisation
  initialiseFertilisationParameters();
#endif
  valve_stop_fertilization(1);
  valve_stop_fertilization(2);
  valve_stop_fertilization(3);
  valve_stop_fertilization(4);

  initialiseIrrFlowMet(); //REsetovanje Zapremine i globalnog proteklog vremena na 0 vrednosti.
  
  Wire.begin();
  
#ifdef Sensor_EC  
  setConductivityDriver(70, 1); // setovanje SINusnog generatora: 70Hz, 1-> sinus.  
  setupTemperaturePT1000();// Setovanje temperaturnog senzora za PT1000.
#endif

#ifdef SERVISE_Setings
  DS3231_init(DS3231_INTCN);
  DS3231_clear_a1f();
#endif
  
  unsigned long now = millis(); ///  ?  ?   ?  ?   ?  ?   ?  ?   ?  ?   ?  ?   ?  ?   ?  ?   ?  ?   ?  ?   ?  ?   ?  ?   ?  ? 
  struct ts t;

#ifdef SERVISE_Setings  //podesavanje vremena
  t.sec = 0;
  t.min = 30;
  t.hour =18;
  t.mday = 4;
  t.mon = 4;
  t.year = 2020;
  DS3231_set(t);
#endif

  DS3231_get(&t);

#ifdef INTERRUPTFLOW
  // enable a call to the 'interrupt service handler' (ISR) on every rising edge at the interrupt pin
  attachInterrupt(INT7, MeterISR, RISING);
  // sometimes initializing the gear generates some pulses that we should ignore
  Meter.reset();
#endif
						   
  pinMode(sim_restart_pin, OUTPUT);
  digitalWrite(sim_restart_pin, HIGH);
  digitalWrite(sim_restart_pin, LOW);
  delay(500);
  digitalWrite(sim_restart_pin, HIGH);
  delay(50);

#ifdef DEBUG_BASIC
  Serial.print(F("GSM="));
#endif 
  
  if (gsm.begin(9600))
  {
    gsm_started = true;    
#ifdef DEBUG_BASIC
    Serial.println(F("READY"));
#endif 

//Pri Debagovanju, preskoci jedan deo koda, da ubrza startovanje uC-a
#ifdef SKIP_Operater_Detecting
    goto skip_operater_detecting;
#endif

  }
  else
  {
    gsm_started = false;
#ifdef DEBUG_BASIC
    Serial.println(F("IDLE"));
#endif
  
  }

doesOperaterChanging();// Promena operatera

EEPROM_readAnything(EERPOM_mem_loc_CarrierData, carrierdata);

#ifdef DEBUG_Operater 
    char tmp[2]="00"; 
    Serial.println("\n---------------------------");    
    Serial.println("Sadrzaj EEPROM-a : ");   
    EEPROM_readAnything(64, tmp);
    Serial.println(String(tmp).c_str());     
    Serial.println("Data in the Carrier Data Structure");+
    Serial.println(carrierdata.apnname);
    Serial.println(carrierdata.apnuser);
    Serial.println(carrierdata.apnpass);        
#endif; 

if (gsm_started)
  {
    Serial.print(F("GPRS="));    
//Pri Debagovanju, preskoci jedan deo koda, da ubrza startovanje uC-a    
#ifdef SKIP_Operater_Detecting  
    skip_operater_detecting:
    Serial.print(F("GPRS="));  
    if (inet.attachGPRS("internet", "telenor", "gprs"))
    {
#else
    if (inet.attachGPRS(String(carrierdata.apnname).c_str(), String(carrierdata.apnuser).c_str(), String(carrierdata.apnpass).c_str()))
    {
#endif    
    
#ifdef DEBUG_BASIC
      Serial.println(F("ATTACHED")); 
#endif
#ifdef DEBUG
     gsm.SimpleWriteln("AT+CIFSR");//Read IP address.
     delay(5000);     
     gsm.WhileSimpleRead();//Read until serial buffer is empty
#endif
    }
    else
    {
#ifdef DEBUG_BASIC
      Serial.println(F("ERROR"));
#endif
    }
    delay(1000); 
  }
else
  {
#ifdef DEBUG_BASIC    
   Serial.println(F("GSM&GPRS=NOTstarted"));
#endif  
  }  
  // Initialize  PINs
#ifdef AC_Valves  
  for (int x=0; x < NUM_SLOTS;  x++)
  {
    pinMode(valves[x], OUTPUT);
    digitalWrite(valves[x], relay_stop);
  }
#endif  

#ifdef DC_Valves
   for (int i=0; i<NUM_SLOTS;i++ )
  {
    pinMode(valves_on[i], OUTPUT);
    digitalWrite(valves_on[i],LOW);
    
    pinMode(valves_off[i], OUTPUT);
    digitalWrite(valves_off[i],LOW);
    
  }
  DDRG  = DDRG  | 0x08; // Relay DC4_N <OUT>  
  PORTG = PORTG & 0xF7; // Relay DC4_N <LOW>

#endif

#ifdef FLOW_Toggling
   pinMode(flow_pin_Toggling, INPUT);
//   DDRE  = DDRE & 0xEF;  // Za SENS - FLOW input 
#endif

  pinMode(mainpump_pin, OUTPUT);
  digitalWrite(mainpump_pin, relay_stop);

  pinMode(mixer_pin, OUTPUT);
  digitalWrite(mixer_pin, relay_stop);

  pinMode(sim_restart_pin, OUTPUT);
  digitalWrite(sim_restart_pin, HIGH);        

};
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void loop()
{
#ifdef DEBUG_BASIC
  Serial.println(F("Usao u loop"));
#endif

#if defined FLOW || defined INTERRUPTFLOW
  struct flowmeter_struct flow = checkFlowMeter(); //azurira vrednost protoka i zapremine
  checkValves(flow.currentvolume);
#else
  checkValves(0);
#endif
 
#ifdef INTERRUPTFLOW  
   Meter.reset();				 
#endif				   
#ifdef FLOW_Toggling 
   bool currentFlowStatus = checkFlow() ? 1 : 0;  // ako ima bilo kakvog protoka vraca '1'
#endif

  if(isautomode) {
    checkLastSeen();
  }

#ifdef Sensor_pH  
  ph_value = getPh();
#endif

#ifdef SHT1sensor
   Wire.end();  // u konfliktu dva I2C-a RTC i Temp-Humid senzor
   temperature = SHT1x_sensor.readTemperatureC();  
   humidity = SHT1x_sensor.readHumidity();
   Wire.begin();    
#endif

#ifdef Sensor_EC
  el_conductance = getELconductance(); 
#endif  

  params = "";  
  params.concat("did=" + String(parcel_id));
//params.concat("pid=" + String(parcel_id));
 
//----------------------- SENTEK sensors 1, 2, 3 -------------------------
#ifdef SENTEC_Include   

        reqSENTEKpresetHoldReg_Temperature();
        delay(200);  
        params.concat("&t1=" + String(getSENTEKtemperature(1))); //temperature
        delay(200);  
        params.concat("&t2=" + String(getSENTEKtemperature(2)));
        delay(200);  
        params.concat("&t3=" + String(getSENTEKtemperature(3)));  
        delay(200);        
        reqSENTEKpresetHoldReg_Moisture();
        delay(800);
        params.concat("&m1=" + String(getSENTEKmoisture(1))); //moisture
        delay(200);        
        params.concat("&m2=" + String(getSENTEKmoisture(2)));
        delay(200);
        params.concat("&m3=" + String(getSENTEKmoisture(3)));
        
   #ifdef SENTEK_Salinity      
        delay(50);
        reqSENTEKpresetHoldReg_Salinity();
        delay(50);
        params.concat("&s1=" + String(getSENTEKsalinity(1)));//salinity
        delay(50);
        params.concat("&s2=" + String(getSENTEKsalinity(2)));
        delay(50);
        params.concat("&s3=" + String(getSENTEKsalinity(3)));
        
   #endif   
#endif
//----------------------- SENTEK sensors 4, 5, 6 -------------------------
#ifdef SENTEK_Include_Extend_Sens

        reqSENTEKpresetHoldReg_Temperature();
        delay(200);  
        params.concat("&t4=" + String(getSENTEKtemperature(4))); //temperature
        delay(200);  
        params.concat("&t5=" + String(getSENTEKtemperature(5)));
        delay(200);  
        params.concat("&t6=" + String(getSENTEKtemperature(6)));  
        delay(200);
        reqSENTEKpresetHoldReg_Moisture();
        delay(800);
        params.concat("&m4=" + String(getSENTEKmoisture(4))); //moisture
        delay(200);
        params.concat("&m5=" + String(getSENTEKmoisture(5)));
        delay(200);
        params.concat("&m6=" + String(getSENTEKmoisture(6)));
    #ifdef SENTEK_Salinity        
        delay(50);
        reqSENTEKpresetHoldReg_Salinity();
        delay(50);
        params.concat("&s4=" + String(getSENTEKsalinity(4)));//salinity
        delay(50);
        params.concat("&s5=" + String(getSENTEKsalinity(5)));
        delay(50);
        params.concat("&s6=" + String(getSENTEKsalinity(6)));
    #endif
#endif

#ifdef Sensor_10HS
  params.concat("&h1=" + String(analogRead(A8)/2)); // podeljeno sa 2, jer je analogni ulaz pojacan sa 2x. A8 - Chanl 4 (na PCB je labela AN_IN3)
#endif  
//params.concat("&h2=" + String(0));

#ifdef SHT1sensor
  params.concat("&ah=" + String(humidity));
  params.concat("&at=" + String(temperature));
#endif  
//  params.concat("&temp=" + String(0));

#ifdef DC_Power_SENS  
  params.concat("&pow=" + String((float)analogRead(A8) * 0.0148 ));// Battery Voltage for SENS board
  params.concat("&sol=" + String((float)analogRead(A9) * 0.0280 ));// Solar Voltage for SENS board
#endif
  
#ifdef AC_Power_FERT
int power = getPower();
  params.concat("&pow=" + String(power));                           // za FERT plocu
#endif

  params.concat("&mid=" + String(mixer_id));
  
#ifdef Fertilisation  
  #ifdef FERT_0
     params.concat("&fid="  + String(fertilizations[0].fertilization_id));
  #endif
  #ifdef FERT_2
     params.concat("&fid2=" + String(fertilizations[1].fertilization_id));
  #endif
  #ifdef FERT_3
     params.concat("&fid3=" + String(fertilizations[2].fertilization_id));
  #endif
  #ifdef FERT_4 
     params.concat("&fid4=" + String(fertilizations[3].fertilization_id));
  #endif
#endif

#ifdef Sensor_pH
  params.concat("&ferph=" + String(pHmeasuring.rawPh));//salje se RAW pH.
#endif
 
#if defined FLOW || defined INTERRUPTFLOW 
  params.concat("&cfl=" + String(flow.currentflow));
  params.concat("&tfv=" + String(flow.currentvolume));  
#endif  

#ifdef FLOW_Toggling  
  params.concat("&cfl=" + String(currentFlowStatus));
#endif    

#ifdef Sensor_EC
  params.concat("&ecv=" + String(el_conductance));
//  params.concat("&ect=" + String(tempPT1000)); // Samao za debagovanje. Zakomenentarisati!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#endif
  
  String activeirrigations;
  for (int i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id != "")
    {
      //activeirrigations.concat("," + String(zonal_irrigations[i].irrigation_id) + "("+ String(currentTime() - zonal_irrigations[i].started_time[processing_zone - 1]) +")");
      activeirrigations.concat("," + String(zonal_irrigations[i].irrigation_id));
    }
  }
  activeirrigations.remove(0, 1);
  params.concat("&iids=" + String(activeirrigations));

#ifdef DEBUG_BASIC  
  Serial.println(params.c_str());
#endif

  memset(msg, 0, sizeof(msg)); 
//apiCall(server, 80, "/api/post/sync.php", params.c_str(), msg, MSG_BUF);
  apiCall(server, 80, "/api/post/sync_device.php", params.c_str(), msg, MSG_BUF);


  char *jsonResponse = strstr(msg, "{"); 
#ifdef DEBUG_BASIC  
  /*Serial.println(F("json:"));*/ Serial.println(jsonResponse);
#endif
//--------------------------------------------------------------------------------
  JsonObject &root = jsonBuffer.parseObject(jsonResponse);
//--------------------------------------------------------------------------------


  JsonVariant numberofzones_json = root["zones"];
  if (numberofzones_json.success())
  {
    if (number_of_zones != root["zones"])
    {
      number_of_zones = root["zones"];
    }
  }


#ifdef DC_Valves
  JsonVariant valvedelay_json = root["valvedelay"];
  if (valvedelay_json.success())
  {
    if (valvedelay != root["valvedelay"])
    {
      valvedelay = root["valvedelay"];
    }
  }
#endif
//---------------------------------------------------------------------
  JsonObject &flowmeter_config = root["flowconf"];
  if (flowmeter_config.success())
  {
#ifdef FLOW
    irrigationFlowMetering[FlowSensorCH5].flowMeterCalibKons = flowmeter_config["kf"];
#endif

#ifdef INTERRUPTFLOW
bool flow_config_changed = false;
    if (flowmeter_config["cap"] != flowmeter_capacity)
    {
      flowmeter_capacity = flowmeter_config["cap"];
      flow_config_changed = true;
    }
    if (flowmeter_config["kf"] != flowmeter_kfactor)
    {
      flowmeter_kfactor = flowmeter_config["kf"];
      flow_config_changed = true;
    }
    if (flowmeter_config["mf"] != flowmeter_mfactor)
    {
      flowmeter_mfactor = flowmeter_config["mf"];
      flow_config_changed = true;
    }

    if (flow_config_changed)
    {
      FlowSensorProperties flowmeter_sensor = {flowmeter_capacity, flowmeter_kfactor, {flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor, flowmeter_mfactor}};
      Meter = FlowMeter(2, flowmeter_sensor);
    }
#endif								 
  }
  JsonObject &start_mixer = root["mix"];
  if (start_mixer.success())
  {
    if (strcmp(start_mixer["mid"].as<String>().c_str(), mixer_id.c_str()) != 0)
    {
      mixer_start(start_mixer["mid"].as<String>(), start_mixer["iid"].as<String>(), start_mixer["duration"]);
    }
  }

  JsonObject &start_irrigation = root["irr"];
  if (start_irrigation.success())
  {
    // TODO: check if this irrigationid is started
    if (strcmp(start_irrigation["irrigationid"].as<String>().c_str(), root["irr"]["iid"].as<String>().c_str()) != 0)
    {
      isautomode = start_irrigation["automode"];
      if (start_irrigation["zonal"])
      {
        if (start_irrigation["duration"])
        {
          //per time
          isperm3 = false;
          zonal_irrigation_start(start_irrigation["iid"].as<String>(), start_irrigation["duration"], start_irrigation["zones"]);
          //Serial.println("Per Time");
          //zonal_irrigation_start_DBG(start_irrigation["iid"].as<String>(), start_irrigation["duration"], start_irrigation["zones"]);
        }
        else
        {
          //per m3
          isperm3 = true;
          zonal_irrigation_start(start_irrigation["iid"].as<String>(), start_irrigation["amount"], start_irrigation["zones"], start_irrigation["maxduration"]);          
        }
      }
    }
  }

//-----------------------koeficijent za kalibraciju pH senzora-- Y = k*X + M -----------------------//
   JsonVariant ferphK_json = root["ferphk"];
    if (ferphK_json.success())
    {
      if (pHmeasuring.ferphK != root["ferphk"])
      {
          pHmeasuring.ferphK = root["ferphk"];
      }
    }

   JsonVariant ferphM_json = root["ferphm"];
    if (ferphK_json.success())
    {
      if (pHmeasuring.ferphM != root["ferphm"])
      {
          pHmeasuring.ferphM  = root["ferphm"];
      }
    }
//-----------------Koeficijenti P I D regulatora za fert ----------------------------------
  JsonVariant ferPID_Kp_json = root["ferkp"];
    if (ferPID_Kp_json.success())
    {
      if (pHmeasuring.ferKp != root["ferkp"])
      {
          pHmeasuring.ferKp = root["ferkp"];
      }
    }
      JsonVariant ferPID_Ki_json = root["ferki"];
    if (ferPID_Ki_json.success())
    {
      if (pHmeasuring.ferKi != root["ferki"])
      {
          pHmeasuring.ferKi = root["ferki"];
      }
    }
        JsonVariant ferPID_Kd_json = root["ferkd"];
    if (ferPID_Kd_json.success())
    {
      if (pHmeasuring.ferKd != root["ferkd"])
      {
          pHmeasuring.ferKd = root["ferkd"];
      }
    }
//---------------Fertilisation Flow Calibration Koeficient------------------
#ifdef Fertilisation
    JsonVariant ferFlowKoef_json = root["ferkf"];
    if (ferFlowKoef_json.success())
    {
      if (fertilizations[0].fertFlowMeterCallCoeff != root["ferkf"])
      {
          fertilizations[0].fertFlowMeterCallCoeff = root["ferkf"];
      }
    }

//--------------Fertilisation Current Driver Calibration Koeficient----------------------------
    JsonVariant ferCurrentKoef_json = root["ferdrv"];
    if (ferCurrentKoef_json.success())
    {
      if (fertilizations[0].fertCurrentDriverCoeff != root["ferdrv"])
      {
          fertilizations[0].fertCurrentDriverCoeff = root["ferdrv"];
      }
    }
//----------ovi nisu implementirani---------------------------------------------------------
//        JsonVariant ferCurrentKoef_json = root["ferdrv"];
//    if (ferCurrentKoef_json.success())
//    {
//      if (fertilizations[0].fertCurrentDriverCoeff != root["ferdrv"])
//      {
//          fertilizations[0].fertCurrentDriverCoeff = root["ferdrv"];
//      }
//    }
//        JsonVariant ferCurrentKoef_json = root["ferdrv"];
//    if (ferCurrentKoef_json.success())
//    {
//      if (fertilizations[0].fertCurrentDriverCoeff != root["ferdrv"])
//      {
//          fertilizations[0].fertCurrentDriverCoeff = root["ferdrv"];
//      }
//    }
//        JsonVariant ferCurrentKoef_json = root["ferdrv"];
//    if (ferCurrentKoef_json.success())
//    {
//      if (fertilizations[0].fertCurrentDriverCoeff != root["ferdrv"])
//      {
//          fertilizations[0].fertCurrentDriverCoeff = root["ferdrv"];
//      }
//    }
#endif

//---------EC calbracioni koeficijent EC = k*X + m -----------------------------------------
#ifdef Sensor_EC
 JsonVariant EC_CalibrCoeff_json = root["eck"];
    if (EC_CalibrCoeff_json.success())
    {
      if ( EC_calibr_Coeff != root["eck"])
      {
           EC_calibr_Coeff = root["eck"];
      }
    }

 JsonVariant EC_CalibrCoeff_M_json = root["ecm"];
    if (EC_CalibrCoeff_M_json.success())
    {
      if ( EC_calibr_M_Coeff != root["ecm"])
      {
           EC_calibr_M_Coeff = root["ecm"];
      }
    }
#endif    

//---------------------------------------------------------------------------
#ifdef Fertilisation
JsonObject &start_fertilization = root["fer"];
  if (start_fertilization.success())
{
    fertilization_start(start_fertilization["fid"].as<String>(), start_fertilization["iid"].as<String>(), start_fertilization["duration"]
                        ,fertilizations[0].fertilization_pin, 1, start_fertilization["dph"]);
}

  JsonObject &start_fertilization2 = root["fer2"];
  if (start_fertilization2.success())
  {
    fertilization_start(start_fertilization2["fid2"].as<String>(), start_fertilization2["iid"].as<String>(), start_fertilization2["duration"]
                        ,fertilizations[1].fertilization_pin, 2, start_fertilization2["dph"] ); //fer2drv i ostali nisu na serveru implementirani
  }

  JsonObject &start_fertilization3 = root["fer3"];
  if (start_fertilization3.success())
  {
    fertilization_start(start_fertilization3["fid3"].as<String>(), start_fertilization3["iid"].as<String>(), start_fertilization3["duration"]
                        ,fertilizations[2].fertilization_pin, 3, start_fertilization3["dph"]);
  }
  JsonObject &start_fertilization4 = root["fer4"];
  if (start_fertilization4.success())
  {
    fertilization_start(start_fertilization3["fid4"].as<String>(), start_fertilization4["iid"].as<String>(), start_fertilization4["duration"]
                        ,fertilizations[3].fertilization_pin, 4, start_fertilization4["dph"] );
  }
#endif  
//-----------------------------------------------------------------------------------------------------//

  JsonVariant stop_irrigation_id_json = root["stopirr"];
  if (stop_irrigation_id_json.success())
  {    
    char irrid[5];
    strcpy(irrid, root["stopirr"].as<const char *>());
    
    if (mixer_id != "")
      stop_mixer(irrid);
#ifdef Fertilisation  
    if (fertilizations[0].fertilization_id != "") stop_fertilization(irrid, 1);
      
    if (fertilizations[1].fertilization_id != "") stop_fertilization(irrid, 2);
      
    if (fertilizations[2].fertilization_id != "") stop_fertilization(irrid, 3);
      
    if (fertilizations[3].fertilization_id != "") stop_fertilization(irrid, 4);
#endif      
    delay(4000); // saceka da se strujni drajver smanji na min pa tek onda da gasi reley-pumpu  
    stop_irrigation(irrid);
    
  }
//--------------------------------------------------------------------------------																				  
#ifdef Power_Saving

 JsonVariant PowerStandByTime_json = root["slp"];
    if (PowerStandByTime_json.success())
    {
      if (PowerStandByTimeInSeconds != root["slp"])
      {
          PowerStandByTimeInSeconds = root["slp"];
      }
   }
   
  PowerStandBy(PowerStandByTimeInSeconds); // kad proradi podeasvanje sa servera -> otkomentarisati
  //PowerStandBy(80); // Stand-BY 80 sekundi, dok ne proradi sa servera slanje inf o sllep-u
  PowerWakeUP();

#endif		
  
jsonBuffer.clear();

}//--------- END  ----  of  -----  L O O P ( )  -----------------
//------------------------------------------------------------------------------------------------
//per duration
void zonal_irrigation_start(String irrigationid, int duration, JsonArray &zones)
{
  for (int i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id == "")
    {
      zonal_irrigations[i].irrigation_id = irrigationid;
      for (int zone : zones)
      {
        delay(170);
        zonal_irrigations[i].duration = duration;
#ifdef AC_Valves        
        digitalWrite(valves[zone - 1], relay_start);
#endif
        
#ifdef DC_Valves
    startValve(zone-1);
#endif        
        digitalWrite(mainpump_pin, relay_start);
        zonal_irrigations[i].started_time[zone - 1] = currentTime();
        zonal_irrigations[i].status[zone - 1] = 1;
#ifdef DEBUG
        String logger = "Palim zonu " + String(zone);
        Serial.println(logger);
#endif
      }
#ifdef DEBUG
      String logger = "Started zonal irrigation ID " + String(zonal_irrigations[i].irrigation_id);
      Serial.println(logger);
#endif
      params = "";
      memset(msg, 0, sizeof(msg));
      params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));
      apiCall(server, 80, "/api/post/irrigation_started.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif
      return;
    }
  } //for (int i = 0; i < NUM_SLOTS; i++)
}
//-------------------- F O R   D E B A G I N G----------------------------------
//void zonal_irrigation_start_DBG(String irrigationid, int duration, JsonArray &zones)
//{
// zones.printTo(Serial) ;
//
//  for (int i = 0; i < NUM_SLOTS; i++)
//  {
//    if (zonal_irrigations[i].irrigation_id == "")
//    {
//      zonal_irrigations[i].irrigation_id = irrigationid;
//      for (int zone : zones)
//      {
////        delay(170);
//        Serial.print("zone: ");Serial.print(zone);
//        zonal_irrigations[i].duration = duration;
//        Serial.print(zonal_irrigations[i].duration);Serial.print(" ");
//       digitalWrite(mainpump_pin, relay_start);
//        zonal_irrigations[i].started_time[zone - 1] = currentTime();
//        Serial.print(zonal_irrigations[i].started_time[zone - 1]);Serial.print(" ");
//        zonal_irrigations[i].status[zone - 1] = 1;
//        Serial.print(zonal_irrigations[i].status[zone - 1]);Serial.print(" ");
//
//      }
//      Serial.println("");
//
//      return;
//    }
//  } //for (int i = 0; i < NUM_SLOTS; i++)
//}
//------------------------------------------------------------------------------
//per m3
void zonal_irrigation_start(String irrigationid, long amount, JsonArray &zones, int max_duration)
{

  for (int i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id == "")
    {
      zonal_irrigations[i].irrigation_id = irrigationid;
      for (int zone : zones)
      {
        delay(100);
        zonal_irrigations[i].amount = amount;
        zonal_irrigations[i].max_duration = max_duration;
        
#ifdef AC_Valves        
        digitalWrite(valves[zone - 1], relay_start);
#endif

#ifdef DC_Valves
    startValve(zone-1);
#endif        
        digitalWrite(mainpump_pin, relay_start);
        zonal_irrigations[i].current[zone - 1] = 0;
        zonal_irrigations[i].status[zone - 1] = 1;
        zonal_irrigations[i].started_time[zone - 1] = currentTime();
#ifdef DEBUG
        String logger = "Starting zone " + String(zone);
        Serial.println(logger);
#endif
      }
#ifdef DEBUG
      String logger = "Started zonal irrigation ID " + String(zonal_irrigations[i].irrigation_id);
      Serial.println(logger);
#endif
      params = "";
      memset(msg, 0, sizeof(msg));
      params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));
      apiCall(server, 80, "/api/post/irrigation_started.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif
      return;
    }
  }
}

void mixer_start(String mixerid, String irrigationid, int duration)
{
  mixer_id = mixerid;
  mixer_irrigation_id = irrigationid;
  mixer_duration = duration;
  mixer_status = 1;
  digitalWrite(mixer_pin, relay_start);
  mixer_started_time = currentTime();
#ifdef DEBUG
  String logger = "Started mixer ID " + String(mixer_id);
  Serial.println(logger);
#endif
  params = "";
  params.concat("mid=" + String(mixer_id));
  memset(msg, 0, sizeof(msg));
  apiCall(server, 80, "/api/post/mixer_started.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
  char *jsonResponse = strstr(msg, "{");
  Serial.println(jsonResponse);
#endif
}

#ifdef Fertilisation
void fertilization_start(String fertilizationid, String irrigationid, int duration, int pinnumber, int fertnum , float fert_pHtarget )
{
    fertilizations[fertnum-1].fertilization_id = fertilizationid;
    fertilizations[fertnum-1].fertilization_irrigation_id = irrigationid;
    fertilizations[fertnum-1].fertilization_duration = duration;
    fertilizations[fertnum-1].fertilization_status = 1;
    fertilizations[fertnum-1].fertilization_started_time = currentTime();

    if(fertnum == 1){
        pHmeasuring.desiredPh = fert_pHtarget;
    }
    valve_start_fertilization(fertnum); //ukljucuje relej-pumpu za fertilizaciju.

    fertilizationCheck( fertnum, fert_pHtarget);// pocni sa merenjem pH i doziranjem djubriva

    #ifdef DEBUG_Extend  
      String logger = "Started fertilization " + String(fertnum) + "  ID:"  + fertilizationid;
      Serial.println(logger);
    #endif
      params = "";
      params.concat("fid=" + String(fertilizationid));
      memset(msg, 0, sizeof(msg));
      apiCall(server, 80, "/api/post/fertilization_started.php", params.c_str(), msg, MSG_BUF);
    #ifdef DEBUG
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
    #endif
}
#endif

void stop_irrigation(String irrigationid)
{
  // stop zonal irrigation
  for (int i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id == irrigationid)
    {
      // turn of all active zones
      for (int checking_zone = 1; checking_zone <= number_of_zones; checking_zone++)
      {
        if (zonal_irrigations[i].status[checking_zone - 1] == 1)
        {
delay(170);
#ifdef AC_Valves          
     digitalWrite(valves[checking_zone - 1], relay_stop);
#endif
#ifdef DC_Valves
    stopValve(checking_zone-1);
#endif
#ifdef DEBUG_Extend
     Serial.print(F("Gasim zonu "));Serial.println(checking_zone);
#endif          
          zonal_irrigations[i].status[checking_zone - 1] = 0;
        }
        zonal_irrigations[i].started_time[checking_zone - 1] = 0L;
      }
      zonal_irrigations[i].irrigation_id = "";
      zonal_irrigations[i].duration = 0;

      // check to turn off pump if there is no active irrrigation
      bool is_some_irrigation_on = false;
      for (int checking_slot = 0; checking_slot < NUM_SLOTS; checking_slot++)
      {
        if (zonal_irrigations[checking_slot].irrigation_id != "")
        {
          for (int checking_zone = 1; checking_zone <= number_of_zones; checking_zone++)
          {
            if (zonal_irrigations[checking_slot].status[checking_zone - 1] == 1)
            {
              is_some_irrigation_on = true;
              break;
            }
          }
        }
      }
      if (!is_some_irrigation_on)
      {
        digitalWrite(mainpump_pin, relay_stop);
      //  main_pump_status = 0;
      }
#ifdef DEBUG
      String logger = "Stopped irrigation ID " + String(irrigationid);
      Serial.println(logger);
#endif
      params = "";
      params.concat("iid=" + String(irrigationid));
      memset(msg, 0, sizeof(msg));
      apiCall(server, 80, "/api/post/irrigation_stopped.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif
    }
  }
}

void valve_mixer_stop() {
   digitalWrite(mixer_pin, relay_stop);
    mixer_status = 0;
    mixer_duration = 0;
    mixer_started_time = 0L;
    String stopped_mixer_id = String(mixer_id);
    mixer_id = "";
    mixer_irrigation_id = "";

#ifdef DEBUG
    String logger = "Stopped mixer ID " + String(stopped_mixer_id);
    Serial.println(logger);
#endif
}

void stop_mixer(String irrigationid)
{
  if (mixer_irrigation_id == irrigationid)
  {
    valve_mixer_stop();
    params = "";
    params.concat("mid=" + String(mixer_id));
    memset(msg, 0, sizeof(msg));
    apiCall(server, 80, "/api/post/mixer_stopped.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
    char *jsonResponse = strstr(msg, "{");
    Serial.println(msg);
#endif
  }
}

#ifdef Fertilisation
void stop_fertilization(String irrigationid, int fertnum)
{
   if (fertilizations[fertnum-1].fertilization_irrigation_id == irrigationid)
    {
      String tmp = fertilizations[fertnum-1].fertilization_id ; // funkcija ispod resetuje ovaja parametar
      valve_stop_fertilization(fertnum); // Resetuje parametre ferta-a i gasi releje-pumpu.
#ifdef DEBUG_Extend
      String logger = "Stopped fertilization " + String(fertnum) + "  ID:"  + tmp;
      Serial.println(logger);
#endif
      params = "";
      params.concat("fid=" + tmp);
      memset(msg, 0, sizeof(msg));
      apiCall(server, 80, "/api/post/fertilization_stopped.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
      char *jsonResponse = strstr(msg, "{");
      Serial.println(msg);
#endif    
    }
}
#endif

void checkValves(float totalVolume)
{
  // mixer check
  if (mixer_status == 1 && !(currentTime() - mixer_started_time < mixer_duration * 60L))
  {
    digitalWrite(mixer_pin, relay_stop);
    mixer_status = 0;
    mixer_duration = 0;
    mixer_started_time = 0L;
    String completed_mixer_id = String(mixer_id);
    mixer_id = "";
    mixer_irrigation_id = "";

#ifdef DEBUG
    String logger = "Completed mixer ID " + String(completed_mixer_id);
    Serial.println(logger);
#endif
    params = "";
    params.concat("mid=" + String(completed_mixer_id));
    memset(msg, 0, sizeof(msg));
    apiCall(server, 80, "/api/post/mixer_finished.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
    char *jsonResponse = strstr(msg, "{");
    Serial.println(msg);
#endif
  }
//----------------------------------------------------------------
#ifdef Fertilisation
  // fertilization check
  if (fertilizations[0].fertilization_status == 1 && !(currentTime() - fertilizations[0].fertilization_started_time < fertilizations[0].fertilization_duration * 60L))
  {    
#ifdef DEBUG_Extend
    Serial.println("Gasim Fertilizaciju 1"); 
#endif    
valve_stop_fertilization(1); //fertnum = 1;
  }
  else if (fertilizations[0].fertilization_status == 1) {
   fertilizationCheck( 1 , pHmeasuring.desiredPh);
  }  
  
  // fertilization2 check
  if (fertilizations[1].fertilization_status == 1 && !(currentTime() - fertilizations[1].fertilization_started_time < fertilizations[1].fertilization_duration * 60L))
  { 
#ifdef DEBUG_Extend
    Serial.println("Gasim Fertilizaciju 2"); 
#endif 
   valve_stop_fertilization(2); //fertnum = 1; setuje sve vrednosti na 0, gasi relej koji napaja pumpu   
  }
  else if (fertilizations[1].fertilization_status == 1){
   fertilizationCheck( 2 , pHmeasuring.desiredPh);
  }

  // fertilization3 check
  if (fertilizations[2].fertilization_status == 1 && !(currentTime() - fertilizations[2].fertilization_started_time < fertilizations[2].fertilization_duration * 60L))
  {
#ifdef DEBUG_Extend
    Serial.println("Gasim Fertilizaciju 3"); 
#endif
    valve_stop_fertilization(3);
  }
  else if (fertilizations[2].fertilization_status == 1){
     fertilizationCheck( 3 , pHmeasuring.desiredPh);
  }

  // fertilization4 check
  if (fertilizations[3].fertilization_status == 1 && !(currentTime() - fertilizations[3].fertilization_started_time < fertilizations[3].fertilization_duration * 60L))
  {
#ifdef DEBUG_Extend
    Serial.println("Gasim Fertilizaciju 4"); 
#endif
    valve_stop_fertilization(4);
  }
  else if (fertilizations[3].fertilization_status == 1){
   fertilizationCheck( 4 , pHmeasuring.desiredPh);
  }
#endif  
//--------------------------------------------------------------------
  // zonal check
  if (!isperm3)
  {
    for (int i = 0; i < NUM_SLOTS; i++)
    {
      if (zonal_irrigations[i].irrigation_id != "")
      {
        for (int processing_zone = 1; processing_zone <= number_of_zones; processing_zone++)
        {
          if ((zonal_irrigations[i].status[processing_zone - 1] == 1) && !(currentTime() - zonal_irrigations[i].started_time[processing_zone - 1] < zonal_irrigations[i].duration * 60L))
          {
delay(170);            
#ifdef AC_Valves            
            digitalWrite(valves[processing_zone - 1], relay_stop);
#endif            
#ifdef DC_Valves
    stopValve(processing_zone-1);
#endif  
#ifdef DEBUG_Extend
    Serial.print(F("GasiM ZonU "));Serial.println(processing_zone);
#endif          
            zonal_irrigations[i].status[processing_zone - 1] = 0;
            bool is_some_zone_on = false;
            for (int checking_zone = 1; checking_zone <= number_of_zones; checking_zone++)
            {
              if (zonal_irrigations[i].status[checking_zone - 1] == 1)
              {
                is_some_zone_on = true;
                break;
              }
            }
            if (is_some_zone_on == false)
            {
              bool is_some_irrigation_on = false;
              for (int checking_slot = 0; checking_slot < NUM_SLOTS; checking_slot++)
              {
                if (zonal_irrigations[checking_slot].irrigation_id != "")
                {
                  for (int checking_zone = 1; checking_zone <= number_of_zones; checking_zone++)
                  {
                    if (zonal_irrigations[checking_slot].status[checking_zone - 1] == 1)
                    {
                      is_some_irrigation_on = true;
                      break;
                    }
                  }
                }
              }

              if (!is_some_irrigation_on)
              {
                digitalWrite(mainpump_pin, relay_stop);
              //  main_pump_status = 0;
              }
#ifdef DEBUG
              String logger = "Completed zonal irrigation ID " + String(zonal_irrigations[i].irrigation_id);
              Serial.println(logger);
#endif
              params = "";
              params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));
              memset(msg, 0, sizeof(msg));
              apiCall(server, 80, "/api/post/irrigation_finished.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
              char *jsonResponse = strstr(msg, "{");
              Serial.println(jsonResponse);
#endif

              // clear slot
              zonal_irrigations[i].irrigation_id = "";
              zonal_irrigations[i].duration = 0;
              for (int checking_zone = 1; checking_zone <= number_of_zones; checking_zone++)
              {
                zonal_irrigations[i].status[checking_zone - 1] = 0;
                zonal_irrigations[i].started_time[checking_zone - 1] = 0L;
              }
              initialiseIrrFlowMet();// resetuje parametre o protekloj kolicini vode i vreme start-stop.

            }
          }
        }
      }
    }
  }
  else
  {
    for (int i = 0; i < NUM_SLOTS; i++)
    {
      if (zonal_irrigations[i].irrigation_id != "")
      {
        for (int processing_zone = 1; processing_zone <= number_of_zones; processing_zone++)
        {
          if (zonal_irrigations[i].status[processing_zone - 1] == 1)
          {
            zonal_irrigations[i].current[processing_zone - 1] += totalVolume;
            if (zonal_irrigations[i].current[processing_zone - 1] >= zonal_irrigations[i].amount)
            {
delay(170);              
#ifdef AC_Valves            
    digitalWrite(valves[processing_zone - 1], relay_stop);
#endif            
#ifdef DC_Valves
    stopValve(processing_zone-1);
#endif     
#ifdef DEBUG_Extend         
     Serial.print(F("GaSiM zONu "));Serial.println(processing_zone);
#endif          
              zonal_irrigations[i].status[processing_zone - 1] = 0;
              bool is_some_zone_on = false;
              for (int checking_zone = 1; checking_zone <= number_of_zones; checking_zone++)
              {
                if (zonal_irrigations[i].status[checking_zone - 1] == 1)
                {
                  is_some_zone_on = true;
                  break;
                }
              }
              if (is_some_zone_on == false)
              {
                #ifdef DEBUG
                  String logger = "Completed zonal irrigation ID " + String(zonal_irrigations[i].irrigation_id);
                  Serial.println(logger);
                #endif

                params = "";
                params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));
                memset(msg, 0, sizeof(msg));
                apiCall(server, 80, "/api/post/irrigation_finished.php", params.c_str(), msg, MSG_BUF);

                digitalWrite(mainpump_pin, relay_stop);
               // main_pump_status = 0;

                // clear slot
                zonal_irrigations[i].irrigation_id = "";
                zonal_irrigations[i].amount = 0L;
                zonal_irrigations[i].max_duration = 0;
                for (int checking_zone = 1; checking_zone <= number_of_zones; checking_zone++)
                {
                  zonal_irrigations[i].status[checking_zone - 1] = 0;
                  zonal_irrigations[i].current[checking_zone - 1] = 0;
                }
                initialiseIrrFlowMet();
              }
            }
            if ((zonal_irrigations[i].status[processing_zone - 1] == 1) && zonal_irrigations[i].max_duration > 0 && !(currentTime() - zonal_irrigations[i].started_time[processing_zone - 1] < zonal_irrigations[i].max_duration * 60L))
            {
              digitalWrite(mainpump_pin, relay_stop);
              zonal_irrigations[i].status[processing_zone - 1] = 0;
              bool is_some_zone_on = false;
              for (int checking_zone = 1; checking_zone <= number_of_zones; checking_zone++)
              {
                if (zonal_irrigations[i].status[checking_zone - 1] == 1)
                {
                  is_some_zone_on = true;
                  break;
                }
              }
              if (is_some_zone_on == false)
              {
        #ifdef DEBUG
            String logger = "Stopped zonal irrigation ID " + String(zonal_irrigations[i].irrigation_id) + " - max duration";
            Serial.println(logger);
        #endif
                params = "";
                params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));
                params.concat("&maxduration=1");
                memset(msg, 0, sizeof(msg));
                apiCall(server, 80, "/api/post/irrigation_stopped.php", params.c_str(), msg, MSG_BUF);
        #ifdef DEBUG
            char *jsonResponse = strstr(msg, "{");
            Serial.println(jsonResponse);
        #endif

                // clear slot
                zonal_irrigations[i].irrigation_id = "";
                zonal_irrigations[i].amount = 0L;
                zonal_irrigations[i].max_duration = 0;
                for (int checking_zone = 1; checking_zone <= number_of_zones; checking_zone++)
                {
                  zonal_irrigations[i].status[checking_zone - 1] = 0;
                  zonal_irrigations[i].current[checking_zone - 1] = 0;
                }
                initialiseIrrFlowMet();
              }
            }
          }
        }
      }
    }
  }
}

time_t currentTime()
{
  struct ts t;
  tmElements_t te;
  DS3231_get(&t);
  //char buff[BUFF_RTC_TIME];
  //snprintf(buff, BUFF_RTC_TIME, "Trenutno vreme %d.%02d.%02d %02d:%02d:%02d", t.year, t.mon, t.mday, t.hour, t.min, t.sec);
  //Serial.println(buff);

  te.Second = t.sec;
  te.Minute = t.min;
  te.Hour = t.hour;
  te.Day = t.mday;
  te.Month = t.mon;
  te.Year = t.year - 1970;

  time_t timeInSeconds = makeTime(te);
  return timeInSeconds;
}
//==================================================================================================================================
int apiCall(const char *server, int port, const char *path, const char *parameters, char *result, int resultlength)
{
  int completed = 0;
  delay(200);
  completed = inet.httpPOST(server, port, path, parameters, result, resultlength);

  if (completed == 0)
  {
#ifdef DEBUG
    Serial.println(F("Dettach GPRS connection..."));
#endif
    inet.dettachGPRS();
    delay(2000);
#ifdef DEBUG
    Serial.println(F("Restarting SIM module..."));
#endif
    digitalWrite(sim_restart_pin, LOW);
    delay(5000);
    digitalWrite(sim_restart_pin, HIGH);
    delay(5000);
#ifdef DEBUG
    Serial.println(F("GSM Shield testing."));
#endif
    // Start configuration of shield with baudrate.
    // For http uses is recommended to use 4800 or slower.
    if (gsm.begin(9600))
    {
#ifdef DEBUG
      Serial.println(F("\nstatus=READY"));
#endif
      gsm_started = true;
    }
    else
    {
#ifdef DEBUG
      Serial.println(F("\nstatus=IDLE"));
#endif
    }
    if (gsm_started)
    {
#ifdef DEBUG
      Serial.println(F("Attach GPRS connection..."));
#endif
      gsm.SimpleWriteln("AT+CGATT=1");
      delay(3000);

      if (inet.attachGPRS(String(carrierdata.apnname).c_str(), String(carrierdata.apnuser).c_str(), String(carrierdata.apnpass).c_str()))
      {
#ifdef DEBUG
        Serial.println(F("status=ATTACHED"));         
        gsm.SimpleWriteln("AT+CIFSR");
        delay(5000);
      //Read until serial buffer is empty.s
        gsm.WhileSimpleRead();
#endif
      }
      else
      {
#ifdef DEBUG
        Serial.println(F("status=ERROR"));
#endif
      }
    
    }
    delay(1000);
  }

  
  if (completed == 0)
  {
    // check to turn off pump if there is no active irrrigation
    bool is_some_irrigation_on = false;
    for (int checking_slot = 0; checking_slot < NUM_SLOTS; checking_slot++)
    {
      if (zonal_irrigations[checking_slot].irrigation_id != "")
      {
        for (int checking_zone = 1; checking_zone <= number_of_zones; checking_zone++)
        {
          if (zonal_irrigations[checking_slot].status[checking_zone - 1] == 1)
          {
            is_some_irrigation_on = true;
            break;
          }
        }
      }
    }
    if (!is_some_irrigation_on)
    {
#ifdef DEBUG
      Serial.println(F("Reseting Arduino..."));
#endif
      digitalWrite(arduino_restart_pin, LOW); // Reset Arduino
//      delay(5000);
    }
    else
    {
#ifdef DEBUG
      Serial.println(F("Not reseting Arduino beacuse of active irrigation."));
#endif
    }
  }
  return completed;
}

#ifdef FLOW
struct flowmeter_struct checkFlowMeter()
{
  // output some measurement result
  #ifdef  DEBUG_Telemetry                                                                       //merenje protoka vode na kanalu CH = 5
    Serial.println("Currently " + String(getCurrentFlow(3)*60) + " L/min, " + String(getCurrentVOL(FlowSensorCH5)) + "L total.");
  #endif
  struct flowmeter_struct flow;
  flow.currentflow = getCurrentFlow(FlowSensorCH5)*60; //getCurrentFlow vraca protok u sekundama, pa *60 dobija se u min/L protok
  flow.currentvolume = getCurrentVOL( FlowSensorCH5);  
#ifdef DEBUG_Telemetry
  FlowDebug( FlowSensorCH5);
#endif
  return flow;
}
#endif

#ifdef INTERRUPTFLOW
struct flowmeter_struct checkFlowMeter()
{
struct flowmeter_struct flow_tmp;
    // process the (possibly) counted ticks
  Meter.tick(flowmeter_period); 
  flow_tmp.currentflow = Meter.getCurrentFlowrate();
  flow_tmp.currentvolume = Meter.getCurrentVolume();
  Meter.reset();
  return flow_tmp;
}
#endif
			
void checkLastSeen() {
  if (last_seen > 0 && !(currentTime() - last_seen < 20 * 60L)) {
    stopAll();
  }
}

void stopAll() {

#ifdef Fertilisation
  // stop fertilization
  valve_stop_fertilization(1);
  valve_stop_fertilization(2);
  valve_stop_fertilization(3);
  valve_stop_fertilization(4);
  delay(2000);// saceka da se zaustavi isticanje prihrane u sistem.......
#endif
  
  //stop mixer
  valve_mixer_stop();
  // stop zonal irrigations
  for (int checking_slot = 0; checking_slot < NUM_SLOTS; checking_slot++) {
    for (int checking_zone=1; checking_zone <= number_of_zones; checking_zone++) {
      
#ifdef AC_Valves            
    digitalWrite(valves[checking_zone - 1], relay_stop);
#endif            
#ifdef DC_Valves
    stopValve(checking_zone-1);
#endif
#ifdef DEBUG_Extend      
     Serial.print(F("Gasim ZONU: "));Serial.println(checking_zone);
#endif      
      zonal_irrigations[checking_slot].status[checking_zone-1] = 0;
      zonal_irrigations[checking_slot].started_time[checking_zone-1] = 0L;
      zonal_irrigations[checking_slot].current[checking_zone-1] = 0;
    }
    zonal_irrigations[checking_slot].irrigation_id = "";
    zonal_irrigations[checking_slot].amount = 0L;
    zonal_irrigations[checking_slot].duration = 0;
    zonal_irrigations[checking_slot].max_duration = 0;
  }

  initialiseIrrFlowMet();
}
//---------------DC Valves permanent ON/OFF-----------------------------------------
#ifdef DC_Valves_Permanent
void startValve(int zone) {
    digitalWrite(valves_off[zone], HIGH);// Releji za N (negativan) prikljucak ventila
    if (zone == 3) { 
      PORTG = PORTG | 0x08; 
     }
  #ifdef DEBUG
        Serial.println("DC Relay Start");
  #endif 
}

void stopValve(int zone) {
    digitalWrite(valves_off[zone], LOW);   //Releji za N (negativan) prikljucak ventila
    if (zone == 3) { 
      PORTG = PORTG & 0xF7; 
    } 
  #ifdef DEBUG
        Serial.println("DC Relay STOP");
  #endif
}
#endif
//------------------------------------------------------------------------
#if defined(DC_Valves) && !defined(DC_Valves_Permanent)
void startValve(int zone) {
    digitalWrite(valves_on[zone], LOW);  // Releji za P (pozitivan) prikljucak vential 
    digitalWrite(valves_off[zone], HIGH);// Releji za N (negativan) prikljucak ventila
    if (zone == 3) { PORTG = PORTG | 0x08; }
    delay(valvedelay);
    digitalWrite(valves_off[zone], LOW);
    if (zone == 3) { PORTG = PORTG & 0xF7; }
  #ifdef DEBUG
        Serial.println("DC Relay Start");
  #endif 
}

void stopValve(int zone) {
    digitalWrite(valves_off[zone], LOW);   //Releji za N (negativan) prikljucak ventila
    if (zone == 3) { PORTG = PORTG & 0xF7; }
    digitalWrite(valves_on[zone], HIGH);  // Releji za P (pozitivan) prikljucak vential   
    delay(valvedelay);
    digitalWrite(valves_on[zone], LOW); 
  #ifdef DEBUG
        Serial.println("DC Relay STOP");
  #endif
}
#endif

#ifdef FLOW_Toggling
bool checkFlow() {
     byte flowreadcount = 0;  
     byte firstflowstate = digitalRead(flow_pin_Toggling);
//     byte firstflowstate = ( PINE & 0x80);
     while (flowreadcount < 30) {
        byte flowstate = digitalRead(flow_pin_Toggling); // Za FERT
//        byte flowstate = ( PINE & 0x80);               // Za SENS
        if (flowstate != firstflowstate) {
          return true;
        }
        delay(50);
        flowreadcount++;
     }  
     return false;
  }
#endif

#ifdef TIME_F_Measure
void funcTimeMeasure(String funcBlock){
    Serial.print("Exe Time of "); Serial.print(funcBlock);Serial.print(" = "); Serial.print((float)(millis()-currentTimeMilles),0); Serial.println(" mS");
    currentTimeMilles = millis();
}
#endif
