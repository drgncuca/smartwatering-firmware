#include <String.h>

#include <TimeLib.h>
#include <SHT1x.h>

#ifndef Telemetry_h                                                   
  #define Telemetry_h 
  
//================================  B O A R D   HW    V E R S I O N   =================================//
#define BOARD_FERT_R002

//================================   P O D E S I T I    P R E    I S P O R U K E  ====================//

#define secondsWaitingSMS 5  // Podesiti na 15 - 20,  4s * 15 = 60s cekanje na SMS
#define GSM_SMS_Replay       // Ako korisik hoce povratni SMS -> Otkomentarisati

//#define SKIP_Operater_Detecting// zakomentarisati ovo !!!! ( samo za debagovanje)
//#define MainPumpIssueSkip      // zakomentarisati ovo !!!! ( samo za debagovanje)


//---------------------------------------------------------------------------------------------
#define BUFF_RTC_TIME 1 // Za debagovanje stavi 256
#define MSG_BUF  830 // bilo 830

#define BUFF_SMS 50
#define BUFF_MOB_NUM 20

//============================= V A L V E S   &&    R E L A Y S  ==========================//
#define AC_Valves

#define NUM_SLOTS 4 // each SLOT consume 150B of memory

#define MainPump

//============================= D E B U G   D I R E C T I V E ============================//

//#define DEBUG_Server_Comun
#define DEBUG                //210B
#define DEBUG_Extend
//#define DEBUG_Telemetry
//#define DEBUG_Fertilisation
//#define DEBUG_FLOW
//#define Irigation_Per_m3
//#define DEBUG_ON          //od GSM modula
//#define DEBUG_Operater
//#define Operater_Setings_Skip

//=================================   M O D U L E S  ========================================//
#define MIXER //umesto FERT-a
#define GSM_Carrier_Change 

#define FLOW   //33imp/L
//#define FLOW_1000L_imp
//#define FLOW_100L_imp
//#define FLOW_10L_imp 
        
#define Fertilisation  

#define WhatchDog											 
//=================================== P O W E R ==========================================//
/*
#define AC_power_IN 7

int getPower(void) ;
*/
#define relay_stop   LOW
#define relay_start  HIGH

//====================================Whatch Dog ==========================================//
void initiWD(void);
void enableWD(byte enable);
void togleWD(void);	
void stopAll(void);	

//-------------------------Carier Operater Changing----------------------------------------------//
#define EERPOM_mem_loc_SW  64
#define EEPROM_mem_loc_Operater 62
#define EERPOM_mem_loc_CarrierData 0
#define EERPOM_mem_loc_sms_Confugured 70										

#define Telenor 1
#define Vip 2
#define Telekom 3
#define BHmob 4
#define MTEL 5
#define Haloo 6
#define BHeronet 7
#define mtelCG 8

#define Mobily 9
#define Zain 10
#define STC 11

#define MEO 12
#define Vodafone 13
#define NOS 14
#define LucaMobile 15
#define ONI 16
#define NOWO 17

#define KosovoD3 18
#define KosovoIPKO 19
#define KosovoMTS 20
#define KosovoVala 21

struct carrier
{
  char apnname[20];
  char apnuser[20];
  char apnpass[20];
  char operater[20];
} ;

extern carrier carrierdata; 

 struct struct_operater_mtelCG
{
  char apnname[20]="mtelinternet";
  char apnuser[20]="internet";
  char apnpass[20]="068";  
} ;
extern struct_operater_mtelCG operaterMTEL_CG;

 struct struct_operater_vip
{
  char apnname[20]="internet";
  char apnuser[20]="internet";
  char apnpass[20]="internet";  
} ;
extern struct_operater_vip operaterVIP;

 struct struct_operater_MTS
{
  char apnname[20]="gprswap"; 
  char apnuser[20]="mts";
  char apnpass[20]="064";  
} ;
extern struct_operater_MTS operaterMTS;

 struct struct_operater_Telenor
{
  char apnname[20]="internet";
  char apnuser[20]="telenor";
  char apnpass[20]="gprs";  
} ;
extern struct_operater_Telenor operaterTelenor;

 struct struct_operater_BHmob
{
  //char apnname[20]="active.bhmobile.ba"; //pripaid
  char apnname[20]="smart.bhmobile.ba";  //postpaid
  char apnuser[20]="U";
  char apnpass[20]="P";  
} ;
extern struct_operater_BHmob operaterBHmob;

 struct struct_operater_MTEL
{
  char apnname[20]="mtelfrend";
  char apnuser[20]="";
  char apnpass[20]="";  
} ;
extern struct_operater_MTEL operaterMTEL;

 struct struct_operater_BHeronet
{
  char apnname[20]="gprs.eronet.ba";
  char apnuser[20]="";
  char apnpass[20]="";  
} ;
extern struct_operater_BHeronet operaterBHeronet;

 struct struct_operater_Haloo
{
  char apnname[20]="web.haloo.ba";
  char apnuser[20]="";
  char apnpass[20]="";  
} ;
extern struct_operater_Haloo operaterHaloo;

// Saudijska Arabija
 struct struct_operater_Mobily
{
  char apnname[20]="web2";
  char apnuser[20]="web";
  char apnpass[20]="web";  
} ;
extern struct_operater_Mobily operaterMobily;

 struct struct_operater_Zain
{
  char apnname[20]="blank";
  char apnuser[20]="blank";
  char apnpass[20]="blank";  
} ;
extern struct_operater_Zain operaterZain;

 struct struct_operater_STC
{
  char apnname[20]="blank";
  char apnuser[20]="blank";
  char apnpass[20]="blank";  
} ;
extern struct_operater_STC operaterSTC;

// Portugal
 struct struct_operater_MEO
{
  char apnname[20]="internet";
  char apnuser[20]="";
  char apnpass[20]="";  
} ;
extern struct_operater_MEO operaterMEO;

 struct struct_operater_Vodafone
{
  char apnname[20]="vodafone";
  char apnuser[20]="vodafone";
  char apnpass[20]="vodafone";  
} ;
extern struct_operater_Vodafone operaterVodafone;

 struct struct_operater_NOS
{
  char apnname[20]="blank";
  char apnuser[20]="blank";
  char apnpass[20]="blank";  
} ;
extern struct_operater_NOS operaterNOS;

 struct struct_operater_LucaMobile
{
  char apnname[20]="data.lycamobile.pt";
  char apnuser[20]="lmpt";
  char apnpass[20]="plus";  
} ;
extern struct_operater_LucaMobile operaterLucaMobile;

 struct struct_operater_ONI
{
  char apnname[20]="blank";
  char apnuser[20]="blank";
  char apnpass[20]="blank";  
} ;
extern struct_operater_ONI operaterONI;

 struct struct_operater_NOWO
{
  char apnname[20]="internet";
  char apnuser[20]="";
  char apnpass[20]="";  
} ;
extern struct_operater_NOWO operaterNOWO;

 struct struct_operater_KosovoD3
{
  char apnname[20]="IPKO";
  char apnuser[20]="";
  char apnpass[20]="";  
} ;
extern struct_operater_KosovoD3 operaterKosovoD3;

 struct struct_operater_KosovoIPKO
{
  char apnname[20]="IPKO";
  char apnuser[20]="";
  char apnpass[20]="";  
} ;
extern struct_operater_KosovoIPKO operaterKosovoIPKO;

 struct struct_operater_KosovoMTS
{
  char apnname[20]="gprswap";
  char apnuser[20]="mts";
  char apnpass[20]="064";  
} ;
extern struct_operater_KosovoMTS operaterKosovoMTS;

 struct struct_operater_KosovoVala
{
  char apnname[20]="Vala 4G";
  char apnuser[20]="";
  char apnpass[20]="";  
} ;
extern struct_operater_KosovoVala operaterKosovoVala;


void doesOperaterChanging(void);
bool startsWith(const char *pre, const char *str);
void initOperators(void);
void deleteALLsms(void);
float getRSSI(void);

#define GSM_LTE
#define GSM_GPRS

byte initGSM_LTE(void);
byte setAPN(char* apnname, char* apnuser, char* apnpass);
byte apiCall(const char *path, String& postData,  char* serverResponse);
bool waitForNetworkRegistration(byte timeout);
bool isNetworkConnected(void);

void readLastSMS(void) ;
bool sendSMS(const char* phoneNumber, const char* message) ;
int checkForSMS(void);
void read_ALL_SMS(void) ;
void readReadedSMS(void) ;
void setSMSMode(void) ;
void setSMSMemory(void);
void SMSprocessing(void);
void deleteAllSMS(void);
int calculateHTTPHeaderSize(const char *path, int contentLength);
int getSignalStrength(void);
int convertRSSIToDBm(int );	
byte checkSIMPresence(void);

#define flowConstantImpuls33PerSecond 117.0 
            


struct irrigation_struct_FlowMetering
{
	float irrigationFlowVolume;	
	unsigned long ElapsedTime;
	float flowMeterCalibKons;

};
extern  irrigation_struct_FlowMetering  irrigationFlowMetering;
void FlowDebug(void);
void resetFlowMetVolume(void);											   
float getCurrentVOL(void);	
float getCurrentFlow(void);									   
void initialiseIrrFlowMet(void);					
#ifdef FLOW_100L_imp
void flowImplseTick100L_Procesing(void);
unsigned int getFlowImplseTick100L(void);
float getFlow100L(void);
void resetFlowImpulseTick100L(void);
float getCurrentVOLtick100L(void);
#endif

#ifdef FLOW_1000L_imp
void flowImplseTick1000L_Procesing(void);
unsigned int getFlowImplseTick1000L(void);
float getFlow1000L(void);
void resetFlowImpulseTick1000L(void);
float getCurrentVOLtick1000L(void);
#endif
#ifdef FLOW_10L_imp
void flowImplseTick10L_Procesing(void);
unsigned int getFlowImplseTick10L(void);
float getFlow10L(void);
void resetFlowImpulseTick10L(void);
float getCurrentVOLtick10L(void);
#endif
				
#endif