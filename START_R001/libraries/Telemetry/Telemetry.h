#include <String.h>

#include <TimeLib.h>
#include <SHT1x.h>

#ifndef Telemetry_h                                                   
  #define Telemetry_h 
  
//================================  B O A R D   HW    V E R S I O N   =================================//
#define BOARD_FERT_R002

//================================   P O D E S I T I    P R E    I S P O R U K E  ====================//

#define secondsWaitingSMS 5  // Podesiti na 15 - 20,  4s * 15 = 60s cekanje na SMS
#define GSM_SMS_Replay       // Ako korisik hoce povratni SMS -> Otkomentarisati

//#define SKIP_Operater_Detecting// zakomentarisati ovo !!!! ( samo za debagovanje)
//#define MainPumpIssueSkip      // zakomentarisati ovo !!!! ( samo za debagovanje)


//---------------------------------------------------------------------------------------------
#define BUFF_RTC_TIME 1 // Za debagovanje stavi 256
#define MSG_BUF  830 // bilo 830

#define BUFF_SMS 50
#define BUFF_MOB_NUM 20

//============================= V A L V E S   &&    R E L A Y S  ==========================//
#define AC_Valves

#define NUM_SLOTS 4 // each SLOT consume 150B of memory

#define MainPump

//============================= D E B U G   D I R E C T I V E ============================//

//#define DEBUG_Server_Comun
#define DEBUG                //210B
#define DEBUG_Extend
//#define DEBUG_Telemetry
//#define DEBUG_Fertilisation
//#define DEBUG_FLOW
//#define Irigation_Per_m3
//#define DEBUG_ON          //od GSM modula
//#define DEBUG_Operater
//#define Operater_Setings_Skip

//=================================   M O D U L E S  ========================================//
#define MIXER //umesto FERT-a
#define GSM_Carrier_Change 

//#define FLOW   //33imp/L
//#define FLOW_1000L_imp
//#define FLOW_100L_imp
#define FLOW_10L_imp 
        
#define Fertilisation  

#define WhatchDog											 
//=================================== P O W E R ==========================================//
/*
#define AC_power_IN 7

int getPower(void) ;
*/
#define relay_stop   LOW
#define relay_start  HIGH

//====================================Whatch Dog ==========================================//
void initiWD(void);
void enableWD(byte enable);
void togleWD(void);	
void stopAll(void);	

//-------------------------Carier Operater Changing----------------------------------------------//
#define EERPOM_mem_loc_SW  64
#define EEPROM_mem_loc_Operater 62
#define EERPOM_mem_loc_CarrierData 0

#define Telenor 1
#define Vip 2
#define Telekom 3
#define BHmob 4

struct carrier
{
  char apnname[20];
  char apnuser[20];
  char apnpass[20];
  char operater[20];
} ;

extern carrier carrierdata; 

 struct struct_operater_vip
{
  char apnname[20]="internet";
  char apnuser[20]="internet";
  char apnpass[20]="internet";  
} ;
extern struct_operater_vip operaterVIP;

 struct struct_operater_MTS
{
  char apnname[20]="gprswap"; 
  char apnuser[20]="mts";
  char apnpass[20]="064";  
} ;
extern struct_operater_MTS operaterMTS;

 struct struct_operater_Telenor
{
  char apnname[20]="internet";
  char apnuser[20]="telenor";
  char apnpass[20]="gprs";  
} ;
extern struct_operater_Telenor operaterTelenor;

 struct struct_operater_BHmob
{
//char apnname[20]="active.bhmobile.ba"; //pripaid
  char apnname[20]="smart.bhmobile.ba";  //postpaid
  char apnuser[20]="";
  char apnpass[20]="";  
} ;
extern struct_operater_BHmob operaterBHmob;

void doesOperaterChanging(void);
bool startsWith(const char *pre, const char *str);
void initOperators(void);
void deleteALLsms(void);
float getRSSI(void);


#define flowConstantImpuls33PerSecond 117.0 
            


struct irrigation_struct_FlowMetering
{
	float irrigationFlowVolume;	
	unsigned long ElapsedTime;
	float flowMeterCalibKons;

};
extern  irrigation_struct_FlowMetering  irrigationFlowMetering;
void FlowDebug(void);
void resetFlowMetVolume(void);											   
float getCurrentVOL(void);	
float getCurrentFlow(void);									   
void initialiseIrrFlowMet(void);					
#ifdef FLOW_100L_imp
void flowImplseTick100L_Procesing(void);
unsigned int getFlowImplseTick100L(void);
float getFlow100L(void);
void resetFlowImpulseTick100L(void);
float getCurrentVOLtick100L(void);
#endif

#ifdef FLOW_1000L_imp
void flowImplseTick1000L_Procesing(void);
unsigned int getFlowImplseTick1000L(void);
float getFlow1000L(void);
void resetFlowImpulseTick1000L(void);
float getCurrentVOLtick1000L(void);
#endif
#ifdef FLOW_10L_imp
void flowImplseTick10L_Procesing(void);
unsigned int getFlowImplseTick10L(void);
float getFlow10L(void);
void resetFlowImpulseTick10L(void);
float getCurrentVOLtick10L(void);
#endif
				
#endif