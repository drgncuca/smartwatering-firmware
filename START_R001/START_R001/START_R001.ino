#include <EEPROM.h>
#include "EEPROMAnything.h"
#include <SoftwareSerial.h>
#include <Wire.h>
#include <TimeLib.h>
#include <ArduinoJson.h>
#include "SIM900.h"
#include "ds3231.h"
#include "inetGSM.h"
#include "sms.h"
#include <String.h>
#include "Telemetry.h"					
#include <SPI.h>


#define FirwareVersion "FV103"

InetGSM inet;
//TODO Check max size of buffer
StaticJsonBuffer<750> jsonBuffer; // bilo je 650

boolean gsm_started = false;

//bool is_some_zone_on = false;

bool isperm3 = false;

bool isautomode = false;

char server[] = "app.smartwatering.rs";

//char parcel_id[] = "riGzyK";//Darko Živković	
//char parcel_id[] = "7tdRHC";//Almex doo Crepaja
//char parcel_id[] = "s4IUok";//Strahinja Malin Cortanovci Airplant
//char parcel_id[] = "aprGnO";//Matijevic doo Novo Orahovo	
//char parcel_id[] = "fWtFgC";//Matijevic doo Novo Orahovo	
//char parcel_id[] = "sWELev";//Goran Banjaluka Metronix doo
//char parcel_id[] = "g7ukrt";//Goran Banjaluka Metronix doo	
//char parcel_id[] = "0yqsXM";//Goran Banjaluka Metronix doo			
char parcel_id[] = "S2c0Pu";//BioPlod doo Slobodan Arsenić - BiH	
//char parcel_id[] = "7hREg3";//AQM Aleksandar Vidojevic Gornji MIlanovac	1					
//char parcel_id[] = "JMP9Gu";//AQM Aleksandar Vidojevic Gornji MIlanovac	2			
//char parcel_id[] = "rmlWnZ";//AQM Aleksandar Vidojevic Gornji MIlanovac	3	
//char parcel_id[] = "lCUEnt";//	Kristina Kobilje
//char parcel_id[] = "yzWe9q";//	Stipan Novi Zasad
//char parcel_id[] = "ZVfQjp"; //Bojan Kalicanin ex-ARDUINO





struct irrigation_struct
{
  unsigned int duration = 0; // vreme trajanja navodnjavanja u sekundama
  unsigned long amount = 0L; // Kolicina vode u litrima
  byte status[8] = {0, 0, 0, 0, 0, 0, 0, 0}; // 0 = off, 1 = on
  //byte status[19] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 , 0, 0, 0, 0, 0}; // 0 = off, 1 = on
  unsigned long started_time =  0L;// u sekundama
//  unsigned int max_duration = 0; // max vreme navodnjavanja po zapremini, zastitni parametar.
  String irrigation_id = "";
  float current = 0;// kolicinu vode u litrima.
  //float current = 0L; 
} zonal_irrigation_struct;


irrigation_struct  zonal_irrigations[NUM_SLOTS];

irrigation_struct_FlowMetering  irrigationFlowMetering; // Globalna promenljiva koja sadrzi informacije o protekloj kolicini VODE na 5 kanalala flowMeter-a i proteklo vreme.

byte number_of_zones = 8;

              //    1   2   3   4   5   6   7   8        
byte valves[] = {19, 18, 41, 40, 27, 26, 25, 24};   

byte mainpump_pin = 23; // Main Pump Relay AC9. 


struct fert_struct
{
  byte fert_pin = 22;
  byte fertilization_status; // 0 = off, 1 = on
  String fertilization_id;
  String fertilization_irrigation_id;
  unsigned int      fertilization_duration;
  unsigned long int fertilization_started_time;           
};

fert_struct fertilizations;

byte sim_restart_pin = 7;
byte arduino_restart_pin = 12;

String params = "";
//TODO: Check max size of the message and reduce it's size accordingly
char msg[MSG_BUF]; 

carrier carrierdata;
struct_operater_vip operaterVIP;
struct_operater_MTS operaterMTS;
struct_operater_Telenor operaterTelenor;
struct_operater_BHmob operaterBHmob;

// last time successfully contacted server
unsigned long last_seen = 0L;
   
struct flowmeter_struct
{
  float currentflow;
  float currentvolume;
};

bool carrierChange = false;
//============================================= S E T U P =========================================================
void setup()
{  

  Serial.begin(115200); // Serial connection.
  Serial.print(F("\n\n(RE)start uC - FW Version : ")); Serial.println(FirwareVersion);

initiWD();

  digitalWrite(arduino_restart_pin, HIGH);
  pinMode(arduino_restart_pin, OUTPUT);
  
  pinMode(sim_restart_pin, OUTPUT);
  digitalWrite(sim_restart_pin, HIGH);  
  

  delay(100); 
  pinMode(8, OUTPUT); 
  //digitalWrite(8, HIGH); //on 
  //delay(1050);  
  digitalWrite(8, LOW); //off
  delay(100);
  digitalWrite(sim_restart_pin, LOW);
  delay(150);
  digitalWrite(sim_restart_pin, HIGH);
  delay(500);
  
  
    
  for (byte x=0; x < number_of_zones;  x++)
  {
    pinMode(valves[x], OUTPUT);
    digitalWrite(valves[x], relay_stop);
  }

  pinMode(mainpump_pin, OUTPUT);
  digitalWrite(mainpump_pin, relay_stop);
  
  pinMode(fertilizations.fert_pin, OUTPUT);
  digitalWrite(fertilizations.fert_pin, LOW);
  
  initialiseIrrFlowMet(); //REsetovanje Zapremine i globalnog proteklog vremena na 0 vrednosti.
  
  Wire.begin();

  struct ts t;

  DS3231_get(&t);
  
  Serial.print(F("GSM="));
  
  if (gsm.begin(9600))
  {
    gsm_started = true;  

    Serial.println(F("READY"));
	
//Pri Debagovanju, preskoci jedan deo koda, da ubrza startovanje uC-a
#ifdef SKIP_Operater_Detecting
    goto skip_operater_detecting;
#endif

  }
  else
  {
    gsm_started = false;
    Serial.println(F("IDLE"));  
  }

doesOperaterChanging();// Promena operatera

EEPROM_readAnything(EERPOM_mem_loc_CarrierData, carrierdata);

#ifdef DEBUG_Operater 
    char tmp[2]="00"; 
    Serial.println("\n---------------------------");    
    Serial.println("Sadrzaj EEPROM-a : ");   
    EEPROM_readAnything(64, tmp);
    Serial.println(String(tmp).c_str());     
    Serial.println("Data in the Carrier Data Structure");+
    Serial.println(carrierdata.apnname);
    Serial.println(carrierdata.apnuser);
    Serial.println(carrierdata.apnpass);        
#endif

if (gsm_started)
  {
    Serial.print(F("GPRS="));    
//Pri Debagovanju, preskoci jedan deo koda, da ubrza startovanje uC-a    
#ifdef SKIP_Operater_Detecting  
    skip_operater_detecting:
    Serial.print(F("GPRS="));  

  if (inet.attachGPRS("internet", "internet", "internet"))
  //if (inet.attachGPRS("GPRSWAP", "MTS", "064"))
    {
#else
    if (inet.attachGPRS(String(carrierdata.apnname).c_str(), String(carrierdata.apnuser).c_str(), String(carrierdata.apnpass).c_str()))
    {
#endif    
    
     Serial.println(F("ATTACHED"));     
     gsm.SimpleWriteln("AT+CIFSR");//Read IP address.
     delay(5000);     
     gsm.WhileSimpleRead();//Read until serial buffer is empty

    }
    else
    {

      Serial.println(F("ERROR"));

    }
    delay(1000); 
  }
else
  {
  
   Serial.println(F("GSM&GPRS=NOTstarted"));
 
  }  
  
  enableWD(1);

// Serial.println(F("FV"));
// gsm.SimpleWriteln("AT+CGMR");
// delay(8000);
// gsm.WhileSimpleRead();
#ifdef FLOW_1000L_imp
	DDRE  = DDRE  & 0x7F; // Input
  // enable a call to the 'interrupt service handler' (ISR) on every rising edge at the interrupt pin
  attachInterrupt(INT7, flowImplseTick1000L_Procesing, RISING);   

#endif
#ifdef FLOW_100L_imp
	DDRE  = DDRE  & 0x7F; // Input
  // enable a call to the 'interrupt service handler' (ISR) on every rising edge at the interrupt pin
  attachInterrupt(INT7, flowImplseTick100L_Procesing, RISING);   

#endif
#ifdef FLOW_10L_imp
	DDRE  = DDRE  & 0x7F; // Input
  // enable a call to the 'interrupt service handler' (ISR) on every rising edge at the interrupt pin
  attachInterrupt(INT7, flowImplseTick10L_Procesing, RISING);   

#endif


};
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void loop()
{
  Serial.println(F("Usao u loop"));
  togleWD();


  params = "";  
  params.concat("did=" + String(parcel_id));
  params.concat("&" + String(FirwareVersion));
  params.concat("&fid="  + String(fertilizations.fertilization_id));


  params.concat("&h1=" + String(analogRead(A15)));  
  params.concat("&h2=" + String(analogRead(A7)));

#ifdef FLOW
  struct flowmeter_struct flow = checkFlowMeter(); //azurira vrednost protoka i zapremine
  checkValves(flow.currentvolume);
  params.concat("&cfl=" + String(flow.currentflow));
  params.concat("&tfv=" + String(flow.currentvolume));  
#else
  checkValves(0);
#endif

#ifdef FLOW_10L_imp 
//if (isSomeIrigationON() == 1){
  params.concat("&cfl=" + String(getFlow10L()));
  unsigned int tmpFlowVolume = getCurrentVOLtick10L();
  params.concat("&tfv=" + String(tmpFlowVolume)); 
  checkValves(tmpFlowVolume);
//}
/*else {
  params.concat("&cfl=0.0");
  params.concat("&tfv=0.0");	
  resetFlowImpulseTick100L(); 
  checkValves(0);	
}*/
#endif 

#ifdef FLOW_100L_imp 
//if (isSomeIrigationON() == 1){
  params.concat("&cfl=" + String(getFlow100L()));
  unsigned int tmpFlowVolume = getCurrentVOLtick100L();
  params.concat("&tfv=" + String(tmpFlowVolume)); 
  checkValves(tmpFlowVolume);
//}
/*else {
  params.concat("&cfl=0.0");
  params.concat("&tfv=0.0");	
  resetFlowImpulseTick100L(); 
  checkValves(0);	
}*/
#endif 

#ifdef FLOW_1000L_imp 
//if (isSomeIrigationON() == 1){
  params.concat("&cfl=" + String(getFlow1000L()));
  unsigned int tmpFlowVolume = getCurrentVOLtick1000L();
  params.concat("&tfv=" + String(tmpFlowVolume)); 
  checkValves(tmpFlowVolume);
//}
/*else {
  params.concat("&cfl=0.0");
  params.concat("&tfv=0.0");	
  resetFlowImpulseTick100L(); 
  checkValves(0);	
}*/
#endif

  String activeirrigations;
  for (byte i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id != "")
    {
      activeirrigations.concat("," + String(zonal_irrigations[i].irrigation_id));
    }
  }
  activeirrigations.remove(0, 1);
  params.concat("&iids=" + String(activeirrigations));

  Serial.println(params.c_str());

  memset(msg, 0, sizeof(msg)); 
  apiCall(server, 80, "/api/post/sync_device.php", params.c_str(), msg, MSG_BUF);
  char *jsonResponse = strstr(msg, "{"); 			  
  Serial.println(jsonResponse);

//--------------------------------------------------------------------------------
  JsonObject &root = jsonBuffer.parseObject(jsonResponse);
//--------------------------------------------------------------------------------
 
  JsonVariant numberofzones_json = root["zones"];
  if (numberofzones_json.success())
  {
    if (number_of_zones != root["zones"])
    {
      number_of_zones = root["zones"];
      if (number_of_zones > 10) number_of_zones = 10;
    }
  }

  JsonObject &start_irrigation = root["irr"];
  if (start_irrigation.success())
  {
    // TODO: check if this irrigationid is started
    if (strcmp(start_irrigation["irrigationid"].as<String>().c_str(), root["irr"]["iid"].as<String>().c_str()) != 0)
    {
      isautomode = start_irrigation["automode"];
      if (start_irrigation["zonal"])
      {
        if (start_irrigation["duration"])
        {
			isperm3 = false;
			zonal_irrigation_start(start_irrigation["iid"].as<String>(), start_irrigation["duration"], start_irrigation["zones"]);  
 			
        }
        else
        {
          //per m3
          isperm3 = true;       //maxduration - u minutama vrednost u sinku
          zonal_irrigation_start(start_irrigation["iid"].as<String>(), start_irrigation["amount"], start_irrigation["zones"], start_irrigation["maxduration"]);
        }
      }
    }
  }
//-----------------------------------------------------------------------------------------------------
JsonObject &start_fertilization = root["fer"];

  if (start_fertilization.success())
{	
    fertilization_start(start_fertilization["fid"].as<String>() , start_fertilization["iid"].as<String>() , start_fertilization["duration"]);
}  
  
//-----------------------------------------------------------------------------------------------------//
JsonVariant stop_irrigation_id_json = root["stopirr"];
  if (stop_irrigation_id_json.success())
  {    
    char irrid[5];
    strcpy(irrid, root["stopirr"].as<const char *>());
#ifdef DEBUG_Extend
      Serial.print(F("Forced Stop Irigation ID "));Serial.println(irrid);
#endif
	if (fertilizations.fertilization_id != "") fertilization_stop();
    delay(4000); // saceka neko vreme da pumpa ispere cevovod od djubriva 
    stop_irrigation(irrid);    
  }  
jsonBuffer.clear();

}
//================================ END  OF   L O O P ( )  ===========================================
//per duration
void zonal_irrigation_start(String irrigationid, int duration, JsonArray &zones)
{
  for (byte i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id == "")
    {
	  zonal_irrigations[i].started_time = currentTime(); 																											  
      zonal_irrigations[i].irrigation_id = irrigationid;
#ifdef DEBUG
        Serial.println(F("Palim Zone [per time] :"));
#endif	  
      for (byte zone : zones)
      {
        zonal_irrigations[i].duration = duration * 60L;
        digitalWrite(valves[zone - 1], relay_start);
   		delay(200); 
        zonal_irrigations[i].status[zone - 1] = 1;
#ifdef DEBUG
        Serial.print(F("zona : ")); Serial.println(zone);
#endif
      }

	  digitalWrite(mainpump_pin, relay_start);
#ifdef DEBUG
      String logger = "Started zonal Irrigation [per time] ID " + String(zonal_irrigations[i].irrigation_id);
      Serial.println(logger);
#endif
      params = "";
      memset(msg, 0, sizeof(msg));
      params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));
      apiCall(server, 80, "/api/post/irrigation_started.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif
      return;
    }
  } //for (int i = 0; i < NUM_SLOTS; i++)
}

//per m3 
void zonal_irrigation_start(String irrigationid, long amount, JsonArray &zones, int max_duration){

  for (byte i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id == "")
    {
	  zonal_irrigations[i].started_time = currentTime(); 
      zonal_irrigations[i].current = 0;                  	  
      zonal_irrigations[i].irrigation_id = irrigationid;
      zonal_irrigations[i].amount = amount; // vrednost u litrima.
      zonal_irrigations[i].duration = max_duration * 60L; // vreme u sekundama  
#ifdef DEBUG_Extend
        Serial.println(F("Palim zone [per m3] :")); 
#endif	  
      for (byte zone : zones) //ukljucuje zadate zone
      {
        delay(200);
        digitalWrite(valves[zone - 1], relay_start);    
        zonal_irrigations[i].status[zone - 1] = 1;
#ifdef DEBUG_Extend
        Serial.print(F("zona : ")); Serial.println(zone);
#endif
      }

	  digitalWrite(mainpump_pin, relay_start);
#ifdef DEBUG_Extend
      Serial.print(F("Started zonal Irigation [per m3] ID:")); Serial.println(zonal_irrigations[i].irrigation_id);
#endif
      params = "";
      memset(msg, 0, sizeof(msg));
      params.concat("iid=" + String(zonal_irrigations[i].irrigation_id));
      apiCall(server, 80, "/api/post/irrigation_started.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG_Extend
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif
      return;
    }
  }
}
//-------------------------------------------------------------------------------------------------------
void fertilization_start(String fertilizationid, String irrigationid, int duration )
{
    fertilizations.fertilization_id = fertilizationid;
    fertilizations.fertilization_irrigation_id = irrigationid;
    
    fertilizations.fertilization_status = 1;
    fertilizations.fertilization_started_time = currentTime();  

 
    fertilizations.fertilization_duration = duration * 60L;    
    digitalWrite(fertilizations.fert_pin, HIGH);
	
#ifdef DEBUG_Extend 
      Serial.println("Start FERT ");
#endif
      params = "";
      params.concat("fid=" + String(fertilizationid));
      memset(msg, 0, sizeof(msg));
      apiCall(server, 80, "/api/post/fertilization_started.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG_Extend
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif

}

void fertilization_stop(void){

	digitalWrite(fertilizations.fert_pin, LOW); //gasi  strujni drajver(pumpu) za fertilization.

String tmp = fertilizations.fertilization_id ;

	fertilizations.fertilization_status = 0;
	fertilizations.fertilization_duration = 0;
	fertilizations.fertilization_started_time = 0L;  
	fertilizations.fertilization_id = "";
	fertilizations.fertilization_irrigation_id = ""; 
	
#ifdef DEBUG_Extend
      Serial.print(F("Stoped FERT "));
#endif
      params = "";
      params.concat("fid=" + tmp);
      memset(msg, 0, sizeof(msg));
      apiCall(server, 80, "/api/post/fertilization_stopped.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG
      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);
#endif  

}
//--------------------------------------------------------------------------------------------------------

void stop_irrigation(String irrigationid)
{
  // stop zonal irrigation
  for (byte i = 0; i < NUM_SLOTS; i++)
  {
    if (zonal_irrigations[i].irrigation_id == irrigationid)
    {
      // turn of all active zones
      for (byte checking_zone = 0; checking_zone < number_of_zones; checking_zone++)
      {
        if (zonal_irrigations[i].status[checking_zone ] == 1)
        {
delay(170);
         
     digitalWrite(valves[checking_zone ], relay_stop);

     Serial.print(F("Gasim zonu "));Serial.println(checking_zone+1);
        
          zonal_irrigations[i].status[checking_zone ] = 0;
        }
        //zonal_irrigations[i].started_time[checking_zone ] = 0L;
      }
      zonal_irrigations[i].irrigation_id = "";
      zonal_irrigations[i].duration = 0;
      zonal_irrigations[i].started_time = 0L;

      // check to turn off pump if there is no active irrrigation
      bool is_some_irrigation_on = false;
      for (byte checking_slot = 0; checking_slot < NUM_SLOTS; checking_slot++)
      {
        if (zonal_irrigations[checking_slot].irrigation_id != "")
        {
          for (byte checking_zone = 0; checking_zone < number_of_zones; checking_zone++)
          {
            if (zonal_irrigations[checking_slot].status[checking_zone ] == 1)
            {
              is_some_irrigation_on = true;
              break;
            }
          }
        }
      }
      if (!is_some_irrigation_on)
      {
        digitalWrite(mainpump_pin, relay_stop);
		fertilization_stop();
      }

      String logger = "Stopped irrigation ID " + String(irrigationid);
      Serial.println(logger);

      params = "";
      params.concat("iid=" + String(irrigationid));
      memset(msg, 0, sizeof(msg));
      apiCall(server, 80, "/api/post/irrigation_stopped.php", params.c_str(), msg, MSG_BUF);

      char *jsonResponse = strstr(msg, "{");
      Serial.println(jsonResponse);

    }
  }
}

//============== P R O C E S I N G ========================================

void checkValves(float deltaVolumePerOneLoop)
{
	FertilisationProcesing();
	
	if (!isperm3)// I R I G A T I O N   per   T I M E =======================
	{
	  
		IrigationProcesingBYtime();

	}
	else// I R I G A T I O N   per   VOLUME Water and FERT ===================
	{
		
	  IrigationProcesingBYvolumetric(deltaVolumePerOneLoop);
	  
	}
}

///////////////////////////////////////////////////////////////////////////////
void IrigationProcesingBYtime(void){
    for (byte Irigation = 0; Irigation < NUM_SLOTS; Irigation++)
    {
		if (zonal_irrigations[Irigation].irrigation_id != "")
		{
unsigned long tmpTime = (currentTime() - zonal_irrigations[Irigation].started_time);
			if (  tmpTime > zonal_irrigations[Irigation].duration  )
			{  

	Serial.println(F("Gasim Zone:"));

				for (byte processing_zone = 0; processing_zone < number_of_zones; processing_zone++)
				{
					if ((zonal_irrigations[Irigation].status[processing_zone] == 1))
					{
						delay(150);       
						digitalWrite(valves[processing_zone], relay_stop);
						zonal_irrigations[Irigation].status[processing_zone] = 0;					
						Serial.print(F("zona :"));Serial.println(processing_zone+1);					     
					}
				}  
				
				zonal_irrigations[Irigation].duration = 0;   
				zonal_irrigations[Irigation].started_time = 0L;
				
				String temp_irr_id = zonal_irrigations[Irigation].irrigation_id;
		
				zonal_irrigations[Irigation].irrigation_id = "";
				
				if ( ! isSomeIrigationON() )
			    {											   
					digitalWrite(mainpump_pin, relay_stop);
			    }

				Serial.print(F("Completed zonal Irigation[by time] ID: ")); Serial.println(temp_irr_id);

				params = "";
				params.concat("iid=" + String(temp_irr_id));
				memset(msg, 0, sizeof(msg));
				apiCall(server, 80, "/api/post/irrigation_finished.php", params.c_str(), msg, MSG_BUF);

				char *jsonResponse = strstr(msg, "{");
				Serial.println(jsonResponse);

				  
			}  
		}//if (zonal_irrigations[Irigation].irrigation_id != "")
	}//for (byte Irigation = 0; Irigation < NUM_SLOTS; Irigation++)	
	
}
//////////////////////////////////////////////////////////////////////////////
void IrigationProcesingBYvolumetric(float deltaVolumePerOneLoop){
	bool fertRun = true;				 
    for (byte i = 0; i < NUM_SLOTS; i++)
    {
     
	 if (zonal_irrigations[i].irrigation_id != "")
      {
		zonal_irrigations[i].current += deltaVolumePerOneLoop; 

	Serial.print(F("SLOT [")); Serial.print(i); Serial.print(F("],"));
	Serial.print(F(" Isteklo Vode = ")); Serial.print(zonal_irrigations[i].current); Serial.print(F(" L"));
	//Serial.print(F(" Proteklo Vreme = ")); Serial.print(currentTime() - zonal_irrigations[i].started_time); Serial.println(F(" S"));
		
		if (zonal_irrigations[i].current > zonal_irrigations[i].amount)																														 
            {
				Serial.println(F("Gasim zone: "));
				//gasi zone koje su aktivne za SLOT (zadato navodnjavanje)
				for (byte processing_zone = 0; processing_zone < number_of_zones; processing_zone++)
				{
					delay(150);   
					if(zonal_irrigations[i].status[processing_zone ] == 1)
				    {	
					digitalWrite(valves[processing_zone ], relay_stop);	
					zonal_irrigations[i].status[processing_zone ] = 0;			
        
					Serial.print(F("zona: "));Serial.println(processing_zone+1); 

					}  
				}
			
				// clear slot		   
				String temp_irr_id = zonal_irrigations[i].irrigation_id ;
				zonal_irrigations[i].irrigation_id = "";
                zonal_irrigations[i].amount = 0L;
                zonal_irrigations[i].duration = 0;
				zonal_irrigations[i].current = 0 ;
				initialiseIrrFlowMet();
				
		//proveri da li treba gasiti glavnu pumpu;
				if ( ! isSomeIrigationON() )
			    {											   
					digitalWrite(mainpump_pin, relay_stop);
			    }

				Serial.print(F("Completed zonal Irigation [by m3] ID: ")); Serial.println(temp_irr_id);
				
                params = "";
                params.concat("iid=" + String(temp_irr_id));
                memset(msg, 0, sizeof(msg));
                apiCall(server, 80, "/api/post/irrigation_finished.php", params.c_str(), msg, MSG_BUF);          

				char *jsonResponse = strstr(msg, "{");
				Serial.println(jsonResponse);
	
			}// Za SLOT [i] istekla zadata kolicina vode, ugasene pripadajuce zone i server obaveste.	
	
	    }
		
// ako je za dati SLOT (zadato navodnjavanje) isteklo max vreme -> gasi navodnjavanje		
		if ( (zonal_irrigations[i].duration != 0) && (  (currentTime() - zonal_irrigations[i].started_time)  >  zonal_irrigations[i].duration))
		{
 				Serial.print(F("SLOT [")); Serial.print(i); Serial.print(F("],"));
				Serial.print(F("Isteklo Zastitno Vreme = ")); Serial.print(zonal_irrigations[i].duration ); Serial.print(F(" S")); 	Serial.println(F(", Gasim zone: "));   
				
				for (byte processing_zone = 0; processing_zone < number_of_zones; processing_zone++)
				{
					delay(150);   
					if(zonal_irrigations[i].status[processing_zone ] == 1)
				    {	
					digitalWrite(valves[processing_zone ], relay_stop);	
					zonal_irrigations[i].status[processing_zone ] = 0;
     
					Serial.print(F("zona: "));Serial.println(processing_zone+1); 
 
					}  
				}

				String logger = "Completed zonal irrigation[maxdur] ID " + String(zonal_irrigations[i].irrigation_id);
				Serial.println(logger);
		   
				String temp_irr_id = zonal_irrigations[i].irrigation_id ;
				zonal_irrigations[i].irrigation_id = "";
                zonal_irrigations[i].amount = 0L;
                zonal_irrigations[i].duration = 0;
				zonal_irrigations[i].current = 0 ;
				initialiseIrrFlowMet();
				
		//proveri da li treba gasiti glavnu pumpu;
				if ( ! isSomeIrigationON() )
			    {											   
					digitalWrite(mainpump_pin, relay_stop);
			    }
        
				params = "";
                params.concat("iid=" + String(temp_irr_id));
                params.concat("&maxduration=1");
                memset(msg, 0, sizeof(msg));
                apiCall(server, 80, "/api/post/irrigation_stopped.php", params.c_str(), msg, MSG_BUF);
#ifdef DEBUG_Extend
            char *jsonResponse = strstr(msg, "{");
            Serial.println(jsonResponse);
#endif		
			
		}// Isteklo zastitno vreme.			
	} // SLOTS for(....)	
}
/////////////////////////////////////////////////////////
void FertilisationProcesing(void){
	  if ( (fertilizations.fertilization_status == 1) && ( (currentTime() - fertilizations.fertilization_started_time) > fertilizations.fertilization_duration))
	  { 
	#ifdef DEBUG_Extend
		Serial.println(F("Gasim FERT")); 
	#endif 
	   fertilization_stop(); 

	  }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////
time_t currentTime()
{
  struct ts t;
  tmElements_t te;
  DS3231_get(&t);

  te.Second = t.sec;
  te.Minute = t.min;
  te.Hour = t.hour;
  te.Day = t.mday;
  te.Month = t.mon;
  te.Year = t.year - 1970;

  time_t timeInSeconds = makeTime(te);
  return timeInSeconds;
}
//==================================================================================================================================
int apiCall(const char *server, int port, const char *path, const char *parameters, char *result, int resultlength)
{
  int completed = 0;
  delay(200);
  completed = inet.httpPOST(server, port, path, parameters, result, resultlength);

  if (completed == 0)
  {
#ifdef DEBUG
    Serial.println(F("Dettach GPRS connection..."));
#endif
    inet.dettachGPRS();
    delay(200);
#ifdef DEBUG
    Serial.println(F("Restarting SIM module..."));
#endif
    digitalWrite(sim_restart_pin, LOW);
    delay(500);
    digitalWrite(sim_restart_pin, HIGH);
    delay(500);
#ifdef DEBUG
    Serial.print(F("GSM = "));
#endif
    // Start configuration of shield with baudrate.
    // For http uses is recommended to use 4800 or slower.
    if (gsm.begin(9600))
    {
#ifdef DEBUG
      Serial.println(F("READY"));
#endif
      gsm_started = true;
    }
    else
    {
#ifdef DEBUG
      Serial.println(F("IDLE"));
#endif
    }
    if (gsm_started)
    {
#ifdef DEBUG
      Serial.print(F("GPRS = "));
#endif
      gsm.SimpleWriteln("AT+CGATT=1");
      delay(3000);

      if (inet.attachGPRS(String(carrierdata.apnname).c_str(), String(carrierdata.apnuser).c_str(), String(carrierdata.apnpass).c_str()))
      {
  
#ifdef DEBUG
        Serial.println(F("ATTACHED"));         
        gsm.SimpleWriteln("AT+CIFSR");
        delay(5000);
      //Read until serial buffer is empty.s
        gsm.WhileSimpleRead();
#endif
      }
      else
      {
#ifdef DEBUG
        Serial.println(F("ERROR"));
#endif
      }
    
    }
    delay(1000);
  }

  
  if (completed == 0)
  {
    // check to turn off pump if there is no active irrrigation
    bool is_some_irrigation_on = false;
    for (int checking_slot = 0; checking_slot < NUM_SLOTS; checking_slot++)
    {
      if (zonal_irrigations[checking_slot].irrigation_id != "")
      {
        for (int checking_zone = 0; checking_zone < number_of_zones; checking_zone++)
        {
          if (zonal_irrigations[checking_slot].status[checking_zone ] == 1)
          {
            is_some_irrigation_on = true;
            break;
          }
        }
      }
    }
    if (!is_some_irrigation_on)
    {
#ifdef DEBUG
      Serial.println(F("Reseting Arduino..."));
#endif
      digitalWrite(arduino_restart_pin, LOW); // Reset Arduino
//      delay(5000);
    }
    else
    {
#ifdef DEBUG
      Serial.println(F("Not reseting Arduino beacuse of active irrigation."));
#endif
    }
  }
  return completed;
}


// VRACA trenutni protok [L/min], i proteklu kolicinu vode u[L] u jednom loop-u za glavnog cevovoda (FlowSensorCH5). I takodje radi obracun protoka za ostale protokomere																																										 
struct flowmeter_struct checkFlowMeter()
{
  struct flowmeter_struct flow;
  flow.currentflow   = getCurrentFlow()*60; //getCurrentFlow vraca protok L/s, sa *60 dobija se u L/min protok
  flow.currentvolume = getCurrentVOL( ); // kolicina vode u litrima za jedan loop 
return flow;
}
		
void checkLastSeen() {
  if (last_seen > 0 && !(currentTime() - last_seen < 20 * 60L)) {
     
     Serial.println(F("Stop ALL"));
 						 
    stopAll();
  }
}

void stopAll() {  
  // stop zonal irrigations
  for (byte checking_slot = 0; checking_slot < NUM_SLOTS; checking_slot++) {
    for (byte checking_zone=0; checking_zone < number_of_zones; checking_zone++) {
      
		digitalWrite(valves[checking_zone ], relay_stop);
		Serial.print(F("Gasim ZONU: "));Serial.println(checking_zone+1);
	
		zonal_irrigations[checking_slot].status[checking_zone] = 0;
		zonal_irrigations[checking_slot].current= 0;
    }
    zonal_irrigations[checking_slot].started_time = 0L;
    zonal_irrigations[checking_slot].irrigation_id = "";
    zonal_irrigations[checking_slot].amount = 0L;
    zonal_irrigations[checking_slot].duration = 0;
    zonal_irrigations[checking_slot].duration = 0;

  }
  digitalWrite(mainpump_pin, relay_stop);

  initialiseIrrFlowMet();
}


byte isSomeIrigationON(void){
		
	    byte slot_i  = 0;
	    byte count_active_slots  = 0;	
        for (byte slot_i = 0; slot_i < NUM_SLOTS; slot_i++){
		      if (zonal_irrigations[slot_i].irrigation_id != "")	count_active_slots++;		
        }
	    if (count_active_slots == 0) return 0;
		  else return 1;		

}
